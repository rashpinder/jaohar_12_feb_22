package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class AddContactMailListActivity extends BaseActivity {
    Activity mActivity = AddContactMailListActivity.this;
    String TAG = AddContactMailListActivity.this.getClass().getSimpleName();
    //    TextView textHeadingTV;
//    RelativeLayout clossRL;
    Button btnSend;
    EditText editFirstNameET, editLastNameET, editCompanyET, editEmail1ET, editEmail2ET;
    String strIsADDClick, strListID, strContactID, strfirstname, strlastname, strCompany, stremail1, stremail2;
    TextView txtCenter, txtRight, txtMailTV;
    ImageView imgBack, imgRight;
    RelativeLayout imgRightLL;
    LinearLayout llLeftLL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_add_contact_mail_list);
    }

    @Override
    protected void setViewsIDs() {
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgRight = (ImageView) findViewById(R.id.imgRight);
        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        imgRightLL.setVisibility(View.VISIBLE);

        /* set top bar */
        imgBack.setImageResource(R.drawable.back);
        txtCenter.setText(getString(R.string.recipient_list));

//        textHeadingTV = findViewById(R.id.textHeadingTV);
//        clossRL = findViewById(R.id.clossRL);
        btnSend = findViewById(R.id.btnSend);
//        btnCancel = findViewById(R.id.btnCancel);
        editFirstNameET = findViewById(R.id.editFirstNameET);
        editLastNameET = findViewById(R.id.editLastNameET);
        editCompanyET = findViewById(R.id.editCompanyET);
        editEmail1ET = findViewById(R.id.editEmail1ET);
        editEmail2ET = findViewById(R.id.editEmail2ET);

        if (getIntent() != null) {
            if (getIntent().getStringExtra("isAddClick") != null) {
                strIsADDClick = getIntent().getStringExtra("isAddClick");
                if (strIsADDClick.equals("true")) {
                    if (getIntent().getStringExtra("listID") != null) {
                        strListID = getIntent().getStringExtra("listID");
                    }
                    txtCenter.setText("Add Recipient");
                } else if (strIsADDClick.equals("false")) {
                    txtCenter.setText("Edit Recipient");

                    if (getIntent().getStringExtra("contact_id") != null) {
                        strContactID = getIntent().getStringExtra("contact_id");
                    }
                    if (getIntent().getStringExtra("firstname") != null) {
                        strfirstname = getIntent().getStringExtra("firstname");
                        editFirstNameET.setText(strfirstname);
                    }
                    if (getIntent().getStringExtra("lastname") != null) {
                        strlastname = getIntent().getStringExtra("lastname");
                        editLastNameET.setText(strlastname);
                    }
                    if (getIntent().getStringExtra("company") != null) {
                        strCompany = getIntent().getStringExtra("company");
                        editCompanyET.setText(strCompany);
                    }
                    if (getIntent().getStringExtra("email1") != null) {
                        stremail1 = getIntent().getStringExtra("email1");
                        editEmail1ET.setText(stremail1);
                    }
                    if (getIntent().getStringExtra("email2") != null) {
                        stremail2 = getIntent().getStringExtra("email2");
                        editEmail2ET.setText(stremail2);
                    }
                }
            }
        }
    }

    @Override
    protected void setClickListner() {
//        clossRL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
//        btnCancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });

        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransitionExit();
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editFirstNameET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.please_enter_first_name));
                } else if (editLastNameET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.please_enter_last_name));
                } else if (editCompanyET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.please_enter_company_name));
                } else if (editEmail1ET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.please_enter_valid_email1));
                } else if (Utilities.isValidEmaillId(editEmail1ET.getText().toString()) != true) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                } else if (!editEmail2ET.getText().toString().equals("")) {
                    if (Utilities.isValidEmaillId(editEmail2ET.getText().toString()) != true) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                    } else {
                        if (Utilities.isNetworkAvailable(mActivity) == false) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                        } else {
                            executeAPI();
                        }
                    }
                } else {
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeAPI();
                    }
                }
            }
        });
    }

    public void executeAPI() {
        if (strIsADDClick.equals("true")) {
        executeAddMailListApi();
         } else if (strIsADDClick.equals("false")) {
            executeEditMailListApi();
        }
    }

    private void executeEditMailListApi() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.editMailListContactRequest(strContactID,JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""),editFirstNameET.getText().toString().trim(),editLastNameET.getText().toString().trim(),editCompanyET.getText().toString().trim(),editEmail1ET.getText().toString().trim(),editEmail2ET.getText().toString());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus()==1) {
                    showAlertDialog(mActivity, getResources().getString(R.string.app_name),mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void executeAddMailListApi() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.addMailListContactRequest(strListID,JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""),editFirstNameET.getText().toString().trim(),editLastNameET.getText().toString().trim(),editCompanyET.getText().toString().trim(),editEmail1ET.getText().toString().trim(),editEmail2ET.getText().toString());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus()==1) {
                    showAlertDialog(mActivity, getResources().getString(R.string.app_name),mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    public void executeAPI() {
//        String strUrl = null;
//        if (strIsADDClick.equals("true")) {
//            strUrl = JaoharConstants.AddMailListContact + "?list_id=" + strListID + "&user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "&firstname=" + editFirstNameET.getText().toString().trim() + "&lastname=" + editLastNameET.getText().toString().trim() + "&company=" + editCompanyET.getText().toString().trim() + "&email1=" + editEmail1ET.getText().toString().trim() + "&email2=" + editEmail2ET.getText().toString();// + "?page_no=" + page_no ;
//        } else if (strIsADDClick.equals("false")) {
//            strUrl = JaoharConstants.EditMailListContact + "?contact_id=" + strContactID + "&user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&firstname=" + editFirstNameET.getText().toString().trim() + "&lastname=" + editLastNameET.getText().toString().trim() + "&company=" + editCompanyET.getText().toString().trim() + "&email1=" + editEmail1ET.getText().toString().trim() + "&email2=" + editEmail2ET.getText().toString().trim();// + "?page_no=" + page_no ;
//        }
//
//        strUrl = strUrl.replace(" ", "%20");
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//
//                try {
//                    JSONObject mJsonData = new JSONObject(response);
//                    if (mJsonData.getString("status").equals("1")) {
//                        showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonData.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonData.getString("message"));
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    public void showAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                onBackPressed();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
