package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class AddMailingListActivity extends BaseActivity {
    Activity mActivity = AddMailingListActivity.this;
    String TAG = AddMailingListActivity.this.getClass().getSimpleName();
    //    TextView textHeadingTV;
//    RelativeLayout clossRL;
    Button btnSend;
    String strIsADDClick, strListID;
    EditText editToET;

    TextView txtCenter, txtRight;
    ImageView imgBack, imgRight;
    RelativeLayout imgRightLL;
    LinearLayout llLeftLL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_add_mailing_list);
    }

    @Override
    protected void setViewsIDs() {
        llLeftLL = findViewById(R.id.llLeftLL);
        txtCenter = findViewById(R.id.txtCenter);
        imgBack = findViewById(R.id.imgBack);
        imgRight = findViewById(R.id.imgRight);
        imgRightLL = findViewById(R.id.imgRightLL);
        imgRightLL.setVisibility(View.VISIBLE);

//        textHeadingTV = findViewById(R.id.textHeadingTV);
//        clossRL = findViewById(R.id.clossRL);
//        btnCancel = findViewById(R.id.btnCancel);
        btnSend = findViewById(R.id.btnSend);
        editToET = findViewById(R.id.editToET);

        /* set top bar */
        imgBack.setImageResource(R.drawable.back);

        if (getIntent() != null) {
            if (getIntent().getStringExtra("isAddClick") != null) {
                strIsADDClick = getIntent().getStringExtra("isAddClick");
                if (strIsADDClick.equals("true")) {

                    txtCenter.setText("Add List");
                } else if (strIsADDClick.equals("false")) {
                    txtCenter.setText("Edit List");

                    if (getIntent().getStringExtra("listID") != null) {
                        strListID = getIntent().getStringExtra("listID");
                    }
                    if (getIntent().getStringExtra("listNAME") != null) {
                        editToET.setText(getIntent().getStringExtra("listNAME"));
                    }
                }
            }
        }
    }

    @Override
    protected void setClickListner() {
//        clossRL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
//        btnCancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });

        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransitionExit();
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editToET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.please_enter_list_value));
                } else {
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        executeAPI();
                    }
                }
            }
        });
    }

//    public void executeAPI() {
//        String strUrl = null;
//        AlertDialogManager.showProgressDialog(mActivity);
//        if (strIsADDClick.equals("true")) {
//            strUrl = JaoharConstants.AddMailList + "?list_name=" + editToET.getText().toString().trim() + "&user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");// + "?page_no=" + page_no ;
//
//        } else if (strIsADDClick.equals("false")) {
//            strUrl = JaoharConstants.EditMailList + "?list_name=" + editToET.getText().toString().trim() + "&user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "&list_id=" + strListID;// + "?page_no=" + page_no ;
//        }
//        Log.e(TAG, "***URL***" + strUrl);
//
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//
//                try {
//                    JSONObject mJsonData = new JSONObject(response);
//                    if (mJsonData.getString("status").equals("1")) {
//                        showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonData.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonData.getString("message"));
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }
    public void executeAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        if (strIsADDClick.equals("true")) {
            executeAddMailListRequest();
        } else if (strIsADDClick.equals("false")) {
            executeEditMailListRequest();
        }
    }


    private void executeEditMailListRequest() {
        ApiInterface mApiInterface =  ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.editMAilListRequest(editToET.getText().toString().trim(),JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""),strListID);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus()==1) {
                    showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );
    }

    private void executeAddMailListRequest() {
        ApiInterface mApiInterface =  ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.addMAilListRequest(editToET.getText().toString().trim(),JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus()==1) {
                    showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );
    }

    public void showAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                onBackPressed();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }
}
