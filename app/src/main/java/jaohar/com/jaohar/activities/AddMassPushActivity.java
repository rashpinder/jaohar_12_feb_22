package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class AddMassPushActivity extends BaseActivity {
    Activity mActivity = AddMassPushActivity.this;
    String TAG = AddMassPushActivity.this.getClass().getSimpleName();
    //WIDGETS
    ImageView imgBack, imgRight;
    LinearLayout llLeftLL, selectLangLL, selectTypeLL;
    RelativeLayout imgRightLL;
    TextView txtCenter, typeUserTV, selectUserTV;
    EditText messageET;
    String arrayLanguage[], arrayTypeOfUser[];
    Button sendPushTV;
    String strTypeUser = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_add_mass_push);

        arrayLanguage = mActivity.getResources().getStringArray(R.array.language_array);
        arrayTypeOfUser = mActivity.getResources().getStringArray(R.array.userType_array);
    }

    @Override
    protected void setViewsIDs() {
        /*SET UP TOOLBAR*/
        imgRight = (ImageView) findViewById(R.id.imgRight);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        selectLangLL = (LinearLayout) findViewById(R.id.selectLangLL);
        selectTypeLL = (LinearLayout) findViewById(R.id.selectTypeLL);
        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        imgRightLL.setVisibility(View.GONE);
        selectUserTV = (TextView) findViewById(R.id.selectUserTV);
        sendPushTV = findViewById(R.id.sendPushTV);
        typeUserTV = (TextView) findViewById(R.id.typeUserTV);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        messageET = (EditText) findViewById(R.id.messageET);

        /* set top bar views data */
        txtCenter.setText(getString(R.string.add_notifications));
        imgBack.setImageResource(R.drawable.back);
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        selectLangLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Open Select Language PopUP

                AlertDialogManager.showSelectItemFromArrayText(mActivity, "Choose Language", arrayLanguage, selectUserTV);
            }
        });
        selectTypeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Open Select Type PopUP
                AlertDialogManager.showSelectItemFromArrayText(mActivity, "Type of User", arrayTypeOfUser, typeUserTV);
            }
        });
        sendPushTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Hit API for Editing Mass PUSH
                if (selectUserTV.getText().toString().trim().equals("")) {
                    // POPUP Alert For LangUage
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_language));
                } else if (messageET.getText().toString().trim().equals("")) {
                    // POPUP Alert For Message
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_message));

                } else if (typeUserTV.getText().toString().trim().equals("")) {
                    // POPUP Alert For Type USer
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_user_type));

                } else {
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        AddMassPushAPI();
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        mMap.put("message", messageET.getText().toString());
        mMap.put("language", selectUserTV.getText().toString());
        mMap.put("user_type", strTypeUser);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void AddMassPushAPI() {
        if (typeUserTV.getText().toString().equals("All")) {
            strTypeUser = "All";
        } else if (typeUserTV.getText().toString().equals("Admin")) {
            strTypeUser = "Admin";
        } else if (typeUserTV.getText().toString().equals("Staff")) {
            strTypeUser = "Staff";
        } else if (typeUserTV.getText().toString().equals("User")) {
            strTypeUser = "User";
        }
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addMassPushRequest(mParam()).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    Log.e(TAG, "Messsage******:" + mModel.getMessage());
                    showAlerDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    Log.e(TAG, "Unexpected*********:" + mModel.getMessage());
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());

                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


//    private void AddMassPushAPI() {
//        //    http://root.jaohar.net/Staging/JaoharWebServicesNew/AddMassPush.php
//        String strTypeUser = "";
//        if (typeUserTV.getText().toString().equals("All")) {
//            strTypeUser = "All";
//        } else if (typeUserTV.getText().toString().equals("Admin")) {
//            strTypeUser = "Admin";
//        } else if (typeUserTV.getText().toString().equals("Staff")) {
//            strTypeUser = "Staff";
//        } else if (typeUserTV.getText().toString().equals("User")) {
//            strTypeUser = "User";
//        }
//        String strAPIUrl = "";
//        JSONObject jsonObject = new JSONObject();
//        strAPIUrl = JaoharConstants.Add_Mass_Push;
//        try {
//            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
//            jsonObject.put("message", messageET.getText().toString());
//            jsonObject.put("language", selectUserTV.getText().toString());
//            jsonObject.put("user_type", strTypeUser);
//
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//
//        AlertDialogManager.showProgressDialog(mActivity);
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + jsonObject.toString());
//                try {
//                    AlertDialogManager.hideProgressDialog();
//                    String status = jsonObject.getString("status");
//                    String message = jsonObject.getString("message");
//                    if (status.equals("1")) {
//                        // tell everybody you have succed upload image and post strings
//                        Log.e(TAG, "Messsage******:" + message);
//                        showAlerDialog(mActivity, getString(R.string.app_name), "" + message);
//                    } else {
//                        Log.e(TAG, "Unexpected*********:" + message);
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******error*****" + error);
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//
//    }

    public void showAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);
        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                onBackPressed();

            }
        });
        alertDialog.show();
    }
}
