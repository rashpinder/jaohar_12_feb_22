package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.VesselTypesAdapter;
import jaohar.com.jaohar.interfaces.DeleteVesselTypeInterface;
import jaohar.com.jaohar.interfaces.EditVesselTypeInterface;
import jaohar.com.jaohar.models.GetVesselTypeData;
import jaohar.com.jaohar.models.GetVesselTypeModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class AllVesselTypesActivity extends BaseActivity {
    Activity mActivity = AllVesselTypesActivity.this;
    String TAG = AllVesselTypesActivity.this.getClass().getSimpleName();
    //WIDGETS
    ImageView imgBack;
    LinearLayout llLeftLL;
    RelativeLayout imgRightLL;
    ImageView imgRight;
    TextView txtCenter;
    RecyclerView vesselsTypesRV;
    VesselTypesAdapter mVesselTypesAdapter;
    ArrayList<GetVesselTypeData> mArrayList = new ArrayList<GetVesselTypeData>();


    EditVesselTypeInterface mEditVesselTypeInterface = new EditVesselTypeInterface() {
        @Override
        public void mEditVesselTypeInterface(GetVesselTypeData mVesselTypesModel) {
            editDialog(mVesselTypesModel);
        }
    };

    DeleteVesselTypeInterface mDeleteVesselTypeInterface = new DeleteVesselTypeInterface() {
        @Override
        public void mDeleteVesselTypeInterface(GetVesselTypeData mVesselTypesModel) {
            deleteConfirmDialog(mVesselTypesModel);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_all_vessel_types);
    }

    @Override
    protected void setViewsIDs() {
        /*SET UP TOOLBAR*/
        imgBack = findViewById(R.id.imgBack);
        llLeftLL = findViewById(R.id.llLeftLL);
        imgRightLL = findViewById(R.id.imgRightLL);
        imgRight = findViewById(R.id.imgRight);
        txtCenter = findViewById(R.id.txtCenter);
        vesselsTypesRV = findViewById(R.id.vesselsTypesRV);

        imgRightLL.setVisibility(View.VISIBLE);
        imgRight.setVisibility(View.VISIBLE);

        /* set tool bar */
        txtCenter.setText(getString(R.string.vessel_types));
        imgRight.setImageResource(R.drawable.add_icon);
        imgBack.setImageResource(R.drawable.back);

        if (!Utilities.isNetworkAvailable(mActivity)) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            executeGettingAllTypes();
        }
    }


    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDialog();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    private void setAdapter() {
        vesselsTypesRV.setNestedScrollingEnabled(false);
        vesselsTypesRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mVesselTypesAdapter = new VesselTypesAdapter(mActivity, mArrayList, mEditVesselTypeInterface, mDeleteVesselTypeInterface);
        vesselsTypesRV.setAdapter(mVesselTypesAdapter);
    }

    private void executeGettingAllTypes() {
        mArrayList.clear();
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetVesselTypeModel> call1 = mApiInterface.getVesselTypeRequest();
        call1.enqueue(new Callback<GetVesselTypeModel>() {
            @Override
            public void onResponse(Call<GetVesselTypeModel> call, retrofit2.Response<GetVesselTypeModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                GetVesselTypeModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus().equals("1")) {
                    mArrayList=mModel.getData();
                    /*setAdapter*/
                    setAdapter();
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GetVesselTypeModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );}

    /*Add Sttaus Dialog*/
    public void addDialog() {
        final Dialog mDialog = new Dialog(mActivity, R.style.myFullscreenAlertDialogStyle);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_add_status);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        // set the custom dialog components - text, image and button
        final EditText editVesselTypeET = (EditText) mDialog.findViewById(R.id.editVesselTypeET);
        Button btnAddB = (Button) mDialog.findViewById(R.id.btnAddB);
        ImageView cancelImg = mDialog.findViewById(R.id.cancelImg);

        btnAddB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editVesselTypeET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getString(R.string.please_enter_the_status_name));
                } else {
                    if (!Utilities.isNetworkAvailable(mActivity)) {
                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        mDialog.dismiss();
                        executeAddAPI(editVesselTypeET.getText().toString());
                    }
                }
            }
        });

        cancelImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    private void executeAddAPI(String strVesselName) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.addVesselTypeRequest(strVesselName);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
//                assert mModel != null;
                if (mModel.getStatus()==1) {
                    showAlertDialog(mActivity, getTitle().toString(), mModel.getMessage());
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );}


    /*Edit VESSEL TYPE Dialog*/
    public void editDialog(final GetVesselTypeData mVesselTypesModel) {
        final Dialog mDialog = new Dialog(mActivity, R.style.myFullscreenAlertDialogStyle);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_edit_status);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));

        // set the custom dialog components - text, image and button
        final EditText editVesselTypeET = (EditText) mDialog.findViewById(R.id.editVesselTypeET);
        editVesselTypeET.setText(mVesselTypesModel.getName());
        editVesselTypeET.setSelection(editVesselTypeET.getText().toString().length());
        Button btnUpdateB = (Button) mDialog.findViewById(R.id.btnUpdateB);
        ImageView cancelImg = mDialog.findViewById(R.id.cancelImg);

        btnUpdateB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editVesselTypeET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getString(R.string.please_enter_the_status_name));
                } else {
                    if (!Utilities.isNetworkAvailable(mActivity)) {
                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        mDialog.dismiss();
                        executeEDITAPI(editVesselTypeET.getText().toString(), mVesselTypesModel.getId());
                    }
                }
            }
        });

        cancelImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    private void executeEDITAPI(String strVesselName, String strVesselID) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.editVesselTypeRequest(strVesselName,strVesselID);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus()==1) {
                    showAlertDialog(mActivity, getTitle().toString(), mModel.getMessage());
                }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );}



    public void deleteConfirmDialog(final GetVesselTypeData mVesselTypesModel) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_vessel_type));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(mVesselTypesModel);
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    private void executeDeleteAPI(GetVesselTypeData mVesselTypesModel) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteVesselTypeRequest(mVesselTypesModel.getId());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus()==1) {
                    showAlertDialog(mActivity, getTitle().toString(), mModel.getMessage());
                }
                else {
                    showAlertDialog(mActivity, getTitle().toString(),mModel.getMessage());  }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());}}
        );}


    public void showAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
                executeGettingAllTypes();
            }
        });
        alertDialog.show();
    }


}
