package jaohar.com.jaohar.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.fiberlink.maas360.android.richtexteditor.RichEditText;
import com.fiberlink.maas360.android.richtexteditor.RichTextActions;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.models.CompanyData;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class EditInternalNewsActivity extends BaseActivity {
    public static final int CAMERA_REQUEST = 111;
    public static final int GALLERY_REQUEST = 222;
    private static final int PERMISSION_REQUEST_CODE = 200;
    String strScrollingType, strStaffNEwsStatus, strStatusCount;
    String TAG = "EditInternalNewsActivity";
    Activity mActivity = EditInternalNewsActivity.this;

    LinearLayout llLeftLL, staffLL,mainLL;
    ImageView imgBack, mImage, mImage1,itemDeleteIV1,itemDeleteIV;
    TextView txtCenter;
    Spinner spinnerType, spinnerType2;
    Button btnAddNews;
    com.fiberlink.maas360.android.richtexteditor.RichEditText mRichEditText, rich_edit_text1;
    com.fiberlink.maas360.android.richtexteditor.RichTextActions richTextActions, richTextActions1;
    boolean isImage1 = false, isImage2 = false;
    CompanyData mNewsModel;
    private String cameraStr = Manifest.permission.CAMERA;
    private String readStorageStr = Manifest.permission.READ_EXTERNAL_STORAGE, mCurrentPhotoPath, mStoragePath, strBase64, strBase641;
Bitmap rotate =null,rotate1=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_internal_news);

        if (getIntent() != null) {
            mNewsModel = (CompanyData) getIntent().getSerializableExtra("Model");
        }
        setViews();
        setClickr();
    }


    protected void setViews() {
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        staffLL = (LinearLayout) findViewById(R.id.staffLL);
        mainLL = (LinearLayout) findViewById(R.id.mainLL);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        mImage1 = (ImageView) findViewById(R.id.mImage1);
        mImage = (ImageView) findViewById(R.id.mImage);
        itemDeleteIV1 = (ImageView) findViewById(R.id.itemDeleteIV1);
        itemDeleteIV = (ImageView) findViewById(R.id.itemDeleteIV);
        imgBack.setImageResource(R.drawable.back);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        txtCenter.setText(getString(R.string.edit_internal_news));

        btnAddNews = (Button) findViewById(R.id.btnAddNews);
        spinnerType = (Spinner) findViewById(R.id.spinnerType);
        spinnerType2 = (Spinner) findViewById(R.id.spinnerType2);
        mRichEditText = (RichEditText) findViewById(R.id.rich_edit_text);
        mRichEditText.setOnKeyListener(null);
        rich_edit_text1 = (RichEditText) findViewById(R.id.rich_edit_text1);
        rich_edit_text1.setOnKeyListener(null);
        richTextActions = (RichTextActions) findViewById(R.id.richTextActions);
        mRichEditText.setRichTextActionsView(richTextActions);
        richTextActions1 = (RichTextActions) findViewById(R.id.richTextActions1);
        rich_edit_text1.setRichTextActionsView(richTextActions1);
        if(JaoharConstants.IS_CLICK_FROM_COMPANY==true){
            staffLL.setVisibility(View.VISIBLE);
            mainLL.setVisibility(View.GONE);
        }else {
            staffLL.setVisibility(View.GONE);
            mainLL.setVisibility(View.VISIBLE);
        }
        setUpTypeSpinnerAdapter();
        setUpTypeSpinnerstaffAdapter();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (0 != (getApplication().getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE)) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
        }

        if (mNewsModel != null) {
            if (mNewsModel.getType().equals("Scrolling")) {
                strScrollingType=mNewsModel.getType();
                spinnerType.setSelection(1);
            }
            else if (mNewsModel.getType().equals("Static")) {

                strScrollingType=mNewsModel.getType();
                spinnerType.setSelection(2);
                staffLL.setVisibility(View.VISIBLE);

                if(mNewsModel.getStaffNewsStatus().equals("0")){
                    strStatusCount=mNewsModel.getStaffNewsStatus();
                    spinnerType2.setSelection(2);
                    strStaffNEwsStatus="Inactive";
                }
                else if(mNewsModel.getStaffNewsStatus().equals("1")){
                    strStatusCount=mNewsModel.getStaffNewsStatus();
                    spinnerType2.setSelection(1);
                    strStaffNEwsStatus="Active";
                }
            }



            strBase64 = mNewsModel.getPhoto();

            if (!strBase64.equals("")) {
                itemDeleteIV.setVisibility(View.VISIBLE);
                Picasso.get().load(strBase64).into(mImage);
            }
            strBase641 = mNewsModel.getStaffPhoto();
            if (!strBase641.equals("")) {
                itemDeleteIV1.setVisibility(View.VISIBLE);
                Picasso.get().load(strBase641).into(mImage1);
            }
            mRichEditText.setHtml(mNewsModel.getNews());
            rich_edit_text1.setHtml(mNewsModel.getStaffNews());
        }

    }

    private void setUpTypeSpinnerAdapter() {
        List<String> list = new ArrayList<String>();
        list.add("Select Type");
        list.add("Scrolling");
        list.add("Static");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerType.setAdapter(dataAdapter);


        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strScrollingType = adapterView.getItemAtPosition(i).toString();
                if (strScrollingType.equals("Scrolling")) {
                    staffLL.setVisibility(View.GONE);
                    spinnerType2.setSelection(0);
                    rich_edit_text1.setHtml("");
                } else if (strScrollingType.equals("Static")) {
                    staffLL.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setUpTypeSpinnerstaffAdapter() {
        List<String> list = new ArrayList<String>();
        list.add("Select Status");
        list.add("Active");
        list.add("Inactive");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerType2.setAdapter(dataAdapter);
        spinnerType2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strStaffNEwsStatus = adapterView.getItemAtPosition(i).toString();
                if (strStaffNEwsStatus.equals("Active")) {
                    strStatusCount = "1";
                } else if (strStaffNEwsStatus.equals("Inactive")) {
                    strStatusCount = "0";
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }



    protected void setClickr() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnAddNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(JaoharConstants.IS_CLICK_FROM_COMPANY==true){

                    if (spinnerType2.getSelectedItem().toString().equals("Select Status")) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_news_status));
                    }
                    else if (rich_edit_text1.getHtml().toString().equals("")) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter__staff_news));
                    }
                    else {
                        if (!Utilities.isNetworkAvailable(mActivity)) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                        } else {
                        /*ExecuteApi*/
                            executeEditAPI();
                        }
                    }



                }else {
                    if (spinnerType.getSelectedItem().toString().equals("Select Type")) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_news_type));
                    } else if (mRichEditText.getHtml().toString().equals("")) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_news));
                    }
                    else if(spinnerType.getSelectedItem().toString().equals("Static")){
                        if (spinnerType2.getSelectedItem().toString().equals("Select Status")) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_news_status));
                        }
                        else if (rich_edit_text1.getHtml().toString().equals("")) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter__staff_news));
                        }
                        else {
                            if (!Utilities.isNetworkAvailable(mActivity)) {
                                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                            } else {
                        /*ExecuteApi*/
                                executeEditAPI();
                            }
                        }
                    }

                    else {
                        if (!Utilities.isNetworkAvailable(mActivity)) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                        } else {
                        /*ExecuteApi*/
                            executeEditAPI();
                        }
                    }
                }
            }
        });
        mImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isImage1=true;
                if (checkPermission()) {
                    openCameraGalleryDialog();
                } else {
                    requestPermission();
                }
            }
        });
        mImage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isImage2=true;
                if (checkPermission()) {
                    openCameraGalleryDialog();
                } else {
                    requestPermission();
                }
            }
        });

        itemDeleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemDeleteIV.setVisibility(View.GONE);
                mImage .setImageResource(R.drawable.palace_holder);
                rotate= null;
                strBase64="";
            }
        });
        itemDeleteIV1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemDeleteIV1.setVisibility(View.GONE);
                mImage1 .setImageResource(R.drawable.palace_holder);
                strBase641="";
                rotate1=null;
            }
        });
    }
    private void openCameraGalleryDialog() {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_display_dialog);

        TextView text_camra = (TextView) dialog.findViewById(R.id.txt_camra);
        TextView text_gallery = (TextView) dialog.findViewById(R.id.txt_gallery);
        TextView txt_files = (TextView) dialog.findViewById(R.id.txt_files);
        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);
        txt_files.setVisibility(View.GONE);
        text_camra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openCamera();
            }
        });
        text_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        Log.e("error is occured", dialog.toString());
    }

    public void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
//                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }

    public void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);
    }

    /*********
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/

    private boolean checkPermission() {
        int camera = ContextCompat.checkSelfPermission(getApplicationContext(), cameraStr);
        int readStorage = ContextCompat.checkSelfPermission(getApplicationContext(), readStorageStr);
        return camera == PackageManager.PERMISSION_GRANTED && readStorage == PackageManager.PERMISSION_GRANTED;
    }


    void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{cameraStr, readStorageStr}, PERMISSION_REQUEST_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
//                            startActivity(new Intent(SplashSlidesActivity.this, SelectionActivity.class));
//                            finish();
//                            Toast.makeText(context,"on",Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e(TAG, "onRequestPermissionsResult: failed");
                            requestPermission();
//                            permissionAccepted = false;
                        }
                    }
                }
                break;
        }
    }
    private void executeEditAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        if(JaoharConstants.IS_CLICK_FROM_COMPANY==true){
            executeeditCompanyNewsApi();
        }else {
           executeeditInternalNewsApi();}
        }

    private void executeeditCompanyNewsApi() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.editCompanyNewsRequest(mEditCompanyNewsParam()).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    mAlerDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
                AlertDialogManager.hideProgressDialog();
            }
        });
    }



    private Map<String, String> mEditCompanyNewsParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("staff_news_status", strStatusCount);
        mMap.put("staff_news", rich_edit_text1.getHtml().toString());
        mMap.put("staff_photo",strBase641);
        mMap.put("news_id",mNewsModel.getId());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private Map<String, String> mEditInternalNewsParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("news", mRichEditText.getHtml().toString());
        mMap.put("type", strScrollingType);
        mMap.put("staff_news_status", strStatusCount);
        mMap.put("photo1", strBase64);
        mMap.put("staff_news", rich_edit_text1.getHtml().toString());
        mMap.put("staff_photo",strBase641);
        mMap.put("news_id",mNewsModel.getId());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeeditInternalNewsApi(){
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.editInternalNewsRequest(mEditInternalNewsParam()).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    mAlerDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
                AlertDialogManager.hideProgressDialog();
            }
        });
    }


//    private void executeEditAPI() {
//        String url ="";
//        JSONObject jsonObject = new JSONObject();
//        AlertDialogManager.showProgressDialog(mActivity);
//        if(JaoharConstants.IS_CLICK_FROM_COMPANY==true){
//            //        https://root.jaohar.com/Staging/JaoharWebServicesNew/EditCompanyNews.php
//             url = JaoharConstants.Edit_Company_News;
//            try {
//
//                jsonObject.put("staff_news_status", strStatusCount);
//                jsonObject.put("staff_news", rich_edit_text1.getHtml().toString());
//                jsonObject.put("staff_photo",strBase641);
//                jsonObject.put("news_id",mNewsModel.getId());
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }else {
//             url = JaoharConstants.EDIT_INTERNAL_NEWS;
//
//            try {
//                jsonObject.put("news", mRichEditText.getHtml().toString());
//                jsonObject.put("type", strScrollingType);
//                jsonObject.put("staff_news_status", strStatusCount);
//
//                jsonObject.put("photo1", strBase64);
//                jsonObject.put("staff_news", rich_edit_text1.getHtml().toString());
//                jsonObject.put("staff_photo",strBase641);
//                jsonObject.put("news_id",mNewsModel.getId());
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//
//
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
//                url, jsonObject,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        AlertDialogManager.hideProgressDialog();
//                        Log.d(TAG, response.toString());
//                        try {
//                            if (response.getString("status").equals("1")) {
//                                mAlerDialog(mActivity, getString(R.string.app_name), response.getString("message"));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                VolleyLog.d(TAG, "Error: " + error.getMessage());
//
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
////      Adding request to request queue
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjReq);
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    public void mAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mActivity.finish();
            }
        });
        alertDialog.show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        InputStream stream = null;
        Bitmap bitmap = null;
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
            if(isImage1==true){
                isImage1=false;
                Uri uri = data.getData();
                File finalFile = new File(getRealPathFromURI(uri));

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 0;

                bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
//                   For COnvert And rotate image
                ExifInterface exifInterface = null;
                try {
                    exifInterface = new ExifInterface(finalFile.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                Matrix matrix = new Matrix();
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        matrix.setRotate(90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        matrix.setRotate(180);
                        break;
                    default:

                }
                rotate = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

                mImage.setImageBitmap(rotate);
                itemDeleteIV.setVisibility(View.VISIBLE);
                strBase64 = getBase64String(rotate);
            }
            else if(isImage2==true){
                isImage2=false;
                Uri uri = data.getData();
                File finalFile = new File(getRealPathFromURI(uri));

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 0;

                bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
//                   For COnvert And rotate image
                ExifInterface exifInterface = null;
                try {
                    exifInterface = new ExifInterface(finalFile.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                Matrix matrix = new Matrix();
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        matrix.setRotate(90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        matrix.setRotate(180);
                        break;
                    default:

                }
                rotate1 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

                mImage1.setImageBitmap(rotate1);
                itemDeleteIV1.setVisibility(View.VISIBLE);
                strBase641 = getBase64String(rotate1);
            }


        }
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            if(isImage1==true){
                isImage1=false;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 8;
                bitmap = BitmapFactory.decodeFile(mStoragePath, options);
                rotate = rotateImage(bitmap);

                mImage.setImageBitmap(rotate);
                itemDeleteIV.setVisibility(View.VISIBLE);
                strBase64 = getBase64String(rotate);
            }else if(isImage2==true){
                isImage2=false;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 8;
                bitmap = BitmapFactory.decodeFile(mStoragePath, options);
                rotate1 = rotateImage(bitmap);

                mImage1.setImageBitmap(rotate1);
                itemDeleteIV1.setVisibility(View.VISIBLE);
                strBase641 = getBase64String(rotate1);
            }



        }
    }
    //       Method Correct Rotate Image when Capture From Camera....

    private Bitmap rotateImage(Bitmap bitmap) {
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(mStoragePath);


        } catch (IOException e) {
            e.printStackTrace();
        }

        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            default:

        }
        Bitmap rotate = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        return rotate;
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private String getBase64String(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] imageBytes = baos.toByteArray();

        String base64String = Base64.encodeToString(imageBytes, Base64.NO_WRAP);

        return base64String;
    }
}
