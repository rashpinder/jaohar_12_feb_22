package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.JaoharApplication;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.beans.VesselSearchInvoiceModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class EditInvoiceVesselActivity extends BaseActivity {
    Activity mActivity = EditInvoiceVesselActivity.this;
    String TAG = EditInvoiceVesselActivity.this.getClass().getSimpleName();
    //Toolbar
    LinearLayout llLeftLL;
    ImageView imgBack;
    TextView txtCenter;

    EditText editVesselNameET, editImoNumET, editFlagET;
    Button btnEditVesselB;

    VesselSearchInvoiceModel mModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_invoice_vessel);//edit_invoice_vessel
        if (getIntent() != null) {
            mModel = (VesselSearchInvoiceModel) getIntent().getSerializableExtra("Model");
        }
    }

    @Override
    protected void setViewsIDs() {
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.back);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        txtCenter.setText(getString(R.string.edit_invoice_vessel));
        editVesselNameET = (EditText) findViewById(R.id.editVesselNameET);
        editImoNumET = (EditText) findViewById(R.id.editImoNumET);
        editFlagET = (EditText) findViewById(R.id.editFlagET);
        btnEditVesselB = (Button) findViewById(R.id.btnEditVesselB);

        /*SetData on Widgets*/
        setDataOnWidgets();
    }

    private void setDataOnWidgets() {
        editVesselNameET.setText(mModel.getVessel_name());
        editVesselNameET.setSelection(mModel.getVessel_name().length());
        editVesselNameET.requestFocus();
        editImoNumET.setText(mModel.getIMO_no());
        editImoNumET.setSelection(mModel.getIMO_no().length());
        editFlagET.setText(mModel.getFlag());
        editFlagET.setSelection(mModel.getFlag().length());
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnEditVesselB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editVesselNameET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_vessel_name));
                } else if (editImoNumET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_imo_num));
                } else if (editFlagET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_flag));
                } else {
                    /*EXECUTE API*/
                    executeEditInvoiceVessel();
                }
            }
        });
    }

    private void executeEditInvoiceVessel() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.editInvoiceVesselRequest(editVesselNameET.getText().toString(),editImoNumET.getText().toString(),editFlagET.getText().toString(),JaoharPreference.readString(mActivity,JaoharPreference.STAFF_ID,""),mModel.getVessel_id());
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().toString());
                    if (jsonObject.getString("status").equals("1")) {
                        JaoharConstants.IS_CURRENCY_EDIT = true;
                        showAlerDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
                    } else if (jsonObject.getString("status").equals("100")) {
                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {
            AlertDialogManager.hideProgressDialog();
            Log.e(TAG, "******error*****" + t.getMessage());
        }
    });
    }

//    private void executeEditInvoiceVessel() {
//        String strAPIUrl = "";
//        strAPIUrl = JaoharConstants.EDIT_INVOICE_VESSEL + "?vessel_name=" + editVesselNameET.getText().toString() + "&IMO_no=" +editImoNumET.getText().toString()  + "&flag=" +editFlagET.getText().toString() + "&user_id=" + JaoharPreference.readString(mActivity,JaoharPreference.STAFF_ID,"") + "&vessel_id=" + mModel.getVessel_id();
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strAPIUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "*****Response****" + response);
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    if (jsonObject.getString("status").equals("1")) {
//                        JaoharConstants.IS_CURRENCY_EDIT = true;
//                        showAlerDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    } else if (jsonObject.getString("status").equals("100")) {
//                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        });
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    public void showAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);
        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mActivity.finish();
            }
        });
        alertDialog.show();
    }

}
