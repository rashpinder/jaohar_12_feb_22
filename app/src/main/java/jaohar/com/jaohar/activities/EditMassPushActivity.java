package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class EditMassPushActivity extends BaseActivity {
    Activity mActivity = EditMassPushActivity.this;
    String TAG = EditMassPushActivity.this.getClass().getSimpleName();
    //WIDGETS
    ImageView imgBack, imgRight;
    LinearLayout llLeftLL, selectLangLL, selectTypeLL;
    RelativeLayout imgRightLL;
    TextView txtCenter, sendPushTV, typeUserTV, selectUserTV;
    EditText messageET;
    String arrayLanguage[], arrayTypeOfUser[], strNotificationID;
    String strTypeUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_edit_mass_push);

        arrayLanguage = mActivity.getResources().getStringArray(R.array.language_array);
        arrayTypeOfUser = mActivity.getResources().getStringArray(R.array.userType_array);
    }

    @Override
    protected void setViewsIDs() {
        /*SET UP TOOLBAR*/
        imgRight = (ImageView) findViewById(R.id.imgRight);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        messageET = (EditText) findViewById(R.id.messageET);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        selectLangLL = (LinearLayout) findViewById(R.id.selectLangLL);
        selectTypeLL = (LinearLayout) findViewById(R.id.selectTypeLL);
        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        imgRightLL.setVisibility(View.GONE);
        selectUserTV = (TextView) findViewById(R.id.selectUserTV);
        sendPushTV = (TextView) findViewById(R.id.sendPushTV);
        typeUserTV = (TextView) findViewById(R.id.typeUserTV);
        txtCenter = (TextView) findViewById(R.id.txtCenter);

        /* set to bar data */
        txtCenter.setText(getString(R.string.edit_notifications));
        imgBack.setImageResource(R.drawable.back);

        Bundle bundle = getIntent().getExtras();
        strNotificationID = bundle.getString("noficationID");
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            getMassPushSingle();
        }
    }

    @Override
    protected void setClickListner() {

        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        selectLangLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Open Select Language PopUP
                AlertDialogManager.showSelectItemFromArrayText(mActivity, "Choose Language", arrayLanguage, selectUserTV);
                if (selectUserTV.getText().toString().equals("English")) {
//                    messageET  .setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                } else if (selectUserTV.getText().toString().equals("Arabic")) {
//                    messageET  .setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
                }
            }
        });

        selectTypeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Open Select Type PopUP
                AlertDialogManager.showSelectItemFromArrayText(mActivity, "Type of User", arrayTypeOfUser, typeUserTV);
            }
        });

        sendPushTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Hit API for Editing Mass PUSH
                if (selectUserTV.getText().toString().trim().equals("")) {
                    // POPUP Alert For LangUage
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_language));
                } else if (messageET.getText().toString().trim().equals("")) {
                    // POPUP Alert For Message
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_message));
                } else if (typeUserTV.getText().toString().trim().equals("")) {
                    // POPUP Alert For Type USer
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_user_type));
                } else {
                    if (Utilities.isNetworkAvailable(mActivity) == false) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        EditMassPushAPI();
                    }
                }
            }
        });
    }

    private void getMassPushSingle() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getMassPushByIdRequest(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""),strNotificationID);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Response***" + response);
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    if (mJsonObject.getString("status").equals("1")) {
                        if (!mJsonObject.isNull("data")) {
                            JSONObject mDataObject = mJsonObject.getJSONObject("data");
                            if (!mDataObject.getString("language").equals("")) {
                                selectUserTV.setText(mDataObject.getString("language"));
                                if (mDataObject.getString("language").equals("English")) {
//                                    messageET  .setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                                    if (!mDataObject.getString("message").equals("")) {
                                        messageET.setText(html2text(mDataObject.getString("message")));
                                        messageET.setSelection(messageET.getText().length());

                                    }
                                } else if (mDataObject.getString("language").equals("Arabic")) {
//                                    messageET  .setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
                                    if (!mDataObject.getString("message").equals("")) {
                                        messageET.setText(html2text(mDataObject.getString("message")));
                                        messageET.setSelection(messageET.getText().length());

                                    }
                                }
                            }
                            if (!mDataObject.getString("user_type").equals("")) {
                                strTypeUser = mDataObject.getString("user_type");
                                typeUserTV.setText(mDataObject.getString("user_type"));
                            }
                        }
                    } else if (mJsonObject.getString("status").equals("100")) {
                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
                    } else if (mJsonObject.getString("status").equals("0")) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                AlertDialogManager.hideProgressDialog();
            }

        @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {
            AlertDialogManager.hideProgressDialog();
            Log.e(TAG, "******error*****" + t.getMessage());
        }
    });
    }


//    private void getMassPushSingle() {
////      http://root.jaohar.net/Staging/JaoharWebServicesNew/GetMassPushById.php?user_id=158&notification_id=153
//        String strUrl = JaoharConstants.Get_Mass_Push_By_Id + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&notification_id=" + strNotificationID;
//        AlertDialogManager.showProgressDialog(mActivity);
//        Log.e(TAG, "***Response***" + strUrl);
//        StringRequest mStringRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
////              AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Response***" + response);
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        if (!mJsonObject.isNull("data")) {
//                            JSONObject mDataObject = mJsonObject.getJSONObject("data");
//                            if (!mDataObject.getString("language").equals("")) {
//                                selectUserTV.setText(mDataObject.getString("language"));
//                                if (mDataObject.getString("language").equals("English")) {
////                                    messageET  .setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
//                                    if (!mDataObject.getString("message").equals("")) {
//                                        messageET.setText(html2text(mDataObject.getString("message")));
//                                        messageET.setSelection(messageET.getText().length());
//
//                                    }
//                                } else if (mDataObject.getString("language").equals("Arabic")) {
////                                    messageET  .setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
//                                    if (!mDataObject.getString("message").equals("")) {
//                                        messageET.setText(html2text(mDataObject.getString("message")));
//                                        messageET.setSelection(messageET.getText().length());
//
//                                    }
//                                }
//                            }
//                            if (!mDataObject.getString("user_type").equals("")) {
//                                strTypeUser = mDataObject.getString("user_type");
//                                typeUserTV.setText(mDataObject.getString("user_type"));
//                            }
//                        }
//                    } else if (mJsonObject.getString("status").equals("100")) {
//                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    } else if (mJsonObject.getString("status").equals("0")) {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                AlertDialogManager.hideProgressDialog();
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error***" + error.toString());
//            }
//        });
//        JaoharApplication.getInstance().addToRequestQueue(mStringRequest);
//    }

    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        mMap.put("notification_id", strNotificationID);
        mMap.put("message", messageET.getText().toString());
        mMap.put("language", selectUserTV.getText().toString());
        mMap.put("user_type", strTypeUser);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void EditMassPushAPI() {
        if (typeUserTV.getText().toString().equals("All")) {
            strTypeUser = "All";
        } else if (typeUserTV.getText().toString().equals("Admin")) {
            strTypeUser = "Admin";
        } else if (typeUserTV.getText().toString().equals("Staff")) {
            strTypeUser = "Staff";
        } else if (typeUserTV.getText().toString().equals("User")) {
            strTypeUser = "User";
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.editMassPushRequest(mParam());
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                try {
                JSONObject jsonObject = new JSONObject(response.body().toString());
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    if (status.equals("1")) {
                        // tell everybody you have succed upload image and post strings
                        Log.e(TAG, "Messsage******:" + message);
                        showAlerDialog(mActivity, getString(R.string.app_name), "" + message);
                    } else {
                        Log.e(TAG, "Unexpected*********:" + message);
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }



//    private void EditMassPushAPI() {
////        http://root.jaohar.net/Staging/JaoharWebServicesNew/EditMassPush.php
////        {"user_id":"158","notification_id":"154","message":"test notification from api","language":"English","user_type":"all"}
////        param - user_type values all , admin , staff , user
////        param language values English & Arabic
//        if (typeUserTV.getText().toString().equals("All")) {
//            strTypeUser = "All";
//        } else if (typeUserTV.getText().toString().equals("Admin")) {
//            strTypeUser = "Admin";
//        } else if (typeUserTV.getText().toString().equals("Staff")) {
//            strTypeUser = "Staff";
//        } else if (typeUserTV.getText().toString().equals("User")) {
//            strTypeUser = "User";
//        }
//        String strAPIUrl = "";
//        JSONObject jsonObject = new JSONObject();
//        strAPIUrl = JaoharConstants.Edit_Mass_Push;
//        try {
//            jsonObject.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
//            jsonObject.put("notification_id", strNotificationID);
//            jsonObject.put("message", messageET.getText().toString());
//            jsonObject.put("language", selectUserTV.getText().toString());
//            jsonObject.put("user_type", strTypeUser);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        AlertDialogManager.showProgressDialog(mActivity);
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + jsonObject.toString());
//                try {
//                    AlertDialogManager.hideProgressDialog();
//                    String status = jsonObject.getString("status");
//                    String message = jsonObject.getString("message");
//                    if (status.equals("1")) {
//                        // tell everybody you have succed upload image and post strings
//                        Log.e(TAG, "Messsage******:" + message);
//                        showAlerDialog(mActivity, getString(R.string.app_name), "" + message);
//                    } else {
//                        Log.e(TAG, "Unexpected*********:" + message);
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******error*****" + error);
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//
//    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    public void showAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);
        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                onBackPressed();

            }
        });
        alertDialog.show();
    }
}
