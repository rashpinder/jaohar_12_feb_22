package jaohar.com.jaohar.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fiberlink.maas360.android.richtexteditor.RichEditText;
import com.fiberlink.maas360.android.richtexteditor.RichTextActions;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.models.Datum;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class EditNewsActivity extends BaseActivity {
    String TAG = "AddNewsActivity";
    Activity mActivity = EditNewsActivity.this;
    public static final int CAMERA_REQUEST = 111;
    public static final int GALLERY_REQUEST = 222;
    private static final int PERMISSION_REQUEST_CODE = 200;
    LinearLayout llLeftLL;
    ImageView imgBack, mImage, itemDeleteIV;
    TextView txtCenter;
    Bitmap rotate = null;
    Spinner spinnerType;
    String strType = "";
    Button btnAddNews;
    com.fiberlink.maas360.android.richtexteditor.RichEditText mRichEditText;
    com.fiberlink.maas360.android.richtexteditor.RichTextActions richTextActions;
    private String cameraStr = Manifest.permission.CAMERA;
    private String readStorageStr = Manifest.permission.READ_EXTERNAL_STORAGE, mCurrentPhotoPath, mStoragePath, strBase64;
//    NewsModel mNewsModel;
    Datum mNewsModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_edit_news);

        /* initialize view ids */
        setViewss();

        /* set clicks on views */
        setClick();
    }

    protected void setViewss() {
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        mImage = (ImageView) findViewById(R.id.mImage);
        itemDeleteIV = (ImageView) findViewById(R.id.itemDeleteIV);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        btnAddNews = (Button) findViewById(R.id.btnAddNews);

        /* set tool bar */
        txtCenter.setText(getString(R.string.edit_news));
        imgBack.setImageResource(R.drawable.back);


        spinnerType = (Spinner) findViewById(R.id.spinnerType);
        setUpTypeSpinnerAdapter();

        mRichEditText = (RichEditText) findViewById(R.id.rich_edit_text);
        mRichEditText.setOnKeyListener(null);
        richTextActions = (RichTextActions) findViewById(R.id.richTextActions);
        mRichEditText.setRichTextActionsView(richTextActions);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (0 != (getApplication().getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE)) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
        }
        if (getIntent() != null) {
//            mNewsModel = (NewsModel) getIntent().getSerializableExtra("Model");
            mNewsModel = (Datum) getIntent().getSerializableExtra("Model");
        }
        if (mNewsModel != null) {
            if (mNewsModel.getType().equals("Scrolling"))
                spinnerType.setSelection(1);
            else if (mNewsModel.getType().equals("Static"))
                spinnerType.setSelection(2);
            String str = mNewsModel.getPhoto();
            if (!mNewsModel.getPhoto().equals("")) {
                strBase64 = mNewsModel.getPhoto();
                itemDeleteIV.setVisibility(View.VISIBLE);
                Glide.with(mActivity).load(mNewsModel.getPhoto()).into(mImage);
            }
            mRichEditText.setHtml(mNewsModel.getNews());
        }
    }

    private void setUpTypeSpinnerAdapter() {
        final List<String> list = new ArrayList<String>();
        list.add("Select Type");
        list.add("Scrolling");
        list.add("Static");

//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
//                android.R.layout.simple_spinner_item, list);
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinnerType.setAdapter(dataAdapter);

        final Typeface font = Typeface.createFromAsset(mActivity.getAssets(), "Poppins-Regular.ttf");
        ArrayAdapter<String> mCategoryAd = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);

                    TextView itemTextView = v.findViewById(R.id.itemTextView);
                    String strName = list.get(position);
                    itemTextView.setText(strName);
                    itemTextView.setTypeface(font);
                    itemTextView.setTextColor(getResources().getColor(R.color.black));
                }
                return v;
            }
        };
        spinnerType.setAdapter(mCategoryAd);
        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                strType = spinnerType.getSelectedItem().toString();
                ((TextView) view).setTypeface(font);
                ((TextView) view).setTextColor(getResources().getColor(R.color.black));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    protected void setClick() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPermission()) {
                    openCameraGalleryDialog();
                } else {
                    requestPermission();
                }
            }
        });
        btnAddNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strType.equals("Select Type")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_news_type));
                } else if (mRichEditText.getHtml().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_news));
                } else {
                    if (!Utilities.isNetworkAvailable(mActivity)) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        /*ExecuteApi*/
                        executeEditAPI();
                    }
                }
            }
        });
        itemDeleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImage.setImageResource(R.drawable.palace_holder);
                itemDeleteIV.setVisibility(View.GONE);
                rotate = null;
                strBase64 = "";
            }
        });
    }

    private void executeEditAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.editNewsRequest(mParam()).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mAlerDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
                AlertDialogManager.hideProgressDialog();
            }
        });
    }



    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("news", mRichEditText.getHtml().toString());
        mMap.put("type", strType);
        mMap.put("photo1", strBase64);
        mMap.put("news_id", mNewsModel.getId());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }


//    private void executeEditAPI() {
//        AlertDialogManager.showProgressDialog(mActivity);
//        String url = JaoharConstants.EDIT_NEWS;
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("news", mRichEditText.getHtml().toString());
//            jsonObject.put("type", strType);
//            jsonObject.put("photo1", strBase64);
//            jsonObject.put("news_id", mNewsModel.getId());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
//                url, jsonObject,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        AlertDialogManager.hideProgressDialog();
//                        Log.d(TAG, response.toString());
//                        try {
//                            if (response.getString("status").equals("1")) {
//                                mAlerDialog(mActivity, getString(R.string.app_name), response.getString("message"));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                VolleyLog.d(TAG, "Error: " + error.getMessage());
//
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
////      Adding request to request queue
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjReq);
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

    public void mAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mActivity.finish();
            }
        });
        alertDialog.show();
    }

    /*********
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/

    private boolean checkPermission() {
        int camera = ContextCompat.checkSelfPermission(getApplicationContext(), cameraStr);
        int readStorage = ContextCompat.checkSelfPermission(getApplicationContext(), readStorageStr);
        return camera == PackageManager.PERMISSION_GRANTED && readStorage == PackageManager.PERMISSION_GRANTED;
    }


    void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{cameraStr, readStorageStr}, PERMISSION_REQUEST_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
//                            startActivity(new Intent(SplashSlidesActivity.this, SelectionActivity.class));
//                            finish();
//                            Toast.makeText(context,"on",Toast.LENGTH_SHORT).show();
                            openCameraGalleryDialog();
                        } else {
                            Log.e(TAG, "onRequestPermissionsResult: failed");
                            requestPermission();
//                            permissionAccepted = false;
                        }
                    }
                }
                break;
        }
    }

    private void openCameraGalleryDialog() {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_display_dialog);

        TextView text_camra = (TextView) dialog.findViewById(R.id.txt_camra);
        TextView text_gallery = (TextView) dialog.findViewById(R.id.txt_gallery);
        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);

        text_camra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openCamera();
            }
        });
        text_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        Log.e("error is occured", dialog.toString());
    }

    public void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
//                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }

    public void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        InputStream stream = null;
        Bitmap bitmap = null;
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            File finalFile = new File(getRealPathFromURI(uri));

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 0;

            bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
//                   For COnvert And rotate image
            ExifInterface exifInterface = null;
            try {
                exifInterface = new ExifInterface(finalFile.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }

            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            Matrix matrix = new Matrix();
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.setRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.setRotate(180);
                    break;
                default:

            }
            rotate = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

            mImage.setImageBitmap(rotate);
            itemDeleteIV.setVisibility(View.VISIBLE);
            strBase64 = getBase64String(rotate);

        }
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;
            bitmap = BitmapFactory.decodeFile(mStoragePath, options);
            rotate = rotateImage(bitmap);

            mImage.setImageBitmap(rotate);
            itemDeleteIV.setVisibility(View.VISIBLE);
            strBase64 = getBase64String(rotate);


        }
    }

    //       Method Correct Rotate Image when Capture From Camera....

    private Bitmap rotateImage(Bitmap bitmap) {
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(mStoragePath);


        } catch (IOException e) {
            e.printStackTrace();
        }

        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            default:

        }
        Bitmap rotate = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        return rotate;
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private String getBase64String(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] imageBytes = baos.toByteArray();

        String base64String = Base64.encodeToString(imageBytes, Base64.NO_WRAP);

        return base64String;
    }
}
