package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.DiscountModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.Utilities;

public class EditSubtractActivity extends BaseActivity {
    Activity mActivity = EditSubtractActivity.this;
    String TAG = EditSubtractActivity.this.getClass().getSimpleName(), strItemID,strItem,strQuantity,strPrice,strTotal,strSubtractValue,strDescription;
    LinearLayout llLeftLL;
    TextView txtSubTotalTV,txtQuantityTV,txtVatPriceTV,txtTotalTV;
    EditText AddValueTV,txtDescriptionTV;
    Button btnSubmitB;
    InvoiceAddItemModel mModel, pModel;
    int mPosition;
    ArrayList<DiscountModel> mDiscountModelArrayList = new ArrayList<DiscountModel>();
    private  long parcentValue;
    DiscountModel mDiscountModel ;
    private long sumData;
    private double totalPrice;
    double totalPercent ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_subtract);

    }


    @Override
    protected void setViewsIDs() {
        llLeftLL=(LinearLayout)findViewById(R.id.llLeftLL);
        txtSubTotalTV=(TextView)findViewById(R.id.txtSubTotalTV);
        txtQuantityTV=(TextView)findViewById(R.id.txtQuantityTV);
        txtDescriptionTV=(EditText)findViewById(R.id.txtDescriptionTV);
        txtVatPriceTV=(TextView)findViewById(R.id.txtVatPriceTV);
        txtTotalTV=(TextView)findViewById(R.id.txtTotalTV);

        AddValueTV=(EditText) findViewById(R.id.AddValueTV);
        btnSubmitB=(Button) findViewById(R.id.btnSubmitB);
        if (getIntent() != null) {
            mDiscountModel =new DiscountModel();
            mDiscountModel = (DiscountModel) getIntent().getSerializableExtra("Model");
//            mDiscountModelArrayList =mModel.getmDiscountModelArrayList();
            mPosition = getIntent().getIntExtra("Position", 0);
//            mDiscountModel=mDiscountModelArrayList.get(mPosition);
//            strItemID = mModel.getItemID();
        }

//        if(!mDiscountModel.getStrTotalEditAmount().equals("")){
//            double totalunitPrice = Double.parseDouble(mDiscountModel.getStrTotalEditAmount());
//            totalPrice = Double.parseDouble(String.valueOf(mDiscountModel.getSubtract_quantity())) *totalunitPrice ;
//        }else {
//        }
//if(JaoharConstants.ISupdate==true){
    totalPrice =  Double.parseDouble(mDiscountModel.getSubtract_unitPrice());

//}else {
//    totalPrice =  Double.parseDouble(String.valueOf(mDiscountModel.getSubtract_quantity())) *Double.parseDouble(mDiscountModel.getSubtract_unitPrice());
//
//}
        totalPercent = Double.parseDouble(mDiscountModel.getStrtotalPercentValue());
        String strTotal1= String.valueOf(totalPrice);
        strItem = mDiscountModel.getSubtract_items();
        strQuantity = String.valueOf(mDiscountModel.getSubtract_quantity());
        strPrice = String.valueOf(mDiscountModel.getSubtract_unitPrice());
        strTotal = String.valueOf(mDiscountModel.getAddAndSubtractTotal());
        strSubtractValue = String.valueOf(mDiscountModel.getSubtract_Value());
        strDescription = String.valueOf(mDiscountModel.getSubtract_description());








        txtSubTotalTV.setText(strItem);
        txtVatPriceTV.setText(Utilities.convertEvalueToNormal(Double.parseDouble(strTotal1)));
//        txtVatPriceTV.setText(strPrice);
        txtDescriptionTV.setText(strDescription);
        txtQuantityTV.setText(strQuantity);
        AddValueTV.setText(Utilities.convertEvalueToNormal(Double.parseDouble(strSubtractValue)));
        txtTotalTV.setText(Utilities.convertEvalueToNormal(Double.parseDouble(strTotal)));

    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });


        btnSubmitB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtDescriptionTV.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_description));
                } else if (AddValueTV.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.enter_invoice_value));
                } else {
                    mDiscountModel = new DiscountModel();
                    mDiscountModel.setSubtract_description(txtDescriptionTV.getText().toString());
                    mDiscountModel.setSubtract_items(strItem);
                    mDiscountModel.setSubtract_quantity(strQuantity);
                    mDiscountModel.setSubtract_unitPrice(strPrice);
                    mDiscountModel.setSubtract_Value(AddValueTV.getText().toString());
                    mDiscountModel.setType("SUBTRACT");
                    mDiscountModel.setStrtotalPercentValue(String.valueOf(totalPercent));
                    mDiscountModel.setAddAndSubtractTotal(txtTotalTV.getText().toString());
//                    mDiscountModelArrayList.add(mPosition,mDiscountModel);
//                  mModel.setmDiscountModelArrayList(mDiscountModelArrayList);
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("pModel", mDiscountModel);
//                    returnIntent.putExtra("pModel1", mDiscountModelArrayList);
                    returnIntent.putExtra("Position", mPosition);
                    setResult(123, returnIntent);
                    onBackPressed();
                    finish();
                }
            }
        });


        AddValueTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                double basePrice=0;

                double sum;
                String str = AddValueTV.getText().toString();
                try
                {
//                    double i = Double.parseDouble(String.valueOf(mDiscountModel.getSubtract_quantity())) *Double.parseDouble(mDiscountModel.getSubtract_unitPrice()) ;

                    basePrice = totalPrice;//   Integer.parseInt()
                    float percentValue = (Float.parseFloat(str))/100;

                    totalPercent= ((basePrice * Double.parseDouble(str)) / 100);
                    totalPercent = Utilities.getRoundOff2Decimal(totalPercent);
                    sum =totalPrice-totalPercent;
                    sum = Utilities.getRoundOff2Decimal(sum);
                    txtTotalTV.setText(""+sum);
                }
                catch (Exception ex) {

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
