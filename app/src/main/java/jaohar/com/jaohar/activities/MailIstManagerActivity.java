package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.MailingListManagerAdapter;
import jaohar.com.jaohar.interfaces.DeleteMAlingList;
import jaohar.com.jaohar.interfaces.EditMailingListInterFace;
import jaohar.com.jaohar.interfaces.SendingMulipleLISTID;
import jaohar.com.jaohar.models.AllMailList;
import jaohar.com.jaohar.models.GetAllMailListsModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import jaohar.com.jaohar.views.CustomNestedScrollView;
import retrofit2.Call;
import retrofit2.Callback;

public class MailIstManagerActivity extends BaseActivity {
    Activity mActivity = MailIstManagerActivity.this;
    String TAG = MailIstManagerActivity.this.getClass().getSimpleName();
    TextView txtCenter, txtRight, txtMailTV;
    ImageView imgBack, imgRight;
    RelativeLayout imgRightLL;
    LinearLayout llLeftLL;
    RecyclerView mailingListRV;
    SwipeRefreshLayout swipeToRefresh;
    CustomNestedScrollView parentSV;
    boolean isSwipeRefresh = false;
    int page_no = 1;
    public String strLastPage = "FALSE";
    boolean isLoadMore = false;
    MailingListManagerAdapter mAdapter;
    ArrayList<AllMailList> mArrayList = new ArrayList();
    ArrayList<String> mListID = new ArrayList();
    //    ArrayList<MailingListModel> loadMoreArrayList = new ArrayList();
    ArrayList<AllMailList> loadMoreArrayList = new ArrayList();
    ProgressBar progressBottomPB;

    MailingListManagerAdapter.ClickManagerModulesInterface clickManagerModulesInterface = new MailingListManagerAdapter.ClickManagerModulesInterface() {
        @Override
        public void mClickManagerModulesInterface(int position, String strListId, String strListName) {
            PerformOptionsClick(position, strListId, strListName);
        }
    };

    private void PerformOptionsClick(final int position, final String strListID, final String strListNAME) {
        View view = getLayoutInflater().inflate(R.layout.bottom_sheet_mailing_options, null);
        final BottomSheetDialog dialog = new BottomSheetDialog(mActivity);
        dialog.setContentView(view);
        dialog.show();

        RelativeLayout editRL = view.findViewById(R.id.editRL);
        RelativeLayout viewRL = view.findViewById(R.id.viewRL);
        RelativeLayout mailRL = view.findViewById(R.id.mailRL);
        RelativeLayout deleteRL = view.findViewById(R.id.deleteRL);
        RelativeLayout cancelRL = view.findViewById(R.id.cancelRL);

        editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                strLastPage = "FALSE";
                Intent mIntent = new Intent(mActivity, AddMailingListActivity.class);
                mIntent.putExtra("isAddClick", "false");
                mIntent.putExtra("listID", strListID);
                mIntent.putExtra("listNAME", strListNAME);
                startActivity(mIntent);
            }
        });

        viewRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent mIntent = new Intent(mActivity, ShowMailingContactLISTActivity.class);
                mIntent.putExtra("listID", strListID);
                mActivity.startActivity(mIntent);
            }
        });

        mailRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                /*Send Email*/
                Intent mIntent = new Intent(mActivity, SendMailingListActivity.class);
//                mIntent.putExtra("listIDArray", getMultiDetailsData());
                mIntent.putExtra("listID", strListID);
                mIntent.putExtra("type", "staff");
                mActivity.startActivity(mIntent);
            }
        });

        deleteRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                deleteConfirmDialog(strListID);
            }
        });

        cancelRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    EditMailingListInterFace mEDitLIST = new EditMailingListInterFace() {
        @Override
        public void editmailingByListID(String strListID, String strListNAME) {
            strLastPage = "FALSE";
            Intent mIntent = new Intent(mActivity, AddMailingListActivity.class);
            mIntent.putExtra("isAddClick", "false");
            mIntent.putExtra("listID", strListID);
            mIntent.putExtra("listNAME", strListNAME);
            startActivity(mIntent);
        }
    };

    static int IsActive = 0;

    //    private final ArrayList<MailingListModel> multiSelectArrayList = new ArrayList<MailingListModel>();
    private final ArrayList<AllMailList> multiSelectArrayList = new ArrayList<AllMailList>();

    SendingMulipleLISTID mMultimaIlInterface = new SendingMulipleLISTID() {
        @Override
        public void mSendingMultipleLIST(AllMailList mMailingLIST, boolean mDelete) {
            if (mMailingLIST != null) {
                if (!mDelete) {
                    IsActive = IsActive + 1;
                    multiSelectArrayList.add(mMailingLIST);
                    String strID = mMailingLIST.getListId();
                    mListID.add(mMailingLIST.getListId());
                } else {
                    IsActive = IsActive - 1;
                    multiSelectArrayList.remove(mMailingLIST);
                    String strID = mMailingLIST.getListId();
                    mListID.remove(mMailingLIST.getListId());
                }
                if (IsActive > 0) {
                    txtMailTV.setVisibility(View.VISIBLE);
                } else {
                    txtMailTV.setVisibility(View.GONE);
                }
            }
        }
    };

    DeleteMAlingList mDeleteLIST = new DeleteMAlingList() {
        @Override
        public void DeleteMailBYID(String strListID) {
            deleteConfirmDialog(strListID);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_mail_ist_manager);
    }

    @Override
    protected void setViewsIDs() {
        progressBottomPB = findViewById(R.id.progressBottomPB);
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        mailingListRV = (RecyclerView) findViewById(R.id.mailingListRV);
        swipeToRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        txtMailTV = (TextView) findViewById(R.id.txtMailTV);
//        txtRight = (TextView) findViewById(R.id.txtRight);
//        txtRight.setVisibility(View.GONE);

        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgRight = (ImageView) findViewById(R.id.imgRight);
        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        imgRightLL.setVisibility(View.VISIBLE);
        parentSV = (CustomNestedScrollView) findViewById(R.id.parentSV);

        /* set top bar */
        imgBack.setImageResource(R.drawable.back);
        imgRight.setImageResource(R.drawable.add_icon);
        txtCenter.setText(getString(R.string.mailing_list));

        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                page_no = 1;
                mArrayList.clear();
                loadMoreArrayList.clear();
                if (Utilities.isNetworkAvailable(mActivity) == false) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    executeAPI(page_no);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
//            if (strLastPage.equals("FALSE")) {
            page_no = 1;
            mArrayList.clear();
            loadMoreArrayList.clear();
            executeAPI(page_no);
//            }
        }
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransitionExit();
            }
        });

        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strLastPage = "FALSE";
                Intent mIntent = new Intent(mActivity, AddMailingListActivity.class);
                mIntent.putExtra("isAddClick", "true");
                startActivity(mIntent);


            }
        });

        parentSV.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    progressBottomPB.setVisibility(View.VISIBLE);
                    isLoadMore = true;
                    ++page_no;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (strLastPage.equals("FALSE")) {
                                executeAPI(page_no);
                            } else {
                                progressBottomPB.setVisibility(View.GONE);
                            }
                        }
                    }, 500);
                }
            }
        });
        txtMailTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Send Email*/
                Intent mIntent = new Intent(mActivity, SendMailingListActivity.class);
                mIntent.putExtra("listIDArray", getMultiDetailsData());
                mIntent.putExtra("type", "staff");
                mActivity.startActivity(mIntent);
            }
        });
    }

    private ArrayList<String> getMultiDetailsData() {
        ArrayList<String> strData = new ArrayList<>();
        strData.clear();

        for (int i = 0; i < mListID.size(); i++) {
            strData.add(mListID.get(i));
        }
        mListID.clear();
        return strData;
    }

//    public void executeAPI(int page_no) {
//        String strUrl = JaoharConstants.GetAllMailLists + "?page_no=" + page_no + "&user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");// + "?page_no=" + page_no ;
//        Log.e(TAG, "***URL***" + strUrl);
//        if (page_no > 1) {
//            progressBottomPB.setVisibility(View.VISIBLE);
//            AlertDialogManager.hideProgressDialog();
//        }
//        if (page_no == 1) {
//            progressBottomPB.setVisibility(View.GONE);
//            AlertDialogManager.showProgressDialog(mActivity);
////          Utilities.showProgressDialog(getActivity());
//        }
//
//        if (!isSwipeRefresh) {
//            progressBottomPB.setVisibility(View.GONE);
//
//        }
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                progressBottomPB.setVisibility(View.GONE);
//
//                if (isSwipeRefresh) {
//                    swipeToRefresh.setRefreshing(false);
//                }
//
//                try {
//                    JSONObject mJsonData = new JSONObject(response);
//                    if (mJsonData.getString("status").equals("1")) {
//                        parseResponce(response);
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonData.getString("message"));
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }


    public void executeAPI(int page_no) {
        if (page_no > 1) {
            progressBottomPB.setVisibility(View.VISIBLE);
            AlertDialogManager.hideProgressDialog();
        }
        if (page_no == 1) {
            progressBottomPB.setVisibility(View.GONE);
            AlertDialogManager.showProgressDialog(mActivity);
//          Utilities.showProgressDialog(getActivity());
        }

        if (!isSwipeRefresh) {
            progressBottomPB.setVisibility(View.GONE);

        }
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetAllMailListsModel> call1 = mApiInterface.getAllMailsListRequest(String.valueOf(page_no), JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<GetAllMailListsModel>() {
            @Override
            public void onResponse(Call<GetAllMailListsModel> call, retrofit2.Response<GetAllMailListsModel> response) {
                AlertDialogManager.hideProgressDialog();
                GetAllMailListsModel mModel = response.body();
                assert mModel != null;
                progressBottomPB.setVisibility(View.GONE);

                if (isSwipeRefresh) {
                    swipeToRefresh.setRefreshing(false);
                }
                if (mModel.getStatus().equals("1")) {
                    strLastPage = mModel.getData().getLastPage();
                    if (page_no == 1) {
                        mArrayList = mModel.getData().getAllMailLists();
                    } else if (page_no > 1) {
                        loadMoreArrayList = mModel.getData().getAllMailLists();
                    }

                    if (loadMoreArrayList.size() > 0) {
                        mArrayList.addAll(loadMoreArrayList);
                    }
                    if (page_no == 1) {
                        setAdapter();
                    } else {
                        mAdapter.notifyDataSetChanged();
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<GetAllMailListsModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    public void executeDeleteAPI(String strListID) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteMailListRequest(strListID);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
                    page_no = 1;
                    mArrayList.clear();
                    loadMoreArrayList.clear();
                    executeAPI(page_no);
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }


//    public void executeDeleteAPI(String strListID) {
//        String strUrl = JaoharConstants.DeleteMailList + "?list_id=" + strListID;// + "?page_no=" + page_no ;
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "******Response*****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonData = new JSONObject(response);
//                    if (mJsonData.getString("status").equals("1")) {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonData.getString("message"));
//                        page_no = 1;
//                        mArrayList.clear();
//                        loadMoreArrayList.clear();
//                        executeAPI(page_no);
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonData.getString("message"));
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

//    private void parseResponce(String response) {
//        try {
//            JSONObject mDataObj = new JSONObject(response);
//            JSONObject mDataObject = mDataObj.getJSONObject("data");
//            strLastPage = mDataObject.getString("last_page");
//
//            JSONArray mJsonArray = mDataObject.getJSONArray("all_mail_lists");
//            for (int i = 0; i < mJsonArray.length(); i++) {
//                JSONObject mJsonObject1 = mJsonArray.getJSONObject(i);
//                MailingListModel mMailingModel = new MailingListModel();
//                if (!mJsonObject1.getString("list_id").equals("")) {
//                    mMailingModel.setList_id(mJsonObject1.getString("list_id"));
//                }
//                if (!mJsonObject1.getString("list_name").equals("")) {
//                    mMailingModel.setList_name(mJsonObject1.getString("list_name"));
//                }
//                if (!mJsonObject1.getString("added_by").equals("")) {
//                    mMailingModel.setAdded_by(mJsonObject1.getString("added_by"));
//                }
//                if (!mJsonObject1.getString("modification_date").equals("")) {
//                    mMailingModel.setModification_date(mJsonObject1.getString("modification_date"));
//                }
//                if (page_no == 1) {
//                    mArrayList.add(mMailingModel);
//                } else if (page_no > 1) {
//                    loadMoreArrayList.add(mMailingModel);
//                }
//            }
//            if (loadMoreArrayList.size() > 0) {
//                mArrayList.addAll(loadMoreArrayList);
//            }
//            setAdapter();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    private void setAdapter() {
        mailingListRV.setNestedScrollingEnabled(false);
        mailingListRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mAdapter = new MailingListManagerAdapter(mActivity, mArrayList, mEDitLIST, mDeleteLIST, mMultimaIlInterface, clickManagerModulesInterface);
        mailingListRV.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent mIntent = new Intent(mActivity, ManagerActivity.class);
//        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        mIntent.putExtra(JaoharConstants.LOGIN, "StaffHome");
//        mActivity.startActivity(mIntent);
//        mActivity.finish();
    }

    public void deleteConfirmDialog(final String strListID) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_data));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(strListID);
            }
        });

        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });

        deleteConfirmDialog.show();
    }
}
