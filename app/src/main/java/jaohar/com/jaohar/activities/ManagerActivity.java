package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.forum_module_admin.AdminForumActivity;
import jaohar.com.jaohar.activities.trash_module.TrashActivity;
import jaohar.com.jaohar.adapters.manager_module.ManagerModulesAdapter;
import jaohar.com.jaohar.beans.ManagerModule.ManagerModulesModel;
import jaohar.com.jaohar.interfaces.managermodules.ClickManagerModulesInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class ManagerActivity extends BaseActivity implements ClickManagerModulesInterface {
    Activity mActivity = ManagerActivity.this;
    String TAG = ManagerActivity.this.getClass().getSimpleName();

    //WIDGETS
    LinearLayout llLeftLL;
    RelativeLayout imgRightLL;
    CircleImageView imgRight, statusIMG;
    TextView txtCenter, badgesTV;
    RecyclerView tabListRV;
    LinearLayout homeLL, aboutLL, servicesLL, ourOfficesLL, ourHistory, contactUsLL, staffLL, adminAndRoleLL, newRequestsLL, vesselsShipsLL, logoutLL,
            dynamicAdditionLL, massPushLL, mailingListLL, addressBookLL, trashLL, forumLL;

    List<ManagerModulesModel> ManagerModulesModelList = new ArrayList<>();
    ClickManagerModulesInterface clickManagerModulesInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_manager);

        /* set click interface */
        clickManagerModulesInterface = this;

        /* initialize all ids */
        setViewsIDs();

        /* set all modules list */
        setModulesList();
    }

    private void setModulesList() {
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.home), R.drawable.ic_home_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.admin_and_role), R.drawable.ic_admin_role_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.new_requests), R.drawable.ic_new_requests_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.vessels_ships), R.drawable.ic_vessels_ships_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.staff), R.drawable.ic_staff_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.dynamic_addition), R.drawable.ic_dynamic_additions_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.mass_push), R.drawable.ic_mass_push_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.about), R.drawable.ic_about_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.services), R.drawable.ic_services_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.our_offfices), R.drawable.ic_our_offices_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.our_history), R.drawable.ic_our_history_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.mailing_list), R.drawable.ic_mailing_list_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.contact_us), R.drawable.ic_contact_us_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.address_book_), R.drawable.ic_address_book_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.trash), R.drawable.ic_trash_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.fund_trash), R.drawable.ic_fund_trash_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.letter_trash), R.drawable.ic_letter_trash_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.forum), R.drawable.ic_form_png));
        ManagerModulesModelList.add(new ManagerModulesModel(getResources().getString(R.string.chat), R.drawable.ic_chat_png));

        /* set modules adapter */
        setAdapter("0");
    }

    private void setAdapter(String BadgeCount) {
        tabListRV.setHasFixedSize(false);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mActivity, 3);
        tabListRV.setLayoutManager(gridLayoutManager);
        ManagerModulesAdapter mAdapter = new ManagerModulesAdapter(mActivity, ManagerModulesModelList, clickManagerModulesInterface, BadgeCount);
        tabListRV.setAdapter(mAdapter);
    }

    public void setViewsIDs() {
        /*SET UP TOOLBAR*/
        llLeftLL = findViewById(R.id.llLeftLL);
        imgRight = findViewById(R.id.imgRight);
        statusIMG = findViewById(R.id.statusIMG);
        imgRightLL = findViewById(R.id.imgRightLL);
        imgRightLL.setVisibility(View.VISIBLE);
        txtCenter = (TextView) findViewById(R.id.txtCenter);

        /*MAIN LAYOUT IDS*/
        homeLL = findViewById(R.id.homeLL);
        aboutLL = findViewById(R.id.aboutLL);
        servicesLL = findViewById(R.id.servicesLL);
        ourOfficesLL = findViewById(R.id.ourOfficesLL);
        ourHistory = findViewById(R.id.ourHistory);
        contactUsLL = findViewById(R.id.contactUsLL);
        staffLL = findViewById(R.id.staffLL);
        adminAndRoleLL = findViewById(R.id.adminAndRoleLL);
        newRequestsLL = findViewById(R.id.newRequestsLL);
        vesselsShipsLL = findViewById(R.id.vesselsShipsLL);
        trashLL = findViewById(R.id.trashLL);
        dynamicAdditionLL = findViewById(R.id.dynamicAdditionLL);
        massPushLL = findViewById(R.id.massPushLL);
        mailingListLL = findViewById(R.id.mailingListLL);
        addressBookLL = findViewById(R.id.addressBookLL);
        forumLL = findViewById(R.id.forumLL);
        badgesTV = findViewById(R.id.badgesTV);
        tabListRV = findViewById(R.id.tabListRV);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!Utilities.isNetworkAvailable(mActivity)) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            /*Execute Api*/
            gettingProfileDATA(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        }
    }

    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JaoharConstants.Is_click_form_Manager = true;
                JaoharConstants.BOOLEAN_USER_LOGOUT = false;
                JaoharConstants.BOOLEAN_STAFF_LOGOUT = false;
                Intent mIntent = new Intent(mActivity, UserProfileActivity.class);
                startActivity(mIntent);
            }
        });

        addressBookLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, AddressBookListStaffActivity.class));
            }
        });

        homeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, HomeActivity.class);
                mIntent.putExtra(JaoharConstants.LOGIN, "Home");
                startActivity(mIntent);
                finish();
                overridePendingTransitionExit();
            }
        });

        aboutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mActivity, "Coming Soon...", Toast.LENGTH_SHORT).show();
            }
        });

        servicesLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mActivity, "Coming Soon...", Toast.LENGTH_SHORT).show();
            }
        });

        ourOfficesLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mActivity, "Coming Soon...", Toast.LENGTH_SHORT).show();
            }
        });

        ourHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mActivity, "Coming Soon...", Toast.LENGTH_SHORT).show();
            }
        });

        mailingListLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, MailIstManagerActivity.class));
                overridePendingTransitionEnter();
            }
        });

        contactUsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mActivity, "Coming Soon...", Toast.LENGTH_SHORT).show();
            }
        });

        forumLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, AdminForumActivity.class);
                mIntent.putExtra("isClick", "vesselClick");
                startActivity(mIntent);
                overridePendingTransitionEnter();
            }
        });

        staffLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, HomeActivity.class);
                mIntent.putExtra(JaoharConstants.LOGIN, "StaffHome");
                startActivity(mIntent);
                finish();
                overridePendingTransitionExit();
            }
        });

        adminAndRoleLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, AdminRoleActivity.class));
                overridePendingTransitionEnter();
            }
        });

        newRequestsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JaoharConstants.Is_click_form_Manager = true;
                startActivity(new Intent(mActivity, NewRequestActivity.class));
                finish();
                overridePendingTransitionEnter();
            }
        });

        vesselsShipsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JaoharConstants.Is_click_form_Manager = true;
                startActivity(new Intent(mActivity, VesselsShipsActivity.class));
                finish();
                overridePendingTransitionEnter();
            }
        });

        trashLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, TrashActivity.class);
                mIntent.putExtra("isClick", "vesselClick");
                startActivity(mIntent);
                overridePendingTransitionEnter();
            }
        });

        dynamicAdditionLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, DynamicAditionsActivity.class));
                overridePendingTransitionEnter();
            }
        });

        massPushLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, MassPushActivity.class));
                overridePendingTransitionEnter();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent mIntent = new Intent(mActivity, HomeActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mIntent.putExtra(JaoharConstants.LOGIN, "StaffHome");
        mActivity.startActivity(mIntent);
        mActivity.finish();
    }

    //ADMIN_LOGOUT_API
    private void executeAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.adminLogoutRequest(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""), JaoharPreference.readString(mActivity, JaoharPreference.FCM_REFRESHED_TOKEN, ""), "Android");
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    SharedPreferences preferences = JaoharPreference.getPreferences(mActivity);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.remove(JaoharPreference.IS_LOGGED_IN_ADMIN);
                    editor.remove(JaoharPreference.ADMIN_ID);
                    editor.remove(JaoharPreference.ADMIN_EMAIL);
                    editor.remove(JaoharPreference.ADMIN_ROLE);
                    editor.commit();
                    Intent mIntent = new Intent(mActivity, HomeActivity.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mIntent.putExtra(JaoharConstants.LOGIN, "StaffHome");
                    mActivity.startActivity(mIntent);
                    mActivity.finish();
                    overridePendingTransitionEnter();
                } else if (mModel.getStatus() == 0) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    public void executeAPI() {
//        String strUrl = JaoharConstants.ADMIN_LOGOUT_API + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&device_token=" + JaoharPreference.readString(mActivity, JaoharPreference.FCM_REFRESHED_TOKEN, "") + "&device_type=" + "Android";
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "*****Response****" + response);
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    if (strStatus.equals("1")) {
//                        SharedPreferences preferences = JaoharPreference.getPreferences(mActivity);
//                        SharedPreferences.Editor editor = preferences.edit();
//                        editor.remove(JaoharPreference.IS_LOGGED_IN_ADMIN);
//                        editor.remove(JaoharPreference.ADMIN_ID);
//                        editor.remove(JaoharPreference.ADMIN_EMAIL);
//                        editor.remove(JaoharPreference.ADMIN_ROLE);
//                        editor.commit();
//                        Intent mIntent = new Intent(mActivity, HomeActivity.class);
//                        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        mIntent.putExtra(JaoharConstants.LOGIN, "StaffHome");
//                        mActivity.startActivity(mIntent);
//                        mActivity.finish();
//                        overridePendingTransitionEnter();
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    /* execute get user profile api */
    private void gettingProfileDATA(String strLoginID) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getuserProfileRequest(strLoginID);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    String strStatus = mJsonObject.getString("status");
                    if (strStatus.equals("1")) {
                        if (!mJsonObject.isNull("data")) {
                            JSONObject mJsonDATA = mJsonObject.getJSONObject("data");
                            if (!mJsonDATA.getString("image").equals("")) {
                                String strImage = mJsonDATA.getString("image");
                                Glide.with(mActivity).load(strImage).into(imgRight);
                            } else {
                                imgRight.setImageResource(R.drawable.profile);
                            }

                            if (!mJsonDATA.getString("chat_status").equals("")) {
                                String strImage = mJsonDATA.getString("chat_status");
                                if (strImage.equals("Available")) {
                                    statusIMG.setImageDrawable(getResources().getDrawable(R.drawable.green_status_icon));
                                } else if (strImage.equals("Busy")) {
                                    statusIMG.setImageDrawable(getResources().getDrawable(R.drawable.red_status_icon));
                                } else if (strImage.equals("Invisible")) {
                                    statusIMG.setImageDrawable(getResources().getDrawable(R.drawable.grey_status_icon));
                                }
                            }

                            if (!mJsonDATA.getString("total_unread_forum_message_count").equals("")) {
                                String strBadgesCount = mJsonDATA.getString("total_unread_forum_message_count");
                                if (strBadgesCount.equals("0")) {
                                    badgesTV.setVisibility(View.GONE);
                                } else {
                                    badgesTV.setVisibility(View.VISIBLE);
                                    badgesTV.setText(strBadgesCount);
                                    setAdapter(strBadgesCount);
                                }
                            } else {
                                badgesTV.setVisibility(View.GONE);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }


    /* execute get user profile api */
//    private void gettingProfileDATA(String strLoginID) {
//        String strUrl = JaoharConstants.GetUserProfile_API + "?user_id=" + strLoginID;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "*****Response****" + response);
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    String strStatus = mJsonObject.getString("status");
//                    if (strStatus.equals("1")) {
//                        if (!mJsonObject.isNull("data")) {
//                            JSONObject mJsonDATA = mJsonObject.getJSONObject("data");
//                            if (!mJsonDATA.getString("image").equals("")) {
//                                String strImage = mJsonDATA.getString("image");
//                                Glide.with(mActivity).load(strImage).into(imgRight);
//                            } else {
//                                imgRight.setImageResource(R.drawable.profile);
//                            }
//
//                            if (!mJsonDATA.getString("chat_status").equals("")) {
//                                String strImage = mJsonDATA.getString("chat_status");
//                                if (strImage.equals("Available")) {
//                                    statusIMG.setImageDrawable(getResources().getDrawable(R.drawable.green_status_icon));
//                                } else if (strImage.equals("Busy")) {
//                                    statusIMG.setImageDrawable(getResources().getDrawable(R.drawable.red_status_icon));
//                                } else if (strImage.equals("Invisible")) {
//                                    statusIMG.setImageDrawable(getResources().getDrawable(R.drawable.grey_status_icon));
//                                }
//                            }
//
//                            if (!mJsonDATA.getString("total_unread_forum_message_count").equals("")) {
//                                String strBadgesCount = mJsonDATA.getString("total_unread_forum_message_count");
//                                if (strBadgesCount.equals("0")) {
//                                    badgesTV.setVisibility(View.GONE);
//                                } else {
//                                    badgesTV.setVisibility(View.VISIBLE);
//                                    badgesTV.setText(strBadgesCount);
//                                    setAdapter(strBadgesCount);
//                                }
//                            } else {
//                                badgesTV.setVisibility(View.GONE);
//                            }
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    @Override
    public void mClickManagerModulesInterface(int position, String name) {
        if (name.equalsIgnoreCase(getString(R.string.home))) {
            Intent mIntent = new Intent(mActivity, HomeActivity.class);
            mIntent.putExtra(JaoharConstants.LOGIN, "Home");
            startActivity(mIntent);
            finish();
            overridePendingTransitionExit();
        } else if (name.equalsIgnoreCase(getString(R.string.admin_and_role))) {
            startActivity(new Intent(mActivity, AdminRoleActivity.class));
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.new_requests))) {
            JaoharConstants.Is_click_form_Manager = true;
            startActivity(new Intent(mActivity, NewRequestActivity.class));
            finish();
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.vessels_ships))) {
            JaoharConstants.Is_click_form_Manager = true;
            startActivity(new Intent(mActivity, VesselsShipsActivity.class));
//            finish();
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.staff))) {
            Intent mIntent = new Intent(mActivity, HomeActivity.class);
            mIntent.putExtra(JaoharConstants.LOGIN, "StaffHome");
            startActivity(mIntent);
            finish();
            overridePendingTransitionExit();
        } else if (name.equalsIgnoreCase(getString(R.string.dynamic_addition))) {
            startActivity(new Intent(mActivity, DynamicAditionsActivity.class));
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.mass_push))) {
            startActivity(new Intent(mActivity, MassPushActivity.class));
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.about))) {
            Toast.makeText(mActivity, "Coming Soon...", Toast.LENGTH_SHORT).show();
        } else if (name.equalsIgnoreCase(getString(R.string.services))) {
            Toast.makeText(mActivity, "Coming Soon...", Toast.LENGTH_SHORT).show();
        } else if (name.equalsIgnoreCase(getString(R.string.our_offfices))) {
            Toast.makeText(mActivity, "Coming Soon...", Toast.LENGTH_SHORT).show();
        } else if (name.equalsIgnoreCase(getString(R.string.our_history))) {
            Toast.makeText(mActivity, "Coming Soon...", Toast.LENGTH_SHORT).show();
        } else if (name.equalsIgnoreCase(getString(R.string.mailing_list))) {
            startActivity(new Intent(mActivity, MailIstManagerActivity.class));
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.contact_us))) {
            Toast.makeText(mActivity, "Coming Soon...", Toast.LENGTH_SHORT).show();
        } else if (name.equalsIgnoreCase(getString(R.string.address_book_))) {
            startActivity(new Intent(mActivity, AddressBookListStaffActivity.class));
        } else if (name.equalsIgnoreCase(getString(R.string.trash))) {
            Intent mIntent = new Intent(mActivity, TrashActivity.class);
            mIntent.putExtra("isClick", "vesselClick");
            startActivity(mIntent);
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.fund_trash))) {
            Toast.makeText(mActivity, "Coming Soon...", Toast.LENGTH_SHORT).show();
        } else if (name.equalsIgnoreCase(getString(R.string.letter_trash))) {
            Toast.makeText(mActivity, "Coming Soon...", Toast.LENGTH_SHORT).show();
        } else if (name.equalsIgnoreCase(getString(R.string.forum))) {
            Intent mIntent = new Intent(mActivity, AdminForumActivity.class);
            mIntent.putExtra("isClick", "vesselClick");
            startActivity(mIntent);
            overridePendingTransitionEnter();
        } else if (name.equalsIgnoreCase(getString(R.string.chat))) {
            Toast.makeText(mActivity, "Coming Soon...", Toast.LENGTH_SHORT).show();
        }
    }
}
