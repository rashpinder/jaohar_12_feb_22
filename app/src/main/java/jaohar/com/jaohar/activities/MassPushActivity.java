package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.MashPushAdapter;
import jaohar.com.jaohar.interfaces.DeleteMassPush;
import jaohar.com.jaohar.interfaces.EditMassPushInterface;
import jaohar.com.jaohar.models.AllNotification;
import jaohar.com.jaohar.models.NotificationModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class MassPushActivity extends BaseActivity {
    Activity mActivity = MassPushActivity.this;
    String TAG = MassPushActivity.this.getClass().getSimpleName();
    //WIDGETS
    ImageView imgBack,imgRight;
    LinearLayout llLeftLL;
    RelativeLayout imgRightLL;
    TextView txtCenter;
//    CustomNestedScrollView parentSV;
    RecyclerView massPushRV;
    SwipeRefreshLayout swipeToRefresh;
    boolean isSwipeRefresh = false;
    private int page_no = 1;
    MashPushAdapter mAdapter;
//    ArrayList<MassPushModel> mArrayList = new ArrayList<MassPushModel>();
    ArrayList<AllNotification> mArrayList = new ArrayList<AllNotification>();
//    ArrayList<MassPushModel> loadMoreArrayList = new ArrayList<MassPushModel>();
    ArrayList<AllNotification> loadMoreArrayList = new ArrayList<AllNotification>();

    EditMassPushInterface mEditMassPush = new EditMassPushInterface() {
        @Override
        public void editMassPush(String strNotificationID) {
            Intent mIntent = new Intent(mActivity, EditMassPushActivity.class);
            mIntent.putExtra("noficationID", strNotificationID);
            startActivity(mIntent);
            overridePendingTransitionEnter();
        }
    };

    DeleteMassPush mDeteeMassPUSH = new DeleteMassPush() {
        @Override
        public void deleteMassPush(String strNotificationID) {
            if (Utilities.isNetworkAvailable(mActivity) == false) {
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
            } else {
                showAlertDeleteDialog(mActivity,strNotificationID);

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_mass_push);
    }
    @Override
    protected void setViewsIDs() {
          /*SET UP TOOLBAR*/
        imgRight = (ImageView) findViewById(R.id.imgRight);
        imgBack = (ImageView) findViewById(R.id.imgBack);

        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        imgRightLL.setVisibility(View.VISIBLE);
        txtCenter = (TextView) findViewById(R.id.txtCenter);
        massPushRV = (RecyclerView) findViewById(R.id.massPushRV);
//        parentSV = (CustomNestedScrollView) findViewById(R.id.parentSV);

        /* set top bar views data */
        txtCenter.setText(getString(R.string.all_notifications));
        imgBack.setImageResource(R.drawable.back);
        imgRight.setImageResource(R.drawable.add_icon);

        swipeToRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                mArrayList.clear();
                loadMoreArrayList.clear();
                page_no = 1;
                if (!Utilities.isNetworkAvailable(mActivity)) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    executeAPI();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            //*Execute Vesseles API*//*
            mArrayList.clear();
            loadMoreArrayList.clear();
            page_no = 1;
            executeAPI();
        }
    }

    @Override
    protected void setClickListner() {

        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, AddMassPushActivity.class));
                overridePendingTransitionEnter();
            }
        });
//      AddMassPushActivity
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransitionExit();
    }

//    public void executeAPI() {
//        mArrayList.clear();
////       +"&page_no="+page_no
////        http://root.jaohar.net/Staging/JaoharWebServicesNew/GetAllNotifications.php?user_id=158&page_no=1
//        String strUrl = JaoharConstants.Get_All_Notifications + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&page_no=" + page_no;
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                if (isSwipeRefresh) {
//                    isSwipeRefresh = false;
//                    swipeToRefresh.setRefreshing(false);
//                }
//                Log.e(TAG, "*****Response****" + response);
//              parseResponse(response);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    public void executeAPI() {
        mArrayList.clear();
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<NotificationModel> call1 = mApiInterface.getAllNotificationsRequest(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""), String.valueOf(page_no));
        call1.enqueue(new Callback<NotificationModel>() {
            @Override
            public void onResponse(Call<NotificationModel> call, retrofit2.Response<NotificationModel> response) {
                AlertDialogManager.hideProgressDialog();
                NotificationModel mModel = response.body();
                    AlertDialogManager.hideProgressDialog();
                    if (isSwipeRefresh) {
                        isSwipeRefresh = false;
                        swipeToRefresh.setRefreshing(false);
                    }
                loadMoreArrayList.clear();
                if (mModel.getStatus().equals("1")) {
              if (page_no == 1) {
                            mArrayList=mModel.getData().getAllNotifications();
                        } else if (page_no > 1) {
                            loadMoreArrayList=mModel.getData().getAllNotifications();
                        }

                    if (loadMoreArrayList.size() > 0) {
                        mArrayList.addAll(loadMoreArrayList);
                    }
           /*SetAdapter*/
            setAdapter();
            } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }}

            @Override
            public void onFailure(Call<NotificationModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }



//    public void executeAPI() {
//        mArrayList.clear();
////       +"&page_no="+page_no
////        http://root.jaohar.net/Staging/JaoharWebServicesNew/GetAllNotifications.php?user_id=158&page_no=1
//        String strUrl = JaoharConstants.Get_All_Notifications + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&page_no=" + page_no;
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                if (isSwipeRefresh) {
//                    isSwipeRefresh = false;
//                    swipeToRefresh.setRefreshing(false);
//                }
//                Log.e(TAG, "*****Response****" + response);
//              parseResponse(response);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

//    private void parseResponse(String response) {
//        loadMoreArrayList.clear();
//        try {
//            JSONObject mJsonObject1 = new JSONObject(response);
//            if (mJsonObject1.getString("status").equals("1")) {
//                if (!mJsonObject1.isNull("data")) {
//                    JSONObject mDataObject = mJsonObject1.getJSONObject("data");
//                    JSONArray mJsonArray = mDataObject.getJSONArray("all_notifications");
//                    for (int i = 0; i < mJsonArray.length(); i++) {
//                        JSONObject mJsonObject11 = mJsonArray.getJSONObject(i);
//                        MassPushModel massPushModel = new MassPushModel();
//                        if (!mJsonObject11.isNull("notification_id")) {
//                            massPushModel.setNotification_id(mJsonObject11.getString("notification_id"));
//                        }
//                        if (!mJsonObject11.isNull("notification_type")) {
//                            massPushModel.setNotification_type(mJsonObject11.getString("notification_type"));
//                        }
//                        if (!mJsonObject11.isNull("language")) {
//                            massPushModel.setLanguage(mJsonObject11.getString("language"));
//                        }
//                        if (!mJsonObject11.isNull("message")) {
//                            massPushModel.setMessage(mJsonObject11.getString("message"));
//                        }
//                        if (!mJsonObject11.isNull("user_type")) {
//                            massPushModel.setUser_type(mJsonObject11.getString("user_type"));
//                        }
//                        if (!mJsonObject11.isNull("viewed")) {
//                            massPushModel.setViewed(mJsonObject11.getString("viewed"));
//                        }
//                        if (!mJsonObject11.isNull("creation_date")) {
//                            massPushModel.setCreation_date(mJsonObject11.getString("creation_date"));
//                        }
//                        if (!mJsonObject11.isNull("created_by")) {
//                            massPushModel.setCreated_by(mJsonObject11.getString("created_by"));
//                        }
//                        if (page_no == 1) {
//                            mArrayList.add(massPushModel);
//                        } else if (page_no > 1) {
//                            loadMoreArrayList.add(massPushModel);
//                        }
//                    }
//                    if (loadMoreArrayList.size() > 0) {
//                        mArrayList.addAll(loadMoreArrayList);
//                    }
//                }
//            }
////            } else if (mJsonObject1.getString("status").equals("100")) {
////                AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject1.getString("message"));
////            } else {
////                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject1.getString("message"));
////            }
//
//           /*SetAdapter*/
//            setAdapter();
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public void deletePush(String strNotificationID) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteMassPushRequest(strNotificationID,JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus()==1) {
                    showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }


//    private void deletePush(String strNotificationID){
////        http://root.jaohar.net/Staging/JaoharWebServicesNew/DeleteMassPush.php//notification_id
//        String strUrl = JaoharConstants.Delete_Mass_Push + "?notification_id=" + strNotificationID + "&user_id="+JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "");
//        Log.e(TAG, "***URL***" + strUrl);
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "*****Response****" + response);
//                try {
//                    JSONObject mJsonObject1 = new JSONObject(response);
//                    if (mJsonObject1.getString("status").equals("1")) {
//                        showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject1.getString("message"));
//                    }else {
//                      AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject1.getString("message"));
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    private void setAdapter() {
        Log.e(TAG, "VesselsAdapter: " + mArrayList);
        massPushRV.setNestedScrollingEnabled(false);
        massPushRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mAdapter = new MashPushAdapter(mActivity, mArrayList,mDeteeMassPUSH,mEditMassPush);
        massPushRV.setAdapter(mAdapter);
    }


    public  void showAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
                mArrayList.clear();
                loadMoreArrayList.clear();
                executeAPI();
            }
        });
        alertDialog.show();
    }

    public  void showAlertDeleteDialog(Activity mActivity, final String strNotificationID) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_delete_confirmation);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtConfirm = (TextView) alertDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) alertDialog.findViewById(R.id.txtCacel);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);

        txtMessage.setText("Are you sure want to delete mass push?");

        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                deletePush(strNotificationID);
            }
        });
        alertDialog.show();
    }
}
