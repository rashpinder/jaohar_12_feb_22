package jaohar.com.jaohar.activities;

import static jaohar.com.jaohar.activities.AddCompanyActivity.showAlerDialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.SelectedEmailShowListAdapter;
import jaohar.com.jaohar.adapters.ShowListEmailAdapter;
import jaohar.com.jaohar.interfaces.SelectEmailListItem;
import jaohar.com.jaohar.interfaces.SelectedAndUnselectedEmailListInterFace;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class SendingMailActivity extends BaseActivity {
    private ImageView clossRL;
    private RecyclerView mRecyclerViewShowLIST, mRecyclerView;
    private LinearLayout listShowLL;
    private EditText editToET, editCCET, editBCCET, editSubjectET;
    private Button btnSend, btnCancel;
    private String strRecordID, strSubject;
    private final Activity mActivity = SendingMailActivity.this;
    String TAG = SendingMailActivity.this.getClass().getSimpleName();
    ArrayList<String> mRecordIDStr = new ArrayList<>();
    ArrayList<String> filteredList = new ArrayList<>();
    ArrayList<String> mSelectedListArray = new ArrayList<>();
    static boolean isAllVessal = false;
    ShowListEmailAdapter mShowListAdapter;
    SelectedEmailShowListAdapter mShowSelectedListAdapter;

    SelectEmailListItem mSelectedItem = new SelectEmailListItem() {
        @Override
        public void mSelectEmailListItem(String strEmail, String cc) {
            editToET.setText("");
            editCCET.setText("");
            editBCCET.setText("");
            listShowLL.setVisibility(View.GONE);
            mSelectedListArray.add(strEmail);
            setUpSelectedItems();
        }
    };

    SelectedAndUnselectedEmailListInterFace selectedAndUnselectedEmailListInterFace = new SelectedAndUnselectedEmailListInterFace() {
        @Override
        public void mSelectedEmail(String strEmail,String cc) {
            if (mSelectedListArray.size() > 0) {
                mSelectedListArray.remove(strEmail);
                setUpSelectedItems();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sending_mail);
        if (getIntent() != null) {
            if (getIntent().getStringExtra("recordID") != null) {
                strRecordID = getIntent().getStringExtra("recordID");
                strSubject = getIntent().getStringExtra("vessalName");
                isAllVessal = false;
            }
            if (getIntent().getStringArrayListExtra("recordIDArray") != null) {
                mRecordIDStr.clear();
                strSubject = getIntent().getStringExtra("vessalName1");
                mRecordIDStr = getIntent().getStringArrayListExtra("recordIDArray");
                Log.e(TAG, "ArrayLIST_DATA============= " + mRecordIDStr);
                isAllVessal = true;
            }
        }
    }

    @Override
    protected void setViewsIDs() {
        listShowLL = findViewById(R.id.listShowLL);
        mRecyclerView = findViewById(R.id.mRecyclerView);
        mRecyclerViewShowLIST = findViewById(R.id.mRecyclerViewShowLIST);
        clossRL = findViewById(R.id.clossRL);
        editToET = findViewById(R.id.editToET);
        editCCET = findViewById(R.id.editCCET);
        editBCCET = findViewById(R.id.editBCCET);
        editSubjectET = findViewById(R.id.editSubjectET);
        btnCancel = findViewById(R.id.btnCancel);
        btnSend = findViewById(R.id.btnSend);
        editSubjectET.setText(strSubject);
    }

    @Override
    protected void setClickListner() {
        clossRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        editToET.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                if (!query.toString().equals("")) {

                    listShowLL.setVisibility(View.VISIBLE);
                    query = query.toString().toLowerCase();
                    filteredList.clear();
                    for (int i = 0; i < JaoharConstants.mEmailsArrayLIST.size(); i++) {
                        Log.e("test", "onTextChanged: " + JaoharConstants.mEmailsArrayLIST.get(i));
                        final String text = JaoharConstants.mEmailsArrayLIST.get(i);
                        if (text.contains(query)) {
                            filteredList.add(JaoharConstants.mEmailsArrayLIST.get(i));
                        }
                    }
                    setShowListAdapter(filteredList);
                } else {
                    listShowLL.setVisibility(View.GONE);
                }
            }
        });

        editCCET.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                if (!query.toString().equals("")) {

                    listShowLL.setVisibility(View.VISIBLE);
                    query = query.toString().toLowerCase();
                    filteredList.clear();
                    for (int i = 0; i < JaoharConstants.mEmailsArrayLIST.size(); i++) {
                        Log.e("test", "onTextChanged: " + JaoharConstants.mEmailsArrayLIST.get(i));
                        final String text = JaoharConstants.mEmailsArrayLIST.get(i);
                        if (text.contains(query)) {
                            filteredList.add(JaoharConstants.mEmailsArrayLIST.get(i));
                        }
                    }
                    setShowListAdapter(filteredList);
                } else {
                    listShowLL.setVisibility(View.GONE);
                }
            }
        });

        editBCCET.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                if (!query.toString().equals("")) {

                    listShowLL.setVisibility(View.VISIBLE);
                    query = query.toString().toLowerCase();
                    filteredList.clear();
                    for (int i = 0; i < JaoharConstants.mEmailsArrayLIST.size(); i++) {
                        Log.e("test", "onTextChanged: " + JaoharConstants.mEmailsArrayLIST.get(i));
                        final String text = JaoharConstants.mEmailsArrayLIST.get(i);
                        if (text.contains(query)) {
                            filteredList.add(JaoharConstants.mEmailsArrayLIST.get(i));
                        }
                    }
                    setShowListAdapter(filteredList);
                } else {
                    listShowLL.setVisibility(View.GONE);
                }
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editToET.getText().toString().equals("")) {
                    if (mSelectedListArray.size() > 0) {
                        if (editSubjectET.getText().toString().equals("")) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_subject));
                        } else {
                            if (!Utilities.isNetworkAvailable(mActivity)) {
                                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                            } else {
                                sendEamilAPI(editToET.getText().toString(), editSubjectET.getText().toString(), strRecordID, editCCET.getText().toString(), editBCCET.getText().toString());
                            }
                        }
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                    }
                } else {
                    if (!Utilities.isValidEmaillId(editToET.getText().toString())) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                    } else if (editSubjectET.getText().toString().equals("")) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_subject));
                    } else {
                        if (!Utilities.isNetworkAvailable(mActivity)) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                        } else {
                            sendEamilAPI(editToET.getText().toString(), editSubjectET.getText().toString(), strRecordID, editCCET.getText().toString(), editBCCET.getText().toString());
                        }
                    }

                }

            }
        });


    }

    public void setShowListAdapter(ArrayList<String> mFilterArray) {
        Log.e(TAG, "FilterArray: " + mFilterArray.size());
        mRecyclerViewShowLIST.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerViewShowLIST.setNestedScrollingEnabled(false);
        mRecyclerViewShowLIST.setHasFixedSize(false);
        mRecyclerViewShowLIST.setLayoutManager(layoutManager);
        mShowListAdapter = new ShowListEmailAdapter(mActivity, mFilterArray, mSelectedItem, "cc");
        mRecyclerViewShowLIST.setAdapter(mShowListAdapter);
    }

    private void setUpSelectedItems() {
        Log.e(TAG, "SelectedArray: " + mSelectedListArray.size());
        mRecyclerView.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(layoutManager);
        mShowSelectedListAdapter = new SelectedEmailShowListAdapter(mActivity, mSelectedListArray, selectedAndUnselectedEmailListInterFace,"to");
        mRecyclerView.setAdapter(mShowSelectedListAdapter);
    }

    private void sendEamilAPI(final String strTO, final String strSubject, final String strRecordID, final String strCC, final String strBCC) {
        AlertDialogManager.showProgressDialog(mActivity);
        if (isAllVessal) {
            executeMultiVesselRequest(strTO, strSubject, strRecordID);
        } else {
            executeSingleVesselRequest(strTO, strSubject, strRecordID, strCC, strBCC);
        }
    }


    private void executeSingleVesselRequest(final String strTO, final String strSubject, final String strRecordID, final String strCC, final String strBCC) {
        String to_emails;
        if (mSelectedListArray.size() > 0) {
            to_emails = String.valueOf(mSelectedListArray);
        } else {
            to_emails = "";
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getSingleVesselsRequest(strTO, strSubject, strRecordID, to_emails, strCC, strBCC).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showAlerDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void executeMultiVesselRequest(final String strTO, final String strSubject, final String strRecordID) {
        String to_emails;
        if (mSelectedListArray.size() > 0) {
            to_emails = String.valueOf(mSelectedListArray);
        } else {
            to_emails = "";
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getMultipleVesselsRequest(strTO, strSubject, strRecordID, to_emails).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showAlerDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

//    private void sendEamilAPI(final String strTO, final String strSubject, final String strRecordID) {
//        String strUrl;
//        if (isAllVessal) {
//            strUrl = JaoharConstants.Multi_Vessals_URL;
//        } else {
//            strUrl = JaoharConstants.Single_Vessals_URL;
//        }
//
//        AlertDialogManager.showProgressDialog(mActivity);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "*************" + response.toString());
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    if (jsonObject.getString("status").equals("1")) {
//                        showAlertDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    } else if (jsonObject.getString("status").equals("100")) {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<>();
//                params.put("to_email", strTO);
//                if(mSelectedListArray.size()>0){
//                    params.put("to_emails", String.valueOf(mSelectedListArray));
//                }else {
//                    params.put("to_emails","");
//                }
//
//                params.put("subject", strSubject);
//                if (isAllVessal) {
//                    Log.e(TAG, "ArrayLIST_DATA============= " + String.valueOf(mRecordIDStr));
//                    params.put("vessel_ids", String.valueOf(mRecordIDStr));
//                } else {
//                    params.put("vessel_id", strRecordID);
//                }
//
//                return params;
//            }
//
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//
//    }


    public void showAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                onBackPressed();
            }
        });
        alertDialog.show();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
