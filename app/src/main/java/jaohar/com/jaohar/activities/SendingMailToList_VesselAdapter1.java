package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.MailingListModel;
import jaohar.com.jaohar.interfaces.SelectingListNameInterface;
import jaohar.com.jaohar.models.AllMailList;

public class SendingMailToList_VesselAdapter1 extends RecyclerView.Adapter<SendingMailToList_VesselAdapter1.ViewHolder> {
    private Activity mActivity;
    private ArrayList<AllMailList> modelArrayList;
    private SelectingListNameInterface mInterface;


    public SendingMailToList_VesselAdapter1(Activity mActivity, ArrayList<AllMailList> modelArrayList, SelectingListNameInterface mInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mInterface = mInterface;

    }

    @Override
    public SendingMailToList_VesselAdapter1.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_mail_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final SendingMailToList_VesselAdapter1.ViewHolder holder, final int position) {
        holder.serialNOTV.setText("");
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView serialNOTV, listNameTV;
        public LinearLayout mainLL;


        ViewHolder(View itemView) {
            super(itemView);
            mainLL = itemView.findViewById(R.id.mainLL);
            listNameTV = itemView.findViewById(R.id.listNameTV);
            serialNOTV = itemView.findViewById(R.id.serialNOTV);


        }
    }
}
