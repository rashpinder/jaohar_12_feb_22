package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.adapters.ShowItemInvoiceAdapter;
import jaohar.com.jaohar.beans.DiscountModel;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;
import jaohar.com.jaohar.interfaces.DeleteInvoiceItemDiscountInterface;
import jaohar.com.jaohar.interfaces.EditInvoiceItemSubtractDiscount;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.Utilities;

public class ShowEditInvoiceItemAciivity extends AppCompatActivity {
    private static Double strAddTotal = 0.0;
    private static Double strSubtractTotal = 0.0;
    Activity mActivity = ShowEditInvoiceItemAciivity.this;
    String TAG = ShowEditInvoiceItemAciivity.this.getClass().getSimpleName();
    LinearLayout llLeftLL;
    RelativeLayout imgRightLL;
    RecyclerView mRecyclerView;
    Button btnUpdate;
    TextView itemTV, quantityTV, descriptionTV, priceTV, txtEdit, txtDelete;
    ShowItemInvoiceAdapter mAdapter;
    InvoiceAddItemModel mModel = new InvoiceAddItemModel();
    ArrayList<InvoiceAddItemModel> mArrayList = new ArrayList();
    ArrayList<DiscountModel> mArrayListDiscount = new ArrayList<DiscountModel>();
    ArrayList<InvoiceAddItemModel> mArrayListPmodel = new ArrayList();
    int mPosition, originalPositioon;
    DiscountModel discountModel;
    Double strTolal;
    boolean type = false;
    static double mTotal = 0;
    static double mTotalASDisc = 0;

    DeleteInvoiceItemDiscountInterface mDeleteItemdiscount = new DeleteInvoiceItemDiscountInterface() {
        @Override
        public void deleteInvoiceItemDiscountInterface(ArrayList<DiscountModel> DiscounModel, int positionDiscount) {
//            mArrayListDiscount.remove(mDiscounModel);
//            mModel.setmDiscountModelArrayList(mArrayListDiscount);
//            if(mModel.getmDiscountModelArrayList().size()>0){
//                setAdapter();
//            }
            Log.e(TAG, "**ArraySize221**" + DiscounModel.size());
            mArrayListDiscount.remove(DiscounModel);
            Log.e(TAG, "**ArraySize22**" + mArrayListDiscount.size());
//           if(mArrayListDiscount.size()>0){
//               mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
//               mAdapter = new ShowItemInvoiceAdapter(mActivity, mArrayListDiscount, mEditInvoiceItemData,mDeleteItemdiscount);
//               mRecyclerView.setAdapter(mAdapter);
//           }
            Log.e(TAG, "**ArraySize223**" + DiscounModel.size());
            if (DiscounModel.size() > 0) {
                for (int i = 0; i < DiscounModel.size(); i++) {

                    DiscountModel mDiscountModel = DiscounModel.get(i);

                    if (mDiscountModel.getType().equals("ADD")) {
                        mTotal = Double.parseDouble(mDiscountModel.getADD_unitPrice());
                        mTotalASDisc = mTotalASDisc + Double.parseDouble(mDiscountModel.getStrtotalPercentValue());

                    } else if (mDiscountModel.getType().equals("ADD_VALUE")) {
                        mTotal = Double.parseDouble(mDiscountModel.getADD_unitPrice());
                        mTotalASDisc = mTotalASDisc + Double.parseDouble(mDiscountModel.getStrtotalPercentValue());

                    } else if (mDiscountModel.getType().equals("SUBTRACT")) {
                        mTotal = Double.parseDouble(mDiscountModel.getSubtract_unitPrice());
                        mTotalASDisc = mTotalASDisc - Double.parseDouble(mDiscountModel.getStrtotalPercentValue());
                    } else if (mDiscountModel.getType().equals("SUBTRACT_VALUE")) {
                        mTotal = Double.parseDouble(mDiscountModel.getSubtract_unitPrice());
                        mTotalASDisc = mTotalASDisc - Double.parseDouble(mDiscountModel.getStrtotalPercentValue());
                    }
//                        mFinalTotal =  mTotal;
//                        mTotalASDisc= mTotalASDisc+mFinalTotal;
                    Log.e(TAG, "***Final Total***" + mTotalASDisc);
                }
                mModel.setTotalAmount(String.valueOf(Utilities.convertEvalueToNormal(mTotal + mTotalASDisc)));
                mTotalASDisc = 0;
            } else {
                mModel.setTotalAmount(mModel.getStrTotalAmountUnit());
            }
        }
    };

    EditInvoiceItemSubtractDiscount mEditInvoiceItemData = new EditInvoiceItemSubtractDiscount() {
        @Override
        public void editInvoiceItemSubtractDiscount(DiscountModel mModel, int position, String strType) {
            JaoharConstants.ISupdate = false;
            if (strType.equals("ADD")) {
                originalPositioon = position;
                Intent mIntent = new Intent(mActivity, EditADDDiscountActivity.class);
                mIntent.putExtra("Model", mModel);
                System.out.print("Item1223" + position);
                mIntent.putExtra("Position", position);
                startActivityForResult(mIntent, 123);
            } else if (strType.equals("ADD_VALUE")) {
                originalPositioon = position;
                Intent mIntent = new Intent(mActivity, EditAdd_ValueDiscountActivity.class);
                mIntent.putExtra("Model", mModel);
                System.out.print("Item1223" + position);
                mIntent.putExtra("Position", position);
                startActivityForResult(mIntent, 123);
            } else if (strType.equals("SUBTRACT")) {
                Intent mIntent = new Intent(mActivity, EditSubtractActivity.class);
                mIntent.putExtra("Model", mModel);
                System.out.print("Item1223" + position);
                mIntent.putExtra("Position", position);
                startActivityForResult(mIntent, 123);
            } else if (strType.equals("SUBTRACT_VALUE")) {
                Intent mIntent = new Intent(mActivity, EditSubtract_ValueDiscountActivity.class);
                mIntent.putExtra("Model", mModel);
                System.out.print("Item1223" + position);
                mIntent.putExtra("Position", position);
                startActivityForResult(mIntent, 123);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_edit_invoice_item_aciivity);
        setViewsIDs();
        setClickListner();
    }


    protected void setViewsIDs() {
        llLeftLL = (LinearLayout) findViewById(R.id.llLeftLL);
        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        itemTV = (TextView) findViewById(R.id.itemTV);
        quantityTV = (TextView) findViewById(R.id.quantityTV);
        descriptionTV = (TextView) findViewById(R.id.descriptionTV);
        priceTV = (TextView) findViewById(R.id.priceTV);
//        txtEdit = (TextView) findViewById(R.id.txtEdit);
//        txtDelete = (TextView) findViewById(R.id.txtDelete);
//        txtDelete.setVisibility(View.GONE);
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        btnUpdate = (Button) findViewById(R.id.btnUpdate);
//        mArrayList.clear();
        if (getIntent() != null) {
            JaoharConstants.ISupdate = false;
            mModel = (InvoiceAddItemModel) getIntent().getSerializableExtra("Model");
            mPosition = getIntent().getIntExtra("Position", 0);
            originalPositioon = mPosition;
            mArrayList.add(mModel);
            mArrayListDiscount = mModel.getmDiscountModelArrayList();
            String strItem = mModel.getItem();
            String strDescription = mModel.getDescription();
            String strQuantity = String.valueOf(mModel.getQuantity());
            String strPrice = String.valueOf(mModel.getUnitprice());
            itemTV.setText(strItem);
            quantityTV.setText(strQuantity);
            descriptionTV.setText(strDescription);
            priceTV.setText(Utilities.convertEvalueToNormal(Double.parseDouble(strPrice)));
        }
        setAdapter();
    }


    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, AddDiscontActivity.class);
                mIntent.putExtra("Model", mModel);
                System.out.print("Item1223" + mPosition);
                mIntent.putExtra("Position", mPosition);
                startActivityForResult(mIntent, 909);
            }
        });

//        txtEdit.setOnClickListener(new View.OnClickListener() {
//                                       @Override
//                                       public void onClick(View v) {
//                                           Intent mIntent = new Intent(mActivity, EditInvoiceItemActivity.class);
//                                           mIntent.putExtra("Model", mModel);
//                                           System.out.print("Item1223" + mModel);
//                                           mIntent.putExtra("Position", mModel);
//                                           startActivityForResult(mIntent, 101);
//                                       }
//                                   }
//        );

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                String strA = mModel.getDescription();
                returnIntent.putExtra("Model", mModel);
                returnIntent.putExtra("Position", mPosition);
                setResult(111, returnIntent);
                finish();
            }
        });
    }

    public void setAdapter() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mAdapter = new ShowItemInvoiceAdapter(mActivity, mModel.getmDiscountModelArrayList(), mEditInvoiceItemData, mDeleteItemdiscount);
        mRecyclerView.setAdapter(mAdapter);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null && data.getSerializableExtra("pModel") != null) {

            if (requestCode == 909) {
                mArrayListDiscount = (ArrayList<DiscountModel>) data.getSerializableExtra("pModel");
                mPosition = data.getIntExtra("Position", 0);
                //mModel.setTotalAmount((mTotalASDisc));
//                mModel.setTotalAmount((strTolal + mModel.getUnitprice()));
                mModel.setmDiscountModelArrayList(mArrayListDiscount);
//                mModel.setmDiscountModelArrayList(mArrayListDiscount);
//                mArrayList.set(originalPositioon, mModel);
                String strItem = mModel.getItem();
                String strDescription = mModel.getDescription();
                String strQuantity = String.valueOf(mModel.getQuantity());
                String strPrice = String.valueOf(mModel.getUnitprice());
                itemTV.setText(strItem);
                quantityTV.setText(strQuantity);
                descriptionTV.setText(strDescription);

                priceTV.setText(Utilities.convertEvalueToNormal(Double.parseDouble(strPrice)));
                double mFinalTotal = 0;
                if (mModel.getmDiscountModelArrayList().size() > 0) {
                    for (int i = 0; i < mModel.getmDiscountModelArrayList().size(); i++) {
                        DiscountModel mDiscountModel = mModel.getmDiscountModelArrayList().get(i);
                        if (mDiscountModel.getType().equals("ADD")) {
                            mTotal = Double.parseDouble(mDiscountModel.getADD_unitPrice());
                            mTotalASDisc = mTotalASDisc + Double.parseDouble(mDiscountModel.getStrtotalPercentValue());

                        } else if (mDiscountModel.getType().equals("ADD_VALUE")) {
                            mTotal = Double.parseDouble(mDiscountModel.getADD_unitPrice());
                            mTotalASDisc = mTotalASDisc + Double.parseDouble(mDiscountModel.getStrtotalPercentValue());
                        } else if (mDiscountModel.getType().equals("SUBTRACT")) {
                            mTotal = Double.parseDouble(mDiscountModel.getSubtract_unitPrice());
                            mTotalASDisc = mTotalASDisc - Double.parseDouble(mDiscountModel.getStrtotalPercentValue());
                        } else if (mDiscountModel.getType().equals("SUBTRACT_VALUE")) {
                            mTotal = Double.parseDouble(mDiscountModel.getSubtract_unitPrice());
                            mTotalASDisc = mTotalASDisc - Double.parseDouble(mDiscountModel.getStrtotalPercentValue());
                        }
//                        mFinalTotal =  mTotal;
//                        mTotalASDisc= mTotalASDisc+mFinalTotal;
                        Log.e(TAG, "***Final Total***" + mTotalASDisc);
                    }
                    mModel.setTotalAmount(String.valueOf(Utilities.convertEvalueToNormal(mTotal + mTotalASDisc)));
                    mTotalASDisc = 0;
                } else {
                    mModel.setTotalAmount(mModel.getStrTotalAmountUnit());
                }
                setAdapter();
            } else if (requestCode == 101) {
                discountModel = new DiscountModel();
                mModel = (InvoiceAddItemModel) data.getSerializableExtra("pModel");
                mPosition = data.getIntExtra("Position", 0);
                Log.e(TAG, "TOTAL1" + (mModel.getmDiscountModelArrayList().size()));
//              mModel.setTotalAmount((mModel.getUnitprice()));
                double total = mModel.getQuantity() * Double.parseDouble(mModel.getUnitprice());
                mModel.setStrTotalunitPrice(String.valueOf(total));
                mModel.setStrTotalAmountUnit(String.valueOf(total));
                String strItem = mModel.getItem();
                String strDescription = mModel.getDescription();
                String strQuantity = String.valueOf(mModel.getQuantity());
                String strPrice = String.valueOf(mModel.getUnitprice());
                itemTV.setText(strItem);
                quantityTV.setText(strQuantity);
                descriptionTV.setText(strDescription);
                priceTV.setText(Utilities.convertEvalueToNormal(Double.parseDouble(strPrice)));
                discountModel.setADD_quantity(strQuantity);
                discountModel.setStrTotalEditAmount(String.valueOf(mModel.getUnitprice()));
                mArrayListDiscount.clear();
                JaoharConstants.ISupdate = true;
                Log.e(TAG, "ARRAYLIST" + mModel.getmDiscountModelArrayList().size());
                for (int i = 0; i < mModel.getmDiscountModelArrayList().size(); i++) {
                    discountModel = new DiscountModel();
                    String strType = mModel.getmDiscountModelArrayList().get(i).getType();
                    if (strType.equals("ADD")) {
                        double totalPrice;
                        double basePrice = 0;
                        double sum;
                        totalPrice = Double.parseDouble(mModel.getStrTotalunitPrice());
                        String strvalue = mModel.getmDiscountModelArrayList().get(i).getAdd_Value();
                        try {
                            basePrice = Double.parseDouble(mModel.getStrTotalunitPrice());//   Integer.parseInt()
//                            totalPercent= ((basePrice * Double.parseDouble(str)) / 100);
//                            sum = basePrice + totalPercent;
//                            float percentValue = (Float.parseFloat(strvalue)) / 100;
                            double totalPercent;
                            totalPercent = ((basePrice * Double.parseDouble(strvalue)) / 100);
                            sum = totalPrice + totalPercent;
                            String strTol = String.valueOf(sum);
                            discountModel.setAdd_description(mModel.getmDiscountModelArrayList().get(i).getAdd_description());
                            discountModel.setADD_items(mModel.getmDiscountModelArrayList().get(i).getADD_items());
                            discountModel.setADD_quantity(String.valueOf(mModel.getQuantity()));
                            discountModel.setADD_unitPrice(String.valueOf(totalPrice));
                            discountModel.setAdd_Value(mModel.getmDiscountModelArrayList().get(i).getAdd_Value());
                            discountModel.setStrtotalPercentValue(String.valueOf(Utilities.getRoundOff2Decimal(totalPercent)));
                            discountModel.setType("ADD");
                            discountModel.setAddAndSubtractTotal(strTol);
                            mArrayListDiscount.add(discountModel);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    } else if (strType.equals("ADD_VALUE")) {
                        double totalPrice;
                        double basePrice = 0;
                        double sum;
                        totalPrice = Double.parseDouble(mModel.getStrTotalunitPrice());
                        String strvalue = mModel.getmDiscountModelArrayList().get(i).getAdd_Value();
                        try {
                            basePrice = Double.parseDouble(mModel.getStrTotalunitPrice());//   Integer.parseInt()
                            float percentValue = (Float.parseFloat(strvalue)) / 100;
                            double totalPercent;
                            totalPercent = Double.parseDouble(strvalue);
                            sum = totalPrice + totalPercent;
                            String strTol = String.valueOf(sum);
                            discountModel.setAdd_description(mModel.getmDiscountModelArrayList().get(i).getAdd_description());
                            discountModel.setADD_items(mModel.getmDiscountModelArrayList().get(i).getADD_items());
                            discountModel.setADD_quantity(String.valueOf(mModel.getQuantity()));
                            discountModel.setADD_unitPrice(String.valueOf(totalPrice));
                            discountModel.setAdd_Value(mModel.getmDiscountModelArrayList().get(i).getAdd_Value());
                            discountModel.setStrtotalPercentValue(String.valueOf(Utilities.getRoundOff2Decimal(totalPercent)));
                            discountModel.setType("ADD_VALUE");
                            discountModel.setAddAndSubtractTotal(strTol);
                            mArrayListDiscount.add(discountModel);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    } else if (strType.equals("SUBTRACT")) {
                        double totalPrice;
                        double basePrice = 0;
                        double sum;
                        totalPrice = Double.parseDouble(mModel.getStrTotalunitPrice());
                        String strvalue = mModel.getmDiscountModelArrayList().get(i).getSubtract_Value();
                        try {
                            basePrice = Double.parseDouble(mModel.getStrTotalunitPrice());//   Integer.parseInt()
                            float percentValue = (Float.parseFloat(strvalue)) / 100;
                            double totalPercent;


//                            totalPercent = ((basePrice * Double.parseDouble(mModel.getStrTotalunitPrice())) / 100);
                            totalPercent = ((basePrice * Double.parseDouble(strvalue)) / 100);
//                            totalPercent = basePrice * percentValue;
                            sum = totalPrice - totalPercent;
                            String strTol = String.valueOf(sum);
                            discountModel.setSubtract_description(mModel.getmDiscountModelArrayList().get(i).getSubtract_description());
                            discountModel.setSubtract_items(mModel.getmDiscountModelArrayList().get(i).getSubtract_items());
                            discountModel.setSubtract_quantity(String.valueOf(mModel.getQuantity()));
                            discountModel.setSubtract_unitPrice(String.valueOf(totalPrice));
                            discountModel.setSubtract_Value(mModel.getmDiscountModelArrayList().get(i).getSubtract_Value());
                            discountModel.setStrtotalPercentValue(String.valueOf(Utilities.getRoundOff2Decimal(totalPercent)));
                            discountModel.setType("SUBTRACT");
                            discountModel.setAddAndSubtractTotal(strTol);
                            mArrayListDiscount.add(discountModel);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    } else if (strType.equals("SUBTRACT_VALUE")) {
                        double totalPrice;
                        double basePrice = 0;
                        double sum;
                        totalPrice = Double.parseDouble(mModel.getStrTotalunitPrice());
                        String strvalue = mModel.getmDiscountModelArrayList().get(i).getSubtract_Value();
                        try {
                            basePrice = Double.parseDouble(mModel.getStrTotalunitPrice());//   Integer.parseInt()
                            float percentValue = (Float.parseFloat(strvalue)) / 100;
                            double totalPercent;
                            totalPercent = Double.parseDouble(strvalue);
                            sum = totalPrice - totalPercent;
                            String strTol = String.valueOf(sum);
                            discountModel.setSubtract_description(mModel.getmDiscountModelArrayList().get(i).getSubtract_description());
                            discountModel.setSubtract_items(mModel.getmDiscountModelArrayList().get(i).getSubtract_items());
                            discountModel.setSubtract_quantity(String.valueOf(mModel.getQuantity()));
                            discountModel.setSubtract_unitPrice(String.valueOf(totalPrice));
                            discountModel.setSubtract_Value(mModel.getmDiscountModelArrayList().get(i).getSubtract_Value());
                            discountModel.setStrtotalPercentValue(String.valueOf(Utilities.getRoundOff2Decimal(totalPercent)));
                            discountModel.setType("SUBTRACT_VALUE");
                            discountModel.setAddAndSubtractTotal(strTol);
                            mArrayListDiscount.add(discountModel);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
                mModel.setmDiscountModelArrayList(mArrayListDiscount);
                setAdapter();
                if (mModel.getmDiscountModelArrayList().size() > 0) {
                    for (int i = 0; i < mModel.getmDiscountModelArrayList().size(); i++) {
                        DiscountModel mDiscountModel = mModel.getmDiscountModelArrayList().get(i);
                        if (mDiscountModel.getType().equals("ADD")) {
                            mTotal = Double.parseDouble(mDiscountModel.getADD_unitPrice());
                            String strTotal = mDiscountModel.getStrtotalPercentValue();
                            mTotalASDisc = mTotalASDisc + Double.parseDouble(strTotal);
                        } else if (mDiscountModel.getType().equals("ADD_VALUE")) {
                            mTotal = Double.parseDouble(mDiscountModel.getADD_unitPrice());
                            String strTotal = mDiscountModel.getStrtotalPercentValue();
                            mTotalASDisc = mTotalASDisc + Double.parseDouble(strTotal);
                        } else if (mDiscountModel.getType().equals("SUBTRACT")) {
                            mTotal = Double.parseDouble(mDiscountModel.getSubtract_unitPrice());
                            mTotalASDisc = mTotalASDisc - Double.parseDouble(mDiscountModel.getStrtotalPercentValue());
                        } else if (mDiscountModel.getType().equals("SUBTRACT_VALUE")) {
                            mTotal = Double.parseDouble(mDiscountModel.getSubtract_unitPrice());
                            mTotalASDisc = mTotalASDisc - Double.parseDouble(mDiscountModel.getStrtotalPercentValue());
                        }
//                        mFinalTotal =  mTotal;
//                        mTotalASDisc= mTotalASDisc+mFinalTotal;
                        Log.e(TAG, "***Final Total***" + mTotalASDisc);
                    }
                    mModel.setTotalAmount(String.valueOf(Utilities.getRoundOff2Decimal(mTotal + mTotalASDisc)));
                    mTotalASDisc = 0;
                } else {
                    mModel.setTotalAmount(mModel.getStrTotalAmountUnit());
                }


//                  mArrayTotalDiscount.clear();
//                  setAdapterTotalDiscount();
//            }


            } else if (requestCode == 123) {
                System.out.print("Size" + mArrayListDiscount.size());

                discountModel = (DiscountModel) data.getSerializableExtra("pModel");
                mPosition = data.getIntExtra("Position", 0);
//                mArrayListDiscount.add(discountModel);
                mArrayListDiscount.set(mPosition, discountModel);
                if (discountModel != null) {
//
                    Log.e(TAG, String.valueOf(mArrayListDiscount.size()));
                }


                mModel.setmDiscountModelArrayList(mArrayListDiscount);
                if (mModel.getmDiscountModelArrayList().size() > 0) {
                    for (int i = 0; i < mModel.getmDiscountModelArrayList().size(); i++) {

                        DiscountModel mDiscountModel = mModel.getmDiscountModelArrayList().get(i);

                        if (mDiscountModel.getType().equals("ADD")) {
                            mTotal = Double.parseDouble(mDiscountModel.getADD_unitPrice());
                            mTotalASDisc = mTotalASDisc + Double.parseDouble(mDiscountModel.getStrtotalPercentValue());

                        } else if (mDiscountModel.getType().equals("ADD_VALUE")) {
                            mTotal = Double.parseDouble(mDiscountModel.getADD_unitPrice());
                            mTotalASDisc = mTotalASDisc + Double.parseDouble(mDiscountModel.getStrtotalPercentValue());

                        } else if (mDiscountModel.getType().equals("SUBTRACT")) {
                            mTotal = Double.parseDouble(mDiscountModel.getSubtract_unitPrice());
                            mTotalASDisc = mTotalASDisc - Double.parseDouble(mDiscountModel.getStrtotalPercentValue());

                        } else if (mDiscountModel.getType().equals("SUBTRACT_VALUE")) {
                            mTotal = Double.parseDouble(mDiscountModel.getSubtract_unitPrice());
                            mTotalASDisc = mTotalASDisc - Double.parseDouble(mDiscountModel.getStrtotalPercentValue());

                        }

//                        mFinalTotal =  mTotal;
//                        mTotalASDisc= mTotalASDisc+mFinalTotal;


                        Log.e(TAG, "***Final Total***" + mTotalASDisc);
                    }

                    mModel.setTotalAmount(String.valueOf(Utilities.getRoundOff2Decimal(mTotal + mTotalASDisc)));
                    mTotalASDisc = 0;


                } else {
                    mModel.setTotalAmount(mModel.getStrTotalAmountUnit());
                }


                setAdapter();
            }
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
//      overridePendingTransitionExit();
    }
}
