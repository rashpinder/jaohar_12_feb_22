package jaohar.com.jaohar.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class SignUpActivity extends BaseActivity {
    Activity mActivity = SignUpActivity.this;
    String TAG = "SignUpActivity";
    View rootView;
    EditText editUserNameET, editPasswordET, editConfirmPasswordET, editFirstNameET, editLastNameET, editCompanyET;
    Button btnSignUp;
    String strIsBack = "";
    String strLoginType = "";
    String refreshedToken = "";
    String strRoleArray[] = {};
    //    VesselesModel mVesselesModel;
    ArrayList<String> mArray;
    String strType = "Romania Office";
    LinearLayout llLeftLL, roleLL;
    ImageView imgBack;
    TextView txtCenter, termsTV;
    Spinner txtSpinnerTV;
    BottomSheetDialog mBottomSheetDialog;
    RelativeLayout romaniaOfficeRL, UkOfficeRL, cyprusOfficeRL, greeceOfficeRL, adminRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_sign_up);
        getPushToken();
//        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "********TOKEN*******" + refreshedToken);
        if (getIntent() != null) {
            strLoginType = getIntent().getStringExtra(JaoharConstants.LOGIN_TYPE);
            if (strLoginType.equals(JaoharConstants.USER)) {
                // mVesselesModel = (VesselesModel) getIntent().getSerializableExtra("Model");
            }
        }

        /*
         * Bottom sheet Functionalty
         * */
//        mBottomSheetDialog = new BottomSheetDialog(mActivity);
//        @SuppressLint("InflateParams") View sheetView = mActivity.getLayoutInflater().inflate(R.layout.sign_up_bottom_sheet, null);
//        mBottomSheetDialog.setContentView(sheetView);
//        View bottomSheet = mBottomSheetDialog.findViewById(R.id.bottom_sheet);
//        assert bottomSheet != null;
//        romaniaOfficeRL = bottomSheet.findViewById(R.id.romaniaOfficeRL);
//        UkOfficeRL = bottomSheet.findViewById(R.id.UkOfficeRL);
//        cyprusOfficeRL = bottomSheet.findViewById(R.id.cyprusOfficeRL);
//        greeceOfficeRL = bottomSheet.findViewById(R.id.greeceOfficeRL);
//        adminRL = bottomSheet.findViewById(R.id.adminRL);
//        adminRL.setVisibility(View.GONE);
//
//        romaniaOfficeRL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                strType = "Romania Office";
//                txtCenter.setText("(" + strType + ") \u2304");
//                mBottomSheetDialog.dismiss();
//            }
//        });
//        UkOfficeRL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                strType = "UK Office";
//                txtCenter.setText("(" + strType + ") \u2304");
//                mBottomSheetDialog.dismiss();
//            }
//        });
//        cyprusOfficeRL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                strType = "Cyprus Office";
//                txtCenter.setText("(" + strType + ") \u2304");
//                mBottomSheetDialog.dismiss();
//            }
//        });
//        greeceOfficeRL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                strType = "Greece Office";
//                txtCenter.setText("(" + strType + ") \u2304");
//                mBottomSheetDialog.dismiss();
//            }
//        });
    }

    private void getPushToken() {
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(task -> {
            if (!task.isSuccessful()) {
                //handle token error
                Log.e(TAG, "**Get Instance Failed**", task.getException());
                return;
            }
            // Get new Instance ID token
            refreshedToken = task.getResult();
            Log.e(TAG, "**Push Token**" + refreshedToken);

        });
    }

    @Override
    protected void setViewsIDs() {
        termsTV = findViewById(R.id.termsTV);
        txtCenter = findViewById(R.id.txtCenter);
        imgBack = findViewById(R.id.imgBack);
        llLeftLL = findViewById(R.id.llLeftLL);
        roleLL = findViewById(R.id.roleLL);
        editUserNameET = findViewById(R.id.editUserNameET);
        editPasswordET = findViewById(R.id.editPasswordET);
        editConfirmPasswordET = findViewById(R.id.editConfirmPasswordET);
        btnSignUp = findViewById(R.id.btnSignUp);
        editFirstNameET = findViewById(R.id.editFirstNameET);
        editLastNameET = findViewById(R.id.editLastNameET);
        editCompanyET = findViewById(R.id.editCompanyET);
        txtSpinnerTV = findViewById(R.id.txtSpinnerTV);

        if (strLoginType.equals(JaoharConstants.STAFF)) {
            roleLL.setVisibility(View.VISIBLE);
        } else {
            roleLL.setVisibility(View.GONE);
        }

        addClicksToTermsAndPrivacyString();
    }

    private void addClicksToTermsAndPrivacyString() {
        SpannableString SpanString = new SpannableString(getString(R.string.by_signing_in));

        ClickableSpan termsAndCondition = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent mIntent = new Intent(mActivity, OpenLinkActivity.class);
                mIntent.putExtra("TITLE", "Terms & Conditions");
                mIntent.putExtra("LINK", JaoharConstants.TERMS_AND_CONDITIONS);
                startActivity(mIntent);

            }
        };

        // Character starting from 31 - 44 is privacy policy.
        // Character starting from 52 - 69 is Terms and condition.

        ClickableSpan privacy = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent mIntent = new Intent(mActivity, OpenLinkActivity.class);
                mIntent.putExtra("TITLE", "Privacy Policy");
                mIntent.putExtra("LINK", JaoharConstants.PRIVACY_POLICY);
                startActivity(mIntent);

            }
        };

        SpanString.setSpan(termsAndCondition, 49, 67, 0);
        SpanString.setSpan(privacy, 30, 44, 0);
        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black)), 30, 44, 0);
        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black)), 49, 67, 0);
        SpanString.setSpan(new UnderlineSpan(), 30, 44, 0);
        SpanString.setSpan(new UnderlineSpan(), 49, 67, 0);

        termsTV.setMovementMethod(LinkMovementMethod.getInstance());
        termsTV.setText(SpanString, TextView.BufferType.SPANNABLE);
        termsTV.setSelected(true);
    }

    @Override
    protected void setClickListner() {
        // Creating adapter for spinner
        mArray = new ArrayList<String>();
        mArray.add("Romania Office");
        mArray.add("UK Office");
        mArray.add("Cyprus Office");
        mArray.add("Greece Office");

        final Typeface fontStyle = Typeface.createFromAsset(mActivity.getAssets(),
                "Poppins-Medium.ttf");
        ArrayAdapter<String> mSizeAd = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mArray) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner_layout, null);
                }
                TextView itemTextView = v.findViewById(R.id.itemTV);
                String strName = mArray.get(position);
                itemTextView.setText(strName);
                itemTextView.setTextColor(getResources().getColor(R.color.black));
                itemTextView.setTypeface(fontStyle);
                return v;
            }
        };
        txtSpinnerTV.setAdapter(mSizeAd);
        txtSpinnerTV.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ((TextView) view).setTypeface(fontStyle);
                ((TextView) view).setTextColor(getResources().getColor(R.color.black));
//                strType = txtSpinnerTV.getSelectedItem().toString();
                strType = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArray);
//        // Drop down layout style - list view with radio button
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        // attaching data adapter to spinner
//        txtSpinnerTV.setAdapter(dataAdapter);
//
//        txtSpinnerTV.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                strType = adapterView.getItemAtPosition(i).toString();
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editFirstNameET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_first_name));
                } else if (editLastNameET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_last_name));
                } else if (editUserNameET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                } else if (Utilities.isValidEmaillId(editUserNameET.getText().toString()) == false) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                } else if (editCompanyET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_company_name));
                } else if (editPasswordET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_password));
                } else if (editConfirmPasswordET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_password_con));
                } else if (!editConfirmPasswordET.getText().toString().equals(editPasswordET.getText().toString())) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.password_should));
                } else {
                    if (Utilities.isNetworkAvailable(mActivity)) {
                        /*Execute Login API*/
                        executeAPI();
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.internetconnection));

                    }

                }
            }

        });

        txtCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (strLoginType.equals(JaoharConstants.STAFF)) {
//                    mBottomSheetDialog.show();
//                } else {
//                    mBottomSheetDialog.dismiss();
//                }
            }
        });
    }

//    public void executeAPI() {
//
//        String strAPIUrl = "";
//        JSONObject jsonObject = new JSONObject();
//        if (strLoginType.equals(JaoharConstants.STAFF)) {
//            strAPIUrl = JaoharConstants.SIGN_UP_API;
//
//        } else {
//            strAPIUrl = JaoharConstants.SIGN_UP_API;
//            strType = "user";
//        }
//        try {
//            jsonObject.put("email", editUserNameET.getText().toString());
//            jsonObject.put("role", strType);
//            jsonObject.put("password", editPasswordET.getText().toString());
//            jsonObject.put("device_token", refreshedToken);
//            jsonObject.put("device_type", "Android");
//            jsonObject.put("first_name", editFirstNameET.getText().toString());
//            jsonObject.put("last_name", editLastNameET.getText().toString());
//            jsonObject.put("company_name", editCompanyET.getText().toString());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//
//        AlertDialogManager.showProgressDialog(mActivity);
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, strAPIUrl, jsonObject, new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******response*****" + jsonObject.toString());
//                try {
//
//                    String strStatus = jsonObject.getString("status");
//                    if (strStatus.equals("1")) {
//                        showAdminDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + jsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "******error*****" + error);
//            }
//        }) {
//            /**
//             * Passing some request headers
//             * */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }


    private Map<String, String> mParams() {
        Map<String, String> params = new HashMap<>();
        params.put("email", editUserNameET.getText().toString());
        params.put("first_name", editFirstNameET.getText().toString());
        params.put("last_name", editLastNameET.getText().toString());
        params.put("company_name", editCompanyET.getText().toString());
        params.put("password", editPasswordET.getText().toString());
        params.put("device_token", refreshedToken);
        params.put("device_type", "Android");
        params.put("role", strType);
        Log.e("**PARAMS**", params.toString());
        return params;
    }

    public void executeAPI() {
        if (strLoginType.equals(JaoharConstants.STAFF)) {
        } else {
            strType = "user";
        }
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.signupRequest(mParams());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showAdminDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    public void showAdminDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (strLoginType.equals(JaoharConstants.STAFF)) {
                    Intent mIntent = new Intent(mActivity, HomeActivity.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mIntent.putExtra(JaoharConstants.LOGIN, "SignUP");
                    mIntent.putExtra("TYPE", strLoginType);
                    startActivity(mIntent);
                    finish();
                } else if (strLoginType.equals(JaoharConstants.USER)) {
                    finish();
                    overridePendingTransitionExit();
                } else if (strLoginType.equals("VesselForSaleActivity")) {
                    finish();
                    overridePendingTransitionExit();
                }
            }
        });
        alertDialog.show();
    }


    @Override
    public void onBackPressed() {
        if (strLoginType.equals(JaoharConstants.USER)) {
            mActivity.finish();
            overridePendingTransitionExit();
        } else if (strLoginType.equals("VesselForSaleActivity")) {
            mActivity.finish();
            overridePendingTransitionExit();
        } else {
            Intent mIntent = new Intent(mActivity, HomeActivity.class);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mIntent.putExtra(JaoharConstants.LOGIN, "SignUP");
            mIntent.putExtra("TYPE", strLoginType);
            mActivity.startActivity(mIntent);
            mActivity.finish();
        }
    }
}
