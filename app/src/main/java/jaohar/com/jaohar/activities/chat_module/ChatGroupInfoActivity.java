package jaohar.com.jaohar.activities.chat_module;

import static android.view.View.GONE;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.chat_module.ChatGroupInfoParticipantsAdapter;
import jaohar.com.jaohar.beans.chat_module.ChatUsersModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import retrofit2.Call;
import retrofit2.Callback;

public class ChatGroupInfoActivity extends BaseActivity {
    /**
     * set Activity
     **/
    Activity mActivity = ChatGroupInfoActivity.this;

    /**
     * set Activity TAG
     **/
    String TAG = ChatGroupInfoActivity.this.getClass().getSimpleName();

    /**
     * Widgets AND Adapters,Strings, Array List,Boolean and Integers
     **/
    @BindView(R.id.participantsRV)
    RecyclerView participantsRV;
    @BindView(R.id.groupNameTV)
    TextView groupNameTV;
    @BindView(R.id.clearChatTV)
    TextView clearChatTV;
    @BindView(R.id.deleteGroupTV)
    TextView deleteGroupTV;
    @BindView(R.id.addParticipantsLL)
    LinearLayout addParticipantsLL;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.groupImage)
    ImageView groupImage;
    @BindView(R.id.deleteView)
    View deleteView;
    @BindView(R.id.participantsView)
    View participantsView;

    String strGroupId = "", strRoomId = "", strGroupName = "", strGroupImage = "";

    ArrayList<ChatUsersModel> mSelectedArrayList = new ArrayList<>();
    ChatGroupInfoParticipantsAdapter mChatGroupInfoParticipantsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_group_info);

        setStatusBar();

        ButterKnife.bind(this);

        getIntentData();
    }

    private void getIntentData() {
        if (getIntent().getExtras() != null) {
            strGroupId = getIntent().getStringExtra(JaoharConstants.GROUP_ID);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mSelectedArrayList != null)
            mSelectedArrayList.clear();

        executeDetailsAPI(strGroupId);
    }

    @OnClick({R.id.imgBack, R.id.addParticipantsLL, R.id.clearChatTV, R.id.deleteGroupTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.imgBack:
                onBackPressed();
                break;

            case R.id.addParticipantsLL:
                performAddParticipantsClick();
                break;

            case R.id.clearChatTV:
                performClearChatClick();
                break;

            case R.id.deleteGroupTV:
                performDeleteGroupClick();
                break;
        }
    }

    private void performDeleteGroupClick() {
        deleteGroupConfirmDialog(getResources().getString(R.string.are_you_sure_want_to_group));
    }

    private void performClearChatClick() {
        deleteChatConfirmDialog(getResources().getString(R.string.are_you_sure_want_to_clear_chat));
    }

    private void performAddParticipantsClick() {
        Intent intent = new Intent(getApplicationContext(), AddNewParticipantsActivity.class);
        intent.putExtra(JaoharConstants.GROUP_ID, strGroupId);
        intent.putExtra(JaoharConstants.GROUP_NAME, strGroupName);
        intent.putExtra(JaoharConstants.GROUP_IMAGE, strGroupImage);
        intent.putExtra(JaoharConstants.GROUP_PARTICIPANTS, (Serializable) mSelectedArrayList);
        startActivity(intent);
    }

    /* *
     * Execute API for getting group details
     * @param
     * @user_id
     * */
    public void executeDetailsAPI(String strGroupId) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getGroupDetail(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), strGroupId);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                parseResponce(response.body().toString());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }

    void parseResponce(String responce) {
        try {
            JSONObject mJSonObject = new JSONObject(responce);
            String strStatus = String.valueOf(mJSonObject.getInt("status"));
            String strMessage = mJSonObject.getString("message");
            if (strStatus.equals("1")) {
                JSONObject mjsonDATA = mJSonObject.getJSONObject("data");

                ChatUsersModel mModel = new ChatUsersModel();

                if (!mjsonDATA.getString("group_id").equals("")) {
                    mModel.setGroup_id(mjsonDATA.getString("group_id"));
                    strGroupId = mjsonDATA.getString("group_id");
                }
                if (!mjsonDATA.getString("admin_id").equals("")) {
                    mModel.setAdmin_id(mjsonDATA.getString("admin_id"));

                    if (JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "").equals(mjsonDATA.getString("admin_id"))) {
                        addParticipantsLL.setVisibility(View.VISIBLE);
                        deleteGroupTV.setVisibility(View.VISIBLE);
                        deleteView.setVisibility(View.VISIBLE);
                        participantsView.setVisibility(View.VISIBLE);
                    } else {
                        addParticipantsLL.setVisibility(GONE);
                        deleteGroupTV.setVisibility(GONE);
                        deleteView.setVisibility(GONE);
                        participantsView.setVisibility(GONE);
                    }
                }
                if (!mjsonDATA.getString("room_id").equals("")) {
                    mModel.setRoom_id(mjsonDATA.getString("room_id"));
                    strRoomId = mjsonDATA.getString("room_id");
                }
                if (!mjsonDATA.getString("chat_users").equals("")) {
                    mModel.setChat_users(mjsonDATA.getString("chat_users"));
                }
                if (!mjsonDATA.getString("group_name").equals("")) {
                    mModel.setName(mjsonDATA.getString("group_name"));
                    groupNameTV.setText(mjsonDATA.getString("group_name"));
                    strGroupName = mjsonDATA.getString("group_name");
                }
                if (!mjsonDATA.getString("group_image").equals("")) {
                    mModel.setImage(mjsonDATA.getString("group_image"));
                    Glide.with(mActivity).load(mjsonDATA.getString("group_image")).placeholder(R.drawable.ic_user)
                            .into(groupImage);
                    strGroupImage = mjsonDATA.getString("group_image");
                } else {
                    groupImage.setImageResource(R.drawable.ic_user_group);
                }
                if (!mjsonDATA.getString("room_id").equals("")) {
                    mModel.setRoom_id(mjsonDATA.getString("room_id"));
                }

                JSONArray mjsonArrayData = mjsonDATA.getJSONArray("participants");
                if (mjsonArrayData != null) {

                    for (int i = 0; i < mjsonArrayData.length(); i++) {
                        JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                        ChatUsersModel mParticipantsModel = new ChatUsersModel();

                        if (!mJsonDATA.getString("id").equals("")) {
                            mParticipantsModel.setId(mJsonDATA.getString("id"));
                        }
                        if (!mJsonDATA.getString("email").equals("")) {
                            mParticipantsModel.setEmail(mJsonDATA.getString("email"));
                        }
                        if (!mJsonDATA.getString("role").equals("")) {
                            mParticipantsModel.setRole(mJsonDATA.getString("role"));
                        }
                        if (!mJsonDATA.getString("email").equals("")) {
                            mParticipantsModel.setEmail(mJsonDATA.getString("email"));
                        }
                        if (!mJsonDATA.getString("company_name").equals("")) {
                            mParticipantsModel.setCompany_name(mJsonDATA.getString("company_name"));
                        }
                        if (!mJsonDATA.getString("first_name").equals("")) {
                            mParticipantsModel.setFirst_name(mJsonDATA.getString("first_name"));
                        }
                        if (!mJsonDATA.getString("last_name").equals("")) {
                            mParticipantsModel.setLast_name(mJsonDATA.getString("last_name"));
                        }
                        if (!mJsonDATA.getString("job").equals("")) {
                            mParticipantsModel.setJob(mJsonDATA.getString("job"));
                        }
                        if (!mJsonDATA.getString("email").equals("")) {
                            mParticipantsModel.setEmail(mJsonDATA.getString("email"));
                        }
                        if (!mJsonDATA.getString("email").equals("")) {
                            mParticipantsModel.setEmail(mJsonDATA.getString("email"));
                        }
                        if (!mJsonDATA.getString("image").equals("")) {
                            mParticipantsModel.setImage(mJsonDATA.getString("image"));
                        }
                        if (!mJsonDATA.getString("chat_status").equals("")) {
                            mParticipantsModel.setChat_status(mJsonDATA.getString("chat_status"));
                        }
                        if (!mJsonDATA.getString("image").equals("")) {
                            mParticipantsModel.setImage(mJsonDATA.getString("image"));
                        }
                        mSelectedArrayList.add(mParticipantsModel);
                    }

                    setAdapter();
                }
            } else {
                AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setAdapter() {
        participantsRV.setNestedScrollingEnabled(false);
        participantsRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mChatGroupInfoParticipantsAdapter = new ChatGroupInfoParticipantsAdapter(mActivity, mSelectedArrayList);
        participantsRV.setAdapter(mChatGroupInfoParticipantsAdapter);
    }

    /* *
     * PopUp to Delete or Cancel the Message
     * */
    public void deleteChatConfirmDialog(String strMessage) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(strMessage);
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /* *
                 * Execute API to Delete Blog Category
                 * */
                executeDeleteChatAPI();
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    public void deleteGroupConfirmDialog(String strMessage) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(strMessage);
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /* *
                 * Execute API to Delete Blog Category
                 * */
                executeDeleteGroupAPI();
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    /* *
     * Execute API for Delete chat groups
     * @param
     * @message_id
     * @user_id
     * */
    private void executeDeleteGroupAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        String strUrl = JaoharConstants.DeleteGroup + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "") + "&id=" + strGroupId;
        Log.e(TAG, "***URL***" + strUrl);

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.deleteGroup(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), strGroupId);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                try {
                    JSONObject mJsonDATA = new JSONObject(response.body().toString());
                    if (mJsonDATA.getInt("status") == 1) {
                        showFinishAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonDATA.getString("message"));

                    } else {
                        showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonDATA.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }

    private void executeDeleteChatAPI() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.deleteChat(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), strRoomId);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject mJsonDATA = new JSONObject(response.body().toString());
                    if (mJsonDATA.getInt("status") == 1) {
                        showFinishAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonDATA.getString("message"));
                    } else {
                        showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonDATA.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }
}
