package jaohar.com.jaohar.activities.chat_module;

import static android.provider.MediaStore.ACTION_VIDEO_CAPTURE;
import static android.provider.MediaStore.EXTRA_VIDEO_QUALITY;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.google.gson.Gson;
import com.vincent.filepicker.filter.entity.NormalFile;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.JaoharApplication;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.forum_module.VideoTrimerActivity;
import jaohar.com.jaohar.adapters.AddImageListAdapter;
import jaohar.com.jaohar.adapters.chat_module.ChatMessagesAdapter1;
import jaohar.com.jaohar.beans.AddGalleryImagesModel;
import jaohar.com.jaohar.interfaces.DeleteImagesVessels;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.forumModule.Add_Del_Edit_forumchatInterfaceNew;
import jaohar.com.jaohar.models.chatmodels.AddNewMessageModel;
import jaohar.com.jaohar.models.chatmodels.AllChatsItem;
import jaohar.com.jaohar.models.chatmodels.AppendData;
import jaohar.com.jaohar.models.chatmodels.DeleteMessageModel;
import jaohar.com.jaohar.models.chatmodels.GetAllChatMessagesModel;
import jaohar.com.jaohar.models.chatmodels.ReplyFor;
import jaohar.com.jaohar.models.chatmodels.UserDetail;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.FileUtil;
import jaohar.com.jaohar.utils.ImageUtils;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.RealPathUtil;
import jaohar.com.jaohar.utils.Utilities;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatMessagesListActivity extends BaseActivity {
    /**
     * set Activity
     **/
    Activity mActivity = ChatMessagesListActivity.this;
    /**
     * set Activity TAG
     **/
    String TAG = ChatMessagesListActivity.this.getClass().getSimpleName();
    /*
    Widgets
     */
    @BindView(R.id.dataRV)
    RecyclerView dataRV;
    @BindView(R.id.textED)
    EditText textED;
    @BindView(R.id.progressBottomPB)
    ProgressBar progressBottomPB;
    @BindView(R.id.sendProgressBar)
    ProgressBar sendProgressBar;
    @BindView(R.id.recordIDheadingTV)
    TextView recordIDheadingTV;
    @BindView(R.id.recordIDTV)
    TextView recordIDTV;
    @BindView(R.id.headingTV)
    TextView headingTV;
    @BindView(R.id.textUserName)
    TextView textUserName;
    @BindView(R.id.textActiveStatus)
    TextView textActiveStatus;
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    @BindView(R.id.recordIDLL)
    LinearLayout recordIDLL;
    @BindView(R.id.headingIDLL)
    LinearLayout headingIDLL;
    @BindView(R.id.galaryLL)
    LinearLayout galaryLL;
    @BindView(R.id.imageLL)
    LinearLayout imageLL;
    @BindView(R.id.profileLL)
    LinearLayout profileLL;
    @BindView(R.id.imagePic)
    CircleImageView imagePic;
    @BindView(R.id.sendIV)
    ImageView sendIV;
    @BindView(R.id.closeImageIV)
    ImageView closeImageIV;
    @BindView(R.id.addImgIV)
    ImageView addImgIV;
    @BindView(R.id.item_Image_Status)
    ImageView item_Image_Status;
    @BindView(R.id.messageReplyRL)
    RelativeLayout messageReplyRL;
    @BindView(R.id.crossRL)
    RelativeLayout crossRL;
    @BindView(R.id.swipeTorefefresh)
    SwipeRefreshLayout swipeTorefefresh;
    @BindView(R.id.editMessageText)
    TextView editMessageText;
    @BindView(R.id.galleryRecyclerView)
    RecyclerView galleryRecyclerView;

    @BindView(R.id.imageRplyRL)
    RelativeLayout imageRplyRL;
    @BindView(R.id.messagePicReplyIV)
    ImageView messagePicReplyIV;
    @BindView(R.id.replyBlurIV)
    ImageView replyBlurIV;
    @BindView(R.id.blurPicRL)
    RelativeLayout blurPicRL;
    @BindView(R.id.textCountPicTv)
    TextView textCountPicTv;

    /**
     * Adapters,Strings, Array List,Boolean and Integers
     **/
    String ReceiverID = "", strLastPage = "FALSE", full_name = "", first_name = "", last_name = "", RECEIVER_PIC = "";
    ArrayList<AddGalleryImagesModel> mGalleryArrayList = new ArrayList<AddGalleryImagesModel>();
    ChatMessagesAdapter1 mAdapterNew;
    int page_num = 0, currentMSGpositon = 0;
    AddImageListAdapter mImageAdapter;
    String strPhoto1 = "", strPhoto2 = "", strPhoto3 = "", strPhoto4 = "", strPhoto5 = "", strPhoto6 = "",
            strPhoto7 = "", strPhoto8 = "", strPhoto9 = "", strPhoto10 = "", strDocumentNameforSend = "", strDoc = "",
            strDocname1 = "", strDocname2 = "", strDocname3 = "", strDocname4 = "", strDocname5 = "",
            strImagePath1 = "", strImagePath2 = "", strImagePath3 = "", strImagePath4 = "", strImagePath5 = "";
    Bitmap bitmap1 = null, bitmap2 = null, bitmap3 = null, bitmap4 = null, bitmap5 = null;
    String strReplyID = "";
    Uri fileUri;
    byte[] byteArray1;
    byte[] byteArray2;
    byte[] byteArray3;
    byte[] byteArray4;
    byte[] byteArray5;
    ArrayList<NormalFile> list = new ArrayList<>();
    boolean isAddClick = false;
    boolean isSwipeRefresh = false;
    String roomDetailsonline_state = "", roomDetailsonline_time = "";
    private String mCurrentPhotoPath = "", mStoragePath = "", strMessageID = "", strRoomID = "",
            strOnlineState = "", strGroupId = "", strPushType = "", strRole = "";
    private LinearLayoutManager linearLayoutManager;

    /*Timer*/
    private Socket mSocket;
    private boolean mTyping = false;
    private Boolean isConnected = true;
    private final Handler mTypingHandler = new Handler();

    /*
     * Permissions for camera and gallary
     * */
    public static final int CAMERA_REQUEST = 111;
    public static final int GALLERY_REQUEST = 222;
    public static String[] thumbColumns = {MediaStore.Video.Thumbnails.DATA};
    public static String[] mediaColumns = {MediaStore.Video.Media._ID};
    static boolean isEditMessage = false, isReplyMessage = false, isAddVideo = false;
    private final String READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    private final String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private final String CAMERA = Manifest.permission.CAMERA;
    private final String recorfAudioStr = Manifest.permission.RECORD_AUDIO;

    private List<AllChatsItem> mGetAllChatMessagesAllChatModel = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_chat_messages_list);
        ButterKnife.bind(this);

        setClickListnerr();

        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        /*Retrive Data from Privious Activity*/
        if (getIntent() != null) {
            if (getIntent().getStringExtra(JaoharConstants.NOTIFICATION_TYPE) != null) {
                strPushType = getIntent().getStringExtra(JaoharConstants.NOTIFICATION_TYPE);
            }

            if (getIntent().getStringExtra(JaoharConstants.RECEIVER_ID) != null) {
                ReceiverID = getIntent().getStringExtra(JaoharConstants.RECEIVER_ID);
            }

            if (getIntent().getStringExtra(JaoharConstants.USER_NAME) != null) {
                full_name = getIntent().getStringExtra(JaoharConstants.USER_NAME);
                textUserName.setText(full_name);
            }

            if (getIntent().getStringExtra(JaoharConstants.USER_FIRST_NAME) != null) {
                first_name = getIntent().getStringExtra(JaoharConstants.USER_FIRST_NAME);
            }

            if (getIntent().getStringExtra(JaoharConstants.USER_LAST_NAME) != null) {
                last_name = getIntent().getStringExtra(JaoharConstants.USER_LAST_NAME);
            }

            if (getIntent().getStringExtra(JaoharConstants.ROOM_ID) != null) {
                strRoomID = getIntent().getStringExtra(JaoharConstants.ROOM_ID);
            }

            if (getIntent().getStringExtra(JaoharConstants.ROLE) != null) {
                strRole = getIntent().getStringExtra(JaoharConstants.ROLE);
            }

            if (getIntent().getStringExtra(JaoharConstants.ONLINE_STATE) != null) {
                strOnlineState = getIntent().getStringExtra(JaoharConstants.ONLINE_STATE);

                if (strOnlineState.equals("Available")) {
                    item_Image_Status.setBackgroundResource(R.drawable.bg_green_status);
                } else {
                    item_Image_Status.setBackgroundResource(R.drawable.bg_red_status);
                }
            }

            if (getIntent().getStringExtra(JaoharConstants.GROUP_ID) != null) {
                strGroupId = getIntent().getStringExtra(JaoharConstants.GROUP_ID);

                assert strGroupId != null;
                if (strGroupId.equals("0")) {
                    textActiveStatus.setVisibility(VISIBLE);
                    item_Image_Status.setVisibility(View.VISIBLE);
                    if (getIntent().getStringExtra(JaoharConstants.RECEIVER_PIC) != null && !getIntent().getStringExtra(JaoharConstants.RECEIVER_PIC).equals("")) {
                        RECEIVER_PIC = getIntent().getStringExtra(JaoharConstants.RECEIVER_PIC);
                        Glide.with(mActivity).load(RECEIVER_PIC)
                                .placeholder(R.drawable.ic_user)
                                .into(imagePic);
                    } else {
                        imagePic.setImageResource(R.drawable.ic_user);
                    }
                } else {
                    textActiveStatus.setVisibility(GONE);
                    item_Image_Status.setVisibility(View.GONE);
                    if (getIntent().getStringExtra(JaoharConstants.RECEIVER_PIC) != null && !getIntent().getStringExtra(JaoharConstants.RECEIVER_PIC).equals("")) {
                        RECEIVER_PIC = getIntent().getStringExtra(JaoharConstants.RECEIVER_PIC);
                        Glide.with(mActivity).load(RECEIVER_PIC)
                                .placeholder(R.drawable.ic_user_group)
                                .into(imagePic);
                    } else {
                        imagePic.setImageResource(R.drawable.ic_user_group);
                    }
                }
            } else {
                if (getIntent().getStringExtra(JaoharConstants.RECEIVER_PIC) != null) {
                    if (getIntent().getStringExtra(JaoharConstants.RECEIVER_PIC) != null && !getIntent().getStringExtra(JaoharConstants.RECEIVER_PIC).equals("")) {
                        String RECEIVER_PIC = getIntent().getStringExtra(JaoharConstants.RECEIVER_PIC);
                        Glide.with(mActivity).load(RECEIVER_PIC)
                                .placeholder(R.drawable.ic_user)
                                .into(imagePic);
                    } else {
                        imagePic.setImageResource(R.drawable.ic_user);
                    }
                }
            }
        }

        /*
         * Implement Socket
         **/
        if (!Utilities.isNetworkAvailable(Objects.requireNonNull(mActivity))) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            JaoharApplication app = (JaoharApplication) getApplication();
            mSocket = app.getSocket();
            mSocket.emit("ConncetedChat", strRoomID);
            mSocket.on(Socket.EVENT_CONNECT, onConnect);
            mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
            mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
            mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
            mSocket.on("newMessage", onNewMessage);
            mSocket.on("ChatStatus", onUserJoined);
            mSocket.on("type", onTyping);
            mSocket.connect();

            if (isAddClick == true) {
                isAddClick = false;
            } else {
                //*Execute API For Getting List *//*
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
                mGetAllChatMessagesAllChatModel.clear();
                mGalleryArrayList.clear();
                page_num = 1;
                executeGetALlChatMessagesRetrofit();
            }
        }

        swipeTorefefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                /* only refresh when last page is true i.e. there are more messages */
                if (strLastPage.equals("FALSE")) {
                    ++page_num;
                    executeGetALlChatMessagesRetrofit();
                } else {
                    swipeTorefefresh.setRefreshing(false);
                }
            }
        });
    }

    @OnClick({R.id.profileLL, R.id.llLeftLL, R.id.addImgIV, R.id.closeImageIV, R.id.crossRL, R.id.sendIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.profileLL:
                performProfileClick();
                break;
            case R.id.llLeftLL:
                onBackPressed();
                break;
            case R.id.addImgIV:
                addImage();
                break;
            case R.id.closeImageIV:
                mGalleryArrayList.clear();
                break;
            case R.id.crossRL:
                performCrossClick();
                break;
            case R.id.sendIV:
                sendClick();
                break;
        }
    }

    private void performProfileClick() {
        if (!strGroupId.equals("") && !strGroupId.equals("0")) {
            startActivity(new Intent(mActivity, ChatGroupInfoActivity.class)
                    .putExtra(JaoharConstants.GROUP_ID, strGroupId));
        } else {
            startActivity(new Intent(mActivity, ChatUserProfileActivity.class)
                    .putExtra(JaoharConstants.USER_FIRST_NAME, first_name)
                    .putExtra(JaoharConstants.USER_LAST_NAME, last_name)
                    .putExtra(JaoharConstants.RECEIVER_PIC, RECEIVER_PIC)
                    .putExtra(JaoharConstants.USER_NAME, full_name)
                    .putExtra(JaoharConstants.ROLE, strRole));
        }
    }

    private void addImage() {
        Log.e(TAG, "mGalleryArrayList.size(): " + mGalleryArrayList.size());
        if (mGalleryArrayList.size() <= 5 - 1) {
            mToGrantPermission();
        } else {
            showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_images_limit_chat));
        }
    }

    private void performCrossClick() {
        mGalleryArrayList.clear();
        messageReplyRL.setVisibility(GONE);
        isEditMessage = false;
        isReplyMessage = false;
        imageLL.setVisibility(VISIBLE);
        galaryLL.setVisibility(View.GONE);
        editMessageText.setText("");
        textED.setText("");
    }

    private void sendClick() {
        if (mGalleryArrayList.size() != 0 && mGalleryArrayList.size() <= 5) {
            /* *
             * Send Comments API
             * */
            if (Utilities.isNetworkAvailable(mActivity) == false) {
                showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
            } else {
                if (isEditMessage == true) {
                    isEditMessage = false;
                    executeEditMessageRetrofitNew();
                } else if (isReplyMessage == true) {
                    isReplyMessage = false;
                    sendReplyMessageAPINew(strReplyID);
                } else {
                    executeSendMessageRetrofitNew();
                }
            }

        } else {
            if (textED.getText().toString().trim().equals("")) {
                showAlertDialog(mActivity, getResources().getString(R.string.app_name), getString(R.string.please_enter_text));
            } else {
                /* *
                 * Send Comments API
                 * */
                if (Utilities.isNetworkAvailable(mActivity) == false) {
                    showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    if (isEditMessage == true) {
                        isEditMessage = false;
                        executeEditMessageRetrofitNew();
                    } else if (isReplyMessage == true) {
                        isReplyMessage = false;
                        sendReplyMessageAPINew(strReplyID);
                    } else {
                        executeSendMessageRetrofitNew();
                    }
                }
            }
        }
    }

    private void setClickListnerr() {
        textED.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String strChar = s.toString();
                if (!strChar.equals("")) {
                    if (!mSocket.connected()) return;

                    if (!mTyping) {
                        mTyping = true;
                        mSocket.emit("type", strRoomID, true);
                    }

                    mTypingHandler.removeCallbacks(onTypingTimeout);
                    mTypingHandler.postDelayed(onTypingTimeout, 1000);
                    if (mGetAllChatMessagesAllChatModel != null) {
                        dataRV.scrollToPosition(0);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    /* ******
     * PERMISSIONS
     * ******/
    public void mToGrantPermission() {
        if (mCheckPermission()) {
            if (mGalleryArrayList.size() < 5) {
                openCameraGalleryDialog();
            } else {
                showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_images_limit_chat));
            }
        } else {
            mRequestPermission();
        }
    }

    private boolean mCheckPermission() {
        int camera = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int recordAudio = ContextCompat.checkSelfPermission(getApplicationContext(), recorfAudioStr);
        int read_external_storage = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int write_external_storage = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        return camera == PackageManager.PERMISSION_GRANTED && read_external_storage == PackageManager.PERMISSION_GRANTED && write_external_storage == PackageManager.PERMISSION_GRANTED && recordAudio == PackageManager.PERMISSION_GRANTED;
    }

    private void mRequestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{CAMERA, recorfAudioStr, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, 100);
    }

    private void mRequestPermissionNew() {
        ActivityCompat.requestPermissions(mActivity, new String[]{CAMERA, recorfAudioStr, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, 101);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 100:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    openCameraGalleryDialog();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }
                break;
            case 101:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }
                break;
            default:
                break;
        }
    }

    private void openCameraGalleryDialog() {
        Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_display_dialog);
        TextView text_camra = dialog.findViewById(R.id.txt_camra);
        TextView text_gallery = dialog.findViewById(R.id.txt_gallery);
        TextView txt_files = dialog.findViewById(R.id.txt_files);
        TextView txt_cancel = dialog.findViewById(R.id.txt_cancel);
        TextView txt_addDoc = dialog.findViewById(R.id.txt_addDoc);
        TextView tittleTV = dialog.findViewById(R.id.tittleTV);
        tittleTV.setText("Attach Files :-");
        tittleTV.setVisibility(GONE);
        txt_addDoc.setVisibility(VISIBLE);
        txt_files.setVisibility(GONE);
        txt_cancel.setVisibility(GONE);
        txt_files.setText("Camera Video");
        txt_cancel.setText("Gallery Video");

        text_camra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openCamera();
            }
        });

        txt_addDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (mGalleryArrayList.size() <= 5 - 1) {
                    openInternalStorage();
                } else {
                    showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_doc_limit_chat));
                }
            }
        });

        text_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });

        txt_files.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openVideoCamera();
            }
        });

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openVideoGallery();
            }
        });

        dialog.show();
        Log.e("error is occured", dialog.toString());
    }

    private void openVideoGallery() {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_PICK);
        intent.putExtra(EXTRA_VIDEO_QUALITY, 0);
        startActivityForResult(intent, 777);
    }

    private void openVideoCamera() {
        Intent takeVideoIntent = new Intent(ACTION_VIDEO_CAPTURE);
        takeVideoIntent.putExtra(EXTRA_VIDEO_QUALITY, 0);
        takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 29);

        if (takeVideoIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                ContentValues values = new ContentValues(1);
                values.put(MediaStore.Video.Media.MIME_TYPE, "video/*");
                fileUri = getApplicationContext().getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);

                takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                startActivityForResult(takeVideoIntent, 666);
            } else {
                takeVideoIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                startActivityForResult(takeVideoIntent, 666);

            }
        }
    }

    public void openFiles() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/png/jpg/jpeg");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 205);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri imageURI = FileProvider.getUriForFile(getApplicationContext(), "jaohar.com.jaohar.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */);
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }

    public void openGallery() {
        Log.e(TAG, String.valueOf(mGalleryArrayList.size()));
        if (mGalleryArrayList.size() == 0) {
            Intent intent = new Intent(mActivity, AlbumSelectActivity.class);
            intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 5);
            startActivityForResult(intent, GALLERY_REQUEST);
        } else if (mGalleryArrayList.size() == 5) {
            showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.add_images_limit_chat));
        } else if (mGalleryArrayList.size() != 0) {
            Intent intent = new Intent(mActivity, AlbumSelectActivity.class);
            int GallarySize = 5 - mGalleryArrayList.size();
            intent.putExtra(Constants.INTENT_EXTRA_LIMIT, GallarySize);
            startActivityForResult(intent, GALLERY_REQUEST);
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();

        /* implement api to update badge on users list */
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetAllChatMessagesModel> call1 = mApiInterface.getAllChatMessagesAndroid(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), strRoomID, page_num);
        call1.enqueue(new Callback<GetAllChatMessagesModel>() {
            @Override
            public void onResponse(Call<GetAllChatMessagesModel> call, Response<GetAllChatMessagesModel> response) {
                Log.e(TAG, "***URLResponce***" + response);

                /* to manage onBackPressed for push and normal intent */
                if (!strPushType.equals("") && strPushType.equals(JaoharConstants.PUSH)) {
                    Intent mIntent = new Intent(mActivity, ChatUsersListActivity.class);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    mIntent.putExtra(JaoharConstants.NOTIFICATION_TYPE, JaoharConstants.PUSH);
                    startActivity(mIntent);
                    finish();
                } else {
                    finish();
                }
            }

            @Override
            public void onFailure(Call<GetAllChatMessagesModel> call, Throwable t) {
                Log.e(TAG, "***Error**" + t.getMessage());

                /* to manage onBackPressed for push and normal intent */
                if (!strPushType.equals("") && strPushType.equals(JaoharConstants.PUSH)) {
                    Intent mIntent = new Intent(mActivity, ChatUsersListActivity.class);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    mIntent.putExtra(JaoharConstants.NOTIFICATION_TYPE, JaoharConstants.PUSH);
                    startActivity(mIntent);
                    finish();
                } else {
                    finish();
                }
            }
        });

        mSocket.emit("leaveChat", strRoomID);
    }

    private void openInternalStorage() {
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, 505);
    }

    public String getRealPath(Uri uri) {
        String docId = DocumentsContract.getDocumentId(uri);
        String[] split = docId.split(":");
        String type = split[0];
        Uri contentUri;
        switch (type) {
            case "image":
                contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                break;
            case "video":
                contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                break;
            case "audio":
                contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                break;
            default:
                contentUri = MediaStore.Files.getContentUri("external");
        }
        String selection = "_id=?";
        String[] selectionArgs = new String[]{
                split[1]
        };

        return getDataColumn(mActivity, contentUri, selection, selectionArgs);
    }

    public String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        String column = "_data";
        String[] projection = {
                column
        };
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(column);
                String value = cursor.getString(column_index);
                if (value.startsWith("content://") || !value.startsWith("/") && !value.startsWith("file://")) {
                    return null;
                }
                return value;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    /* *
     * Execute API for Sending Chat Messages
     * @param
     * @user_id
     * @forum_id
     * @message
     * @reply_id// Not Mandatory
     * @photo1// Not Mandatory
     * @photo2// Not Mandatory
     * @photo3// Not Mandatory
     * @photo4// Not Mandatory
     * @photo5// Not Mandatory
     * @photo6// Not Mandatory
     * @photo7// Not Mandatory
     * @photo8// Not Mandatory
     * @photo9// Not Mandatory
     * @photo10// Not Mandatory
     * @doc_name// Not Mandatory
     * @doc// Not Mandatory
     * */
    private void executeSendMessageRetrofitNew() {
        RequestBody requestFile1;
        RequestBody requestFile2;
        RequestBody requestFile3;
        RequestBody requestFile4;
        RequestBody requestFile5;

        MultipartBody.Part mMultipartBody1 = null;
        MultipartBody.Part mMultipartBody2 = null;
        MultipartBody.Part mMultipartBody3 = null;
        MultipartBody.Part mMultipartBody4 = null;
        MultipartBody.Part mMultipartBody5 = null;

        if (mGalleryArrayList.size() > 0) {
            for (int i = 0; i < mGalleryArrayList.size(); i++) {
                if (i == 0) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        byteArray1 = mGalleryArrayList.get(i).getByteArray();
                        strDocname1 = mGalleryArrayList.get(i).getStrDocName();
                        File file = null;
                        try {
                            file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        mMultipartBody1 = MultipartBody.Part.createFormData("photo1", file.getName(), requestFile1);

                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        byteArray1 = mGalleryArrayList.get(i).getByteArray();
                        strDocname1 = mGalleryArrayList.get(i).getStrImageName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody1 = MultipartBody.Part.createFormData("photo1", getAlphaNumericStringRetrofit() + ".jpg", requestFile1);
                        }

                    } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                        byteArray1 = mGalleryArrayList.get(i).getByteArray();
                        strDocname1 = mGalleryArrayList.get(i).getStrDocName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody1 = MultipartBody.Part.createFormData("photo1", getAlphaNumericStringRetrofit() + ".jpg", requestFile1);
                        }
                    }

                } else if (i == 1) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        byteArray2 = mGalleryArrayList.get(i).getByteArray();
                        strDocname2 = mGalleryArrayList.get(i).getStrDocName();

                        File file = null;
                        try {
                            file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        mMultipartBody2 = MultipartBody.Part.createFormData("photo2", file.getName(), requestFile2);

                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        byteArray2 = mGalleryArrayList.get(i).getByteArray();
                        strDocname2 = mGalleryArrayList.get(i).getStrImageName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody2 = MultipartBody.Part.createFormData("photo2", getAlphaNumericStringRetrofit() + ".jpg", requestFile2);
                        }

                    } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                        byteArray2 = mGalleryArrayList.get(i).getByteArray();
                        strDocname2 = mGalleryArrayList.get(i).getStrDocName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody2 = MultipartBody.Part.createFormData("photo2", getAlphaNumericStringRetrofit() + ".jpg", requestFile2);
                        }
                    }

                } else if (i == 2) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        byteArray3 = mGalleryArrayList.get(i).getByteArray();
                        strDocname3 = mGalleryArrayList.get(i).getStrDocName();

                        File file = null;
                        try {
                            file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        mMultipartBody3 = MultipartBody.Part.createFormData("photo3", file.getName(), requestFile3);

                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        byteArray3 = mGalleryArrayList.get(i).getByteArray();
                        strDocname3 = mGalleryArrayList.get(i).getStrImageName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody3 = MultipartBody.Part.createFormData("photo3", getAlphaNumericStringRetrofit() + ".jpg", requestFile3);
                        }

                    } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                        byteArray3 = mGalleryArrayList.get(i).getByteArray();
                        strDocname3 = mGalleryArrayList.get(i).getStrDocName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody3 = MultipartBody.Part.createFormData("photo3", getAlphaNumericStringRetrofit() + ".jpg", requestFile3);
                        }
                    }

                } else if (i == 3) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        byteArray4 = mGalleryArrayList.get(i).getByteArray();
                        strDocname4 = mGalleryArrayList.get(i).getStrDocName();

                        File file = null;
                        try {
                            file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        mMultipartBody4 = MultipartBody.Part.createFormData("photo4", file.getName(), requestFile4);

                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        byteArray4 = mGalleryArrayList.get(i).getByteArray();
                        strDocname4 = mGalleryArrayList.get(i).getStrImageName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody4 = MultipartBody.Part.createFormData("photo4", getAlphaNumericStringRetrofit() + ".jpg", requestFile4);
                        }

                    } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                        byteArray4 = mGalleryArrayList.get(i).getByteArray();
                        strDocname4 = mGalleryArrayList.get(i).getStrDocName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody4 = MultipartBody.Part.createFormData("photo4", getAlphaNumericStringRetrofit() + ".jpg", requestFile4);
                        }
                    }

                } else if (i == 4) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        byteArray5 = mGalleryArrayList.get(i).getByteArray();
                        strDocname5 = mGalleryArrayList.get(i).getStrDocName();

                        File file = null;
                        try {
                            file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        mMultipartBody5 = MultipartBody.Part.createFormData("photo5", file.getName(), requestFile5);

                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        byteArray5 = mGalleryArrayList.get(i).getByteArray();
                        strDocname5 = mGalleryArrayList.get(i).getStrImageName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody5 = MultipartBody.Part.createFormData("photo5", getAlphaNumericStringRetrofit() + ".jpg", requestFile5);
                        }

                    } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                        byteArray5 = mGalleryArrayList.get(i).getByteArray();
                        strDocname5 = mGalleryArrayList.get(i).getStrDocName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody5 = MultipartBody.Part.createFormData("photo5", getAlphaNumericStringRetrofit() + ".jpg", requestFile5);
                        }
                    }
                }
            }
        }

        RequestBody message = RequestBody.create(MediaType.parse("multipart/form-data"), textED.getText().toString());
        RequestBody receiver_id = RequestBody.create(MediaType.parse("multipart/form-data"), ReceiverID);
        RequestBody room_id = RequestBody.create(MediaType.parse("multipart/form-data"), strRoomID);
        RequestBody group_id = RequestBody.create(MediaType.parse("multipart/form-data"), strGroupId);
        RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));

        sendProgressBar.setVisibility(VISIBLE);
        sendIV.setVisibility(GONE);

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addChatMessageRequestNew(message, receiver_id, room_id, group_id, user_id, mMultipartBody1, mMultipartBody2, mMultipartBody3, mMultipartBody4,
                mMultipartBody5).enqueue(new Callback<AddNewMessageModel>() {
            @Override
            public void onResponse(Call<AddNewMessageModel> call, Response<AddNewMessageModel> response) {
                hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());

                sendProgressBar.setVisibility(GONE);
                sendIV.setVisibility(VISIBLE);

                AddNewMessageModel mModel = response.body();

                if (response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        textED.setText("");
                        mTyping = false;
                        mSocket.emit("type", strRoomID, false);
                        if (Utilities.isNetworkAvailable(mActivity) == false) {
                            showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                        } else {
                            if (progressBottomPB != null) {
                                progressBottomPB.setVisibility(GONE);
                            }
                            mGalleryArrayList.clear();
                            setUpGalleryAdapter();
                            list.clear();
                            strPhoto1 = "";
                            strPhoto2 = "";
                            strPhoto3 = "";
                            strPhoto4 = "";
                            strPhoto5 = "";
                            strPhoto6 = "";
                            strPhoto7 = "";
                            strPhoto8 = "";
                            strPhoto9 = "";
                            strPhoto10 = "";
                            strDocname1 = "";
                            strDocname2 = "";
                            strDocname3 = "";
                            strDocname4 = "";
                            strDocname5 = "";
                            byteArray1 = null;
                            byteArray2 = null;
                            byteArray3 = null;
                            byteArray4 = null;
                            byteArray5 = null;
                            strDoc = "";
//                          isDocAdded = false;
                            strDocumentNameforSend = "";
                            imageLL.setVisibility(VISIBLE);
                            galaryLL.setVisibility(View.GONE);
                            messageReplyRL.setVisibility(GONE);
                            isEditMessage = false;
                            editMessageText.setText("");
                            textED.setText("");
                            isAddClick = true;

                            AllChatsItem addNewMessageModel = response.body().getAppendData();
                            mGetAllChatMessagesAllChatModel.add(0, addNewMessageModel);

                            if (mGetAllChatMessagesAllChatModel != null && mGetAllChatMessagesAllChatModel.size() > 0) {
                                mAdapterNew.notifyDataSetChanged();
                                dataRV.scrollToPosition(0);
                            } else {
                                setAdapterNew();
                            }

                            // perform the sending message attempt.
                            Gson gson = new Gson();
                            String mOjectString = gson.toJson(response.body());
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(mOjectString);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.e("TAG", "*****Msg****" + mOjectString);
                            mSocket.emit("newMessage", strRoomID, obj);
                        }

                    } else if (response.body().getStatus() == 101) {
                        Log.e(TAG, "Unexpected*********:" + message);
                        onBackPressed();
                    } else {
                        showAlertDialog(mActivity, getString(R.string.app_name), "Getting Error Here");
                    }
                }
            }

            @Override
            public void onFailure(Call<AddNewMessageModel> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + t.toString());
                sendProgressBar.setVisibility(GONE);
                sendIV.setVisibility(VISIBLE);
            }
        });
    }

    /* *
     * Execute API for Sending Forum Chat  Messages // API  AddStaffForumMessageAndroid
     * @param
     * @user_id
     * @forum_id
     * @message
     * @reply_id// Mandatory
     * @photo1// Not Mandatory
     * @photo2// Not Mandatory
     * @photo3// Not Mandatory
     * @photo4// Not Mandatory
     * @photo5// Not Mandatory
     * @photo6// Not Mandatory
     * @photo7// Not Mandatory
     * @photo8// Not Mandatory
     * @photo9// Not Mandatory
     * @photo10// Not Mandatory
     * @doc_name// Not Mandatory
     * @doc// Not Mandatory
     * */
    private void sendReplyMessageAPINew(String mstrReplyID) {
        strReplyID = mstrReplyID;
        RequestBody requestFile1;
        RequestBody requestFile2;
        RequestBody requestFile3;
        RequestBody requestFile4;
        RequestBody requestFile5;

        MultipartBody.Part mMultipartBody1 = null;
        MultipartBody.Part mMultipartBody2 = null;
        MultipartBody.Part mMultipartBody3 = null;
        MultipartBody.Part mMultipartBody4 = null;
        MultipartBody.Part mMultipartBody5 = null;

        if (mGalleryArrayList.size() > 0) {
            for (int i = 0; i < mGalleryArrayList.size(); i++) {
                if (i == 0) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        byteArray1 = mGalleryArrayList.get(i).getByteArray();
                        strDocname1 = mGalleryArrayList.get(i).getStrDocName();
                        File file = null;
                        try {
                            file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        mMultipartBody1 = MultipartBody.Part.createFormData("photo1", file.getName(), requestFile1);

                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        byteArray1 = mGalleryArrayList.get(i).getByteArray();
                        strDocname1 = mGalleryArrayList.get(i).getStrImageName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody1 = MultipartBody.Part.createFormData("photo1", getAlphaNumericStringRetrofit() + ".jpg", requestFile1);
                        }

                    } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                        byteArray1 = mGalleryArrayList.get(i).getByteArray();
                        strDocname1 = mGalleryArrayList.get(i).getStrDocName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody1 = MultipartBody.Part.createFormData("photo1", getAlphaNumericStringRetrofit() + ".jpg", requestFile1);
                        }
                    }

                } else if (i == 1) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        byteArray2 = mGalleryArrayList.get(i).getByteArray();
                        strDocname2 = mGalleryArrayList.get(i).getStrDocName();

                        File file = null;
                        try {
                            file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        mMultipartBody2 = MultipartBody.Part.createFormData("photo2", file.getName(), requestFile2);

                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        byteArray2 = mGalleryArrayList.get(i).getByteArray();
                        strDocname2 = mGalleryArrayList.get(i).getStrImageName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody2 = MultipartBody.Part.createFormData("photo2", getAlphaNumericStringRetrofit() + ".jpg", requestFile2);
                        }

                    } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                        byteArray2 = mGalleryArrayList.get(i).getByteArray();
                        strDocname2 = mGalleryArrayList.get(i).getStrDocName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody2 = MultipartBody.Part.createFormData("photo2", getAlphaNumericStringRetrofit() + ".jpg", requestFile2);
                        }
                    }

                } else if (i == 2) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        byteArray3 = mGalleryArrayList.get(i).getByteArray();
                        strDocname3 = mGalleryArrayList.get(i).getStrDocName();

                        File file = null;
                        try {
                            file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        mMultipartBody3 = MultipartBody.Part.createFormData("photo3", file.getName(), requestFile3);

                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        byteArray3 = mGalleryArrayList.get(i).getByteArray();
                        strDocname3 = mGalleryArrayList.get(i).getStrImageName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody3 = MultipartBody.Part.createFormData("photo3", getAlphaNumericStringRetrofit() + ".jpg", requestFile3);
                        }

                    } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                        byteArray3 = mGalleryArrayList.get(i).getByteArray();
                        strDocname3 = mGalleryArrayList.get(i).getStrDocName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody3 = MultipartBody.Part.createFormData("photo3", getAlphaNumericStringRetrofit() + ".jpg", requestFile3);
                        }
                    }

                } else if (i == 3) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        byteArray4 = mGalleryArrayList.get(i).getByteArray();
                        strDocname4 = mGalleryArrayList.get(i).getStrDocName();

                        File file = null;
                        try {
                            file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        mMultipartBody4 = MultipartBody.Part.createFormData("photo4", file.getName(), requestFile4);

                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        byteArray4 = mGalleryArrayList.get(i).getByteArray();
                        strDocname4 = mGalleryArrayList.get(i).getStrImageName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody4 = MultipartBody.Part.createFormData("photo4", getAlphaNumericStringRetrofit() + ".jpg", requestFile4);
                        }

                    } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                        byteArray4 = mGalleryArrayList.get(i).getByteArray();
                        strDocname4 = mGalleryArrayList.get(i).getStrDocName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody4 = MultipartBody.Part.createFormData("photo4", getAlphaNumericStringRetrofit() + ".jpg", requestFile4);
                        }
                    }

                } else if (i == 4) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        byteArray5 = mGalleryArrayList.get(i).getByteArray();
                        strDocname5 = mGalleryArrayList.get(i).getStrDocName();

                        File file = null;
                        try {
                            file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        mMultipartBody5 = MultipartBody.Part.createFormData("photo5", file.getName(), requestFile5);

                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        byteArray5 = mGalleryArrayList.get(i).getByteArray();
                        strDocname5 = mGalleryArrayList.get(i).getStrImageName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody5 = MultipartBody.Part.createFormData("photo5", getAlphaNumericStringRetrofit() + ".jpg", requestFile5);
                        }

                    } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                        byteArray5 = mGalleryArrayList.get(i).getByteArray();
                        strDocname5 = mGalleryArrayList.get(i).getStrDocName();

                        if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                            File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                            requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                            mMultipartBody5 = MultipartBody.Part.createFormData("photo5", getAlphaNumericStringRetrofit() + ".jpg", requestFile5);
                        }
                    }
                }
            }
        }

        RequestBody message = RequestBody.create(MediaType.parse("multipart/form-data"), textED.getText().toString());
        RequestBody receiver_id = RequestBody.create(MediaType.parse("multipart/form-data"), ReceiverID);
        RequestBody reply_id = RequestBody.create(MediaType.parse("multipart/form-data"), strReplyID);
        RequestBody room_id = RequestBody.create(MediaType.parse("multipart/form-data"), strRoomID);
        RequestBody group_id = RequestBody.create(MediaType.parse("multipart/form-data"), strGroupId);
        RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));

        sendProgressBar.setVisibility(VISIBLE);
        sendIV.setVisibility(GONE);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.replyChatMessageRequestNew(message, receiver_id, room_id, reply_id, group_id, user_id, mMultipartBody1, mMultipartBody2, mMultipartBody3, mMultipartBody4,
                mMultipartBody5).enqueue(new Callback<AddNewMessageModel>() {
            @Override
            public void onResponse(Call<AddNewMessageModel> call, Response<AddNewMessageModel> response) {
                hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                sendProgressBar.setVisibility(GONE);
                sendIV.setVisibility(VISIBLE);

                if (response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        textED.setText("");
                        mTyping = false;
                        mSocket.emit("type", strRoomID, false);
                        if (Utilities.isNetworkAvailable(mActivity) == false) {
                            showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                        } else {
                            if (progressBottomPB != null) {
                                progressBottomPB.setVisibility(GONE);
                            }
                            mGalleryArrayList.clear();
                            setUpGalleryAdapter();
                            list.clear();
                            strPhoto1 = "";
                            strPhoto2 = "";
                            strPhoto3 = "";
                            strPhoto4 = "";
                            strPhoto5 = "";
                            strPhoto6 = "";
                            strPhoto7 = "";
                            strPhoto8 = "";
                            strPhoto9 = "";
                            strPhoto10 = "";
                            strDocname1 = "";
                            strDocname2 = "";
                            strDocname3 = "";
                            strDocname4 = "";
                            strDocname5 = "";
                            byteArray1 = null;
                            byteArray2 = null;
                            byteArray3 = null;
                            byteArray4 = null;
                            byteArray5 = null;
                            strDoc = "";
//                          isDocAdded = false;
                            strDocumentNameforSend = "";
                            imageLL.setVisibility(VISIBLE);
                            galaryLL.setVisibility(View.GONE);
                            messageReplyRL.setVisibility(GONE);
                            isEditMessage = false;
                            editMessageText.setText("");
                            textED.setText("");
                            isAddClick = true;

                            AllChatsItem addNewMessageModel = new AllChatsItem();
                            addNewMessageModel = response.body().getAppendData();
                            mGetAllChatMessagesAllChatModel.add(0, addNewMessageModel);

                            if (mGetAllChatMessagesAllChatModel != null) {
                                mAdapterNew.notifyDataSetChanged();
                                dataRV.scrollToPosition(0);
                            } else {
                                setAdapterNew();
                            }

                            // perform the sending message attempt.
                            Gson gson = new Gson();
                            String mOjectString = gson.toJson(response.body());
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(mOjectString);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.e("TAG", "*****Msg****" + mOjectString);
                            mSocket.emit("newMessage", strRoomID, obj);
                        }

                    } else if (response.body().getStatus() == 101) {
                        Log.e(TAG, "Unexpected*********:" + message);
                        onBackPressed();
                    } else {
                        showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
                    }
                }
            }

            @Override
            public void onFailure(Call<AddNewMessageModel> call, Throwable t) {

            }
        });
    }

    /* *
     * Execute API for Sending Forum Chat Edit Messages // API  EditStaffForumMessageNew
     * @param
     * @user_id
     * @message_id
     * @message
     * @reply_id// Not Mandatory
     * @photo1// Not Mandatory
     * @photo2// Not Mandatory
     * @photo3// Not Mandatory
     * @photo4// Not Mandatory
     * @photo5// Not Mandatory
     * @photo6// Not Mandatory
     * @photo7// Not Mandatory
     * @photo8// Not Mandatory
     * @photo9// Not Mandatory
     * @photo10// Not Mandatory
     * @doc_name// Not Mandatory
     * @doc// Not Mandatory
     * */
    private void executeEditMessageRetrofitNew() {

        sendProgressBar.setVisibility(VISIBLE);
        sendIV.setVisibility(GONE);

        RequestBody requestFile1;
        RequestBody requestFile2;
        RequestBody requestFile3;
        RequestBody requestFile4;
        RequestBody requestFile5;

        MultipartBody.Part mMultipartBody1 = null;
        MultipartBody.Part mMultipartBody2 = null;
        MultipartBody.Part mMultipartBody3 = null;
        MultipartBody.Part mMultipartBody4 = null;
        MultipartBody.Part mMultipartBody5 = null;
        MultipartBody.Part mMultipartBody6 = null;
        MultipartBody.Part mMultipartBody7 = null;
        MultipartBody.Part mMultipartBody8 = null;
        MultipartBody.Part mMultipartBody9 = null;
        MultipartBody.Part mMultipartBody10 = null;

        if (mGalleryArrayList.size() > 0) {
            for (int i = 0; i < mGalleryArrayList.size(); i++) {
                if (i == 0) {
                    if (mGalleryArrayList.get(i).getByteArray() != null) {
                        byteArray1 = mGalleryArrayList.get(i).getByteArray();
                        strPhoto1 = "";
                        if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                            strDocname1 = mGalleryArrayList.get(i).getStrDocName();

                            File file = null;
                            try {
                                file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                            mMultipartBody1 = MultipartBody.Part.createFormData("photo1", file.getName(), requestFile1);

                        } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                            strDocname1 = mGalleryArrayList.get(i).getStrDocName();

                            if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                                File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                                requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                                mMultipartBody1 = MultipartBody.Part.createFormData("photo1", getAlphaNumericStringRetrofit() + ".jpg", requestFile1);
                            }

                        } else {
                            strDocname1 = mGalleryArrayList.get(i).getStrImageName();

                            if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                                File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                                requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                                mMultipartBody1 = MultipartBody.Part.createFormData("photo1", getAlphaNumericStringRetrofit() + ".jpg", requestFile1);
                            }
                        }

                    } else {
                        strPhoto1 = mGalleryArrayList.get(i).getStrImagePath();
                        strDocname1 = "";

                        if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {

                                try {
                                    final InputStream bs = new URL(mGalleryArrayList.get(i).getStrImagePath()).openStream();

                                    Uri savedFileUri = savePDFFile(mActivity, bs, "files/pdf", System.currentTimeMillis() + ".pdf", "jaohar");
                                    Log.d("URI: ", savedFileUri.toString());
                                    String strPDFPath = getRealPathFromURI(mActivity, savedFileUri);
                                    Log.d(TAG, "File Path: " + strPDFPath);

                                    File file = new File(strPDFPath);

                                    requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                                    mMultipartBody1 = MultipartBody.Part.createFormData("photo1", file.getName(), requestFile1);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    hideProgressDialog();
                                    sendProgressBar.setVisibility(GONE);
                                    sendIV.setVisibility(VISIBLE);
                                }
                            } else {

                                String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
                                File folder = new File(extStorageDirectory, "pdf");
                                folder.mkdir();
                                File file = new File(folder, "Read.pdf");
                                try {
                                    file.createNewFile();
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                    hideProgressDialog();
                                    sendProgressBar.setVisibility(GONE);
                                    sendIV.setVisibility(VISIBLE);
                                }
                                DownloadFile(mGalleryArrayList.get(i).getStrImagePath(), file);

                                requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                                mMultipartBody1 = MultipartBody.Part.createFormData("photo1", file.getName(), requestFile1);
                            }
                        } else {
                            bitmap1 = getBitmapFromURL(mGalleryArrayList.get(i).getStrImagePath());

                            if (bitmap1 != null) {
                                requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), getbyteArray(bitmap1));
                                mMultipartBody1 = MultipartBody.Part.createFormData("photo1", getAlphaNumericStringRetrofit() + ".jpg", requestFile1);
                            }
                        }
                    }
                } else if (i == 1) {
                    if (mGalleryArrayList.get(i).getByteArray() != null) {
                        strPhoto2 = "";
                        byteArray2 = mGalleryArrayList.get(i).getByteArray();
                        if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                            strDocname2 = mGalleryArrayList.get(i).getStrDocName();

                            File file = null;
                            try {
                                file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                            mMultipartBody2 = MultipartBody.Part.createFormData("photo2", file.getName(), requestFile2);

                        } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                            strDocname2 = mGalleryArrayList.get(i).getStrDocName();

                            if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                                File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                                requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                                mMultipartBody2 = MultipartBody.Part.createFormData("photo2", getAlphaNumericStringRetrofit() + ".jpg", requestFile2);
                            }
                        } else {
                            strDocname2 = mGalleryArrayList.get(i).getStrImageName();

                            if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                                File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                                requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                                mMultipartBody2 = MultipartBody.Part.createFormData("photo2", getAlphaNumericStringRetrofit() + ".jpg", requestFile2);
                            }
                        }

                    } else {

                        strPhoto2 = mGalleryArrayList.get(i).getStrImagePath();
                        strDocname2 = "";

                        if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {

                                try {
                                    final InputStream bs = new URL(mGalleryArrayList.get(i).getStrImagePath()).openStream();

                                    Uri savedFileUri = savePDFFile(mActivity, bs, "files/pdf", System.currentTimeMillis() + ".pdf", "jaohar");
                                    Log.d("URI: ", savedFileUri.toString());
                                    String strPDFPath = getRealPathFromURI(mActivity, savedFileUri);
                                    Log.d(TAG, "File Path: " + strPDFPath);

                                    File file = new File(strPDFPath);

                                    requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                                    mMultipartBody2 = MultipartBody.Part.createFormData("photo2", file.getName(), requestFile2);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    hideProgressDialog();
                                    sendProgressBar.setVisibility(GONE);
                                    sendIV.setVisibility(VISIBLE);
                                }
                            } else {

                                String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
                                File folder = new File(extStorageDirectory, "pdf");
                                folder.mkdir();
                                File file = new File(folder, "Read.pdf");
                                try {
                                    file.createNewFile();
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                                DownloadFile(mGalleryArrayList.get(i).getStrImagePath(), file);

                                requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                                mMultipartBody2 = MultipartBody.Part.createFormData("photo2", file.getName(), requestFile2);
                            }

                        } else {
                            bitmap2 = getBitmapFromURL(mGalleryArrayList.get(i).getStrImagePath());

                            if (bitmap2 != null) {
                                requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), getbyteArray(bitmap2));
                                mMultipartBody2 = MultipartBody.Part.createFormData("photo2", getAlphaNumericStringRetrofit() + ".jpg", requestFile2);
                            }
                        }
                    }
                } else if (i == 2) {
                    if (mGalleryArrayList.get(i).getByteArray() != null) {
                        strPhoto3 = "";
                        byteArray3 = mGalleryArrayList.get(i).getByteArray();
                        if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                            strDocname3 = mGalleryArrayList.get(i).getStrDocName();

                            File file = null;
                            try {
                                file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                            mMultipartBody3 = MultipartBody.Part.createFormData("photo3", file.getName(), requestFile3);

                        } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                            strDocname3 = mGalleryArrayList.get(i).getStrDocName();

                            if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                                File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                                requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                                mMultipartBody3 = MultipartBody.Part.createFormData("photo3", getAlphaNumericStringRetrofit() + ".jpg", requestFile3);
                            }
                        } else {
                            strDocname3 = mGalleryArrayList.get(i).getStrImageName();

                            if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                                File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                                requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                                mMultipartBody3 = MultipartBody.Part.createFormData("photo3", getAlphaNumericStringRetrofit() + ".jpg", requestFile3);
                            }
                        }

                    } else {
                        strPhoto3 = mGalleryArrayList.get(i).getStrImagePath();
                        strDocname3 = "";

                        if (mGalleryArrayList.get(i).getStrType().equals("doc")) {

                            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {

                                try {
                                    final InputStream bs = new URL(mGalleryArrayList.get(i).getStrImagePath()).openStream();

                                    Uri savedFileUri = savePDFFile(mActivity, bs, "files/pdf", System.currentTimeMillis() + ".pdf", "jaohar");
                                    Log.d("URI: ", savedFileUri.toString());
                                    String strPDFPath = getRealPathFromURI(mActivity, savedFileUri);
                                    Log.d(TAG, "File Path: " + strPDFPath);

                                    File file = new File(strPDFPath);

                                    requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                                    mMultipartBody3 = MultipartBody.Part.createFormData("photo3", file.getName(), requestFile3);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    hideProgressDialog();
                                    sendProgressBar.setVisibility(GONE);
                                    sendIV.setVisibility(VISIBLE);
                                }
                            } else {

                                String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
                                File folder = new File(extStorageDirectory, "pdf");
                                folder.mkdir();
                                File file = new File(folder, "Read.pdf");
                                try {
                                    file.createNewFile();
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                                DownloadFile(mGalleryArrayList.get(i).getStrImagePath(), file);

                                requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                                mMultipartBody3 = MultipartBody.Part.createFormData("photo3", file.getName(), requestFile3);
                            }

                        } else {

                            bitmap3 = getBitmapFromURL(mGalleryArrayList.get(i).getStrImagePath());

                            if (bitmap3 != null) {
                                requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), getbyteArray(bitmap3));
                                mMultipartBody3 = MultipartBody.Part.createFormData("photo3", getAlphaNumericStringRetrofit() + ".jpg", requestFile3);
                            }
                        }
                    }
                } else if (i == 3) {
                    if (mGalleryArrayList.get(i).getByteArray() != null) {
                        strPhoto4 = "";
                        byteArray4 = mGalleryArrayList.get(i).getByteArray();
                        if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                            strDocname4 = mGalleryArrayList.get(i).getStrDocName();

                            File file = null;
                            try {
                                file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                            mMultipartBody4 = MultipartBody.Part.createFormData("photo4", file.getName(), requestFile4);

                        } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                            strDocname4 = mGalleryArrayList.get(i).getStrDocName();

                            if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                                File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                                requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                                mMultipartBody4 = MultipartBody.Part.createFormData("photo4", getAlphaNumericStringRetrofit() + ".jpg", requestFile4);
                            }
                        } else {
                            strDocname4 = mGalleryArrayList.get(i).getStrImageName();

                            if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                                File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                                requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                                mMultipartBody4 = MultipartBody.Part.createFormData("photo4", getAlphaNumericStringRetrofit() + ".jpg", requestFile4);
                            }
                        }

                    } else {
                        strPhoto4 = mGalleryArrayList.get(i).getStrImagePath();
                        strDocname4 = "";

                        if (mGalleryArrayList.get(i).getStrType().equals("doc")) {

                            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {

                                try {
                                    final InputStream bs = new URL(mGalleryArrayList.get(i).getStrImagePath()).openStream();

                                    Uri savedFileUri = savePDFFile(mActivity, bs, "files/pdf", System.currentTimeMillis() + ".pdf", "jaohar");
                                    Log.d("URI: ", savedFileUri.toString());
                                    String strPDFPath = getRealPathFromURI(mActivity, savedFileUri);
                                    Log.d(TAG, "File Path: " + strPDFPath);

                                    File file = new File(strPDFPath);

                                    requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                                    mMultipartBody4 = MultipartBody.Part.createFormData("photo4", file.getName(), requestFile4);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    hideProgressDialog();
                                    sendProgressBar.setVisibility(GONE);
                                    sendIV.setVisibility(VISIBLE);
                                }
                            } else {

                                String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
                                File folder = new File(extStorageDirectory, "pdf");
                                folder.mkdir();
                                File file = new File(folder, "Read.pdf");
                                try {
                                    file.createNewFile();
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                                DownloadFile(mGalleryArrayList.get(i).getStrImagePath(), file);

                                requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                                mMultipartBody4 = MultipartBody.Part.createFormData("photo4", file.getName(), requestFile4);
                            }

                        } else {

                            bitmap4 = getBitmapFromURL(mGalleryArrayList.get(i).getStrImagePath());

                            if (bitmap4 != null) {
                                requestFile4 = RequestBody.create(MediaType.parse("multipart/form-data"), getbyteArray(bitmap4));
                                mMultipartBody4 = MultipartBody.Part.createFormData("photo4", getAlphaNumericStringRetrofit() + ".jpg", requestFile4);
                            }
                        }
                    }
                } else if (i == 4) {
                    if (mGalleryArrayList.get(i).getByteArray() != null) {
                        strPhoto5 = "";
                        byteArray5 = mGalleryArrayList.get(i).getByteArray();
                        if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                            strDocname5 = mGalleryArrayList.get(i).getStrDocName();

                            File file = null;
                            try {
                                file = FileUtil.from(mActivity, mGalleryArrayList.get(i).getmUri());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                            mMultipartBody5 = MultipartBody.Part.createFormData("photo5", file.getName(), requestFile5);

                        } else if (mGalleryArrayList.get(i).getStrType().equals("video")) {
                            strDocname5 = mGalleryArrayList.get(i).getStrDocName();

                            if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                                File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                                requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                                mMultipartBody5 = MultipartBody.Part.createFormData("photo5", getAlphaNumericStringRetrofit() + ".jpg", requestFile5);
                            }
                        } else {
                            strDocname5 = mGalleryArrayList.get(i).getStrImageName();

                            if (mGalleryArrayList.get(i).getStrImagePath() != null && !mGalleryArrayList.get(i).getStrImagePath().equals("")) {
                                File propertyImageFile = new File(mGalleryArrayList.get(i).getStrImagePath());
                                requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
                                mMultipartBody5 = MultipartBody.Part.createFormData("photo5", getAlphaNumericStringRetrofit() + ".jpg", requestFile5);
                            }
                        }


                    } else {
                        strPhoto5 = mGalleryArrayList.get(i).getStrImagePath();
                        strDocname5 = "";

                        if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {

                                try {
                                    final InputStream bs = new URL(mGalleryArrayList.get(i).getStrImagePath()).openStream();

                                    Uri savedFileUri = savePDFFile(mActivity, bs, "files/pdf", System.currentTimeMillis() + ".pdf", "jaohar");
                                    Log.d("URI: ", savedFileUri.toString());
                                    String strPDFPath = getRealPathFromURI(mActivity, savedFileUri);
                                    Log.d(TAG, "File Path: " + strPDFPath);

                                    File file = new File(strPDFPath);

                                    requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                                    mMultipartBody5 = MultipartBody.Part.createFormData("photo5", file.getName(), requestFile5);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    hideProgressDialog();
                                    sendProgressBar.setVisibility(GONE);
                                    sendIV.setVisibility(VISIBLE);
                                }
                            } else {

                                String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
                                File folder = new File(extStorageDirectory, "pdf");
                                folder.mkdir();
                                File file = new File(folder, "Read.pdf");
                                try {
                                    file.createNewFile();
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                                DownloadFile(mGalleryArrayList.get(i).getStrImagePath(), file);

                                requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                                mMultipartBody5 = MultipartBody.Part.createFormData("photo5", file.getName(), requestFile5);
                            }

                        } else {

                            bitmap5 = getBitmapFromURL(mGalleryArrayList.get(i).getStrImagePath());

                            if (bitmap5 != null) {
                                requestFile5 = RequestBody.create(MediaType.parse("multipart/form-data"), getbyteArray(bitmap5));
                                mMultipartBody5 = MultipartBody.Part.createFormData("photo5", getAlphaNumericStringRetrofit() + ".jpg", requestFile5);
                            }
                        }
                    }
                } else if (i == 5) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        if (mGalleryArrayList.get(i).getStrImagePath().contains(".pdf")) {
                            strPhoto6 = mGalleryArrayList.get(i).getStrImagePath();
                        } else {
                            strDoc = Utilities.convertByteArrayToBase64(mGalleryArrayList.get(i).getByteArray());
                            strDocumentNameforSend = mGalleryArrayList.get(i).getStrDocName();
                        }
                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        if (mGalleryArrayList.get(i).getStrImagePath().contains("http")) {
                            strPhoto6 = mGalleryArrayList.get(i).getStrImagePath();
                        } else {
                            strPhoto6 = ImageUtils.getInstant().getBase64FromBitmap(mGalleryArrayList.get(i).getmBitmap());
                        }
                    }
                } else if (i == 6) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        if (mGalleryArrayList.get(i).getStrImagePath().contains(".pdf")) {
                            strPhoto7 = mGalleryArrayList.get(i).getStrImagePath();
                        } else {
                            strDoc = Utilities.convertByteArrayToBase64(mGalleryArrayList.get(i).getByteArray());
                            strDocumentNameforSend = mGalleryArrayList.get(i).getStrDocName();
                        }
                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        if (mGalleryArrayList.get(i).getStrImagePath().contains("http")) {
                            strPhoto7 = mGalleryArrayList.get(i).getStrImagePath();
                        } else {
                            strPhoto7 = ImageUtils.getInstant().getBase64FromBitmap(mGalleryArrayList.get(i).getmBitmap());
                        }
                    }
                } else if (i == 7) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        if (mGalleryArrayList.get(i).getStrImagePath().contains(".pdf")) {
                            strPhoto8 = mGalleryArrayList.get(i).getStrImagePath();
                        } else {
                            strDoc = Utilities.convertByteArrayToBase64(mGalleryArrayList.get(i).getByteArray());
                            strDocumentNameforSend = mGalleryArrayList.get(i).getStrDocName();
                        }
                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        if (mGalleryArrayList.get(i).getStrImagePath().contains("http")) {
                            strPhoto8 = mGalleryArrayList.get(i).getStrImagePath();
                        } else {
                            strPhoto8 = ImageUtils.getInstant().getBase64FromBitmap(mGalleryArrayList.get(i).getmBitmap());
                        }
                    }
                } else if (i == 8) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        if (mGalleryArrayList.get(i).getStrImagePath().contains(".pdf")) {
                            strPhoto9 = mGalleryArrayList.get(i).getStrImagePath();
                        } else {
                            strDoc = Utilities.convertByteArrayToBase64(mGalleryArrayList.get(i).getByteArray());
                            strDocumentNameforSend = mGalleryArrayList.get(i).getStrDocName();
                        }
                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        if (mGalleryArrayList.get(i).getStrImagePath().contains("http")) {
                            strPhoto9 = mGalleryArrayList.get(i).getStrImagePath();
                        } else {
                            strPhoto9 = ImageUtils.getInstant().getBase64FromBitmap(mGalleryArrayList.get(i).getmBitmap());
                        }
                    }
                } else if (i == 9) {
                    if (mGalleryArrayList.get(i).getStrType().equals("doc")) {
                        if (mGalleryArrayList.get(i).getStrImagePath().contains(".pdf")) {
                            strPhoto10 = mGalleryArrayList.get(i).getStrImagePath();
                        } else {
                            strDoc = Utilities.convertByteArrayToBase64(mGalleryArrayList.get(i).getByteArray());
                            strDocname1 = mGalleryArrayList.get(i).getStrDocName();
                        }
                    } else if (mGalleryArrayList.get(i).getStrType().equals("img")) {
                        if (mGalleryArrayList.get(i).getStrImagePath().contains("http")) {
                            strPhoto10 = mGalleryArrayList.get(i).getStrImagePath();
                        } else {
                            strPhoto10 = ImageUtils.getInstant().getBase64FromBitmap(mGalleryArrayList.get(i).getmBitmap());
                        }
                    }
                }
            }
        }

        RequestBody message = RequestBody.create(MediaType.parse("multipart/form-data"), textED.getText().toString());
        RequestBody message_id = RequestBody.create(MediaType.parse("multipart/form-data"), strMessageID);
        RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.editChatMessageRequestNew(message, message_id, user_id, mMultipartBody1,
                mMultipartBody2, mMultipartBody3, mMultipartBody4,
                mMultipartBody5, mMultipartBody6, mMultipartBody7,
                mMultipartBody8, mMultipartBody9, mMultipartBody10).enqueue(new Callback<AddNewMessageModel>() {
            @Override
            public void onResponse(Call<AddNewMessageModel> call, Response<AddNewMessageModel> response) {
                hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                sendProgressBar.setVisibility(GONE);
                sendIV.setVisibility(VISIBLE);

                if (response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        textED.setText("");
                        mTyping = false;
                        mSocket.emit("type", strRoomID, false);
                        if (Utilities.isNetworkAvailable(mActivity) == false) {
                            showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                        } else {
                            if (progressBottomPB != null) {
                                progressBottomPB.setVisibility(GONE);
                            }
                            mGalleryArrayList.clear();
                            setUpGalleryAdapter();
                            list.clear();
                            strPhoto1 = "";
                            strPhoto2 = "";
                            strPhoto3 = "";
                            strPhoto4 = "";
                            strPhoto5 = "";
                            strPhoto6 = "";
                            strPhoto7 = "";
                            strPhoto8 = "";
                            strPhoto9 = "";
                            strPhoto10 = "";
                            strDocname1 = "";
                            strDocname2 = "";
                            strDocname3 = "";
                            strDocname4 = "";
                            strDocname5 = "";
                            byteArray1 = null;
                            byteArray2 = null;
                            byteArray3 = null;
                            byteArray4 = null;
                            byteArray5 = null;
                            strDoc = "";
                            strDocumentNameforSend = "";
                            imageLL.setVisibility(VISIBLE);
                            galaryLL.setVisibility(View.GONE);
                            messageReplyRL.setVisibility(GONE);
                            isEditMessage = false;
                            editMessageText.setText("");
                            textED.setText("");
                            isAddClick = true;

                            AllChatsItem addNewMessageModel = response.body().getAppendData();
                            mGetAllChatMessagesAllChatModel.set(currentMSGpositon, addNewMessageModel);
                            mAdapterNew.notifyItemChanged(currentMSGpositon);

                            if (mGetAllChatMessagesAllChatModel != null) {
                                dataRV.scrollToPosition(0);
                            } else {
                                setAdapterNew();
                            }

                            // perform the sending message attempt.
                            Gson gson = new Gson();
                            String mOjectString = gson.toJson(response.body());
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(mOjectString);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.e("TAG", "*****Msg****" + mOjectString);
                            mSocket.emit("newMessage", strRoomID, obj);
                        }

                    } else if (response.body().getStatus() == 101) {
                        Log.e(TAG, "Unexpected*********:" + message);
                        onBackPressed();
                    } else {
                        showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
                    }
                }
            }

            @Override
            public void onFailure(Call<AddNewMessageModel> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + t.toString());
                sendProgressBar.setVisibility(GONE);
                sendIV.setVisibility(VISIBLE);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        /* implement socket */
        if (!Utilities.isNetworkAvailable(Objects.requireNonNull(mActivity))) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
//            JaoharApplication app = (JaoharApplication) getApplication();
//            mSocket = app.getSocket();
//            mSocket.emit("ConncetedChat", strRoomID);
//            mSocket.on(Socket.EVENT_CONNECT, onConnect);
//            mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
//            mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
//            mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
//            mSocket.on("newMessage", onNewMessage);
//            mSocket.on("ChatStatus", onUserJoined);
//            mSocket.on("type", onTyping);
//            mSocket.connect();

            /* Implement API to update header data */
            ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
            Call<GetAllChatMessagesModel> call1 = mApiInterface.getAllChatMessagesAndroid(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), strRoomID, page_num);
            call1.enqueue(new Callback<GetAllChatMessagesModel>() {
                @Override
                public void onResponse(Call<GetAllChatMessagesModel> call, Response<GetAllChatMessagesModel> response) {
                    Log.e(TAG, "***URLResponce***" + response);

                    if (response.body() != null) {
                        if (response.body().getStatus().equals("1")) {

                            roomDetailsonline_state = response.body().getRoomDetail().getOnlineState();

                            if (response.body().getRoomDetail().getOnlineState() != null) {
                                if (response.body().getRoomDetail().getOnlineState().equals("Available")) {
                                    item_Image_Status.setBackgroundResource(R.drawable.bg_green_status);
                                } else {
                                    item_Image_Status.setBackgroundResource(R.drawable.bg_red_status);
                                }
                            } else {
                                item_Image_Status.setVisibility(View.GONE);
                            }

                            if (!response.body().getRoomDetail().getOnlineTime().equals("")) {
                                roomDetailsonline_time = response.body().getRoomDetail().getOnlineTime();
                                Log.e(TAG, "**Time**" + roomDetailsonline_time);

                                // convert seconds to milliseconds
                                Date date = new Date(Long.parseLong(roomDetailsonline_time) * 1000L);
                                // the format of your date
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                String formattedDate = sdf.format(date);

                                if (roomDetailsonline_state != null) {
                                    if (roomDetailsonline_state.equals("Available")) {
                                        textActiveStatus.setText("Active Now");
                                    } else {
                                        textActiveStatus.setText(covertTimeToText(formattedDate));
                                    }
                                }
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<GetAllChatMessagesModel> call, Throwable t) {
                    AlertDialogManager.hideProgressDialog();
                    Log.e(TAG, "***Error**" + t.getMessage());
                    Toast.makeText(mActivity, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /* *
     * Execute API for getting Forum Chat All Messages // API  GetAllStaffForumMessages
     * @param
     * @user_id
     * @forum_id
     * */
    public void executeGetALlChatMessagesRetrofit() {
        if (strLastPage.equals("FALSE")) {
            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(GONE);
            }
            if (page_num == 1) {
                showProgressDialog(mActivity);
            }
        } else {
            if (page_num == 1) {
                if (isSwipeRefresh) {
                }
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
            }
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetAllChatMessagesModel> call1 = mApiInterface.getAllChatMessagesAndroid(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), strRoomID, page_num);
        call1.enqueue(new Callback<GetAllChatMessagesModel>() {
            @Override
            public void onResponse(Call<GetAllChatMessagesModel> call, Response<GetAllChatMessagesModel> response) {
                Log.e(TAG, "***URLResponce***" + response);
                hideProgressDialog();
                if (isSwipeRefresh) {
                    swipeTorefefresh.setRefreshing(false);
                }

                if (response.body() != null) {
                    if (response.body().getStatus().equals("1")) {

                        if (!response.body().getData().getLastPage().equals("")) {
                            strLastPage = response.body().getData().getLastPage();
                        }

                        for (int i = 0; i < response.body().getData().getAllChats().size(); i++) {

                            if (!response.body().getData().getAllChats().get(i).getSenderId().equals("")) {
                                if (response.body().getData().getAllChats().get(i).getSenderId().equals(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""))) {
                                    response.body().getData().getAllChats().get(i).setIntType(JaoharConstants.Sender);
                                } else if (response.body().getData().getAllChats().get(i).getSenderId().equals(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""))) {
                                    response.body().getData().getAllChats().get(i).setIntType(JaoharConstants.Sender);
                                } else {
                                    response.body().getData().getAllChats().get(i).setIntType(JaoharConstants.Receiver);
                                }
                            }
                        }

                        roomDetailsonline_state = response.body().getRoomDetail().getOnlineState();

                        if (response.body().getRoomDetail() != null) {

                            if (response.body().getRoomDetail().getOnlineState() != null) {
                                if (response.body().getRoomDetail().getOnlineState().equals("Available")) {
                                    item_Image_Status.setBackgroundResource(R.drawable.bg_green_status);
                                } else {
                                    item_Image_Status.setBackgroundResource(R.drawable.bg_red_status);
                                }
                            } else {
                                item_Image_Status.setVisibility(View.GONE);
                            }

                            if (!response.body().getRoomDetail().getOnlineTime().equals("")) {
                                roomDetailsonline_time = response.body().getRoomDetail().getOnlineTime();
                                Log.e(TAG, "**Time**" + roomDetailsonline_time);

                                // convert seconds to milliseconds
                                Date date = new Date(Long.parseLong(roomDetailsonline_time) * 1000L);
                                // the format of your date
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                String formattedDate = sdf.format(date);

                                if (roomDetailsonline_state != null) {
                                    if (roomDetailsonline_state.equals("Available")) {
                                        textActiveStatus.setText("Active Now");
                                    } else {
                                        textActiveStatus.setText(covertTimeToText(formattedDate));
                                    }
                                }
                            }
                        }

                        List<AllChatsItem> mGetAllLoadMore = new ArrayList<>();

                        if (page_num == 1) {
                            mGetAllChatMessagesAllChatModel = response.body().getData().getAllChats();
                        } else if (page_num > 1) {
                            mGetAllLoadMore = response.body().getData().getAllChats();
                        }

                        if (mGetAllLoadMore.size() > 0) {
                            mGetAllChatMessagesAllChatModel.addAll(mGetAllLoadMore);
                        }

                        if (page_num == 1) {
                            setAdapterNew();
                        } else {
                            mAdapterNew.updateLastPageData(strLastPage);
                            mAdapterNew.notifyDataSetChanged();
                        }
                    } else {
                        if (strOnlineState != null) {
                            if (strOnlineState.equals("Available")) {
                                textActiveStatus.setText("Active Now");
                            } else {
                                textActiveStatus.setText(strRole);
                            }
                        }
                        setAdapterNew();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetAllChatMessagesModel> call, Throwable t) {
                hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
                Toast.makeText(mActivity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /* *
     * Setting up Adapter to show chat in recycler view
     * */
    void setAdapterNew() {
        mAdapterNew = new ChatMessagesAdapter1(mActivity, mGetAllChatMessagesAllChatModel,
                mAdd_Edit_DelInterface, strLastPage, onClickInterface);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager.setReverseLayout(true);
        dataRV.setLayoutManager(linearLayoutManager);
        if (page_num == 1) {
            if (textED.hasFocus()) {
                if (mGetAllChatMessagesAllChatModel != null) {
                    dataRV.scrollToPosition(0);
                }
            }
            if (mGetAllChatMessagesAllChatModel != null) {
                dataRV.scrollToPosition(0);
            }
        }
        dataRV.setItemAnimator(new DefaultItemAnimator());
        dataRV.setAdapter(mAdapterNew);
        mGalleryArrayList.clear();
    }

    private String readTextFromUri(Uri uri) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        try (InputStream inputStream =
                     getContentResolver().openInputStream(uri);
             BufferedReader reader = new BufferedReader(
                     new InputStreamReader(Objects.requireNonNull(inputStream)))) {
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
        }
        return stringBuilder.toString();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                try {
                    if (data != null) {
                        try {
                            ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);//images.get(i).path
                            for (int i = 0, l = images.size(); i < l; i++) {
                                byte[] btye = getFileDataFromBitmap(mActivity, getThumbnailBitmap(images.get(i).path, 1000));
                                AddGalleryImagesModel model = new AddGalleryImagesModel();
                                model.setStrImagePath(images.get(i).path);
                                model.setByteArray(btye);
                                model.setStrType("img");
                                model.setStrImageName(images.get(i).path.substring(images.get(i).path.lastIndexOf("/") + 1));
                                model.setmBitmap(getThumbnailBitmap(images.get(i).path, 1000));
                                if (mGalleryArrayList.size() < 5) {
                                    galaryLL.setVisibility(VISIBLE);
                                    imageLL.setVisibility(VISIBLE);
                                    mGalleryArrayList.add(model);
                                }
                            }
                            /*Set GalleryImages
                             * in recycler View*/
                            setUpGalleryAdapter();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e(TAG, "*****No Picture Selected****");
                        return;
                    }
                } finally {
                }
            } else if (requestCode == CAMERA_REQUEST) {
                try {
                    //Bitmap thumb = ImageUtils.getInstant().rotateBitmapOrientation(mStoragePath);//getCompressedBitmap(mStoragePath);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 0;
                    Bitmap thumb = BitmapFactory.decodeFile(mStoragePath, options);
                    ExifInterface exifInterface = null;
                    try {
                        exifInterface = new ExifInterface(mStoragePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                    Matrix matrix = new Matrix();
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            matrix.setRotate(90);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            matrix.setRotate(180);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            matrix.setRotate(270);
                            break;
                        case ExifInterface.ORIENTATION_NORMAL:
                        default:
                    }

                    Bitmap rotate = getThumbnailBitmap(mStoragePath, 1000);
                    byte[] btye = getFileDataFromBitmap(mActivity, rotate);
                    AddGalleryImagesModel model = new AddGalleryImagesModel();
                    model.setStrImagePath(mStoragePath);
                    model.setByteArray(btye);
                    model.setStrImageName(mStoragePath.substring(mStoragePath.lastIndexOf("/") + 1));
                    model.setStrType("img");
                    // model.setStrImageBitmap(strImageBase64);
                    model.setmBitmap(rotate);
                    if (mGalleryArrayList.size() < 5) {
                        galaryLL.setVisibility(VISIBLE);
                        imageLL.setVisibility(VISIBLE);
                        mGalleryArrayList.add(model);
                    }

                    /*Set GalleryImages
                     * in recycler View*/
                    setUpGalleryAdapter();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 505 && resultCode == Activity.RESULT_OK) {
                try {
                    Uri fileuri = data.getData();
                    String docFilePath = getFileNameByUri(mActivity, fileuri);

                    AddGalleryImagesModel model = new AddGalleryImagesModel();
                    model.setStrImagePath(docFilePath);
                    model.setmUri(fileuri);
                    model.setByteArray(Utilities.convertDocumentToByteArray(docFilePath));
                    model.setStrDocName(docFilePath.substring(docFilePath.lastIndexOf("/") + 1));
                    model.setStrType("doc");

                    if (mGalleryArrayList.size() < 5) {
                        galaryLL.setVisibility(VISIBLE);
                        imageLL.setVisibility(VISIBLE);
                        mGalleryArrayList.add(model);
                    }
                    /*Set GalleryImages in recycler View*/
                    setUpGalleryAdapter();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 666 && resultCode == Activity.RESULT_OK) {
                Uri vd = fileUri;
                Uri videoUri;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    videoUri = fileUri;
                } else {
                    videoUri = data.getData();
                }
                Log.i("", "" + videoUri);

                //To Show The Video Thumbnail......
                String filePath = getRealPathFromURIView(mActivity, videoUri, "VIDEO");
                Intent mIntent = new Intent(mActivity, VideoTrimerActivity.class);
                mIntent.putExtra("filepath", filePath);
                startActivityForResult(mIntent, 201);
            } else if (resultCode == RESULT_OK && requestCode == 777) {
                Uri videoUri = data.getData();
                Log.i("", "" + videoUri);

                try {
                    if (videoUri != null) {
                        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//                      use one of overloaded setDataSource() functions to set your data source
                        retriever.setDataSource(getApplicationContext(), videoUri);
                        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                        int timeInMillisec = Integer.parseInt(time);
                        int temp = timeInMillisec / 1000;
                        retriever.release();
                        if (temp <= 30) {
                            String mUri = RealPathUtil.getRealPath(mActivity, videoUri);
                            Intent mIntent = new Intent(mActivity, VideoTrimerActivity.class);
                            mIntent.putExtra("filepath", mUri);
                            startActivityForResult(mIntent, 201);

                        } else {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(mActivity, "Maximum 30 Seconds", Toast.LENGTH_LONG).show();
                                    Log.e("Maximum 30 Seconds", "*******************Maximum 30 Seconds********************");
                                }
                            }, 3000);

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 201) {
                if (data != null) {
                    InputStream iStream = null;
                    Uri mImageUri = Uri.parse(data.getStringExtra("filepath1"));
                    String fileName = mImageUri.getPath();
                    galaryLL.setVisibility(VISIBLE);
                    imageLL.setVisibility(VISIBLE);
//                Log.e("test","URITESTName===="+fileName.substring(fileName.lastIndexOf("/") + 1));
                    /**Convert URI to Byte Array**/
//                    iStream = getApplicationContext().getContentResolver().openInputStream(mImageUri);
//                    byte[] inputData =getBytes(iStream);
//                    String filepath = getRealPathFromURI(mImageUri);

                    AddGalleryImagesModel model1 = new AddGalleryImagesModel();
                    model1.setStrImagePath(fileName);
                    model1.setByteArray(Utilities.convertDocumentToByteArray(fileName));
                    model1.setStrDocName(fileName.substring(fileName.lastIndexOf("/") + 1));
                    model1.setStrType("video");


                    if (mGalleryArrayList.size() < 5) {
                        isAddVideo = true;

                        mGalleryArrayList.add(model1);
                    }

                    /*Set GalleryImages
                     * in recycler View*/
                    setUpGalleryAdapter();
                }
            }
        }
    }

    // Getting File name from uri
    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    /**
     * Getting Byte Array From Input File Stream
     **/
    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 512;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    private void setUpGalleryAdapter() {
        if (mGalleryArrayList.size() >= 1) {
            Log.e("TAG", "DATASIZE" + "Video1ImageVisisble" + mGalleryArrayList.size());
            galaryLL.setVisibility(VISIBLE);

        } else {
            if (isEditMessage == true) {
                imageLL.setVisibility(VISIBLE);
                galaryLL.setVisibility(VISIBLE);

            } else if (isReplyMessage == true) {
                imageLL.setVisibility(VISIBLE);
                galaryLL.setVisibility(VISIBLE);
            } else {
                imageLL.setVisibility(VISIBLE);
                galaryLL.setVisibility(View.GONE);
            }
        }

        Log.e("TAG", "DATASIZE" + "Video1" + mGalleryArrayList.size());
        mImageAdapter = new AddImageListAdapter(mActivity, mGalleryArrayList, mDeleteImagesInterface);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        galleryRecyclerView.setLayoutManager(horizontalLayoutManagaer);
        galleryRecyclerView.setAdapter(mImageAdapter);
    }

    public String getRealPathFromURI(Uri contentUri) {
        String path = null;
        String[] proj = {MediaStore.MediaColumns.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(column_index);
        }
        cursor.close();
        return path;
    }

    /**
     * returns the thumbnail image bitmap from the given url
     *
     * @param path
     * @param thumbnailSize
     * @return
     **/
    private Bitmap getThumbnailBitmap(final String path, final int thumbnailSize) {
        Bitmap bitmap;
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bounds);
        if ((bounds.outWidth == -1) || (bounds.outHeight == -1)) {
            bitmap = null;
        }
        int originalSize = (bounds.outHeight > bounds.outWidth) ? bounds.outHeight
                : bounds.outWidth;
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = originalSize / thumbnailSize;
        bitmap = BitmapFactory.decodeFile(path, opts);
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(270);
                break;
            case ExifInterface.ORIENTATION_NORMAL:
            default:
        }
        Bitmap rotate = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return rotate;
    }

    /* *
     * PopUp to Delete or Cancel the Message
     * */
    public void deleteConfirmDialog(String strMessage) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(strMessage);
        TextView txtConfirm = deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = deleteConfirmDialog.findViewById(R.id.txtCacel);
        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /* *
                 * Execute API to Delete Forum  Single message
                 * */
                executeDeleteAPINew();
            }
        });
        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });
        deleteConfirmDialog.show();
    }

    private void executeDeleteAPINew() {
        AlertDialogManager.showProgressDialog(mActivity);

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<DeleteMessageModel> call1 = mApiInterface.deleteChatMessageRequestNew(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""), strMessageID);
        call1.enqueue(new Callback<DeleteMessageModel>() {
            @Override
            public void onResponse(Call<DeleteMessageModel> call, Response<DeleteMessageModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);

                if (response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        if (Utilities.isNetworkAvailable(mActivity) == false) {
                            showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                        } else {
                            //*Execute API For Getting List *//
                            if (progressBottomPB != null) {
                                progressBottomPB.setVisibility(GONE);
                            }

                            strDocumentNameforSend = "";
                            messageReplyRL.setVisibility(GONE);
                            isEditMessage = false;
                            editMessageText.setText("");
                            textED.setText("");
                            isAddClick = true;

                            AppendData addNewMessageModel = response.body().getAppendData();

                            // perform the sending message attempt.
                            Gson gson = new Gson();
                            String mOjectString = gson.toJson(response.body());
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(mOjectString);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.e("TAG", "*****Msg****" + mOjectString);
                            mSocket.emit("newMessage", strRoomID, obj);

                            Log.e(TAG, "Size - " + mGetAllChatMessagesAllChatModel.size());
                            for (int i = 0; i < mGetAllChatMessagesAllChatModel.size(); i++) {
                                if (mGetAllChatMessagesAllChatModel.get(i).getChatId().equals(strMessageID)) {
                                    mGetAllChatMessagesAllChatModel.remove(i);
                                }
                            }
                            mAdapterNew.notifyDataSetChanged();
                        }
                    } else {
                        showAlertDialog(mActivity, getResources().getString(R.string.app_name), response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<DeleteMessageModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     *
     * It's return date  before one week timestamp
     *
     *  like return
     *
     *  1 day ago
     *  2 days ago
     *  5 days ago
     *  2 weeks ago
     *  2 months ago
     *  2 years ago
     *
     * */
    public String covertTimeToText(String dataDate) {

        String convTime = "";

        String prefix = "Active";
        String suffix = "Ago";

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date pasTime = dateFormat.parse(dataDate);

            Date nowTime = new Date();

            long dateDiff = nowTime.getTime() - pasTime.getTime();

            if (nowTime.getTime() == pasTime.getTime()) {
                convTime = "Active Now";
            } else {
                long second = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
                long minute = TimeUnit.MILLISECONDS.toMinutes(dateDiff);
                long hour = TimeUnit.MILLISECONDS.toHours(dateDiff);
                long day = TimeUnit.MILLISECONDS.toDays(dateDiff);

                if (second < 60) {
                    if (second == 1) {
                        convTime = prefix + " " + second + " Second " + suffix;
                    } else {
                        convTime = prefix + " " + second + " Seconds " + suffix;
                    }
                } else if (minute < 60) {
                    if (minute == 1) {
                        convTime = prefix + " " + minute + " Minute " + suffix;
                    } else {
                        convTime = prefix + " " + minute + " Minutes " + suffix;
                    }
                } else if (hour < 24) {
                    if (hour == 1) {
                        convTime = prefix + " " + hour + " Hour " + suffix;
                    } else {
                        convTime = prefix + " " + hour + " Hours " + suffix;
                    }
                } else if (day >= 7) {
                    if (day > 360) {
                        convTime = prefix + " " + (day / 360) + " Years " + suffix;
                    } else if (day > 30) {
                        convTime = prefix + " " + (day / 30) + " Months " + suffix;
                    } else {
                        convTime = prefix + " " + (day / 7) + " Week " + suffix;
                    }
                } else if (day < 7) {
                    if (day == 1) {
                        convTime = prefix + " " + day + " Day " + suffix;
                    } else {
                        convTime = prefix + " " + day + " Days " + suffix;
                    }
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("ConvTimeE", e.getMessage());
        }
        return convTime;
    }

    /**
     * Turn bitmap into byte array
     *
     * @param bitmap data
     * @returen byte array
     */
    public static byte[] getFileDataFromBitmap(Context context, Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public static String getRealPathFromURIView(Context context, Uri contentURI, String type) {
        String result = null;
        try {
            Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
            if (cursor == null) { // Source is Dropbox or other similar local file path
                result = contentURI.getPath();
                Log.d("TAG", "result******************" + result);
            } else {
                cursor.moveToFirst();
                int idx = 0;
                if (type.equalsIgnoreCase("IMAGE")) {
                    idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                } else if (type.equalsIgnoreCase("VIDEO")) {
                    idx = cursor.getColumnIndex(MediaStore.Video.VideoColumns.DATA);
                } else if (type.equalsIgnoreCase("AUDIO")) {
                    idx = cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DATA);
                }
                result = cursor.getString(idx);
                Log.d("TAG", "result*************else*****" + result);
                cursor.close();
            }
        } catch (Exception e) {
            Log.e("TAG", "Exception ", e);
        }
        return result;
    }

    public static long getFileId(Activity context, Uri fileUri) {
        Cursor cursor = context.managedQuery(fileUri, mediaColumns, null, null, null);
        if (cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID);
            int id = cursor.getInt(columnIndex);
            return id;
        }
        return 0;
    }

    /*
     * SetUp Gallary Adapter for editing the message
     * */
    private void performGallaryAdapterSetup(AllChatsItem mModel) {
        mGalleryArrayList.clear();
        setUpGalleryAdapter();
        if (!mModel.getDoc1().equals("")) {
            AddGalleryImagesModel model = new AddGalleryImagesModel();
            if (!Utilities.isDocAdded(mModel.getDoc1())) {
                if (Utilities.isVideoAdded(mModel.getDoc1())) {
                    model.setStrImagePath(mModel.getDoc1());
                    model.setStrType("video");
                } else {
                    model.setStrImagePath(mModel.getDoc1());
                    model.setStrType("img");
                }
            } else {
                model.setStrImagePath(mModel.getDoc1());
                model.setStrType("doc");
            }

            mGalleryArrayList.add(model);
        }
        if (!mModel.getDoc2().equals("")) {
            AddGalleryImagesModel model = new AddGalleryImagesModel();

            if (!Utilities.isDocAdded(mModel.getDoc2())) {
                if (Utilities.isVideoAdded(mModel.getDoc2())) {
                    model.setStrImagePath(mModel.getDoc2());
                    model.setStrType("video");
                } else {
                    model.setStrImagePath(mModel.getDoc2());
                    model.setStrType("img");
                }
            } else {
                model.setStrImagePath(mModel.getDoc2());
                model.setStrType("doc");
            }
            mGalleryArrayList.add(model);
        }
        if (!mModel.getDoc3().equals("")) {
            AddGalleryImagesModel model = new AddGalleryImagesModel();
            if (!Utilities.isDocAdded(mModel.getDoc3())) {
                if (Utilities.isVideoAdded(mModel.getDoc3())) {
                    model.setStrImagePath(mModel.getDoc3());
                    model.setStrType("video");
                } else {
                    model.setStrImagePath(mModel.getDoc3());
                    model.setStrType("img");
                }
            } else {
                model.setStrImagePath(mModel.getDoc3());
                model.setStrType("doc");
            }

            mGalleryArrayList.add(model);
        }
        if (!mModel.getDoc4().equals("")) {
            AddGalleryImagesModel model = new AddGalleryImagesModel();

            if (!Utilities.isDocAdded(mModel.getDoc4())) {
                if (Utilities.isVideoAdded(mModel.getDoc4())) {
                    model.setStrImagePath(mModel.getDoc4());
                    model.setStrType("video");
                } else {
                    model.setStrImagePath(mModel.getDoc4());
                    model.setStrType("img");
                }
            } else {
                model.setStrImagePath(mModel.getDoc4());
                model.setStrType("doc");
            }
            mGalleryArrayList.add(model);
        }
        if (!mModel.getDoc5().equals("")) {
            AddGalleryImagesModel model = new AddGalleryImagesModel();

            if (!Utilities.isDocAdded(mModel.getDoc5())) {
                if (Utilities.isVideoAdded(mModel.getDoc5())) {
                    model.setStrImagePath(mModel.getDoc5());
                    model.setStrType("video");
                } else {
                    model.setStrImagePath(mModel.getDoc5());
                    model.setStrType("img");
                }
            } else {
                model.setStrImagePath(mModel.getDoc5());
                model.setStrType("doc");
            }
            mGalleryArrayList.add(model);
        }
        if (!mModel.getDoc6().equals("")) {
            AddGalleryImagesModel model = new AddGalleryImagesModel();

            if (!Utilities.isDocAdded(mModel.getDoc6())) {
                if (Utilities.isVideoAdded(mModel.getDoc6())) {
                    model.setStrImagePath(mModel.getDoc6());
                    model.setStrType("video");
                } else {
                    model.setStrImagePath(mModel.getDoc6());
                    model.setStrType("img");
                }
            } else {
                model.setStrImagePath(mModel.getDoc6());
                model.setStrType("doc");
            }
            mGalleryArrayList.add(model);
        }
        if (!mModel.getDoc7().equals("")) {
            AddGalleryImagesModel model = new AddGalleryImagesModel();

            if (!Utilities.isDocAdded(mModel.getDoc7())) {
                if (Utilities.isVideoAdded(mModel.getDoc7())) {
                    model.setStrImagePath(mModel.getDoc7());
                    model.setStrType("video");
                } else {
                    model.setStrImagePath(mModel.getDoc7());
                    model.setStrType("img");
                }
            } else {
                model.setStrImagePath(mModel.getDoc7());
                model.setStrType("doc");
            }
            mGalleryArrayList.add(model);
        }
        if (!mModel.getDoc8().equals("")) {
            AddGalleryImagesModel model = new AddGalleryImagesModel();

            if (!Utilities.isDocAdded(mModel.getDoc8())) {
                if (Utilities.isVideoAdded(mModel.getDoc8())) {
                    model.setStrImagePath(mModel.getDoc8());
                    model.setStrType("video");
                } else {
                    model.setStrImagePath(mModel.getDoc8());
                    model.setStrType("img");
                }
            } else {
                model.setStrImagePath(mModel.getDoc8());
                model.setStrType("doc");
            }
            mGalleryArrayList.add(model);
        }
        if (!mModel.getDoc9().equals("")) {
            AddGalleryImagesModel model = new AddGalleryImagesModel();

            if (!Utilities.isDocAdded(mModel.getDoc9())) {
                if (Utilities.isVideoAdded(mModel.getDoc9())) {
                    model.setStrImagePath(mModel.getDoc9());
                    model.setStrType("video");
                } else {
                    model.setStrImagePath(mModel.getDoc9());
                    model.setStrType("img");
                }
            } else {
                model.setStrImagePath(mModel.getDoc9());
                model.setStrType("doc");
            }
            mGalleryArrayList.add(model);
        }
        if (!mModel.getDoc10().equals("")) {
            AddGalleryImagesModel model = new AddGalleryImagesModel();

            if (!Utilities.isDocAdded(mModel.getDoc10())) {
                if (Utilities.isVideoAdded(mModel.getDoc10())) {
                    model.setStrImagePath(mModel.getDoc10());
                    model.setStrType("video");
                } else {
                    model.setStrImagePath(mModel.getDoc10());
                    model.setStrType("img");
                }
            } else {
                model.setStrImagePath(mModel.getDoc10());
                model.setStrType("doc");
            }
            mGalleryArrayList.add(model);
        }


       /*Set GalleryImages
         in recycler View*/
        setUpGalleryAdapter();
    }

    /* *
     * Interface to delete images From  Array list
     * */
    DeleteImagesVessels mDeleteImagesInterface = new DeleteImagesVessels() {
        @Override
        public void deleteImagesVessels(AddGalleryImagesModel mGalleryModel, int position) {
            if (mGalleryArrayList != null) {
                mGalleryArrayList.remove(position);
                setUpGalleryAdapter();
            }
        }
    };

    OnClickInterface onClickInterface = new OnClickInterface() {
        @Override
        public void mOnClickInterface(int position) {
            currentMSGpositon = position;
        }
    };

    /* *
     * Interface to Edit , Reply, Delete Forum Message
     * */
    Add_Del_Edit_forumchatInterfaceNew mAdd_Edit_DelInterface = new Add_Del_Edit_forumchatInterfaceNew() {
        @Override
        public void mAdd_Del_ChatInterface(AllChatsItem mModel, View view, String strType, int position) {
            strMessageID = mModel.getChatId();
            currentMSGpositon = position;
            if (strType.equals("sender")) {
                PopupMenu popup = new PopupMenu(mActivity, view);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.reply_btn:
                                strReplyID = mModel.getChatId();
                                isReplyMessage = true;
                                isEditMessage = false;
                                mGalleryArrayList.clear();
                                setUpGalleryAdapter();
                                messageReplyRL.setVisibility(VISIBLE);
                                galaryLL.setVisibility(VISIBLE);
                                imageLL.setVisibility(GONE);

                                if (mModel.getMessage() != null && !mModel.getMessage().equals("")) {
                                    editMessageText.setText(html2text(mModel.getMessage()));
                                } else {
                                    editMessageText.setText("");
                                }

                                /* to show and hide images for reply case */
                                if (mModel.getImageCount() != null && !mModel.getImageCount().equals("")
                                        && !mModel.getImageCount().equals("0")) {
                                    imageRplyRL.setVisibility(VISIBLE);

                                    if (mModel.getImageCount().equals("1")) {
                                        blurPicRL.setVisibility(GONE);
                                        messagePicReplyIV.setVisibility(VISIBLE);

                                        if (!Utilities.isDocAdded(mModel.getDoc1())) {
                                            if (Utilities.isVideoAdded(mModel.getDoc1())) {
                                                messagePicReplyIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.video_icon));
                                            } else {
                                                Glide.with(mActivity).load(mModel.getDoc1()).into(messagePicReplyIV);
                                            }
                                        } else {
                                            messagePicReplyIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.icon_doc));
                                        }

                                    } else {
                                        blurPicRL.setVisibility(VISIBLE);
                                        messagePicReplyIV.setVisibility(VISIBLE);

                                        if (!Utilities.isDocAdded(mModel.getDoc1())) {
                                            if (Utilities.isVideoAdded(mModel.getDoc1())) {
                                                messagePicReplyIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.video_icon));
                                            } else {
                                                Glide.with(mActivity).load(mModel.getDoc1()).into(messagePicReplyIV);
                                            }
                                        } else {
                                            messagePicReplyIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.icon_doc));
                                        }

                                        int imageCount = Integer.parseInt(mModel.getImageCount()) - 1;
                                        String strImageCount = "+" + imageCount;
                                        textCountPicTv.setText(strImageCount);
                                    }
                                } else {
                                    imageRplyRL.setVisibility(GONE);
                                }
                                // EX : call intent if you want to swich to other activity
                                break;
                            case R.id.edit_btn:

                                if (mCheckPermission()) {
                                    isEditMessage = true;
                                    isReplyMessage = false;
                                    mGalleryArrayList.clear();
                                    messageReplyRL.setVisibility(GONE);
                                    galaryLL.setVisibility(GONE);
                                    imageLL.setVisibility(VISIBLE);

                                    if (mModel.getMessage() != null && !mModel.getMessage().equals("")) {
                                        editMessageText.setText(html2text(mModel.getMessage()));
                                        textED.setText(html2text(mModel.getMessage()));
                                        textED.setSelection(textED.getText().length());
                                    } else {
                                        editMessageText.setText("");
                                    }
                                    /* *
                                     * Set up Gallary Adapter if image  is Exists
                                     * */
                                    if (!mModel.getImageCount().equals("0")) {
                                        performGallaryAdapterSetup(mModel);
                                    } else {
                                        mGalleryArrayList.clear();
//                                    setUpGalleryAdapter();
                                    }
                                } else {
                                    mRequestPermissionNew();
                                }

                                // EX : call intent if you want to swich to other activity
                                break;

                            case R.id.del_btn:
                                deleteConfirmDialog(getResources().getString(R.string.are_you_sure_want_to_delete_message_));

                                // EX : call intent if you want to swich to other activity
                                break;

                        }
                        return false;
                    }
                });

                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.option_chat_left_menu, popup.getMenu());
                popup.show();

            } else {
                PopupMenu popup = new PopupMenu(mActivity, view);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.reply_btn:
                                strReplyID = mModel.getChatId();
                                isReplyMessage = true;
                                isEditMessage = false;
                                mGalleryArrayList.clear();
                                setUpGalleryAdapter();
                                messageReplyRL.setVisibility(VISIBLE);
                                galaryLL.setVisibility(VISIBLE);
                                imageLL.setVisibility(GONE);

                                if (mModel.getMessage() != null && !mModel.getMessage().equals("")) {
                                    editMessageText.setText(html2text(mModel.getMessage()));
                                } else {
                                    editMessageText.setText("");
                                }

                                /* to show and hide images for reply case */
                                if (mModel.getImageCount() != null && !mModel.getImageCount().equals("")
                                        && !mModel.getImageCount().equals("0")) {
                                    imageRplyRL.setVisibility(VISIBLE);

                                    if (mModel.getImageCount().equals("1")) {
                                        blurPicRL.setVisibility(GONE);
                                        messagePicReplyIV.setVisibility(VISIBLE);

                                        if (!Utilities.isDocAdded(mModel.getDoc1())) {
                                            if (Utilities.isVideoAdded(mModel.getDoc1())) {
                                                messagePicReplyIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.video_icon));
                                            } else {
                                                Glide.with(mActivity).load(mModel.getDoc1()).into(messagePicReplyIV);
                                            }
                                        } else {
                                            messagePicReplyIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.icon_doc));
                                        }

                                    } else {
                                        blurPicRL.setVisibility(VISIBLE);
                                        messagePicReplyIV.setVisibility(VISIBLE);

                                        if (!Utilities.isDocAdded(mModel.getDoc1())) {
                                            if (Utilities.isVideoAdded(mModel.getDoc1())) {
                                                messagePicReplyIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.video_icon));
                                            } else {
                                                Glide.with(mActivity).load(mModel.getDoc1()).into(messagePicReplyIV);
                                            }
                                        } else {
                                            messagePicReplyIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.icon_doc));
                                        }

                                        int imageCount = Integer.parseInt(mModel.getImageCount()) - 1;
                                        String strImageCount = "+" + imageCount;
                                        textCountPicTv.setText(strImageCount);
                                    }
                                } else {
                                    imageRplyRL.setVisibility(GONE);
                                }

                                break;
                        }
                        return false;
                    }
                });

                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.option_menu_right_chat, popup.getMenu());
                popup.show();
            }
        }
    };
    private final Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject mJsonData = null;
                    try {
                        mJsonData = new JSONObject(String.valueOf(args[1]));
                        JSONObject mJSONAppendData = mJsonData.getJSONObject("append_data");
                        if (mJSONAppendData.getString("message_type").equals("delete")) {
                            Log.e(TAG, "Size - " + mGetAllChatMessagesAllChatModel.size());
                            for (int i = 0; i < mGetAllChatMessagesAllChatModel.size(); i++) {
                                if (mGetAllChatMessagesAllChatModel.get(i).getChatId().equals(mJSONAppendData.getString("chat_id"))) {
                                    mGetAllChatMessagesAllChatModel.remove(i);
                                }
                            }
                            mAdapterNew.notifyDataSetChanged();
                        }
                        if (mJSONAppendData.getString("message_type").equals("update")) {
                            parseEditResponce(mJsonData);
                        } else {
                            parseAddResponce(mJsonData);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };
    private final Runnable onTypingTimeout = new Runnable() {
        @Override
        public void run() {
            mSocket.emit("type", strRoomID, false);
        }
    };
    private final Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!isConnected) {
                        isConnected = true;
                    }
                }
            });
        }
    };
    private final Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "diconnected");
                    isConnected = false;

                }
            });
        }
    };
    private final Emitter.Listener onUserJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                }
            });
        }
    };
    private final Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };
    private final Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };
    private final Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "Error connecting");

                }
            });
        }
    };
    private final Emitter.Listener onStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    try {
                        username = data.getString("username");
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage());
                        return;
                    }
                }
            });
        }
    };

    private void parseAddResponce(JSONObject mJsonDATA) {
        try {
            JSONObject mJsonData = mJsonDATA.getJSONObject("append_data");

            AllChatsItem mAllChatsItem = new AllChatsItem();

            if (!mJsonData.getString("chat_id").equals("")) {
                mAllChatsItem.setChatId(mJsonData.getString("chat_id"));
            }
            if (!mJsonData.getString("reply_id").equals("")) {
                mAllChatsItem.setReplyId(mJsonData.getString("reply_id"));
            }
            if (!mJsonData.getString("sender_id").equals("")) {
                mAllChatsItem.setSenderId(mJsonData.getString("sender_id"));
                if (mJsonData.getString("sender_id").equals(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""))) {
                    mAllChatsItem.setIntType(JaoharConstants.Sender);
                } else if (mJsonData.getString("sender_id").equals(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""))) {
                    mAllChatsItem.setIntType(JaoharConstants.Sender);
                } else {
                    mAllChatsItem.setIntType(JaoharConstants.Receiver);
                }
            }
            if (!mJsonData.getString("message").equals("")) {
                mAllChatsItem.setMessage(mJsonData.getString("message"));
            }
            if (mJsonData.has("edited") && !mJsonData.getString("edited").equals("")) {
                mAllChatsItem.setEdited(mJsonData.getString("edited"));
            }
            if (!mJsonData.getString("creation_date").equals("")) {
                mAllChatsItem.setCreationDate(mJsonData.getString("creation_date"));
            }
            if (!mJsonData.getString("doc1").equals("")) {
                mAllChatsItem.setDoc1(mJsonData.getString("doc1"));
            }
            if (!mJsonData.getString("doc2").equals("")) {
                mAllChatsItem.setDoc2(mJsonData.getString("doc2"));
            }
            if (!mJsonData.getString("doc3").equals("")) {
                mAllChatsItem.setDoc3(mJsonData.getString("doc3"));
            }
            if (!mJsonData.getString("doc4").equals("")) {
                mAllChatsItem.setDoc4(mJsonData.getString("doc4"));
            }
            if (!mJsonData.getString("doc5").equals("")) {
                mAllChatsItem.setDoc5(mJsonData.getString("doc5"));
            }
            if (!mJsonData.getString("doc6").equals("")) {
                mAllChatsItem.setDoc6(mJsonData.getString("doc6"));
            }
            if (!mJsonData.getString("doc7").equals("")) {
                mAllChatsItem.setDoc7(mJsonData.getString("doc7"));
            }
            if (!mJsonData.getString("doc8").equals("")) {
                mAllChatsItem.setDoc8(mJsonData.getString("doc8"));
            }
            if (!mJsonData.getString("doc9").equals("")) {
                mAllChatsItem.setDoc9(mJsonData.getString("doc9"));
            }
            if (!mJsonData.getString("doc10").equals("")) {
                mAllChatsItem.setDoc10(mJsonData.getString("doc10"));
            }
            if (!mJsonData.getString("image_count").equals("")) {
                mAllChatsItem.setImageCount(mJsonData.getString("image_count"));
            }
            if (mJsonData.has("date")) {
                if (!mJsonData.getString("date").equals("")) {
                    mAllChatsItem.setDate(mJsonData.getString("date"));
                }
            } else {
                if (!mJsonData.getString("creation_date").equals("")) {
                    mAllChatsItem.setDate(getDateFromUixTimeStampFormat(Long.parseLong(mJsonData.getString("creation_date"))));
                }
            }

            if (!mJsonData.getString("user_detail").equals("")) {
                JSONObject mjsonUserDetail = mJsonData.getJSONObject("user_detail");
                UserDetail mUserDetail = new UserDetail();
                if (!mjsonUserDetail.getString("id").equals("")) {
                    mUserDetail.setId(mjsonUserDetail.getString("id"));
                }
                if (!mjsonUserDetail.getString("email").equals("")) {
                    mUserDetail.setEmail(mjsonUserDetail.getString("email"));
                }
                if (!mjsonUserDetail.getString("password").equals("")) {
                    mUserDetail.setPassword(mjsonUserDetail.getString("password"));
                }
                if (!mjsonUserDetail.getString("role").equals("")) {
                    mUserDetail.setRole(mjsonUserDetail.getString("role"));
                }
                if (!mjsonUserDetail.getString("status").equals("")) {
                    mUserDetail.setStatus(mjsonUserDetail.getString("status"));
                }
                if (!mjsonUserDetail.getString("created").equals("")) {
                    mUserDetail.setCreated(mjsonUserDetail.getString("created"));
                }
                if (!mjsonUserDetail.getString("enabled").equals("")) {
                    mUserDetail.setEnabled(mjsonUserDetail.getString("enabled"));
                }
                if (!mjsonUserDetail.getString("company_name").equals("")) {
                    mUserDetail.setCompanyName(mjsonUserDetail.getString("company_name"));
                }
                if (!mjsonUserDetail.getString("first_name").equals("")) {
                    mUserDetail.setFirstName(mjsonUserDetail.getString("first_name"));
                }
                if (!mjsonUserDetail.getString("last_name").equals("")) {
                    mUserDetail.setLastName(mjsonUserDetail.getString("last_name"));
                }
                if (!mjsonUserDetail.getString("image").equals("")) {
                    mUserDetail.setImage(mjsonUserDetail.getString("image"));
                }
                mAllChatsItem.setUserDetail(mUserDetail);
            }

            if (mJsonData.has("reply_for") && !mJsonData.getString("reply_for").equals("")) {
                JSONObject mjsonReplyForDetail = mJsonData.getJSONObject("reply_for");
                ReplyFor mReplyDetail = new ReplyFor();

                if (!mjsonReplyForDetail.getString("chat_id").equals("")) {
                    mReplyDetail.setChatId(mjsonReplyForDetail.getString("chat_id"));
                }

                if (!mjsonReplyForDetail.getString("reply_id").equals("")) {
                    mReplyDetail.setReplyId(mjsonReplyForDetail.getString("reply_id"));
                }
                if (!mjsonReplyForDetail.getString("sender_id").equals("")) {
                    mReplyDetail.setSenderId(mjsonReplyForDetail.getString("sender_id"));
                }
                if (!mjsonReplyForDetail.getString("message").equals("")) {
                    mReplyDetail.setMessage(mjsonReplyForDetail.getString("message"));
                }
                if (!mjsonReplyForDetail.getString("doc1").equals("")) {
                    mReplyDetail.setDoc1(mjsonReplyForDetail.getString("doc1"));
                }
                if (!mjsonReplyForDetail.getString("doc2").equals("")) {
                    mReplyDetail.setDoc2(mjsonReplyForDetail.getString("doc2"));
                }
                if (!mjsonReplyForDetail.getString("doc3").equals("")) {
                    mReplyDetail.setDoc3(mjsonReplyForDetail.getString("doc3"));
                }
                if (!mjsonReplyForDetail.getString("doc4").equals("")) {
                    mReplyDetail.setDoc4(mjsonReplyForDetail.getString("doc4"));
                }
                if (!mjsonReplyForDetail.getString("doc5").equals("")) {
                    mReplyDetail.setDoc5(mjsonReplyForDetail.getString("doc5"));
                }
                if (!mjsonReplyForDetail.getString("doc6").equals("")) {
                    mReplyDetail.setDoc6(mjsonReplyForDetail.getString("doc6"));
                }
                if (!mjsonReplyForDetail.getString("doc7").equals("")) {
                    mReplyDetail.setDoc7(mjsonReplyForDetail.getString("doc7"));
                }
                if (!mjsonReplyForDetail.getString("doc8").equals("")) {
                    mReplyDetail.setDoc8(mjsonReplyForDetail.getString("doc8"));
                }
                if (!mjsonReplyForDetail.getString("doc9").equals("")) {
                    mReplyDetail.setDoc9(mjsonReplyForDetail.getString("doc9"));
                }
                if (!mjsonReplyForDetail.getString("doc10").equals("")) {
                    mReplyDetail.setDoc10(mjsonReplyForDetail.getString("doc10"));
                }
                if (!mjsonReplyForDetail.getString("creation_date").equals("")) {
                    mReplyDetail.setCreationDate(mjsonReplyForDetail.getString("creation_date"));
                }
                if (!mjsonReplyForDetail.getString("enable").equals("")) {
                    mReplyDetail.setEnable(mjsonReplyForDetail.getString("enable"));
                }
                if (!mjsonReplyForDetail.getString("image_count").equals("")) {
                    mReplyDetail.setImageCount(mjsonReplyForDetail.getString("image_count"));
                }
                if (mjsonReplyForDetail.has("date")) {
                    if (!mjsonReplyForDetail.getString("date").equals("")) {
                        mReplyDetail.setDate(mjsonReplyForDetail.getString("date"));
                    }
                }
                if (!mjsonReplyForDetail.getString("user_detail").equals("")) {
                    JSONObject mjsonUserDetail = mjsonReplyForDetail.getJSONObject("user_detail");
                    UserDetail mUserDetail = new UserDetail();
                    if (!mjsonUserDetail.getString("id").equals("")) {
                        mUserDetail.setId(mjsonUserDetail.getString("id"));
                    }
                    if (!mjsonUserDetail.getString("email").equals("")) {
                        mUserDetail.setEmail(mjsonUserDetail.getString("email"));
                    }
                    if (!mjsonUserDetail.getString("password").equals("")) {
                        mUserDetail.setPassword(mjsonUserDetail.getString("password"));
                    }
                    if (!mjsonUserDetail.getString("role").equals("")) {
                        mUserDetail.setRole(mjsonUserDetail.getString("role"));
                    }
                    if (!mjsonUserDetail.getString("status").equals("")) {
                        mUserDetail.setStatus(mjsonUserDetail.getString("status"));
                    }
                    if (!mjsonUserDetail.getString("created").equals("")) {
                        mUserDetail.setCreated(mjsonUserDetail.getString("created"));
                    }
                    if (!mjsonUserDetail.getString("enabled").equals("")) {
                        mUserDetail.setEnabled(mjsonUserDetail.getString("enabled"));
                    }
                    if (!mjsonUserDetail.getString("company_name").equals("")) {
                        mUserDetail.setCompanyName(mjsonUserDetail.getString("company_name"));
                    }
                    if (!mjsonUserDetail.getString("first_name").equals("")) {
                        mUserDetail.setFirstName(mjsonUserDetail.getString("first_name"));
                    }
                    if (!mjsonUserDetail.getString("last_name").equals("")) {
                        mUserDetail.setLastName(mjsonUserDetail.getString("last_name"));
                    }
                    if (!mjsonUserDetail.getString("image").equals("")) {
                        mUserDetail.setImage(mjsonUserDetail.getString("image"));
                    }
                    mReplyDetail.setUserDetail(mUserDetail);
                }
                mAllChatsItem.setReplyFor(mReplyDetail);
            }

            mGetAllChatMessagesAllChatModel.add(0, mAllChatsItem);
            setAdapterNew();
            if (mGetAllChatMessagesAllChatModel != null) {
                dataRV.scrollToPosition(0);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseEditResponce(JSONObject mJsonDATA) {
        try {
            JSONObject mJsonData = mJsonDATA.getJSONObject("append_data");

            AllChatsItem mAllChatsItem = new AllChatsItem();

            if (!mJsonData.getString("chat_id").equals("")) {
                mAllChatsItem.setChatId(mJsonData.getString("chat_id"));
            }
            if (!mJsonData.getString("reply_id").equals("")) {
                mAllChatsItem.setReplyId(mJsonData.getString("reply_id"));
            }
            if (!mJsonData.getString("sender_id").equals("")) {
                mAllChatsItem.setSenderId(mJsonData.getString("sender_id"));
                if (mJsonData.getString("sender_id").equals(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""))) {
                    mAllChatsItem.setIntType(JaoharConstants.Sender);
                } else {
                    mAllChatsItem.setIntType(JaoharConstants.Receiver);
                }
            }
            if (!mJsonData.getString("message").equals("")) {
                mAllChatsItem.setMessage(mJsonData.getString("message"));
            }
            if (mJsonData.has("edited") && !mJsonData.getString("edited").equals("")) {
                mAllChatsItem.setEdited(mJsonData.getString("edited"));
            }
            if (!mJsonData.getString("creation_date").equals("")) {
                mAllChatsItem.setCreationDate(mJsonData.getString("creation_date"));
            }
            if (!mJsonData.getString("doc1").equals("")) {
                mAllChatsItem.setDoc1(mJsonData.getString("doc1"));
            }
            if (!mJsonData.getString("doc2").equals("")) {
                mAllChatsItem.setDoc2(mJsonData.getString("doc2"));
            }
            if (!mJsonData.getString("doc3").equals("")) {
                mAllChatsItem.setDoc3(mJsonData.getString("doc3"));
            }
            if (!mJsonData.getString("doc4").equals("")) {
                mAllChatsItem.setDoc4(mJsonData.getString("doc4"));
            }
            if (!mJsonData.getString("doc5").equals("")) {
                mAllChatsItem.setDoc5(mJsonData.getString("doc5"));
            }
            if (!mJsonData.getString("doc6").equals("")) {
                mAllChatsItem.setDoc6(mJsonData.getString("doc6"));
            }
            if (!mJsonData.getString("doc7").equals("")) {
                mAllChatsItem.setDoc7(mJsonData.getString("doc7"));
            }
            if (!mJsonData.getString("doc8").equals("")) {
                mAllChatsItem.setDoc8(mJsonData.getString("doc8"));
            }
            if (!mJsonData.getString("doc9").equals("")) {
                mAllChatsItem.setDoc9(mJsonData.getString("doc9"));
            }
            if (!mJsonData.getString("doc10").equals("")) {
                mAllChatsItem.setDoc10(mJsonData.getString("doc10"));
            }
            if (!mJsonData.getString("image_count").equals("")) {
                mAllChatsItem.setImageCount(mJsonData.getString("image_count"));
            }
            if (mJsonData.has("date")) {
                if (!mJsonData.getString("date").equals("")) {
                    mAllChatsItem.setDate(mJsonData.getString("date"));
                }
            } else {
                if (!mJsonData.getString("creation_date").equals("")) {
                    mAllChatsItem.setDate(getDateFromUixTimeStampFormat(Long.parseLong(mJsonData.getString("creation_date"))));
                }
            }

            if (!mJsonData.getString("user_detail").equals("")) {
                JSONObject mjsonUserDetail = mJsonData.getJSONObject("user_detail");
                UserDetail mUserDetail = new UserDetail();
                if (!mjsonUserDetail.getString("id").equals("")) {
                    mUserDetail.setId(mjsonUserDetail.getString("id"));
                }
                if (!mjsonUserDetail.getString("email").equals("")) {
                    mUserDetail.setEmail(mjsonUserDetail.getString("email"));
                }
                if (!mjsonUserDetail.getString("password").equals("")) {
                    mUserDetail.setPassword(mjsonUserDetail.getString("password"));
                }
                if (!mjsonUserDetail.getString("role").equals("")) {
                    mUserDetail.setRole(mjsonUserDetail.getString("role"));
                }
                if (!mjsonUserDetail.getString("status").equals("")) {
                    mUserDetail.setStatus(mjsonUserDetail.getString("status"));
                }
                if (!mjsonUserDetail.getString("created").equals("")) {
                    mUserDetail.setCreated(mjsonUserDetail.getString("created"));
                }
                if (!mjsonUserDetail.getString("enabled").equals("")) {
                    mUserDetail.setEnabled(mjsonUserDetail.getString("enabled"));
                }
                if (!mjsonUserDetail.getString("company_name").equals("")) {
                    mUserDetail.setCompanyName(mjsonUserDetail.getString("company_name"));
                }
                if (!mjsonUserDetail.getString("first_name").equals("")) {
                    mUserDetail.setFirstName(mjsonUserDetail.getString("first_name"));
                }
                if (!mjsonUserDetail.getString("last_name").equals("")) {
                    mUserDetail.setLastName(mjsonUserDetail.getString("last_name"));
                }
                if (!mjsonUserDetail.getString("image").equals("")) {
                    mUserDetail.setImage(mjsonUserDetail.getString("image"));
                }
                mAllChatsItem.setUserDetail(mUserDetail);
            }

            if (mJsonData.has("reply_for") && !mJsonData.getString("reply_for").equals("")) {
                JSONObject mjsonReplyForDetail = mJsonData.getJSONObject("reply_for");
                ReplyFor mReplyDetail = new ReplyFor();

                if (!mjsonReplyForDetail.getString("chat_id").equals("")) {
                    mReplyDetail.setChatId(mjsonReplyForDetail.getString("chat_id"));
                }
                if (!mjsonReplyForDetail.getString("reply_id").equals("")) {
                    mReplyDetail.setReplyId(mjsonReplyForDetail.getString("reply_id"));
                }
                if (!mjsonReplyForDetail.getString("sender_id").equals("")) {
                    mReplyDetail.setSenderId(mjsonReplyForDetail.getString("sender_id"));
                }
                if (!mjsonReplyForDetail.getString("message").equals("")) {
                    mReplyDetail.setMessage(mjsonReplyForDetail.getString("message"));
                }
                if (!mjsonReplyForDetail.getString("doc1").equals("")) {
                    mReplyDetail.setDoc1(mjsonReplyForDetail.getString("doc1"));
                }
                if (!mjsonReplyForDetail.getString("creation_date").equals("")) {
                    mReplyDetail.setCreationDate(mjsonReplyForDetail.getString("creation_date"));
                }
                if (!mjsonReplyForDetail.getString("doc2").equals("")) {
                    mReplyDetail.setDoc2(mjsonReplyForDetail.getString("doc2"));
                }
                if (!mjsonReplyForDetail.getString("image_count").equals("")) {
                    mReplyDetail.setImageCount(mjsonReplyForDetail.getString("image_count"));
                }
                if (mjsonReplyForDetail.has("date")) {
                    if (!mjsonReplyForDetail.getString("date").equals("")) {
                        mReplyDetail.setDate(mjsonReplyForDetail.getString("date"));
                    }
                }
                if (!mjsonReplyForDetail.getString("doc3").equals("")) {
                    mReplyDetail.setDoc3(mjsonReplyForDetail.getString("doc3"));
                }
                if (!mjsonReplyForDetail.getString("doc4").equals("")) {
                    mReplyDetail.setDoc4(mjsonReplyForDetail.getString("doc4"));
                }
                if (!mjsonReplyForDetail.getString("doc5").equals("")) {
                    mReplyDetail.setDoc5(mjsonReplyForDetail.getString("doc5"));
                }
                if (!mjsonReplyForDetail.getString("doc6").equals("")) {
                    mReplyDetail.setDoc6(mjsonReplyForDetail.getString("doc6"));
                }
                if (!mjsonReplyForDetail.getString("doc7").equals("")) {
                    mReplyDetail.setDoc7(mjsonReplyForDetail.getString("doc7"));
                }
                if (!mjsonReplyForDetail.getString("doc8").equals("")) {
                    mReplyDetail.setDoc8(mjsonReplyForDetail.getString("doc8"));
                }
                if (!mjsonReplyForDetail.getString("doc9").equals("")) {
                    mReplyDetail.setDoc9(mjsonReplyForDetail.getString("doc9"));
                }
                if (!mjsonReplyForDetail.getString("doc10").equals("")) {
                    mReplyDetail.setDoc10(mjsonReplyForDetail.getString("doc10"));
                }
                if (!mjsonReplyForDetail.getString("enable").equals("")) {
                    mReplyDetail.setEnable(mjsonReplyForDetail.getString("enable"));
                }
                if (!mjsonReplyForDetail.getString("user_detail").equals("")) {
                    JSONObject mjsonUserDetail = mjsonReplyForDetail.getJSONObject("user_detail");
                    UserDetail mUserDetail = new UserDetail();
                    if (!mjsonUserDetail.getString("id").equals("")) {
                        mUserDetail.setId(mjsonUserDetail.getString("id"));
                    }
                    if (!mjsonUserDetail.getString("email").equals("")) {
                        mUserDetail.setEmail(mjsonUserDetail.getString("email"));
                    }
                    if (!mjsonUserDetail.getString("password").equals("")) {
                        mUserDetail.setPassword(mjsonUserDetail.getString("password"));
                    }
                    if (!mjsonUserDetail.getString("role").equals("")) {
                        mUserDetail.setRole(mjsonUserDetail.getString("role"));
                    }
                    if (!mjsonUserDetail.getString("status").equals("")) {
                        mUserDetail.setStatus(mjsonUserDetail.getString("status"));
                    }
                    if (!mjsonUserDetail.getString("created").equals("")) {
                        mUserDetail.setCreated(mjsonUserDetail.getString("created"));
                    }
                    if (!mjsonUserDetail.getString("enabled").equals("")) {
                        mUserDetail.setEnabled(mjsonUserDetail.getString("enabled"));
                    }
                    if (!mjsonUserDetail.getString("company_name").equals("")) {
                        mUserDetail.setCompanyName(mjsonUserDetail.getString("company_name"));
                    }
                    if (!mjsonUserDetail.getString("first_name").equals("")) {
                        mUserDetail.setFirstName(mjsonUserDetail.getString("first_name"));
                    }
                    if (!mjsonUserDetail.getString("last_name").equals("")) {
                        mUserDetail.setLastName(mjsonUserDetail.getString("last_name"));
                    }
                    if (!mjsonUserDetail.getString("image").equals("")) {
                        mUserDetail.setImage(mjsonUserDetail.getString("image"));
                    }
                    mReplyDetail.setUserDetail(mUserDetail);
                }
                mAllChatsItem.setReplyFor(mReplyDetail);
            }

            for (int i = 0; i < mGetAllChatMessagesAllChatModel.size(); i++) {
                if (mGetAllChatMessagesAllChatModel.get(i).getChatId().equals(mJsonData.getString("chat_id"))) {
                    mAllChatsItem.setEdited("1");
                    mGetAllChatMessagesAllChatModel.set(i, mAllChatsItem);
                    mAdapterNew.notifyItemChanged(i);
                }
            }

//            mGetAllChatMessagesAllChatModel.set(currentMSGpositon, mAllChatsItem);
//            mAdapterNew.notifyItemChanged(currentMSGpositon);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}