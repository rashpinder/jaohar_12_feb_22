package jaohar.com.jaohar.activities.educational_blogs_module;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.singleton.JaoharSingleton;
import jaohar.com.jaohar.utils.JaoharConstants;

public class SearchBlogsActivity extends BaseActivity {
    /**
     * Initialize the Activity
     */
    private Activity mActivity = SearchBlogsActivity.this;

    /*
     * Getting the Class Name
     * */
    String TAG = SearchBlogsActivity.this.getClass().getSimpleName();

    /**
     * Widgets
     */
    @BindView(R.id.editSearchET)
    EditText editSearchET;
    @BindView(R.id.cancelTV)
    TextView cancelTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setStatusBarColor(Color.WHITE);
        setContentView(R.layout.activity_search_blogs);
        setStatusBar();
        ButterKnife.bind(this);
        setWidgetsIds();
    }


    @OnClick({R.id.cancelTV})
    public  void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.cancelTV:
                onBackPressed();
                break;
        }
    }

    private void setWidgetsIds() {

        JaoharSingleton.getInstance().setSearchedEducationalBlog("");

        editSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event == null || event.getAction() != KeyEvent.ACTION_DOWN) {
                    //do something

                    onBackPressed();
                }
                return false;
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (editSearchET.getText().toString().trim().equals("")) {
            JaoharSingleton.getInstance().setSearchedEducationalBlog("");
        } else {
            JaoharSingleton.getInstance().setSearchedEducationalBlog(editSearchET.getText().toString().trim());
        }
    }

    /*
     * Set Up Status Bar
     * */
    public void setStatusBar() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
    }
}
