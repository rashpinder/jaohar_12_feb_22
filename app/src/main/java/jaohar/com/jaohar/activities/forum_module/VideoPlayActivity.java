package jaohar.com.jaohar.activities.forum_module;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;

public class VideoPlayActivity extends BaseActivity {
    VideoView videoView;
    RelativeLayout imgCrossRV;
    ProgressBar progrss;
    private MediaController mediacontroller;
    private Uri uri;
    private String strVideoUrl;
    private boolean isContinuously = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_video_play);
    }

    @Override
    protected void setViewsIDs() {
        progrss = (ProgressBar) findViewById(R.id.progrss);
        videoView = (VideoView) findViewById(R.id.videoView);
        imgCrossRV = (RelativeLayout) findViewById(R.id.imgCrossRV);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            strVideoUrl = extras.getString("video_URL");
        }
        setUpPlayVideo();
    }

    @Override
    protected void setClickListner() {
        imgCrossRV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void setUpPlayVideo() {


        uri = Uri.parse(strVideoUrl);
        videoView.requestFocus();
        videoView.setVideoURI(uri);
        videoView.start();
//        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//            @Override
//            public void onPrepared(MediaPlayer mp) {
//                mp.setLooping(true);
//                videoView.start();
//            }
//        });
        progrss.setVisibility(View.VISIBLE);
//        videoView.setMediaController(mediacontroller);
//        videoView.setVideoURI(uri);
//        videoView.requestFocus();
//        videoView.start();

//        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//            @Override
//            public void onCompletion(MediaPlayer mp) {
//                if (isContinuously) {
//                    videoView.start();
//                }
//            }
//        });

        /*btnstop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoView.pause();
            }
        });

        btnplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoView.start();
            }
        });

        btnonce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isContinuously = false;
                progrss.setVisibility(View.VISIBLE);
                videoView.setMediaController(mediacontroller);
                videoView.setVideoURI(uri);
                videoView.requestFocus();
                videoView.start();
            }
        });

        btncontinuously.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isContinuously = true;
                progrss.setVisibility(View.VISIBLE);
                videoView.setMediaController(mediacontroller);
                videoView.setVideoURI(uri);
                videoView.requestFocus();
                videoView.start();
            }
        });*/

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                progrss.setVisibility(View.GONE);
            }
        });

        /*btnstop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoView.pause();
            }
        });

        btnplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoView.start();
            }
        });

        btnonce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isContinuously = false;
                progrss.setVisibility(View.VISIBLE);
                videoView.setMediaController(mediacontroller);
                videoView.setVideoURI(uri);
                videoView.requestFocus();
                videoView.start();
            }
        });

        btncontinuously.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isContinuously = true;
                progrss.setVisibility(View.VISIBLE);
                videoView.setMediaController(mediacontroller);
                videoView.setVideoURI(uri);
                videoView.requestFocus();
                videoView.start();
            }
        });*/
    }
}
