package jaohar.com.jaohar.activities.forum_module_admin;

import android.app.Activity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.fragments.AdminStaffForumFragment;
import jaohar.com.jaohar.fragments.UKforumAdminFragment;
import jaohar.com.jaohar.utils.JaoharConstants;

public class AdminForumActivity extends BaseActivity {

    /**
     * Initialize the Activity
     **/
    private Activity mActivity = AdminForumActivity.this;


    /**
     * Widgets
     **/
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    @BindView(R.id.vesselTrashViewLL)
    LinearLayout vesselTrashViewLL;
    @BindView(R.id.manageInvoiceviewLL)
    LinearLayout manageInvoiceviewLL;
    @BindView(R.id.uk_ForumRL)
    RelativeLayout uk_ForumRL;
    @BindView(R.id.staff_ForumRL)
    RelativeLayout staff_ForumRL;

    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.txtCenter)
    TextView txtCenter;
    @BindView(R.id.vesselTrashTV)
    TextView vesselTrashTV;
    @BindView(R.id.manageInvoiceTV)
    TextView manageInvoiceTV;

    public static RelativeLayout imgRightLL;
    String strIsClickFrom = "ForumOPEN";

   public static boolean isUKOfficeAddClick=false;


    /**
     * Activity @Override method
     * #onCreate
     **/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_forum);

        ButterKnife.bind(this);
    }

    /**
     * Binding variables with views from layout
     **/
    @Override
    protected void setViewsIDs() {

        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.back));
        txtCenter.setText(getResources().getString(R.string.forums));
        imgRightLL = (RelativeLayout) findViewById(R.id.imgRightLL);
        // Getting Clicked Value From Intent
        if (getIntent() != null) {
            if (getIntent().getStringExtra("isClick").equals("manageUtilities")) {
                strIsClickFrom = getIntent().getStringExtra("isClick");
            }
        }


        if(isUKOfficeAddClick==true){
            vesselTrashTV.setTextColor(getResources().getColor(R.color.black));
            manageInvoiceTV.setTextColor(getResources().getColor(R.color.colorPrimary));
            manageInvoiceviewLL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            vesselTrashViewLL.setBackgroundColor(getResources().getColor(R.color.white));
            switchFragment((FragmentActivity) mActivity, new UKforumAdminFragment(), JaoharConstants.UK_forum_Admin_Fragment, false, null);

        }else {
            /*
             * Default Click
             * */
            vesselTrashTV.setTextColor(getResources().getColor(R.color.colorPrimary));
            manageInvoiceTV.setTextColor(getResources().getColor(R.color.black));
            vesselTrashViewLL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            manageInvoiceviewLL.setBackgroundColor(getResources().getColor(R.color.white));
            switchFragment((FragmentActivity) mActivity, new AdminStaffForumFragment(), JaoharConstants.Admin_Staff_Forum_Fragment, false, null);

        }

    }


    @OnClick({R.id.llLeftLL,R.id.uk_ForumRL,R.id.staff_ForumRL})
    public  void onViewClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.llLeftLL:
                onBackPressed();
                break;
            case R.id.uk_ForumRL:
                ukForumClick();
                break;
            case R.id.staff_ForumRL:
                staffForumClick();
                break;
        }
    }



    private void ukForumClick() {
        vesselTrashTV.setTextColor(getResources().getColor(R.color.black));
        manageInvoiceTV.setTextColor(getResources().getColor(R.color.colorPrimary));
        manageInvoiceviewLL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        vesselTrashViewLL.setBackgroundColor(getResources().getColor(R.color.white));
        switchFragment((FragmentActivity) mActivity, new UKforumAdminFragment(), JaoharConstants.UK_forum_Admin_Fragment, false, null);

    }

    private void staffForumClick() {
        vesselTrashTV.setTextColor(getResources().getColor(R.color.colorPrimary));
        manageInvoiceTV.setTextColor(getResources().getColor(R.color.black));
        vesselTrashViewLL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        manageInvoiceviewLL.setBackgroundColor(getResources().getColor(R.color.white));
        switchFragment((FragmentActivity) mActivity, new AdminStaffForumFragment(), JaoharConstants.Admin_Staff_Forum_Fragment, false, null);

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    /**********
     *Replace Fragment In Activity
     **********/
    public static void switchFragment(FragmentActivity activity, Fragment fragment, String TAG, boolean addToStack, Bundle bundle) {
        FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (fragment != null) {
            // Replace current fragment by this new one
            ft.replace(R.id.layoutContainerLL, fragment, TAG);
            if (addToStack)
                ft.addToBackStack(null);
            if (bundle != null)
                fragment.setArguments(bundle);
            ft.commitAllowingStateLoss();
        }
    }
}
