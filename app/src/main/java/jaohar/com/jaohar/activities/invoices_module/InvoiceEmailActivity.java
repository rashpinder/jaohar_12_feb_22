package jaohar.com.jaohar.activities.invoices_module;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.SendingInvoiceEmailActivity;
import jaohar.com.jaohar.activities.chat_module.ChatMessagesListActivity;
import jaohar.com.jaohar.adapters.SelectedEmailShowListAdapter;
import jaohar.com.jaohar.adapters.ShowListEmailAdapter;
import jaohar.com.jaohar.adapters.ShowfromDATAEmailsAdapter;
import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.interfaces.SelectEmailListItem;
import jaohar.com.jaohar.interfaces.SelectFromEmailInterface;
import jaohar.com.jaohar.interfaces.SelectedAndUnselectedEmailListInterFace;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class InvoiceEmailActivity extends BaseActivity {
    /**
     * set Activity
     **/
    Activity mActivity = InvoiceEmailActivity.this;
    /**
     * set Activity TAG
     **/
    String TAG = InvoiceEmailActivity.this.getClass().getSimpleName();
    /*
    Widgets
     */
    @BindView(R.id.toEditText)
    EditText toEditText;
    @BindView(R.id.imgRightLL)
    RelativeLayout imgRightLL;
    @BindView(R.id.llLeftLL)
    LinearLayout llLeftLL;
    @BindView(R.id.ccEditText)
    EditText ccEditText;
    @BindView(R.id.bccEditText)
    EditText bccEditText;
    @BindView(R.id.subjectEditText)
    EditText subjectEditText;
    @BindView(R.id.fromEditText)
    EditText fromEditText;
    @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.mRecyclerViewCCList)
    RecyclerView mRecyclerViewCCList;
    @BindView(R.id.listDisplayItemBCC)
    LinearLayout listDisplayItemBCC;
    @BindView(R.id.mRecyclerViewCC)
    RecyclerView mRecyclerViewCC;
    @BindView(R.id.mRecyclerViewBCC)
    RecyclerView mRecyclerViewBCC;
    @BindView(R.id.mRecyclerViewBCCList)
    RecyclerView mRecyclerViewBCCList;
    @BindView(R.id.mRecyclerViewFromList)
    RecyclerView mRecyclerViewFromList;
    @BindView(R.id.listFromLL)
    LinearLayout listFromLL;
    @BindView(R.id.listShowLL)
    LinearLayout listShowLL;
    @BindView(R.id.listDisplayItem)
    LinearLayout listDisplayItem;
    @BindView(R.id.listCCLL)
    LinearLayout listCCLL;
    @BindView(R.id.listBCCLL)
    LinearLayout listBCCLL;
    @BindView(R.id.listDisplayItemCC)
    LinearLayout listDisplayItemCC;

    /*Invoice Model*/
    InVoicesModel mInVoicesModel;
    ArrayList<String> mRecordIDStr = new ArrayList<>();
    ArrayList<String> mModel = new ArrayList<>();
    static ArrayList<String> mStringFROM = new ArrayList<>();
    static ArrayList<String> idd = new ArrayList<>();
    ArrayList<String> filteredList = new ArrayList<>();
    ArrayList<String> mfilteredFromList = new ArrayList<>();
    ArrayList<String> mSelectedListArray = new ArrayList<>();
    ArrayList<String> mSelectedCCListArray = new ArrayList<>();
    ArrayList<String> mSelectedBCCListArray = new ArrayList<>();
    static String isAllVessal = "";
    String strTO;
    String strFROM;
    String strCC;
    String strBCC;
    String id;
    ShowfromDATAEmailsAdapter mShowfromDATAEmailsAdapter;
    ShowListEmailAdapter mShowListAdapter;
    SelectedEmailShowListAdapter mShowSelectedListAdapter;
    private String strRecordID = "", strSubject = "", strOwnerID = "";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_email);

        setStatusBar();

        ButterKnife.bind(this);
        
        
        if (getIntent() != null) {
                mInVoicesModel = (InVoicesModel) getIntent().getSerializableExtra("Model");
                Log.e(TAG, "mModel: "+mModel );

            if (getIntent().getStringExtra("vesselID") != null) {
                    strRecordID = getIntent().getStringExtra("vesselID");
                    strSubject = getIntent().getStringExtra("SubjectName");
                    isAllVessal = "1";
                }

            if (getIntent().getStringExtra("vesselIDUniversal") != null) {
                strRecordID = getIntent().getStringExtra("vesselIDUniversal");
                strSubject = getIntent().getStringExtra("SubjectName");
                strOwnerID = getIntent().getStringExtra("OwnerID");
                isAllVessal = "4";
            }
            if (getIntent().getStringArrayListExtra("recordIDArray") != null) {
                mRecordIDStr.clear();
                strSubject = getIntent().getStringExtra("vessalName1");
                mRecordIDStr = getIntent().getStringArrayListExtra("recordIDArray");
                Log.e(TAG, "ArrayLIST_DATA============= " + mRecordIDStr);
                isAllVessal = "2";
            }
            if (getIntent().getStringExtra("vessalName2") != null) {
                strSubject = getIntent().getStringExtra("vessalName2");
                isAllVessal = "3";
            }
        }
        subjectEditText.setText(strSubject);
        listFromLL.setVisibility(View.GONE);
        if (Utilities.isNetworkAvailable(mActivity)) {
            gettingFromEmail();
        } else {
            AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), getResources().getString(R.string.internetconnection));
        }
    }


    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        toEditText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                listFromLL.setVisibility(View.GONE);
                if (!query.toString().equals("")) {
                    listFromLL.setVisibility(View.VISIBLE);
                    query = query.toString().toLowerCase();
                    filteredList.clear();
                    for (int i = 0; i < JaoharConstants.mEmailsArrayLIST.size(); i++) {
                        Log.e("test", "onTextChanged: " + JaoharConstants.mEmailsArrayLIST.get(i).toString());
                        final String text = JaoharConstants.mEmailsArrayLIST.get(i).toString();
                        if (text.contains(query)) {
                            filteredList.add(JaoharConstants.mEmailsArrayLIST.get(i).toString());
                        }
                    }
                    setShowListAdapter(filteredList);
                } else {
                    listFromLL.setVisibility(View.GONE);
                }
            }
        });

        ccEditText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                listFromLL.setVisibility(View.GONE);
                if (!query.toString().equals("")) {
                    listCCLL.setVisibility(View.VISIBLE);
                    query = query.toString().toLowerCase();
                    filteredList.clear();
                    for (int i = 0; i < JaoharConstants.mEmailsArrayLIST.size(); i++) {
                        Log.e("test", "onTextChanged: " + JaoharConstants.mEmailsArrayLIST.get(i));
                        final String text = JaoharConstants.mEmailsArrayLIST.get(i);
                        if (text.contains(query)) {
                            filteredList.add(JaoharConstants.mEmailsArrayLIST.get(i));
                        }
                    }
                    setShowCCListAdapter(filteredList);
                } else {
                    listCCLL.setVisibility(View.GONE);
                }
            }
        });

        bccEditText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                listFromLL.setVisibility(View.GONE);
                if (!query.toString().equals("")) {
                    listBCCLL.setVisibility(View.VISIBLE);
                    query = query.toString().toLowerCase();
                    filteredList.clear();
                    for (int i = 0; i < JaoharConstants.mEmailsArrayLIST.size(); i++) {
                        Log.e("test", "onTextChanged: " + JaoharConstants.mEmailsArrayLIST.get(i).toString());
                        final String text = JaoharConstants.mEmailsArrayLIST.get(i);
                        if (text.contains(query)) {
                            filteredList.add(JaoharConstants.mEmailsArrayLIST.get(i));
                        }
                    }
                    setShowBCCListAdapter(filteredList);
                } else {
                    listBCCLL.setVisibility(View.GONE);
                }
            }
        });


        fromEditText.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                listFromLL.setVisibility(View.GONE);
                if (!query.toString().equals("")) {
                    listFromLL.setVisibility(View.VISIBLE);
                    query = query.toString().toLowerCase();
                    mfilteredFromList.clear();
                    for (int i = 0; i < mStringFROM.size(); i++) {
                        Log.e("test", "onTextChanged: " + mStringFROM.get(i).toString());
                        final String text = mStringFROM.get(i);
                        if (text.contains(query)) {
                            mfilteredFromList.add(mStringFROM.get(i));
                        }
                    }
                    mShowfromDATAEmailsAdapter = new ShowfromDATAEmailsAdapter(mActivity,mfilteredFromList, mSeletFromEmail, idd);
                    mRecyclerViewFromList.setAdapter(mShowfromDATAEmailsAdapter);
                } else {
                    listFromLL.setVisibility(View.GONE);
//                    mRecyclerViewFromList.setVisibility(View.GONE);
//                    mShowfromDATAEmailsAdapter = new ShowfromDATAEmailsAdapter(mActivity, mStringFROM, mSeletFromEmail, idd);
//                    mRecyclerViewFromList.setAdapter(mShowfromDATAEmailsAdapter);
                }
            }
        });

        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (toEditText.getText().toString().equals("")) {
                    if (mSelectedListArray.size() > 0) {
                        if (subjectEditText.getText().toString().equals("")) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_subject));
                        } else if (fromEditText.getText().toString().equals("")) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_from_subject));

                        } else {
                            if (!Utilities.isNetworkAvailable(mActivity)) {
                                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                            } else {

                                if (mStringFROM.contains(fromEditText.getText().toString())) {
                                    sendEamilAPI(toEditText.getText().toString(), subjectEditText.getText().toString(), strRecordID, fromEditText.getText().toString(),ccEditText.getText().toString(),bccEditText.getText().toString());
                                } else {
                                    fromEditText.setText("");
                                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_from_subject));
//                                    https://root.jaohar.com/Staging/JaoharWebServicesNew/MailSingleInvoice.php
                                    //                            sendEamilAPI(toEditText.getText().toString(), subjectEditText.getText().toString(), strRecordID);

                                }
                            }
                        }
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                    }
                } else {
                    if (Utilities.isValidEmaillId(toEditText.getText().toString()) != true) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                    } else if (subjectEditText.getText().toString().equals("")) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_subject));
                    } else if (fromEditText.getText().toString().equals("")) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_from_subject));

                    } else {
                        if (!Utilities.isNetworkAvailable(mActivity)) {
                            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                        } else {
                            if (mStringFROM.contains(fromEditText.getText().toString())) {
                                sendEamilAPI(toEditText.getText().toString(), subjectEditText.getText().toString(), strRecordID, fromEditText.getText().toString(),ccEditText.getText().toString(),bccEditText.getText().toString());
                            } else {
                                fromEditText.setText("");
                                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_enter_from_subject));
                            }
                        }
                    }
                }
            }
        });
        subjectEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                listFromLL.setVisibility(View.GONE);
                return false;
            }
        });

        fromEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(mStringFROM.size()>0){

                    listFromLL.setVisibility(View.VISIBLE);
                    mShowfromDATAEmailsAdapter = new ShowfromDATAEmailsAdapter(mActivity,mStringFROM , mSeletFromEmail, idd);
                    mRecyclerViewFromList.setAdapter(mShowfromDATAEmailsAdapter);
                }else {

                    gettingFromEmail();
                    listFromLL.setVisibility(View.VISIBLE);
                }
                return false;
            }
        });

        fromEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mStringFROM.size()>0){
                    fromEditText.setText("");
                    listFromLL.setVisibility(View.VISIBLE);
                    mShowfromDATAEmailsAdapter = new ShowfromDATAEmailsAdapter(mActivity,mStringFROM , mSeletFromEmail, idd);
                    mRecyclerViewFromList.setAdapter(mShowfromDATAEmailsAdapter);
                }else {
                    fromEditText.setText("");
                    gettingFromEmail();
                    listFromLL.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void gettingFromEmail() {
        mStringFROM.clear();
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getInvoiceMailsRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    if (mJsonObject.getString("status").equals("1")) {
                        JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            if (!mJsonArray.getString(i).equals("")) {
                                String email = mJsonArray.getJSONObject(i).get("email").toString();
                                String id = mJsonArray.getJSONObject(i).get("id").toString();
                                mStringFROM.add(email);
                                idd.add(id);
                            }
                        }
                        setShowListFromAdapter();
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), mJsonObject.getString("message"));
                    }

                    Log.e(TAG, "*****Response****" + "Array List All EMAILS SIZE ======  " + JaoharConstants.mEmailsArrayLIST.size());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void sendEamilAPI(final String strTO, final String strSubject, final String strRecordID, final String strFROM,String strCC,String strBCC) {
        this.strFROM = strFROM;
        this.strTO = strTO;
        this.strCC = strCC;
        this.strBCC = strBCC;

        if (isAllVessal.equals("2")) {
            executeMailMultipleInvoiceApi();
        } else if (isAllVessal.equals("1")) {
            executeMailSingleInvoiceApi();
        } else if (isAllVessal.equals("3")) {
            executeMailAllInvoiceApi();
        } else if (isAllVessal.equals("4")) {
            executeMailSingleUniversalInvoiceApi();
        }
    }


    private void executeMailSingleUniversalInvoiceApi() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.mailSingleUniversalInvoiceRequestt(strTO, String.valueOf(mSelectedListArray), strSubject, id, String.valueOf(mRecordIDStr), strRecordID, strOwnerID, ccEditText.getText().toString(),bccEditText.getText().toString(),String.valueOf(mSelectedCCListArray), String.valueOf(mSelectedBCCListArray)).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showAlertDialogDismiss(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void executeMailAllInvoiceApi() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.mailAllInvoiceRequestt(strTO, String.valueOf(mSelectedListArray), strSubject, id, String.valueOf(mRecordIDStr), strRecordID, strOwnerID,ccEditText.getText().toString(),bccEditText.getText().toString(),String.valueOf(mSelectedCCListArray), String.valueOf(mSelectedBCCListArray)).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showAlertDialogDismiss(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void executeMailSingleInvoiceApi() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.mailSingleInvoiceRequestt(strTO, String.valueOf(mSelectedListArray), strSubject, id, String.valueOf(mRecordIDStr), strRecordID, strOwnerID,ccEditText.getText().toString(),bccEditText.getText().toString(), String.valueOf(mSelectedCCListArray), String.valueOf(mSelectedBCCListArray)).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showAlertDialogDismiss(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void executeMailMultipleInvoiceApi() {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.mailMultipleInvoiceRequestt(strTO, String.valueOf(mSelectedListArray), strSubject, id, String.valueOf(mRecordIDStr), strRecordID, strOwnerID, ccEditText.getText().toString(),bccEditText.getText().toString(),String.valueOf(mSelectedCCListArray), String.valueOf(mSelectedBCCListArray)).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body());
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showAlertDialogDismiss(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else if (mModel.getStatus() == 100) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    SelectFromEmailInterface mSeletFromEmail = new SelectFromEmailInterface() {
        @Override
        public void mSelectEmail(String strEmail, String idd) {
            id=idd;
            fromEditText.setText(strEmail);
            listBCCLL.setVisibility(View.GONE);
            listCCLL.setVisibility(View.GONE);
            listFromLL.setVisibility(View.GONE);
            gettingFromEmail();
        }
    };

    SelectEmailListItem mSelectedItem = new SelectEmailListItem() {
        @Override
        public void mSelectEmailListItem(String strEmail, String cc) {
            if (cc.equals("to")){
                toEditText.setText("");
                listShowLL.setVisibility(View.GONE);
                mSelectedListArray.add(strEmail);
                setUpSelectedItems();
            }
            else if(cc.equals("cc")){
                ccEditText.setText("");
                listCCLL.setVisibility(View.GONE);
                mSelectedCCListArray.add(strEmail);
                setUpSelectedCCItems();
            }
            else {
                bccEditText.setText("");
                listBCCLL.setVisibility(View.GONE);
                mSelectedBCCListArray.add(strEmail);
                setUpSelectedBCCItems();
            }

        }
    };

    SelectedAndUnselectedEmailListInterFace selectedAndUnselectedEmailListInterFace = new SelectedAndUnselectedEmailListInterFace() {
        @Override
        public void mSelectedEmail(String strEmail,String cc) {
                if (cc.equals("to")){
                    if (mSelectedListArray.size() > 0) {
                        mSelectedListArray.remove(strEmail);
                    setUpSelectedItems();}
                }
                else if(cc.equals("cc")){
                    if (mSelectedCCListArray.size() > 0) {
                        mSelectedCCListArray.remove(strEmail);
                    setUpSelectedCCItems();
                }}
                else {
                    if (mSelectedBCCListArray.size() > 0) {
                        mSelectedBCCListArray.remove(strEmail);
                        setUpSelectedBCCItems();
                    }
                }}
//                setUpSelectedItems();
    };

    private void setUpSelectedItems() {
        Log.e(TAG, "SelectedArray: " + mSelectedListArray.size());
        mRecyclerView.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(layoutManager);
        mShowSelectedListAdapter = new SelectedEmailShowListAdapter(mActivity, mSelectedListArray, selectedAndUnselectedEmailListInterFace,"to");
        mRecyclerView.setAdapter(mShowSelectedListAdapter);
    }
    private void setUpSelectedCCItems() {
        Log.e(TAG, "SelectedArray: " + mSelectedCCListArray.size());
        mRecyclerViewCC.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerViewCC.setNestedScrollingEnabled(false);
        mRecyclerViewCC.setHasFixedSize(false);
        mRecyclerViewCC.setLayoutManager(layoutManager);
        mShowSelectedListAdapter = new SelectedEmailShowListAdapter(mActivity, mSelectedCCListArray, selectedAndUnselectedEmailListInterFace,"cc");
        mRecyclerViewCC.setAdapter(mShowSelectedListAdapter);
    }
    private void setUpSelectedBCCItems() {
        Log.e(TAG, "SelectedArray: " + mSelectedBCCListArray.size());
        mRecyclerViewBCC.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerViewBCC.setNestedScrollingEnabled(false);
        mRecyclerViewBCC.setHasFixedSize(false);
        mRecyclerViewBCC.setLayoutManager(layoutManager);
        mShowSelectedListAdapter = new SelectedEmailShowListAdapter(mActivity, mSelectedBCCListArray, selectedAndUnselectedEmailListInterFace,"bcc");
        mRecyclerViewBCC.setAdapter(mShowSelectedListAdapter);
    }

    public void setShowListFromAdapter() {
        mRecyclerViewFromList.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerViewFromList.setNestedScrollingEnabled(false);
        mRecyclerViewFromList.setHasFixedSize(false);
        mRecyclerViewFromList.setLayoutManager(layoutManager);
        mShowfromDATAEmailsAdapter = new ShowfromDATAEmailsAdapter(mActivity, mStringFROM, mSeletFromEmail,idd);
        mRecyclerViewFromList.setAdapter(mShowfromDATAEmailsAdapter);
    }

    public void setShowListAdapter(ArrayList<String> mFilterArray) {
        Log.e(TAG, "FilterArray: " + mFilterArray.size());
        mRecyclerView.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(layoutManager);
        mShowListAdapter = new ShowListEmailAdapter(mActivity, mFilterArray, mSelectedItem,"to");
        mRecyclerView.setAdapter(mShowListAdapter);
    }

    public void setShowBCCListAdapter(ArrayList<String> mFilterArray) {
        Log.e(TAG, "FilterArray: " + mFilterArray.size());
        mRecyclerViewBCC.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerViewBCC.setNestedScrollingEnabled(false);
        mRecyclerViewBCC.setHasFixedSize(false);
        mRecyclerViewBCC.setLayoutManager(layoutManager);
        mShowListAdapter = new ShowListEmailAdapter(mActivity, mFilterArray, mSelectedItem,"bcc");
        mRecyclerViewBCC.setAdapter(mShowListAdapter);
    }
    public void setShowCCListAdapter(ArrayList<String> mFilterArray) {
        Log.e(TAG, "FilterArray: " + mFilterArray.size());
        mRecyclerViewCC.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        mRecyclerViewCC.setNestedScrollingEnabled(false);
        mRecyclerViewCC.setHasFixedSize(false);
        mRecyclerViewCC.setLayoutManager(layoutManager);
        mShowListAdapter = new ShowListEmailAdapter(mActivity, mFilterArray, mSelectedItem,"cc");
        mRecyclerViewCC.setAdapter(mShowListAdapter);
    }


    @OnClick({R.id.llLeftLL, R.id.imgRightLL,R.id.toEditText})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llLeftLL:
                onBackPressed();
                break;

            case R.id.imgRightLL:
                /*Send Email*/
//                Intent mIntent = new Intent(mActivity, SendingInvoiceEmailActivity.class);
//                mIntent.putExtra("SubjectName", "Invoice    " + strInvoiceNumber + strInvoiceVesselName);
//                mIntent.putExtra("vesselID", mInVoicesModel.getInvoice_id());
//                startActivity(mIntent);
                break;
        }
    }
}