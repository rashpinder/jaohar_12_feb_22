package jaohar.com.jaohar.activities.modules_module;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.JaoharApplication;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.modules_adapter.ModulesListAdapter;
import jaohar.com.jaohar.beans.staff_module.Staff_Tab_Model;
import jaohar.com.jaohar.interfaces.forumModule.PaginationListForumAdapter;
import jaohar.com.jaohar.interfaces.modules.StaffModuleBottomInterface;
import jaohar.com.jaohar.models.GetAllAdminModuleModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

import static android.view.View.GONE;

public class AdminModuleListActivity extends AppCompatActivity implements StaffModuleBottomInterface {
    /*
     * set Activity
     * */
    Activity mActivity = AdminModuleListActivity.this;


    /*
     * set Activity TAG
     * */
    String TAG = AdminModuleListActivity.this.getClass().getSimpleName();


    /**
     * Widgets
     */
    LinearLayout llLeftLL;
    RelativeLayout imgRightLL, editRL, control_accessRL, disableRL, deleteRL;
    ImageView imgBack, imgRight;
    ProgressBar progressBottomPB;
    RecyclerView dataRV;
    SwipeRefreshLayout swipeToRefresh;
    TextView txtCenter, textDisableTV;
    int page_no = 1;
    String strLastPage = "TRUE", strStatus = "";
    boolean isSwipeRefresh = false;
    BottomSheetDialog mBottomSheetDialog;


    /*
     *Array List And Adapter
     * */
//    ArrayList<Staff_Tab_Model> mTabArrayList = new ArrayList<>();
    ArrayList<GetAllAdminModuleModel.Data.AllModule> mTabArrayList = new ArrayList<>();
    ModulesListAdapter mAdapter;
    StaffModuleBottomInterface mModuleInterFace;

    /*
     * Default Activity
     * @onCreate
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_admin_module_list);
        mModuleInterFace = this;
        /*
         * Setting Up View IDS
         * */
        setViewIDS();

        /*
         * Setting Up View Clicks
         * */
        setViewClicks();
    }

    /**
     * Recycler View Pagination Adapter Interface
     **/
    PaginationListForumAdapter mPaginationInterFace = new PaginationListForumAdapter() {
        @Override
        public void mPaginationforVessels(boolean isLastScroll) {
            if (isLastScroll == true) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (strLastPage.equals("FALSE")) {
                            progressBottomPB.setVisibility(View.VISIBLE);
                            ++page_no;
                            executeAPI();
                        } else {
                            if (progressBottomPB != null) {
                                progressBottomPB.setVisibility(GONE);
                            }
                        }
                    }
                }, 1500);
            }
        }
    };


    private void setViewIDS() {
        progressBottomPB = findViewById(R.id.progressBottomPB);
        dataRV = findViewById(R.id.dataRV);
        imgBack = findViewById(R.id.imgBack);
        llLeftLL = findViewById(R.id.llLeftLL);
        txtCenter = findViewById(R.id.txtCenter);
        imgRightLL = findViewById(R.id.imgRightLL);
        imgRight = findViewById(R.id.imgRight);
        imgRight.setVisibility(View.VISIBLE);
        imgRightLL.setVisibility(View.VISIBLE);

        /* set top bar views*/
        txtCenter.setText(getResources().getText(R.string.all_modules));
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.back));
        imgRight.setImageDrawable(getResources().getDrawable(R.drawable.add_icon));

        swipeToRefresh = findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setColorSchemeResources(R.color.colorAccent);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;
                page_no = 1;
                mTabArrayList.clear();
                executeAPI();
            }
        });

        /*
         * Bottom sheet Functionalty
         * */
        mBottomSheetDialog = new BottomSheetDialog(mActivity);
        @SuppressLint("InflateParams") View sheetView = mActivity.getLayoutInflater().inflate(R.layout.modules_bottom_sheet, null);
        mBottomSheetDialog.setContentView(sheetView);
        View bottomSheet = mBottomSheetDialog.findViewById(R.id.bottom_sheet);
        assert bottomSheet != null;
        editRL = bottomSheet.findViewById(R.id.editRL);
        control_accessRL = bottomSheet.findViewById(R.id.control_accessRL);
        disableRL = bottomSheet.findViewById(R.id.disableRL);
        deleteRL = bottomSheet.findViewById(R.id.deleteRL);
        textDisableTV = bottomSheet.findViewById(R.id.textDisableTV);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            //*Execute API For Getting List *//*
            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(GONE);
            }
            isSwipeRefresh = false;
            swipeToRefresh.setRefreshing(false);
            page_no = 1;
            mTabArrayList.clear();
            executeAPI();
        }
    }

    private void setViewClicks() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(mActivity, AddModuleActivity.class);
                startActivity(mIntent);
            }
        });


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    /* *
     * Execute API for GetAllModulesAdmin Module list
     * @param
     * @user_id
     * page_no
     * */
    public void executeAPI() {
        if (strLastPage.equals("FALSE")) {
            if (progressBottomPB != null) {
                progressBottomPB.setVisibility(View.VISIBLE);
            }
        } else {
            if (page_no == 1) {
                isSwipeRefresh = false;
                swipeToRefresh.setRefreshing(false);
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
            }
        }

//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<GetAllAdminModuleModel> call1 = mApiInterface.getAllModulesAdmin(JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""), String.valueOf(page_no));
        call1.enqueue(new Callback<GetAllAdminModuleModel>() {
            @Override
            public void onResponse(Call<GetAllAdminModuleModel> call, retrofit2.Response<GetAllAdminModuleModel> response) {
//                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLResponce***" + response);
                if (progressBottomPB != null) {
                    progressBottomPB.setVisibility(GONE);
                }
                if (isSwipeRefresh == true) {
                    isSwipeRefresh = false;
                    swipeToRefresh.setRefreshing(false);
                }
                Log.e(TAG, "*****Response****" + response);
                GetAllAdminModuleModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    strLastPage = mModel.getData().getLastPage();
                    ArrayList<GetAllAdminModuleModel.Data.AllModule> mTempraryList = new ArrayList<>();
                    mTempraryList.clear();
                    mTempraryList = mModel.getData().getAllModules();
                    mTabArrayList.addAll(mTempraryList);
                    if (page_no == 1) {
                        setTabAdapter();
                    } else {
                        mAdapter.notifyDataSetChanged();
                    }
                } else if (mModel.getStatus().equals("100")) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GetAllAdminModuleModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }
//    public void executeAPI() {
//        if (strLastPage.equals("FALSE")) {
//            if (progressBottomPB != null) {
//                progressBottomPB.setVisibility(View.VISIBLE);
//            }
//        } else {
//            if (page_no == 1) {
//                isSwipeRefresh = false;
//                swipeToRefresh.setRefreshing(false);
//                if (progressBottomPB != null) {
//                    progressBottomPB.setVisibility(GONE);
//                }
//            }
//        }
//
////        AlertDialogManager.showProgressDialog(mActivity);
//
//        String strUrl = JaoharConstants.GetAllModulesAdmin + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, "") + "&page_no=" + page_no;
//        Log.e(TAG, "***URL***" + strUrl);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
////                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***URLResponce***" + response);
//                if (progressBottomPB != null) {
//                    progressBottomPB.setVisibility(GONE);
//                }
//                if (isSwipeRefresh == true) {
//                    isSwipeRefresh = false;
//                    swipeToRefresh.setRefreshing(false);
//                }
//                Log.e(TAG, "*****Response****" + response);
//
//                try {
//                    JSONObject mJsonData = new JSONObject(response);
//                    if (mJsonData.getString("status").equals("1")) {
//                        parseResponceTabData(mJsonData);
//                    } else if (mJsonData.getString("status").equals("100")) {
//                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), mJsonData.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), mJsonData.getString("message"));
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
////                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }


//    private void parseResponceTabData(JSONObject mJsonData) {
//        try {
//            JSONObject mJsonObject = mJsonData.getJSONObject("data");
//            strLastPage = mJsonObject.getString("last_page");
//            JSONArray mJsonArray = mJsonObject.getJSONArray("all_modules");
//
//            if (mJsonArray != null) {
//                ArrayList<Staff_Tab_Model> mTempraryList = new ArrayList<>();
//                mTempraryList.clear();
//                for (int i = 0; i < mJsonArray.length(); i++) {
//                    JSONObject mJsonTabData = mJsonArray.getJSONObject(i);
//                    Staff_Tab_Model mModel = new Staff_Tab_Model();
//                    if (!mJsonTabData.getString("module_id").equals("")) {
//                        mModel.setModule_id(mJsonTabData.getString("module_id"));
//                    }
//                    if (!mJsonTabData.getString("access_type").equals("")) {
//                        mModel.setAccess_type(mJsonTabData.getString("access_type"));
//                    }
//                    if (!mJsonTabData.getString("module_name").equals("")) {
//                        mModel.setModule_name(mJsonTabData.getString("module_name"));
//                    }
//                    if (!mJsonTabData.getString("module_link").equals("")) {
//                        mModel.setModule_link(mJsonTabData.getString("module_link"));
//                    }
//                    if (!mJsonTabData.getString("module_image").equals("")) {
//                        mModel.setModule_image(mJsonTabData.getString("module_image"));
//                    }
//                    if (!mJsonTabData.getString("app_image").equals("")) {
//                        mModel.setApp_image(mJsonTabData.getString("app_image"));
//                    }
//
//                    if (!mJsonTabData.getString("type").equals("")) {
//                        mModel.setType(mJsonTabData.getString("type"));
//                    }
//                    if (!mJsonTabData.getString("access_to").equals("")) {
//                        mModel.setAccess_to(mJsonTabData.getString("access_to"));
//                    }
//                    if (!mJsonTabData.getString("disable").equals("")) {
//                        mModel.setDisable(mJsonTabData.getString("disable"));
//                    }
//                    if (!mJsonTabData.getString("creation_date").equals("")) {
//                        mModel.setCreation_date(mJsonTabData.getString("creation_date"));
//                    }
//                    mTempraryList.add(mModel);
//                }
//                mTabArrayList.addAll(mTempraryList);
//
//                if (page_no == 1) {
//                    setTabAdapter();
//                } else {
//                    mAdapter.notifyDataSetChanged();
//                }
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//    }

    private void setTabAdapter() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mActivity, 2);
        dataRV.setLayoutManager(gridLayoutManager);
//        dataRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mAdapter = new ModulesListAdapter(mActivity, mTabArrayList, mModuleInterFace, mPaginationInterFace);
        dataRV.setAdapter(mAdapter);
    }

    @Override
    public void mStaffModules(final GetAllAdminModuleModel.Data.AllModule mModel) {
        mBottomSheetDialog.show();

        strStatus = mModel.getDisable();
        if (mModel.getDisable().equals("0")) {
            textDisableTV.setText("Disable Module");
        } else {
            textDisableTV.setText("Enable Module");
        }

        editRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(mActivity, EditModuleActivity.class);
                mIntent.putExtra("model", String.valueOf(mModel));
                startActivity(mIntent);

                mBottomSheetDialog.dismiss();
            }
        });

        deleteRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utilities.isNetworkAvailable(mActivity) == false) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    executeDeleteAPI(mModel.getModuleId());
                }


                mBottomSheetDialog.dismiss();
            }
        });


        disableRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utilities.isNetworkAvailable(mActivity) == false) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    if (strStatus.equals("0")) {
                        strStatus = "1";
                        textDisableTV.setText("Enable Module");
                        executeEnableAPI(strStatus, mModel.getModuleId());

                    } else {
                        strStatus = "0";
                        textDisableTV.setText("Disable Module");
                        executeEnableAPI(strStatus, mModel.getModuleId());
                    }
                }


                mBottomSheetDialog.dismiss();
            }
        });

        control_accessRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(mActivity, StaffUserListActivity.class);
                mIntent.putExtra("model", String.valueOf(mModel));
                startActivity(mIntent);

                mBottomSheetDialog.dismiss();
            }
        });
    }


    /**
     * Implemented API to Enable or Disable Module data in Server @ControlModuleAccess
     *
     * @params
     * @disable
     * @module_id
     **/
    private void executeEnableAPI(final String strStatus, final String strModuleID) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.controlModuleDisability(strStatus, strModuleID, JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLADDResponce***" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                    isSwipeRefresh = false;
                    swipeToRefresh.setRefreshing(false);
                    page_no = 1;
                    mTabArrayList.clear();
                    mAdapter.notifyDataSetChanged();
                    executeAPI();
                } else {
                    Log.e(TAG, "Unexpected*********:" + mModel.getMessage());
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }


//    private void executeEnableAPI(final String strStatus, final String strModuleID) {
//        String strAPIUrl = "";
//        AlertDialogManager.showProgressDialog(mActivity);
//
//        strAPIUrl = JaoharConstants.ControlModuleDisability;
//
//
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strAPIUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "==Response==" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    AlertDialogManager.hideProgressDialog();
//                    String status = jsonObject.getString("status");
//                    String message = jsonObject.getString("message");
//                    if (status.equals("1")) {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), message);
//                        isSwipeRefresh = false;
//                        swipeToRefresh.setRefreshing(false);
//                        page_no = 1;
//                        mTabArrayList.clear();
//                        mAdapter.notifyDataSetChanged();
//                        executeAPI();
//                    } else {
//                        Log.e(TAG, "Unexpected*********:" + message);
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "***Error**" + error.toString());
//                AlertDialogManager.hideProgressDialog();
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + "Oops Some thing went wrong please wait");
//
//
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                HashMap<String, String> params = new HashMap<String, String>();
//                params.put("disable", strStatus);
//                params.put("module_id", strModuleID);
//                params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
//                return params;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }


    /**
     * Implemented API to Delete Module data in Server @DeleteModule
     *
     * @params
     * @module_id
     **/
    private void executeDeleteAPI(final String strModuleID) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteModule(strStatus,strModuleID);
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLADDResponce***" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                    isSwipeRefresh = false;
                    swipeToRefresh.setRefreshing(false);
                    page_no = 1;
                    mTabArrayList.clear();
                    mAdapter.notifyDataSetChanged();
                    executeAPI(); }
                else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }


//    private void executeDeleteAPI(final String strModuleID) {
//        String strAPIUrl = "";
//        AlertDialogManager.showProgressDialog(mActivity);
//        strAPIUrl = JaoharConstants.DeleteModule;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strAPIUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "==Response==" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    AlertDialogManager.hideProgressDialog();
//                    String status = jsonObject.getString("status");
//                    String message = jsonObject.getString("message");
//                    if (status.equals("1")) {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), message);
//                        isSwipeRefresh = false;
//                        swipeToRefresh.setRefreshing(false);
//                        page_no = 1;
//                        mTabArrayList.clear();
//                        mAdapter.notifyDataSetChanged();
//                        executeAPI();
//                    } else {
//                        Log.e(TAG, "Unexpected*********:" + message);
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "***Error**" + error.toString());
//                AlertDialogManager.hideProgressDialog();
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + "Oops Some thing went wrong please wait");
//
//
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                HashMap<String, String> params = new HashMap<String, String>();
//                params.put("disable", strStatus);
//                params.put("module_id", strModuleID);
//                return params;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }
}
