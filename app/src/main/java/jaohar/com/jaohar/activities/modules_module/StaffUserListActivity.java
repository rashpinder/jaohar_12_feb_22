package jaohar.com.jaohar.activities.modules_module;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.JaoharApplication;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.modules_adapter.ControlAccessAdapter;
import jaohar.com.jaohar.beans.staff_module.Staff_Tab_Model;
import jaohar.com.jaohar.beans.staff_module.UserDetailAdminModel;
import jaohar.com.jaohar.interfaces.modules.SendMultipleUserInterface;
import jaohar.com.jaohar.models.GetModuleUsersModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class StaffUserListActivity extends BaseActivity implements SendMultipleUserInterface {
    /*
     * set Activity
     * */
    Activity mActivity = StaffUserListActivity.this;


    /*
     * set Activity TAG
     * */
    String TAG = StaffUserListActivity.this.getClass().getSimpleName();


    /**
     * Widgets
     */
    LinearLayout llLeftLL, assignLL, disAllowLL, allowLL;
    TextView txtCenter;
    ImageView imgBack;
    RecyclerView dataRV;

    /*
     * Models & Array List
     * */
    Staff_Tab_Model mModel;
    ArrayList<UserDetailAdminModel> mUserArrayList = new ArrayList<>();
    ArrayList<String> mUserIDArray = new ArrayList<String>();

    ControlAccessAdapter mAdapter;
    SendMultipleUserInterface mInterfaceMultiSelect;

    /*
     * Strings & Integer
     * */
    String strModuleID = "";

    /*
     * Default Activity
     * @onCreate
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set status bar
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.activity_staff_user_list);

        mInterfaceMultiSelect = this;
    }

    /**
     * Set Widget IDS
     **/
    @Override
    protected void setViewsIDs() {
        imgBack = findViewById(R.id.imgBack);
        assignLL = findViewById(R.id.assignLL);
        disAllowLL = findViewById(R.id.disAllowLL);
        allowLL = findViewById(R.id.allowLL);
        dataRV = findViewById(R.id.dataRV);
        llLeftLL = findViewById(R.id.llLeftLL);
        txtCenter = findViewById(R.id.txtCenter);
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.back));
        txtCenter.setText(getResources().getString(R.string.all_users_list));

        /*
         * Retrieve data from previous Activity
         * */
        if (getIntent() != null) {
            mModel = (Staff_Tab_Model) getIntent().getSerializableExtra("model");
            strModuleID = mModel.getModule_id();
        }

        /*
         * Execute API to get List
         * */
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            mUserArrayList.clear();
            executeAllUserAPI();
        }
    }

    /**
     * Set Widget Clicks
     **/
    @Override
    protected void setClickListner() {
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        assignLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mUserIDArray.size() != 0) {
                    executeAccessSentAPI("selected");
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_user));
                }
            }
        });

        disAllowLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mUserIDArray.size() != 0) {
                    executeAccessSentAPI("disallow_all");
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.please_select_user));
                }
            }
        });

        allowLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeAccessSentAPI("allow_all");
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * Implemented API to Sent Access User List in Server @ControlModuleAccess
     *
     * @params
     * @user_id
     * @module_id
     * @access_type [ allow_all / disallow_all / selected ]
     * @user_list [ this will be an array (either json encoded array or normal array)]
     **/
    void executeAccessSentAPI(final String strType) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.controlModuleAccess(strModuleID,strType,String.valueOf(getMultiData()),JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***URLADDResponce***" + response);
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mUserIDArray.clear();
                    showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "***Error**" + t.getMessage());
            }
        });
    }

//    void executeAccessSentAPI(final String strType) {
//        AlertDialogManager.showProgressDialog(mActivity);
//        String strAPIUrl = JaoharConstants.ControlModuleAccess;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strAPIUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "==Response==" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    AlertDialogManager.hideProgressDialog();
//                    String status = jsonObject.getString("status");
//                    String message = jsonObject.getString("message");
//                    if (status.equals("1")) {
//                        mUserIDArray.clear();
//                        showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
//
//                    } else {
//                        Log.e(TAG, "Unexpected*********:" + message);
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "***Error**" + error.toString());
//                AlertDialogManager.hideProgressDialog();
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + "Oops Some thing went wrong please wait");
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                HashMap<String, String> params = new HashMap<String, String>();
//                params.put("module_id", strModuleID);
//                params.put("access_type", strType);
//                params.put("user_list", String.valueOf(getMultiData()));
//                params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
//                return params;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }


    private ArrayList<String> getMultiData() {
        ArrayList<String> strData = new ArrayList<>();
        strData.clear();
        Log.e(TAG, "ArrayLIST_DATA1============= " + mUserIDArray.size());
        for (int i = 0; i < mUserIDArray.size(); i++) {
            strData.add(mUserIDArray.get(i));
        }

        return removeDuplicates(strData);
    }


    /**
     * Implemented API to Get All User List From Server @GetModuleUsers
     *
     * @params
     * @user_id
     * @module_id
     **/
    void executeAllUserAPI() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getModuleUsers(strModuleID,JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "==Response==" + response);
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().toString());
                    AlertDialogManager.hideProgressDialog();
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    if (status.equals("1")) {
                        parseResponce(jsonObject);
                    } else {
                        Log.e(TAG, "Unexpected*********:" + message);
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

                @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {
            AlertDialogManager.hideProgressDialog();
            Log.e(TAG, "***Error**" + t.getMessage());
        }
    });}

//    void executeAllUserAPI() {
//        AlertDialogManager.showProgressDialog(mActivity);
//        String strAPIUrl = JaoharConstants.GetModuleUsers;
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, strAPIUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "==Response==" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    AlertDialogManager.hideProgressDialog();
//                    String status = jsonObject.getString("status");
//                    String message = jsonObject.getString("message");
//                    if (status.equals("1")) {
//                        parseResponce(jsonObject);
//                    } else {
//                        Log.e(TAG, "Unexpected*********:" + message);
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + message);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "***Error**" + error.toString());
//                AlertDialogManager.hideProgressDialog();
//                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + "Oops Some thing went wrong please wait");
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                HashMap<String, String> params = new HashMap<String, String>();
//                params.put("module_id", strModuleID);
//                params.put("user_id", JaoharPreference.readString(mActivity, JaoharPreference.ADMIN_ID, ""));
//                return params;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

    private void parseResponce(JSONObject jsonObject) {
        try {
            JSONArray mJsonDataArray = jsonObject.getJSONArray("users");
            if (mJsonDataArray != null) {
                for (int i = 0; i < mJsonDataArray.length(); i++) {
                    JSONObject mJsonData = mJsonDataArray.getJSONObject(i);
                    UserDetailAdminModel mUserModel = new UserDetailAdminModel();
                    if (!mJsonData.getString("id").equals("")) {
                        mUserModel.setId(mJsonData.getString("id"));
                    }
                    if (!mJsonData.getString("email").equals("")) {
                        mUserModel.setEmail(mJsonData.getString("email"));
                    }
                    if (!mJsonData.getString("role").equals("")) {
                        mUserModel.setRole(mJsonData.getString("role"));
                    }
                    if (!mJsonData.getString("module_access").equals("")) {
                        mUserModel.setModule_access(mJsonData.getString("module_access"));
                        if (mJsonData.getString("module_access").equals("false")) {
                            mUserModel.setSelected(false);
                        } else {
                            mUserModel.setSelected(true);
                        }
                    }
                    mUserArrayList.add(mUserModel);
                }
                setAdapter();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setAdapter() {
        dataRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mAdapter = new ControlAccessAdapter(mActivity, mUserArrayList, mInterfaceMultiSelect);
        dataRV.setAdapter(mAdapter);
    }

    @Override
    public void mSendMultipleUserInterface(UserDetailAdminModel mModel, boolean isChecked, int position) {
        if (mModel != null) {
            if (!isChecked) {
                mUserIDArray.add(mModel.getId());
            } else {
                mUserIDArray.remove(mModel.getId());
            }
        }
    }


    // Function to remove duplicates from an ArrayList
    public static <T> ArrayList<T> removeDuplicates(ArrayList<T> list) {

        // Create a new ArrayList
        ArrayList<T> newList = new ArrayList<T>();

        // Traverse through the first list
        for (T element : list) {

            // If this element is not present in newList
            // then add it
            if (!newList.contains(element)) {

                newList.add(element);
            }
        }

        // return the new list
        return newList;
    }

    public void showAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
}
