package jaohar.com.jaohar.activities.universal_invoice_module;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jaohar.com.jaohar.BaseActivity;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.interfaces.DeleteInvoiceCompanyInterFace;
import jaohar.com.jaohar.JaoharApplication;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.adapters.trash_module_adapter.InvoicingCompanyAdapter;
import jaohar.com.jaohar.beans.InvoiceCompanyModel;
import jaohar.com.jaohar.models.GetAllInvoiceTemplate;
import jaohar.com.jaohar.models.OwnersModel;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class InvoiceCompanyActivity extends BaseActivity {
    Activity mActivity = InvoiceCompanyActivity.this;
    String TAG = InvoiceCompanyActivity.this.getClass().getSimpleName();
//private ArrayList<InvoiceCompanyModel> modelArrayList = new ArrayList<>();
private ArrayList<OwnersModel.Datum> modelArrayList = new ArrayList<>();
private ImageView imgBack,imgRight;
private LinearLayout llLeftLL;
private RelativeLayout imgRightLL;
private TextView txtCenter;
private RecyclerView inVoicesRV;
private InvoicingCompanyAdapter mAdapter;

    DeleteInvoiceCompanyInterFace mDeleteCompany = new DeleteInvoiceCompanyInterFace() {
        @Override
        public void mDeleteInvoiceInterface(OwnersModel.Datum mModel) {
            deleteConfirmDialog(mModel);
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_company);
    }

    @Override
    protected void setViewsIDs() {
        llLeftLL=(LinearLayout) findViewById(R.id.llLeftLL);
        imgRightLL=(RelativeLayout) findViewById(R.id.imgRightLL);
        imgRightLL.setVisibility(View.VISIBLE);

        txtCenter=(TextView) findViewById(R.id.txtCenter);
        txtCenter.setText(getResources().getString(R.string.invoice_company));
        imgBack=(ImageView) findViewById(R.id.imgBack);
        imgRight=(ImageView) findViewById(R.id.imgRight);
        inVoicesRV=(RecyclerView) findViewById(R.id.inVoicesRV);
        imgRight.setImageResource(R.drawable.plus_symbol);
        imgBack.setImageResource(R.drawable.back);
    }

    @Override
    protected void setClickListner() {
        imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mActivity, AddInvoicingCompanyActivity.class);
                startActivity(mIntent);
            }
        });
        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(mActivity) == false) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            //*Execute Vesseles API*//*
            gettingLIstOFInvoiceCompany();
        }
    }

    public void gettingLIstOFInvoiceCompany() {
        modelArrayList.clear();
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<OwnersModel> call1 = mApiInterface.getAllOwnersRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<OwnersModel>() {
            @Override
            public void onResponse(Call<OwnersModel> call, retrofit2.Response<OwnersModel> response) {
                Log.e(TAG, "******Response*****" + response);
                AlertDialogManager.hideProgressDialog();
                OwnersModel mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    modelArrayList=mModel.getData();
                setAdapter();
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<OwnersModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

//    private void gettingLIstOFInvoiceCompany() {
////      https://root.jaohar.com/Staging/JaoharWebServicesNew/GetAllOwners.php
//        modelArrayList.clear();
//        AlertDialogManager.showProgressDialog(mActivity);
//        String strUrl = JaoharConstants.GetAllOwners + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "");
//        Log.e(TAG, "***URL***" + strUrl);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "*****ResponseALL****" + response);
//                parseResponce(response);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }

//    private void parseResponce(String response) {
//        JSONObject mJSonObject = null;
//        try {
//            mJSonObject = new JSONObject(response);
//            String strStatus = mJSonObject.getString("status");
//            String strMessage = mJSonObject.getString("message");
//            if (strStatus.equals("1")) {
//                JSONArray mjsonArrayData = mJSonObject.getJSONArray("data");
//                if (mjsonArrayData != null) {
//                    for (int i = 0; i < mjsonArrayData.length(); i++) {
//                        JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
//                        InvoiceCompanyModel mInvoiceCompanyModel = new InvoiceCompanyModel();
//                        if(!mJsonDATA.getString("owner_id").equals("")){
//                            mInvoiceCompanyModel.setOwner_id(mJsonDATA.getString("owner_id"));
//                        }if(!mJsonDATA.getString("owner_name").equals("")){
//                            mInvoiceCompanyModel.setOwner_name(mJsonDATA.getString("owner_name"));
//                        }
//                        modelArrayList.add(mInvoiceCompanyModel);
//                    }
//
//                    setAdapter();
//                }
//            }else {
//                AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), strMessage);
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    private void setAdapter() {
        inVoicesRV.setLayoutManager(new LinearLayoutManager(mActivity));
        mAdapter = new InvoicingCompanyAdapter(mActivity, modelArrayList,mDeleteCompany);
        inVoicesRV.setAdapter(mAdapter);
    }

    public void deleteConfirmDialog(final OwnersModel.Datum mModel) {
        final Dialog deleteConfirmDialog = new Dialog(mActivity);
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_delete_invoice_company));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                executeDeleteAPI(mModel.getOwnerId());

            }
        });

        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });

        deleteConfirmDialog.show();
    }


    private void executeDeleteAPI(String strOwnerID) {
        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.deleteOwnerRequest(JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                Log.e(TAG, "******Response*****" + response);
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus()==1) {
                    //Toast.makeText(mActivity,""+mJsonObject.getString("message"),Toast.LENGTH_LONG).show();
                    modelArrayList.clear();
                    gettingLIstOFInvoiceCompany();
                } else if (mModel.getStatus()==100) {
                    AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mModel.getMessage());
                } else {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" +mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }


//    private void executeDeleteAPI(String strOwnerID) {
////        https://root.jaohar.com/Staging/JaoharWebServicesNew/DeleteOwner.php
//        AlertDialogManager.showProgressDialog(mActivity);
//        String strUrl = JaoharConstants.DeleteOwner + "?user_id=" + JaoharPreference.readString(mActivity, JaoharPreference.STAFF_ID, "")+"&owner_id="+strOwnerID;
//        Log.e(TAG, "***URL***" + strUrl);
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                AlertDialogManager.hideProgressDialog();
//
//                Log.e(TAG, "*****ResponseALL****" + response);
//                AlertDialogManager.hideProgressDialog();
//                try {
//                    JSONObject mJsonObject = new JSONObject(response);
//                    if (mJsonObject.getString("status").equals("1")) {
//                        //Toast.makeText(mActivity,""+mJsonObject.getString("message"),Toast.LENGTH_LONG).show();
//                        modelArrayList.clear();
//                        gettingLIstOFInvoiceCompany();
//                    } else if (mJsonObject.getString("status").equals("100")) {
//                        AlertDialogManager.showAccountDiableDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    } else {
//                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "" + mJsonObject.getString("message"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialogManager.hideProgressDialog();
//                Log.e(TAG, "***Error**" + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//        };
//        JaoharApplication.getInstance().addToRequestQueue(jsonObjectRequest);
//    }
}
