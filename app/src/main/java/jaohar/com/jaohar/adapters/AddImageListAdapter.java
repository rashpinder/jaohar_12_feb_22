package jaohar.com.jaohar.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.AddGalleryImagesModel;
import jaohar.com.jaohar.interfaces.DeleteImagesVessels;

/**
 * Created by Dharmani Apps on 5/29/2017.
 */

public class AddImageListAdapter extends RecyclerView.Adapter<AddImageListAdapter.ViewHolder> {

    Context mContext;
    ArrayList<AddGalleryImagesModel> mArrayList;
    ArrayList<String> tempDeletedImages = new ArrayList<String>();
    private DeleteImagesVessels mDeleteImages;

    public AddImageListAdapter(Context mContext, ArrayList<AddGalleryImagesModel> mArrayList, DeleteImagesVessels mDeleteImages) {
        this.mContext = mContext;
        this.mArrayList = mArrayList;
        this.mDeleteImages = mDeleteImages;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_vessel_images, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final AddGalleryImagesModel tempValues = mArrayList.get(position);
        Log.e("TAG", "DATASIZE" + "VideoMain" + mArrayList.size());

        if (tempValues.getStrImagePath().contains("http")) {
            Log.e("TAG","DATASIZE"+"VideoMainHhtp"+mArrayList.size());
            if(tempValues.getStrType().equals("doc")){
                holder.itemIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.icon_doc));

            }else if(tempValues.getStrType().equals("video")){
                Log.e("TAG","DATASIZE"+"Video"+mArrayList.size());
                holder.itemIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.video_icon));

            }else {
                RequestOptions requestOptions = new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
                        .skipMemoryCache(true);
                Glide.with(mContext).asBitmap().apply(requestOptions).load(tempValues.getStrImagePath()).into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        // you can do something with loaded bitmap here
                        holder.itemIV.setImageBitmap(resource);
                    }
                });
            }

        } else {
//        Log.e("TAG", "DATASIZE" + "Video" + mArrayList.size());

        if (tempValues.getmBitmap() != null) {
            holder.itemIV.setImageBitmap(tempValues.getmBitmap());
        } else {
            Log.e("TAG", "DATASIZE" + "Video" + mArrayList.size());
            if (tempValues.getStrType().equals("doc")) {
                holder.itemIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.icon_doc));
            } else if (tempValues.getStrType().equals("video")) {
                Log.e("TAG", "DATASIZE" + "Video" + mArrayList.size());
                holder.itemIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.video_icon));
            }
        }
        }

        holder.itemDeleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tempValues.getStrType().equals("doc")){
                    mDeleteImages.deleteImagesVessels(tempValues,position);
                }else {
                    mDeleteImages.deleteImagesVessels(tempValues,position);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        Log.e("TAG", "DATASIZE" + "VideoMain" + mArrayList.size());
        return mArrayList.size();
    }

    private void loadImageFromStorage(String imgPath, ImageView imgImageView) {
        try {
            File f = new File(imgPath);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            imgImageView.setImageBitmap(b);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }


    private Bitmap loadImageFromBitmap(String base64) {
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Bitmap original = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        original.compress(Bitmap.CompressFormat.PNG, 50, out);
        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
        return decoded;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView itemDeleteIV;
        public ImageView itemIV;

        public ViewHolder(View itemView) {
            super(itemView);
            itemIV = (ImageView) itemView.findViewById(R.id.itemIV);
            itemDeleteIV = (ImageView) itemView.findViewById(R.id.itemDeleteIV);
        }
    }
}

