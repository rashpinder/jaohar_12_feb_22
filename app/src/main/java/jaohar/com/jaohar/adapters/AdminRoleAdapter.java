package jaohar.com.jaohar.adapters;

import android.app.Activity;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.AdminRoleModel;
import jaohar.com.jaohar.interfaces.ChangeRoleInterface;
import jaohar.com.jaohar.interfaces.DeleteUserInterface;
import jaohar.com.jaohar.interfaces.EnableDisableAdimRoleInterface;
import jaohar.com.jaohar.models.AdminRoleData;
import jaohar.com.jaohar.models.AllUser;


/**
 * Created by Dharmani Apps on 7/11/2017.
 */

public class AdminRoleAdapter extends RecyclerView.Adapter<AdminRoleAdapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<AllUser> modelArrayList;
    private EnableDisableAdimRoleInterface mEnableDisableAdimRoleInterface;
    private ChangeRoleInterface mChangeRoleInterface;
    private DeleteUserInterface mDeleteUserInterface;

    public AdminRoleAdapter(Activity mActivity, ArrayList<AllUser> modelArrayList, EnableDisableAdimRoleInterface mEnableDisableAdimRoleInterface, ChangeRoleInterface mChangeRoleInterface, DeleteUserInterface mDeleteUserInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mEnableDisableAdimRoleInterface = mEnableDisableAdimRoleInterface;
        this.mChangeRoleInterface = mChangeRoleInterface;
        this.mDeleteUserInterface = mDeleteUserInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_admin_role, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final AllUser tempValue = modelArrayList.get(position);

        holder.resetLL.setVisibility(View.VISIBLE);
        if (!tempValue.getFirstName().equals("")) {
            holder.itemNameTV.setText(tempValue.getFirstName());
        } else {
            holder.itemNameTV.setText("");
        }

        if (!tempValue.getLastName().equals("")) {
            holder.itemNameTV.setText(tempValue.getFirstName() + " " + tempValue.getLastName());
        }

        holder.itemEmailTV.setText(tempValue.getEmail());
        holder.itemloginTimeTV.setText(tempValue.getLoginTime());
        holder.itemdeviceTV.setText(tempValue.getLoginDevice());
        holder.itemRoleTV.setText(tempValue.getRole());
        holder.itemonlineTV.setText(tempValue.getOnlineStatus());
        holder.itemCompanyTV.setText(tempValue.getCompanyName());

        if (tempValue.getEnabled().equals("1")) {
            holder.itemtxtDisableTV.setText("Disable");
            holder.itemTxtEnableDisableTV.setText("Enabled");
            holder.itemTxtEnableDisableTV.setBackgroundDrawable(ContextCompat.getDrawable(mActivity, R.drawable.bg_green));
        } else if (tempValue.getEnabled().equals("0")) {
            holder.itemtxtDisableTV.setText("Enable");
            holder.itemTxtEnableDisableTV.setText("Disabled");
            holder.itemTxtEnableDisableTV.setBackgroundDrawable(ContextCompat.getDrawable(mActivity, R.drawable.bg_red));
        }

        holder.itemtxtDisableTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEnableDisableAdimRoleInterface.getEnableDisableAdminRole(tempValue);
            }
        });

        holder.itemtxtEditTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mChangeRoleInterface.getChangedRole(tempValue);
            }
        });

        holder.itemtxtDeleteTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDeleteUserInterface.deleteUser(tempValue);
            }
        });

        holder.itemtxtResetTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mChangeRoleInterface.getResstPassword(tempValue);
            }
        });

    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView itemNameTV, itemEmailTV, itemRoleTV, itemtxtDisableTV,
                itemtxtEditTV, itemtxtDeleteTV, itemTxtEnableDisableTV, itemloginTimeTV,
                itemdeviceTV, itemonlineTV, itemtxtResetTV, itemCompanyTV;
        LinearLayout resetLL;

        ViewHolder(View itemView) {
            super(itemView);
            itemNameTV = (TextView) itemView.findViewById(R.id.itemNameTV);
            itemEmailTV = (TextView) itemView.findViewById(R.id.itemEmailTV);
            itemRoleTV = (TextView) itemView.findViewById(R.id.itemRoleTV);
            itemtxtDisableTV = (TextView) itemView.findViewById(R.id.itemtxtDisableTV);
            itemtxtEditTV = (TextView) itemView.findViewById(R.id.itemtxtEditTV);
            itemtxtDeleteTV = (TextView) itemView.findViewById(R.id.itemtxtDeleteTV);
            itemTxtEnableDisableTV = (TextView) itemView.findViewById(R.id.itemTxtEnableDisableTV);
            itemloginTimeTV = (TextView) itemView.findViewById(R.id.itemloginTimeTV);
            itemdeviceTV = (TextView) itemView.findViewById(R.id.itemdeviceTV);
            itemonlineTV = (TextView) itemView.findViewById(R.id.itemonlineTV);
            itemtxtResetTV = (TextView) itemView.findViewById(R.id.itemtxtResetTV);
            resetLL = (LinearLayout) itemView.findViewById(R.id.resetLL);
            itemCompanyTV = itemView.findViewById(R.id.itemCompanyTV);
        }
    }
}