package jaohar.com.jaohar.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.AllNewsActivity;
import jaohar.com.jaohar.activities.DetailsActivity;
import jaohar.com.jaohar.beans.VesselesModel;
import jaohar.com.jaohar.interfaces.FavoriteVesselInterface;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.OpenPopUpVesselInterface;
import jaohar.com.jaohar.interfaces.paginationforVesselsInterface;
import jaohar.com.jaohar.models.AllVessel;
import jaohar.com.jaohar.utils.JaoharConstants;

public class FavoriteVesselAdapter extends RecyclerView.Adapter<FavoriteVesselAdapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<AllVessel> modelArrayList;
    private paginationforVesselsInterface mPagination;
    private OnClickInterface onClickInterface;
    private FavoriteVesselInterface favoriteVesselInterface;
    private ArrayList<String> FavouriteItems = new ArrayList<>();

    public FavoriteVesselAdapter(Activity mActivity, ArrayList<AllVessel> modelArrayList,
                                 paginationforVesselsInterface mPagination,
                                 OnClickInterface onClickInterface, FavoriteVesselInterface favoriteVesselInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mPagination = mPagination;
        this.onClickInterface = onClickInterface;
        this.favoriteVesselInterface = favoriteVesselInterface;
    }

    @Override
    public FavoriteVesselAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_vessels, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final FavoriteVesselAdapter.ViewHolder holder, final int position) {
        final AllVessel tempValue = modelArrayList.get(position);

        if (position >= modelArrayList.size() - 1) {
            mPagination.mPaginationforVessels(true);
        }

        if (tempValue.getPhoto1().length() > 0 && tempValue.getPhoto1().contains("http")) {
            Glide.with(mActivity).load(tempValue.getPhoto1()).into(holder.item_Image);

        } else {
            holder.item_Image.setImageResource(R.drawable.palace_holder);
        }

        if (tempValue.getVesselName().length() > 0) {
            holder.item_Title.setText(tempValue.getVesselName());
        }
        if (tempValue.getVesselType().length() > 0) {
            holder.item_DateTime.setText(tempValue.getVesselType() + "/" + tempValue.getYearBuilt() + "/" + tempValue.getPlaceOfBuilt());
        }
        if (tempValue.getShortDescription().length() > 0) {
            holder.item_Type.setText("DWT " + tempValue.getCapacity() + "/" + tempValue.getPriceIdea() + " " + tempValue.getCurrency());
        }

        holder.imgRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(mActivity, DetailsActivity.class);
                mIntent.putExtra("Model", tempValue);
                mIntent.putExtra("isEditShow", false);
                mActivity.startActivity(mIntent);
            }
        });

        /*Vessels Status on Vessel Image*/
        if (tempValue.getStatus().equals("Sold")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.vessel_sold);
        } else if (tempValue.getStatus().equals("Withdrawn")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.vessel_withdrawn);
        } else if (tempValue.getStatus().equals("Commited")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.vessel_committed);
        } else if (tempValue.getStatus().equals("Scraped")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.vessel_scraped);
        } else if (tempValue.getStatus().equals("Hot Sale")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.vessel_hotsale);
        } else {
            holder.item_Image_Status.setVisibility(View.GONE);
        }

        holder.optionsImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickInterface.mOnClickInterface(position);
            }
        });

        if (tempValue.getFavoriteStatus().equals("0")) {
            holder.favImg.setImageResource(R.drawable.ic_bookmark_border);
            FavouriteItems.remove(String.valueOf(position));
        } else if (tempValue.getFavoriteStatus().equals("1")) {
            holder.favImg.setImageResource(R.drawable.ic_bookmark_yellow);
            FavouriteItems.add(String.valueOf(position));
        } else {
            holder.favImg.setImageResource(R.drawable.ic_bookmark_border);
        }

        holder.favImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favoriteVesselInterface.mFavoriteVesselInterface(position, "0", holder.favImg);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList == null ? 0 : modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView item_Title, item_DateTime, item_Type;
        public ImageView itemImgNext, item_Image_Status;
        public ImageView item_Image, linkIMG, favImg, optionsImg;
        public CardView cardViewItem;
        public RelativeLayout itemClickRL, imgRL;

        ViewHolder(View itemView) {
            super(itemView);

            item_Image = itemView.findViewById(R.id.item_Image);
            item_Image_Status = itemView.findViewById(R.id.item_Image_Status);
            itemImgNext = itemView.findViewById(R.id.itemImgNext);
            item_Title = itemView.findViewById(R.id.item_Title);
            item_DateTime = itemView.findViewById(R.id.item_DateTime);
            item_Type = itemView.findViewById(R.id.item_Type);
            cardViewItem = itemView.findViewById(R.id.cardViewItem);
            itemClickRL = itemView.findViewById(R.id.itemClickRL);
            linkIMG = itemView.findViewById(R.id.linkIMG);
            imgRL = itemView.findViewById(R.id.imgRL);
            favImg = itemView.findViewById(R.id.favImg);
            optionsImg = itemView.findViewById(R.id.optionsImg);
        }
    }
}
