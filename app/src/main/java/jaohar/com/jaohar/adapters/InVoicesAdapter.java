package jaohar.com.jaohar.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.EditInvoiceActivity;
import jaohar.com.jaohar.activities.InvoiceDetailsActivity;
import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.interfaces.DeleteInvoiceInterface;
import jaohar.com.jaohar.interfaces.OnClickInterface;
import jaohar.com.jaohar.interfaces.PDFdownloadInterface;
import jaohar.com.jaohar.interfaces.SendMultipleInvoiceMAIlInterface;
import jaohar.com.jaohar.interfaces.SinglemailInvoiceInterface;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.Utilities;

/**
 * Created by Dharmani Apps on 7/11/2017.
 */

public class InVoicesAdapter extends RecyclerView.Adapter<InVoicesAdapter.ViewHolder> {
    DeleteInvoiceInterface mDeleteVesselsInterface;
    private Activity mActivity;
    private ArrayList<InVoicesModel> modelArrayList;
    PDFdownloadInterface mPdfDownloader;
    SinglemailInvoiceInterface mInvoiceI;
    SendMultipleInvoiceMAIlInterface sendMultipleInvoiceMAIlInterface;
    private static HashMap<InVoicesModel, Boolean> checkedForModel = new HashMap<>();
    private static boolean[] checkBoxState = null;
    private OnClickInterface onClickInterface;

    public InVoicesAdapter(Activity mActivity, ArrayList<InVoicesModel> modelArrayList,
                           DeleteInvoiceInterface mDeleteVesselsInterface, PDFdownloadInterface mPdfDownloader,
                           SinglemailInvoiceInterface mInvoiceI, SendMultipleInvoiceMAIlInterface sendMultipleInvoiceMAIlInterface,
                           OnClickInterface onClickInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mDeleteVesselsInterface = mDeleteVesselsInterface;
        this.mPdfDownloader = mPdfDownloader;
        this.mInvoiceI = mInvoiceI;
        this.sendMultipleInvoiceMAIlInterface = sendMultipleInvoiceMAIlInterface;
        checkBoxState = new boolean[modelArrayList.size()];
        this.onClickInterface = onClickInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_invoice_new, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final InVoicesModel tempValue = modelArrayList.get(position);

        holder.inVoiceNumberTV.setText(tempValue.getInvoice_number());

        if (tempValue.getInv_state() != null && !tempValue.getInv_state().equals("")
                && tempValue.getInv_state().equals("Pending")) {
            holder.statusIV.setImageResource(R.drawable.ic_invoice_pending);
        } else {
            holder.statusIV.setImageResource(R.drawable.ic_tick_green);
        }

        if (tempValue.getmCompaniesModel() != null) {
            holder.inVoiceCompanyNameTV.setText(html2text(tempValue.getmCompaniesModel().getCompany_name()));
        }

        holder.item_InVoiceNumberTV.setText(tempValue.getInvoice_number());
        holder.item_FileDownloadIV.setVisibility(View.VISIBLE);
        String strDateFormat = null;


        /* checkBoxState has the value of checkBox ie true or false,
         * The position is used so that on scroll your selected checkBox maintain its state */
        if (checkBoxState != null) {
            holder.imgMultiSelectIV.setChecked(checkBoxState[position]);
        }

        try {
            strDateFormat = Utilities.gettingFormatTime(tempValue.getInvoice_date().replace("_", " "));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.item_InVoiceDateTV.setText(strDateFormat);

        holder.inVoiceDateTV.setText(strDateFormat);

        if (tempValue.getmVesselSearchInvoiceModel() != null) {
            holder.item_InVoiceVesselTV.setText(html2text(tempValue.getmVesselSearchInvoiceModel().getVessel_name()));
        }
        if (tempValue.getmCompaniesModel() != null) {
            holder.item_InVoiceCompany.setText(html2text(tempValue.getmCompaniesModel().getCompany_name()));
        }

        holder.item_FileDownloadIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPdfDownloader.pdfDownloadInterface(tempValue.getPdf(), tempValue.getPdf_name());
            }
        });

        holder.item_EditIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JaoharConstants.Invoice_ID = tempValue.getInvoice_id();
                Intent mIntent = new Intent(mActivity, EditInvoiceActivity.class);
                mIntent.putExtra("Model", tempValue);
                mActivity.startActivity(mIntent);
            }
        });

        holder.item_DeleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDeleteVesselsInterface.deleteInvoice(tempValue);
            }
        });

        holder.item_MailDetailsIV.setVisibility(View.VISIBLE);

        holder.item_MailDetailsIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mInvoiceI.mSinglemailInvoice(tempValue);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JaoharConstants.Invoice_ID = tempValue.getInvoice_id();
                Intent mIntent = new Intent(mActivity, InvoiceDetailsActivity.class);
                mIntent.putExtra("Model", tempValue);
                mActivity.startActivity(mIntent);
            }
        });

        holder.layoutItemRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JaoharConstants.Invoice_ID = tempValue.getInvoice_id();
                Intent mIntent = new Intent(mActivity, InvoiceDetailsActivity.class);
                mIntent.putExtra("Model", tempValue);
                mActivity.startActivity(mIntent);
            }
        });

        holder.imgMultiSelectIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.imgMultiSelectIV.isChecked()) {
                    checkBoxState[position] = true;
                    ischecked(position, true);
                    sendMultipleInvoiceMAIlInterface.mSendMutliInvoice(tempValue, false);
                } else {
                    checkBoxState[position] = false;
                    ischecked(position, false);
                    sendMultipleInvoiceMAIlInterface.mSendMutliInvoice(tempValue, true);
                }

            }
        });
        /*if country is in checkedForCountry then set the checkBox to true */
        if (checkedForModel.get(tempValue) != null) {
            holder.imgMultiSelectIV.setChecked(checkedForModel.get(tempValue));
        }

        /*Set tag to all checkBox*/
        holder.imgMultiSelectIV.setTag(tempValue);

        holder.optionsIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickInterface.mOnClickInterface(position);
            }
        });
    }

    private void ischecked(int position, boolean flag) {
        checkedForModel.put(this.modelArrayList.get(position), flag);
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView item_InVoiceNumberTV, item_InVoiceDateTV, item_InVoiceVesselTV, item_InVoiceCompany;
        public ImageView item_Image, item_EditIV, item_DeleteIV, item_MailDetailsIV, item_FileDownloadIV,
                statusIV, optionsIV;
        RelativeLayout layoutItemRL;
        CheckBox imgMultiSelectIV;

        private TextView inVoiceNumberTV, inVoiceCompanyNameTV, inVoiceDateTV;

        ViewHolder(View itemView) {
            super(itemView);

            optionsIV = itemView.findViewById(R.id.optionsIV);
            statusIV = itemView.findViewById(R.id.statusIV);
            item_Image = itemView.findViewById(R.id.item_Image);
            item_EditIV = itemView.findViewById(R.id.item_EditIV);
            item_DeleteIV = itemView.findViewById(R.id.item_DeleteIV);
            item_MailDetailsIV = itemView.findViewById(R.id.item_MailDetailsIV);
            item_FileDownloadIV = itemView.findViewById(R.id.item_FileDownloadIV);
            item_InVoiceNumberTV = itemView.findViewById(R.id.item_InVoiceNumberTV);
            item_InVoiceDateTV = itemView.findViewById(R.id.item_InVoiceDateTV);
            item_InVoiceVesselTV = itemView.findViewById(R.id.item_InVoiceVesselTV);
            item_InVoiceCompany = itemView.findViewById(R.id.item_InVoiceCompany);
            layoutItemRL = itemView.findViewById(R.id.layoutItemRL);
            imgMultiSelectIV = itemView.findViewById(R.id.imgMultiSelectIV);

            inVoiceDateTV = itemView.findViewById(R.id.inVoiceDateTV);
            inVoiceNumberTV = itemView.findViewById(R.id.inVoiceNumberTV);
            inVoiceCompanyNameTV = itemView.findViewById(R.id.inVoiceCompanyNameTV);
        }
    }

    //for converting html to text
    public CharSequence html2text(String html) {
        CharSequence trimmed = noTrailingwhiteLines(Html.fromHtml(html.replace("\n", "<br />")));
        return trimmed;
    }

    //for hiding extra white space
    private CharSequence noTrailingwhiteLines(CharSequence text) {

        if (text.length() > 0)
            while (text.charAt(text.length() - 1) == '\n') {
                text = text.subSequence(0, text.length() - 1);
            }
        return text;
    }
}