package jaohar.com.jaohar.adapters;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.InvoiceAddItemModel;


/**
 * Created by Dharmani Apps on 7/11/2017.
 */

public class InvoiceDetailsItemsAdapter extends RecyclerView.Adapter<InvoiceDetailsItemsAdapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<InvoiceAddItemModel> modelArrayList;
    private String strCurrency;
//    private GetSubTotalAmountInterface mGetSubTotalAmountInterface;

    public InvoiceDetailsItemsAdapter(Activity mActivity, ArrayList<InvoiceAddItemModel> modelArrayList, String strCurrency) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.strCurrency = strCurrency;
//        this.mGetSubTotalAmountInterface = mGetSubTotalAmountInterface;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_invoicedetails_item_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final InvoiceAddItemModel tempValue = modelArrayList.get(position);
        Log.e("Adapter", tempValue.getDescription());

        //Set Adapter Values of Items
        if (!tempValue.getItem().equals("0"))
            holder.item_idTV.setText(tempValue.getItem());
        if (!tempValue.getDescription().equals("0"))
            holder.item_DescriptionTV.setText(tempValue.getDescription());
        if (tempValue.getQuantity() != 0)
            holder.item_QuantityTV.setText("" + tempValue.getQuantity());
        if (tempValue.getUnitprice().equals(""))
            holder.item_UnitPriceTV.setText("" + tempValue.getUnitprice() + " " + strCurrency);

        double intItems = tempValue.getQuantity();
        double intUnitPrice = Double.parseDouble(tempValue.getUnitprice());

        //Set the Amount Quantity * UnitPrice
        double mAmount = intItems * intUnitPrice;
        if (mAmount > 0)
            holder.item_AmountTV.setText("" + mAmount + " " + strCurrency);


        //Set Background of Row
        if (position % 2 == 0) {
            holder.itemParentLL.setBackgroundResource(R.drawable.pdf_row_odd);
        } else {
            holder.itemParentLL.setBackgroundResource(R.drawable.pdf_row_even);
        }

    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView item_idTV, item_DescriptionTV, item_QuantityTV, item_UnitPriceTV, item_AmountTV;
        public LinearLayout itemParentLL;

        ViewHolder(View itemView) {
            super(itemView);

            item_idTV = (TextView) itemView.findViewById(R.id.item_idTV);
            item_DescriptionTV = (TextView) itemView.findViewById(R.id.item_DescriptionTV);
            item_QuantityTV = (TextView) itemView.findViewById(R.id.item_QuantityTV);
            item_UnitPriceTV = (TextView) itemView.findViewById(R.id.item_UnitPriceTV);
            item_AmountTV = (TextView) itemView.findViewById(R.id.item_AmountTV);
            itemParentLL = (LinearLayout) itemView.findViewById(R.id.itemParentLL);


        }
    }
}