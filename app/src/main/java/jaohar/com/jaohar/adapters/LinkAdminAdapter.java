package jaohar.com.jaohar.adapters;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.interfaces.DeleteLinkAdminInterface;
import jaohar.com.jaohar.interfaces.EditLinkAdminInterface;
import jaohar.com.jaohar.models.LinksToHomeData;


/**
 * Created by Dharmani Apps on 7/11/2017.
 */

public class LinkAdminAdapter extends RecyclerView.Adapter<LinkAdminAdapter.ViewHolder> {
    EditLinkAdminInterface mEditLinkAdminInterface;
    DeleteLinkAdminInterface mDeleteLinkAdminInterface;
    private Activity mActivity;
    private ArrayList<LinksToHomeData> modelArrayList;

    public LinkAdminAdapter(Activity mActivity, ArrayList<LinksToHomeData> modelArrayList, EditLinkAdminInterface mEditLinkAdminInterface, DeleteLinkAdminInterface mDeleteLinkAdminInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mEditLinkAdminInterface = mEditLinkAdminInterface;
        this.mDeleteLinkAdminInterface = mDeleteLinkAdminInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_link_admin_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final LinksToHomeData tempValue = modelArrayList.get(position);
        holder.txtLinkHomeNameTV.setText(tempValue.getLinkName());
        holder.txtLinkHomeValueTV.setText(tempValue.getLinkValue());


        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditLinkAdminInterface.mEditLinkAdimn(tempValue);
            }
        });
        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDeleteLinkAdminInterface.mDeleteLinkAdmin(tempValue);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtLinkHomeNameTV, txtLinkHomeValueTV, txtEdit, txtDelete;

        ViewHolder(View itemView) {
            super(itemView);
            txtEdit = (TextView) itemView.findViewById(R.id.txtEdit);
            txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);
            txtLinkHomeNameTV = (TextView) itemView.findViewById(R.id.txtLinkHomeNameTV);
            txtLinkHomeValueTV = (TextView) itemView.findViewById(R.id.txtLinkHomeValueTV);
        }
    }
}