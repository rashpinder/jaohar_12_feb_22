package jaohar.com.jaohar.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.SendMailingListActivity;
import jaohar.com.jaohar.beans.MailingListModel;
import jaohar.com.jaohar.interfaces.SendingMulipleLISTID;
import jaohar.com.jaohar.models.AllMailList;

/**
 * Created by dharmaniz on 15/1/19.
 */

public class MailingListStaffAdapter extends RecyclerView.Adapter<MailingListStaffAdapter.ViewHolder> {
    private Activity mActivity;
//    private ArrayList<MailingListModel> modelArrayList;
    private ArrayList<AllMailList> modelArrayList;
    SendingMulipleLISTID mMultimaIlInterface;
//    private static HashMap<MailingListModel, Boolean> checkedForModel = new HashMap<>();
    private static HashMap<AllMailList, Boolean> checkedForModel = new HashMap<>();
    private static boolean[] checkBoxState = null;
    public ArrayList<String> Items = new ArrayList<>();
    ClickManagerModulesInterface clickManagerModulesInterface;

    public interface ClickManagerModulesInterface {
        void mClickManagerModulesInterface(int position, String Id, String name);
    }

    public MailingListStaffAdapter(Activity mActivity, ArrayList<AllMailList> modelArrayList, SendingMulipleLISTID mMultimaIlInterface,
                                   ClickManagerModulesInterface clickManagerModulesInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mMultimaIlInterface = mMultimaIlInterface;
        checkBoxState = new boolean[modelArrayList.size()];
        this.clickManagerModulesInterface = clickManagerModulesInterface;
    }

    @Override
    public MailingListStaffAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_mailing_list, parent, false);
        return new MailingListStaffAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MailingListStaffAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
//        final MailingListModel tempValue = modelArrayList.get(position);
        final AllMailList tempValue = modelArrayList.get(position);
        String strCOUNT = String.valueOf(position + 1);
//        holder.editRL.setVisibility(View.GONE);
//        holder.delRL.setVisibility(View.GONE);
//        holder.mailRL.setVisibility(View.VISIBLE);

        Log.e(String.valueOf(mActivity), "strCount" + strCOUNT);
        holder.listNameTV.setText(tempValue.getListName());
        /* checkBoxState has the value of checkBox ie true or false,
         * The position is used so that on scroll your selected checkBox maintain its state */
        if (checkBoxState != null) {
            holder.imgMultiSelectIV.setChecked(checkBoxState[position]);
        }

//        holder.mailRL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent mIntent = new Intent(mActivity, SendMailingListActivity.class);
//                mIntent.putExtra("listID", tempValue.getList_id());
//                mIntent.putExtra("type", "staff");
//                mActivity.startActivity(mIntent);
//            }
//        });

        holder.imgMultiSelectIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.imgMultiSelectIV.isChecked()) {
                    checkBoxState[position] = true;
                    ischecked(position, true);
                    mMultimaIlInterface.mSendingMultipleLIST(tempValue, false);
                } else {
                    checkBoxState[position] = false;
                    ischecked(position, false);
                    mMultimaIlInterface.mSendingMultipleLIST(tempValue, true);
                }

            }
        });
        /*if country is in checkedForCountry then set the checkBox to true */
        if (checkedForModel.get(tempValue) != null) {
            holder.imgMultiSelectIV.setChecked(checkedForModel.get(tempValue));
        }

        /*Set tag to all checkBox*/
        holder.imgMultiSelectIV.setTag(tempValue);

        holder.checkImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Items.contains(String.valueOf(position))) {
                    Items.remove(String.valueOf(position));
                    holder.checkImg.setImageResource(R.drawable.ic_unchecked_icon);
                    mMultimaIlInterface.mSendingMultipleLIST(tempValue, false);
                } else {
                    Items.add(String.valueOf(position));
                    holder.checkImg.setImageResource(R.drawable.ic_checked_icon);
                    mMultimaIlInterface.mSendingMultipleLIST(tempValue, true);
                }
            }
        });

        holder.itemIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickManagerModulesInterface.mClickManagerModulesInterface(position, tempValue.getListId(), tempValue.getListName());
            }
        });
    }

    private void ischecked(int position, boolean flag) {
        checkedForModel.put(this.modelArrayList.get(position), flag);
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView listNameTV;
        public LinearLayout mainLL;
        //        public RelativeLayout editRL,delRL,mailRL, startMAINLL;
        RelativeLayout startMAINLL;
        public CheckBox imgMultiSelectIV;
        ImageView checkImg, itemIMG;

        ViewHolder(View itemView) {
            super(itemView);

            itemIMG = itemView.findViewById(R.id.itemIMG);
            checkImg = itemView.findViewById(R.id.checkImg);
            listNameTV = itemView.findViewById(R.id.listNameTV);
//            mailRL = itemView.findViewById(R.id.mailRL);
//            delRL = itemView.findViewById(R.id.delRL);
//            editRL = itemView.findViewById(R.id.editRL);
            mainLL = itemView.findViewById(R.id.mainLL);
            startMAINLL = itemView.findViewById(R.id.startMAINLL);
            imgMultiSelectIV = itemView.findViewById(R.id.imgMultiSelectIV);
        }
    }
}
