package jaohar.com.jaohar.adapters;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.NewRequestModel;
import jaohar.com.jaohar.interfaces.ConfirmRejectRequestInterface;
import jaohar.com.jaohar.interfaces.DeleteVesselsInterface;
import jaohar.com.jaohar.models.NewReqData;


/**
 * Created by Dharmani Apps on 7/11/2017.
 */

public class NewRequestAdapter extends RecyclerView.Adapter<NewRequestAdapter.ViewHolder> {
    DeleteVesselsInterface mDeleteVesselsInterface;
    private Activity mActivity;
//    private ArrayList<NewRequestModel> modelArrayList;
    private ArrayList<NewReqData> modelArrayList;
    private ConfirmRejectRequestInterface mConfirmRequestInterface;

    public NewRequestAdapter(Activity mActivity, ArrayList<NewReqData> modelArrayList, ConfirmRejectRequestInterface mConfirmRequestInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mConfirmRequestInterface = mConfirmRequestInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_new_request, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
//        final NewRequestModel tempValue = modelArrayList.get(position);
        final NewReqData tempValue = modelArrayList.get(position);
        holder.companyLL.setVisibility(View.GONE);
        holder.nameLL.setVisibility(View.GONE);
        holder.itemNameTV.setText(tempValue.getCompanyName());
        holder.itemEmailTV.setText(tempValue.getEmail());
        holder.itemRoleTV.setText(tempValue.getRole());
        holder.itemIdTV.setText(tempValue.getId());
        holder.itemDateTV.setText(tempValue.getCreated());

        holder.itemtxtConfirmTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mConfirmRequestInterface.getConfirmRequest(tempValue, "confirm");
            }
        });

        holder.itemtxtRejectTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mConfirmRequestInterface.getConfirmRequest(tempValue, "reject");
            }
        });
    }

    //This method will filter the list
    //here we are passing the filtered data
    //and assigning it to the list with notifydatasetchanged method
    public void filterList(ArrayList<NewReqData> filterdNames) {
        this.modelArrayList = filterdNames;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView itemNameTV, itemEmailTV, itemRoleTV, itemtxtConfirmTV, itemtxtRejectTV, itemIdTV, itemDateTV;
        public LinearLayout companyLL, nameLL;

        ViewHolder(View itemView) {
            super(itemView);
            itemNameTV = (TextView) itemView.findViewById(R.id.itemNameTV);
            itemEmailTV = (TextView) itemView.findViewById(R.id.itemEmailTV);
            itemRoleTV = (TextView) itemView.findViewById(R.id.itemRoleTV);
            itemtxtConfirmTV = (TextView) itemView.findViewById(R.id.itemtxtConfirmTV);
            itemtxtRejectTV = (TextView) itemView.findViewById(R.id.itemtxtRejectTV);
            companyLL = (LinearLayout) itemView.findViewById(R.id.companyLL);
            nameLL = (LinearLayout) itemView.findViewById(R.id.nameLL);
            itemIdTV = itemView.findViewById(R.id.itemIdTV);
            itemDateTV = itemView.findViewById(R.id.itemDateTV);
        }
    }
}