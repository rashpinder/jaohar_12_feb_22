package jaohar.com.jaohar.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;

import jaohar.com.jaohar.interfaces.SelectedAndUnselectedEmailListInterFace;

/**
 * Created by dharmaniz on 31/1/19.
 */

public class SelectedEmailShowListAdapter extends RecyclerView.Adapter<SelectedEmailShowListAdapter.ViewHolder> {

    private Activity mActivity;
    private ArrayList<String> modelArrayList;
    SelectedAndUnselectedEmailListInterFace selectedAndUnselectedEmailListInterFace;
    String cc;

    public SelectedEmailShowListAdapter(Activity mActivity, ArrayList<String> modelArrayList, SelectedAndUnselectedEmailListInterFace selectedAndUnselectedEmailListInterFace,String cc) {
        this.mActivity = mActivity;
        this.cc = cc;
        this.modelArrayList = modelArrayList;
        this.selectedAndUnselectedEmailListInterFace = selectedAndUnselectedEmailListInterFace;

    }

    @Override
    public SelectedEmailShowListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_invoice_item, parent, false);
        return new SelectedEmailShowListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SelectedEmailShowListAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {


        holder.txtItemsTV.setText(modelArrayList.get(position).toString());

        holder.imgRemoveIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedAndUnselectedEmailListInterFace.mSelectedEmail(modelArrayList.get(position),cc);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtItemsTV;
        public ImageView imgRemoveIV;


        ViewHolder(View itemView) {
            super(itemView);
            imgRemoveIV = (ImageView) itemView.findViewById(R.id.imgRemoveIV);
            txtItemsTV = (TextView) itemView.findViewById(R.id.txtItemsTV);


        }
    }
}