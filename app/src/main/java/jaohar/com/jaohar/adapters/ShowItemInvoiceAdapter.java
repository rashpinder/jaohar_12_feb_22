package jaohar.com.jaohar.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.DiscountModel;
import jaohar.com.jaohar.interfaces.DeleteInvoiceItemDiscountInterface;
import jaohar.com.jaohar.interfaces.DeleteNewsInterface;
import jaohar.com.jaohar.interfaces.EditInvoiceItemSubtractDiscount;
import jaohar.com.jaohar.utils.Utilities;

/**
 * Created by dharmaniz on 25/4/18.
 */

public class ShowItemInvoiceAdapter extends RecyclerView.Adapter<ShowItemInvoiceAdapter.ViewHolder> {
    EditInvoiceItemSubtractDiscount mEditDiscountInrterface;
    DeleteNewsInterface mDeleteNewsInterface;
    DeleteInvoiceItemDiscountInterface mDeleteInvoiceItemDiscountInterface;
    private Activity mActivity;
    private ArrayList<DiscountModel> modelArrayList;

    public ShowItemInvoiceAdapter(Activity mActivity, ArrayList<DiscountModel> modelArrayList, EditInvoiceItemSubtractDiscount mEditDiscountInrterface,DeleteInvoiceItemDiscountInterface mDeleteInvoiceItemDiscountInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mEditDiscountInrterface = mEditDiscountInrterface;
        this.mDeleteInvoiceItemDiscountInterface = mDeleteInvoiceItemDiscountInterface;
    }

    @Override
    public ShowItemInvoiceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_show_invoice, parent, false);
        return new ShowItemInvoiceAdapter.ViewHolder(v);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(final ShowItemInvoiceAdapter.ViewHolder holder, final int position) {
        final DiscountModel tempValue = modelArrayList.get(position);
        System.out.print("Item===============" +modelArrayList);
        final String strType = tempValue.getType();
        if(strType.equals("ADD")){
            String strItem=    tempValue.getAdd_description();
            holder.itemTV.setText(strItem);
            String strPrice= String.valueOf(tempValue.getAddAndSubtractTotal());
            holder.priceTV.setText(Utilities.convertEvalueToNormal(Double.parseDouble(strPrice)));
        }
        else if(strType.equals("ADD_VALUE")){
            String strItem=    tempValue.getAdd_description();
            holder.itemTV.setText(strItem);
            String strPrice= String.valueOf(tempValue.getAddAndSubtractTotal());
            holder.priceTV.setText(Utilities.convertEvalueToNormal(Double.parseDouble(strPrice)));
        }else if(strType.equals("SUBTRACT")){
            String strItem=    tempValue.getSubtract_description();
            holder.itemTV.setText(strItem);
            String strPrice= String.valueOf(tempValue.getAddAndSubtractTotal());
            holder.priceTV.setText(Utilities.convertEvalueToNormal(Double.parseDouble(strPrice)));
        }else if(strType.equals("SUBTRACT_VALUE")){
            String strItem=    tempValue.getSubtract_description();
            holder.itemTV.setText(strItem);
            String strPrice= String.valueOf(tempValue.getAddAndSubtractTotal());
            holder.priceTV.setText(Utilities.convertEvalueToNormal(Double.parseDouble(strPrice)));
        }

            //holder.descriptionTV.setText(tempValue.getDescription());
            //holder.quantityTV.setText(tempValue.getQuantity());
            //holder.priceTV.setText((int) tempValue.getUnitprice());
            //holder.txtEdit.setVisibility(View.GONE);

        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(strType.equals("ADD")){
                    mEditDiscountInrterface.editInvoiceItemSubtractDiscount(tempValue,position,strType);
                }else if(strType.equals("SUBTRACT")){
                    mEditDiscountInrterface.editInvoiceItemSubtractDiscount(tempValue,position,strType);
                }else if(strType.equals("SUBTRACT_VALUE")){
                    mEditDiscountInrterface.editInvoiceItemSubtractDiscount(tempValue,position,strType);
                }else if(strType.equals("ADD_VALUE")){
                    mEditDiscountInrterface.editInvoiceItemSubtractDiscount(tempValue,position,strType);
                }
            }
        });

//        holder.txtDelete.setVisibility(View.GONE);
        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelArrayList.remove(position);
                notifyDataSetChanged();
                mDeleteInvoiceItemDiscountInterface.deleteInvoiceItemDiscountInterface(modelArrayList,position);

            }
        });

    }


    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView itemTV, txtEdit, txtDelete,priceTV;
        ViewHolder(View itemView) {
            super(itemView);
            txtEdit = (TextView) itemView.findViewById(R.id.txtEdit);
            txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);
            itemTV = (TextView) itemView.findViewById(R.id.itemTV);
            priceTV = (TextView) itemView.findViewById(R.id.priceTV);
         }
    }
}