package jaohar.com.jaohar.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.interfaces.SelectEmailListItem;

/**
 * Created by dharmaniz on 31/1/19.
 */

public class ShowListEmailAdapter extends RecyclerView.Adapter<ShowListEmailAdapter.ViewHolder> {

    private Activity mActivity;
    private ArrayList<String> modelArrayList;
    SelectEmailListItem mSelectedItem;
    String cc;

    public ShowListEmailAdapter(Activity mActivity, ArrayList<String> modelArrayList, SelectEmailListItem mSelectedItem, String cc) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mSelectedItem = mSelectedItem;
        this.cc = cc;

    }

    @Override
    public ShowListEmailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_mail_list, parent, false);
        return new ShowListEmailAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ShowListEmailAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        holder.listNameTV.setVisibility(View.GONE);
        holder.serialNOTV.setText(modelArrayList.get(position));

        holder.mainLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedItem.mSelectEmailListItem(modelArrayList.get(position),cc);
            }
        });
    }

    @Override
    public int getItemCount() {
        if(modelArrayList.size()>=10){
            return 10;
        }else {
            return modelArrayList.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView serialNOTV, listNameTV;
        public LinearLayout mainLL;

        ViewHolder(View itemView) {
            super(itemView);
            mainLL = (LinearLayout) itemView.findViewById(R.id.mainLL);
            serialNOTV = (TextView) itemView.findViewById(R.id.serialNOTV);
            listNameTV = (TextView) itemView.findViewById(R.id.listNameTV);

        }
    }
}