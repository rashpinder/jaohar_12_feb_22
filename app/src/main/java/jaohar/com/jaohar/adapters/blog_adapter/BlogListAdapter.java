package jaohar.com.jaohar.adapters.blog_adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.activities.LoginActivity;
import jaohar.com.jaohar.beans.blog_module.Blog_List_Model;
import jaohar.com.jaohar.interfaces.blog_module.Blog_Item_Click_Interface;
import jaohar.com.jaohar.interfaces.forumModule.PaginationListForumAdapter;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;

public class BlogListAdapter extends RecyclerView.Adapter<BlogListAdapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<Blog_List_Model> modelArrayList;
    PaginationListForumAdapter mPagination;
    Blog_Item_Click_Interface mBlogClickInterface;

    public BlogListAdapter(Activity mActivity, ArrayList<Blog_List_Model> modelArrayList, PaginationListForumAdapter mPagination, Blog_Item_Click_Interface mBlogClickInterface) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mPagination = mPagination;
        this.mBlogClickInterface = mBlogClickInterface;
    }

    @Override
    public BlogListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_blog_list, parent, false);
        return new BlogListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final BlogListAdapter.ViewHolder holder, final int position) {
        final Blog_List_Model tempValue = modelArrayList.get(position);

        holder.blogHeadingTV.setText(tempValue.getTitle());
        holder.blog_descriptionTV.setText(tempValue.getContent());

        long unix_seconds = Long.parseLong(tempValue.getCreation_date());

        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(unix_seconds * 1000L);

        DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        String date = format.format(cal.getTime());
        holder.postedTimeTV.setText(" " + date);
        // pagination for smooth scrooling
        if (position >= modelArrayList.size() - 1) {
            mPagination.mPaginationforVessels(true);
        }
        if (!tempValue.getImage().equals("")) {
            Glide.with(mActivity)
                    .load(tempValue.getImage())
                    .error(R.drawable.blog_small_icon)
                    .into(holder.blogImgIV);
        } else {
            holder.blogImgIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.blog_small_icon));
        }

        holder.read_moreTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_STAFF, false) || JaoharPreference.readBoolean(mActivity, JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                    mBlogClickInterface.BlogInterface(tempValue);
                } else {
                    Intent mIntent = new Intent(mActivity, LoginActivity.class);
                    mIntent.putExtra(JaoharConstants.LOGIN_TYPE, JaoharConstants.BLOG);
                    mActivity.startActivity(mIntent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView blogHeadingTV, blog_descriptionTV, postedTimeTV, read_moreTV;
        public ImageView blogImgIV;
        public LinearLayout mainLayoutClick;
        public RecyclerView replyCommentRV;

        ViewHolder(View itemView) {
            super(itemView);
            blogHeadingTV = itemView.findViewById(R.id.blogHeadingTV);
            read_moreTV = itemView.findViewById(R.id.read_moreTV);
            blog_descriptionTV = itemView.findViewById(R.id.blog_descriptionTV);
            postedTimeTV = itemView.findViewById(R.id.postedTimeTV);
            mainLayoutClick = itemView.findViewById(R.id.mainLayoutClick);
            blogImgIV = itemView.findViewById(R.id.blogImgIV);
            replyCommentRV = itemView.findViewById(R.id.replyCommentRV);
        }
    }
}
