package jaohar.com.jaohar.adapters.blog_adapter;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import java.util.ArrayList;


import jaohar.com.jaohar.R;
import jaohar.com.jaohar.beans.blog_module.Blog_CategoriesModel;
import jaohar.com.jaohar.interfaces.blog_module.Blog_Categories_Interface;


public class Blog_CategoriesAdapter extends RecyclerView.Adapter<Blog_CategoriesAdapter.ViewHolder> {
    private Activity mActivity;
    private ArrayList<Blog_CategoriesModel> modelArrayList;
    Blog_Categories_Interface mInterfacedata;

    public Blog_CategoriesAdapter(Activity mActivity, ArrayList<Blog_CategoriesModel> modelArrayList,Blog_Categories_Interface mInterfacedata) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mInterfacedata = mInterfacedata;
    }

    @Override
    public Blog_CategoriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_companies, parent, false);
        return new Blog_CategoriesAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final Blog_CategoriesAdapter.ViewHolder holder, final int position) {
        final Blog_CategoriesModel tempValue = modelArrayList.get(position);

        holder.item_Title.setText(tempValue.getCategory());


        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mInterfacedata.mBlogCategoriesInterface(tempValue,"del");
            }
        });

        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mInterfacedata.mBlogCategoriesInterface(tempValue,"edit");
            }
        });

    }


    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView item_Title, txtEdit, txtDelete;

        ViewHolder(View itemView) {
            super(itemView);
            item_Title = (TextView) itemView.findViewById(R.id.item_Title);
            txtEdit = (TextView) itemView.findViewById(R.id.txtEdit);
            txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);

        }
    }
}