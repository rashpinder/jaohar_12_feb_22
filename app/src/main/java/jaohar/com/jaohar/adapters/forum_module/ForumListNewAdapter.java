package jaohar.com.jaohar.adapters.forum_module;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.interfaces.forumModule.ForumItemClickNewInterace;
import jaohar.com.jaohar.interfaces.forumModule.PaginationListForumAdapter;
import jaohar.com.jaohar.models.forummodels.GetAllForumsModel;

public class ForumListNewAdapter extends RecyclerView.Adapter<ForumListNewAdapter.ViewHolder> {
    private Activity mActivity;
    private List<GetAllForumsModel.AllForum> modelArrayList;
    PaginationListForumAdapter mPagination;
    ForumItemClickNewInterace mInterfaceForum;

    public ForumListNewAdapter(Activity mActivity, List<GetAllForumsModel.AllForum> modelArrayList,
                               PaginationListForumAdapter mPagination, ForumItemClickNewInterace mInterfaceForum) {
        this.mActivity = mActivity;
        this.modelArrayList = modelArrayList;
        this.mPagination = mPagination;
        this.mInterfaceForum = mInterfaceForum;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_forum, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final GetAllForumsModel.AllForum tempValue = modelArrayList.get(position);

        // pagination for smooth scrooling
        if (position >= modelArrayList.size() - 1) {
            mPagination.mPaginationforVessels(true);
        }
        holder.vesselNameTV.setText(tempValue.getVesselName());

        if (tempValue.getUnreadMessages() != null &&
                !tempValue.getUnreadMessages().equals(0)) {
            holder.totalMessagesTV.setVisibility(View.VISIBLE);
            holder.totalMessagesTV.setText(String.valueOf(tempValue.getUnreadMessages()));
        } else {
            holder.totalMessagesTV.setVisibility(View.GONE);
        }
        holder.imoNumTV.setText(tempValue.getIMONumber());


        if (tempValue.getPic() != null && tempValue.getPic().contains("http")) {
            Glide.with(mActivity)
                    .load(modelArrayList.get(position).getPic())
                    .into(holder.forumIMG);
        } else {
            holder.forumIMG.setImageDrawable(mActivity.getDrawable(R.drawable.palace_holder));
        }

        holder.mainLayoutClick.setClickable(true);

        /* *
         * Setting Up Click for Viewing Forum
         * */
        holder.mainLayoutClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mInterfaceForum.ForumItemClick(tempValue);
                holder.mainLayoutClick.setClickable(false);
            }
        });


        /*Vessels Status on Vessel Image*/
        if (tempValue.getStatus().equals("Sold")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.vessel_sold);
        } else if (tempValue.getStatus().equals("Withdrawn")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.vessel_withdrawn);
        } else if (tempValue.getStatus().equals("Committed")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.vessel_committed);
        } else if (tempValue.getStatus().equals("Scraped")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.vessel_scraped);
        } else if (tempValue.getStatus().equals("Hot Sale")) {
            holder.item_Image_Status.setVisibility(View.VISIBLE);
            holder.item_Image_Status.setImageResource(R.drawable.vessel_hotsale);
        } else {
            holder.item_Image_Status.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView vesselNameTV, totalMessagesTV, imoNumTV;
        public ImageView item_Image_Status;
        public LinearLayout mainLayoutClick;
        public ImageView forumIMG;

        ViewHolder(View itemView) {
            super(itemView);
            forumIMG = itemView.findViewById(R.id.forumIMG);
            vesselNameTV = (TextView) itemView.findViewById(R.id.vesselNameTV);
            totalMessagesTV = (TextView) itemView.findViewById(R.id.totalMessagesTV);
            imoNumTV = (TextView) itemView.findViewById(R.id.imoNumTV);
            mainLayoutClick = (LinearLayout) itemView.findViewById(R.id.mainLayoutClick);
            item_Image_Status = (ImageView) itemView.findViewById(R.id.item_Image_Status);
        }
    }
}
