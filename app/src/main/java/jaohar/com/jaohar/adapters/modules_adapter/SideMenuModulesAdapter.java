package jaohar.com.jaohar.adapters.modules_adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import jaohar.com.jaohar.R;
import jaohar.com.jaohar.interfaces.modules.ClickSideMenuModuleInterface;
import jaohar.com.jaohar.models.profileandtabsmodels.ModulesItem;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;

public class SideMenuModulesAdapter extends RecyclerView.Adapter<SideMenuModulesAdapter.ViewHolder> {
    private Activity mActivity;
    private List<ModulesItem> mModulesItemList;
    private ClickSideMenuModuleInterface clickSideMenuModuleInterface;
    private SideMenuModulesAdapter.ViewHolder holderNew;
    private int row_index;
    private int count = 0;
    private String strUkForumCount = "", strForumCount = "", unread_chat = "", unread_notifications = "";

    public SideMenuModulesAdapter(Activity mActivity, List<ModulesItem> mModulesItemList,
                                  ClickSideMenuModuleInterface clickSideMenuModuleInterface,
                                  String strForumCount, String strUkForumCount, String unread_chat,
                                  String unread_notifications) {
        this.mActivity = mActivity;
        this.mModulesItemList = mModulesItemList;
        this.clickSideMenuModuleInterface = clickSideMenuModuleInterface;
        this.strUkForumCount = strUkForumCount;
        this.strForumCount = strForumCount;
        this.unread_chat = unread_chat;
        this.unread_notifications = unread_notifications;
    }

    @Override
    public SideMenuModulesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_side_menu_modules, parent, false);
        return new SideMenuModulesAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final SideMenuModulesAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holderNew = holder;
        ModulesItem mModule = mModulesItemList.get(position);

        if (mModulesItemList.get(position).getModuleName() != null && !mModulesItemList.get(position).getModuleName().equals(""))
            holder.moduleTV.setText(mModulesItemList.get(position).getModuleName());

        if (mModulesItemList.get(position).getAppImage() != null && !mModulesItemList.get(position).getAppImage().equals(""))
            Glide.with(mActivity).load(mModulesItemList.get(position).getAppImage()).into(holder.moduleIv);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index = position;
                count = 1;
                notifyDataSetChanged();
                JaoharPreference.writeString(mActivity, JaoharConstants.SIDE_MENU_CLICKED_ID, mModule.getModuleId());
                clickSideMenuModuleInterface.mClickSideMenuModuleInterface(position, mModule);
            }
        });

        if (row_index == position && count == 1) {
            holder.moduleTV.setTextColor(mActivity.getResources().getColor(R.color.blue));
            holder.moduleIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.blue));
        } else if (JaoharPreference.readString(mActivity, JaoharConstants.SIDE_MENU_CLICKED_ID, "") != null
                && !JaoharPreference.readString(mActivity, JaoharConstants.SIDE_MENU_CLICKED_ID, "").equals("")
                && JaoharPreference.readString(mActivity, JaoharConstants.SIDE_MENU_CLICKED_ID, "").equals(mModule.getModuleId())) {
            holder.moduleTV.setTextColor(mActivity.getResources().getColor(R.color.blue));
            holder.moduleIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.blue));
        } else {
            holder.moduleTV.setTextColor(mActivity.getResources().getColor(R.color.black));
            holder.moduleIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.black));
        }

        // Showing Badges According to Type
        if (mModule.getModuleName().equals("Romania Forum")) {
            if (!strForumCount.equals("")) {
                if (!strForumCount.equals("0")) {
                    holder.badgesTV.setVisibility(View.VISIBLE);
                    if (Integer.parseInt(strForumCount) > 99) {
                        holder.badgesTV.setText("99+");
                    } else {
                        holder.badgesTV.setText(strForumCount);
                    }
                } else {
                    holder.badgesTV.setVisibility(View.GONE);
                }
            } else {
                holder.badgesTV.setVisibility(View.GONE);
            }
        } else if (mModule.getModuleName().equals("Notifications")) {
            if (!unread_notifications.equals("")) {
                if (!unread_notifications.equals("0")) {
                    holder.badgesTV.setVisibility(View.VISIBLE);
                    if (Integer.parseInt(unread_notifications) > 99) {
                        holder.badgesTV.setText("99+");
                    } else {
                        holder.badgesTV.setText(unread_notifications);
                    }
                } else {
                    holder.badgesTV.setVisibility(View.GONE);
                }
            } else {
                holder.badgesTV.setVisibility(View.GONE);
            }
        } else if (mModule.getModuleName().equals("Chat")) {
            holder.badgesTV.setVisibility(View.VISIBLE);
            if (!unread_chat.equals("")) {
                if (!unread_chat.equals("0")) {
                    holder.badgesTV.setVisibility(View.VISIBLE);
                    if (Integer.parseInt(unread_chat) > 99) {
                        holder.badgesTV.setText("99+");
                    } else {
                        holder.badgesTV.setText(unread_chat);
                    }
                } else {
                    holder.badgesTV.setVisibility(View.GONE);
                }
            } else {
                holder.badgesTV.setVisibility(View.GONE);
            }

        } else if (mModule.getModuleName().equals("UK Forum")) {
            if (!strUkForumCount.equals("")) {
                if (!strUkForumCount.equals("0")) {
                    holder.badgesTV.setVisibility(View.VISIBLE);
                    if (Integer.parseInt(strForumCount) > 99) {
                        holder.badgesTV.setText("99+");
                    } else {
                        holder.badgesTV.setText(strUkForumCount);
                    }
                } else {
                    holder.badgesTV.setVisibility(View.GONE);
                }
            } else {
                holder.badgesTV.setVisibility(View.GONE);
            }
        } else {
            holder.badgesTV.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mModulesItemList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView moduleIv;
        TextView moduleTV, badgesTV;

        ViewHolder(View itemView) {
            super(itemView);

            moduleIv = itemView.findViewById(R.id.moduleIv);
            moduleTV = itemView.findViewById(R.id.moduleTV);
            badgesTV = itemView.findViewById(R.id.badgesTV);
        }
    }

    public void setHomeClick() {
        holderNew.moduleTV.setTextColor(mActivity.getResources().getColor(R.color.black));
        holderNew.moduleIv.setColorFilter(ContextCompat.getColor(mActivity, R.color.black));
    }
}