package jaohar.com.jaohar.beans;

import java.io.Serializable;

/**
 * Created by dharmaniz on 25/5/18.
 */

public class AddTotalInvoiceDiscountModel implements Serializable {
    private String discountType="";
    private String discount_id="";

    private double ADD_UnitPrice;
    private String ADD_Description="";
    private String ADD_DiscountValue="";
    private String ADD_DiscountQuantitiy="";
    private String strAddAndSubtracttotalPercent="";
    private String strTotalvalue="";
    private double AddAndSubtractTotal;
    private double Subtract_UnitPrice;
    private String Subtract_Description="";
    private String Subtract_DiscountValue="";
    private String Subtract_DiscountQuantity="";

    public String getADD_DiscountQuantitiy() {
        return ADD_DiscountQuantitiy;
    }

    public void setADD_DiscountQuantitiy(String ADD_DiscountQuantitiy) {
        this.ADD_DiscountQuantitiy = ADD_DiscountQuantitiy;
    }

    public String getSubtract_DiscountQuantity() {
        return Subtract_DiscountQuantity;
    }

    public void setSubtract_DiscountQuantity(String subtract_DiscountQuantity) {
        Subtract_DiscountQuantity = subtract_DiscountQuantity;
    }

    public String getDiscount_id() {
        return discount_id;
    }

    public void setDiscount_id(String discount_id) {
        this.discount_id = discount_id;
    }
    public String getStrTotalvalue() {
        return strTotalvalue;
    }

    public void setStrTotalvalue(String strTotalvalue) {
        this.strTotalvalue = strTotalvalue;
    }

    public String getStrAddAndSubtracttotalPercent() {
        return strAddAndSubtracttotalPercent;
    }

    public void setStrAddAndSubtracttotalPercent(String strAddAndSubtracttotalPercent) {
        this.strAddAndSubtracttotalPercent = strAddAndSubtracttotalPercent;
    }



    public double getAddAndSubtractTotal() {
        return AddAndSubtractTotal;
    }

    public void setAddAndSubtractTotal(double addAndSubtractTotal) {
        AddAndSubtractTotal = addAndSubtractTotal;
    }



    public double getADD_UnitPrice() {
        return ADD_UnitPrice;
    }

    public void setADD_UnitPrice(double ADD_UnitPrice) {
        this.ADD_UnitPrice = ADD_UnitPrice;
    }

    public double getSubtract_UnitPrice() {
        return Subtract_UnitPrice;
    }

    public void setSubtract_UnitPrice(double subtract_UnitPrice) {
        Subtract_UnitPrice = subtract_UnitPrice;
    }

    public String getDiscountType() {

        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }



    public String getADD_Description() {
        return ADD_Description;
    }

    public void setADD_Description(String ADD_Description) {
        this.ADD_Description = ADD_Description;
    }

    public String getADD_DiscountValue() {
        return ADD_DiscountValue;
    }

    public void setADD_DiscountValue(String ADD_DiscountValue) {
        this.ADD_DiscountValue = ADD_DiscountValue;
    }

    public String getSubtract_Description() {
        return Subtract_Description;
    }

    public void setSubtract_Description(String subtract_Description) {
        Subtract_Description = subtract_Description;
    }

    public String getSubtract_DiscountValue() {
        return Subtract_DiscountValue;
    }

    public void setSubtract_DiscountValue(String subtract_DiscountValue) {
        Subtract_DiscountValue = subtract_DiscountValue;
    }
}
