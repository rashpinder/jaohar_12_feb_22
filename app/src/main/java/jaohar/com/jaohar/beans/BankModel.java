package jaohar.com.jaohar.beans;

import java.io.Serializable;

/**
 * Created by Dharmani Apps on 1/18/2018.
 */

public class BankModel implements Serializable {
    String id = "";
    String benificiary = "";
    String bankName = "";

    String ibanRON = "";
    String address1 = "";
    String address2 = "";
    String ibanUSD = "";
    String ibanEUR = "";
    String iban_gbp = "";
    String swift = "";
    String added_by = "";

    public String getAdded_by() {
        return added_by;
    }

    public void setAdded_by(String added_by) {
        this.added_by = added_by;
    }

    public String getIban_gbp() {
        return iban_gbp;
    }

    public void setIban_gbp(String iban_gbp) {
        this.iban_gbp = iban_gbp;
    }


    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBenificiary() {
        return benificiary;
    }

    public void setBenificiary(String benificiary) {
        this.benificiary = benificiary;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

//    public String getCityName() {
//        return cityName;
//    }
//
//    public void setCityName(String cityName) {
//        this.cityName = cityName;
//    }

    public String getIbanRON() {
        return ibanRON;
    }

    public void setIbanRON(String ibanRON) {
        this.ibanRON = ibanRON;
    }

    public String getIbanUSD() {
        return ibanUSD;
    }

    public void setIbanUSD(String ibanUSD) {
        this.ibanUSD = ibanUSD;
    }

    public String getIbanEUR() {
        return ibanEUR;
    }

    public void setIbanEUR(String ibanEUR) {
        this.ibanEUR = ibanEUR;
    }

    public String getSwift() {
        return swift;
    }

    public void setSwift(String swift) {
        this.swift = swift;
    }
}
