package jaohar.com.jaohar.beans;

import java.io.Serializable;

/**
 * Created by Dharmani Apps on 3/20/2018.
 */

public class LinkAdminModel implements Serializable {
    String link_id = "",link_name = "", link_value = "";

    public String getLink_id() {
        return link_id;
    }

    public void setLink_id(String link_id) {
        this.link_id = link_id;
    }

    public String getLink_name() {
        return link_name;
    }

    public void setLink_name(String link_name) {
        this.link_name = link_name;
    }

    public String getLink_value() {
        return link_value;
    }

    public void setLink_value(String link_value) {
        this.link_value = link_value;
    }
}
