package jaohar.com.jaohar.beans;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by dharmaniz on 15/6/18.
 */

public class PriviewItemModel implements Serializable {
    String itemID = "";
    String item = "";
    int quantity = 0;
    double unitprice = 0;
    String description = "";
    double TotalAmount = 0;
    double strTotalunitPrice = 0;
    double strTotalAmountUnit = 0;
    ArrayList<DiscountModel> mDiscountModelArrayList = new ArrayList<DiscountModel>();
    String ALLTYPE ="";
    private String discountType="";
    private double ADD_UnitPrice;
    private String ADD_Description="";
    private String ADD_DiscountValue="";
    private String strAddAndSubtracttotalPercent="";
    private String strTotalvalue="";
    private double AddAndSubtractTotal;
    private double Subtract_UnitPrice;
    private String Subtract_Description="";
    private String Subtract_DiscountValue="";




    public double getAddAndSubtractTotal() {
        return AddAndSubtractTotal;
    }

    public void setAddAndSubtractTotal(double addAndSubtractTotal) {
        AddAndSubtractTotal = addAndSubtractTotal;
    }

    public double getSubtract_UnitPrice() {
        return Subtract_UnitPrice;
    }

    public void setSubtract_UnitPrice(double subtract_UnitPrice) {
        Subtract_UnitPrice = subtract_UnitPrice;
    }

    public String getSubtract_Description() {
        return Subtract_Description;
    }

    public void setSubtract_Description(String subtract_Description) {
        Subtract_Description = subtract_Description;
    }

    public String getSubtract_DiscountValue() {
        return Subtract_DiscountValue;
    }

    public void setSubtract_DiscountValue(String subtract_DiscountValue) {
        Subtract_DiscountValue = subtract_DiscountValue;
    }



    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(double unitprice) {
        this.unitprice = unitprice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        TotalAmount = totalAmount;
    }

    public double getStrTotalunitPrice() {
        return strTotalunitPrice;
    }

    public void setStrTotalunitPrice(double strTotalunitPrice) {
        this.strTotalunitPrice = strTotalunitPrice;
    }

    public double getStrTotalAmountUnit() {
        return strTotalAmountUnit;
    }

    public void setStrTotalAmountUnit(double strTotalAmountUnit) {
        this.strTotalAmountUnit = strTotalAmountUnit;
    }

    public ArrayList<DiscountModel> getmDiscountModelArrayList() {
        return mDiscountModelArrayList;
    }

    public void setmDiscountModelArrayList(ArrayList<DiscountModel> mDiscountModelArrayList) {
        this.mDiscountModelArrayList = mDiscountModelArrayList;
    }

    public String getALLTYPE() {
        return ALLTYPE;
    }

    public void setALLTYPE(String ALLTYPE) {
        this.ALLTYPE = ALLTYPE;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public double getADD_UnitPrice() {
        return ADD_UnitPrice;
    }

    public void setADD_UnitPrice(double ADD_UnitPrice) {
        this.ADD_UnitPrice = ADD_UnitPrice;
    }

    public String getADD_Description() {
        return ADD_Description;
    }

    public void setADD_Description(String ADD_Description) {
        this.ADD_Description = ADD_Description;
    }

    public String getADD_DiscountValue() {
        return ADD_DiscountValue;
    }

    public void setADD_DiscountValue(String ADD_DiscountValue) {
        this.ADD_DiscountValue = ADD_DiscountValue;
    }

    public String getStrAddAndSubtracttotalPercent() {
        return strAddAndSubtracttotalPercent;
    }

    public void setStrAddAndSubtracttotalPercent(String strAddAndSubtracttotalPercent) {
        this.strAddAndSubtracttotalPercent = strAddAndSubtracttotalPercent;
    }

    public String getStrTotalvalue() {
        return strTotalvalue;
    }

    public void setStrTotalvalue(String strTotalvalue) {
        this.strTotalvalue = strTotalvalue;
    }
}
