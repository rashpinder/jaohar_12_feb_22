package jaohar.com.jaohar.beans;

import java.io.Serializable;

/**
 * Created by Dharmani Apps on 3/13/2018.
 */

public class StatusModel implements Serializable {
    String id = "",status_name = "";


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus_name() {
        return status_name;
    }

    public void setStatus_name(String status_name) {
        this.status_name = status_name;
    }
}
