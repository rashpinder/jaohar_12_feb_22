package jaohar.com.jaohar.beans.blog_module;

import java.io.Serializable;

public class Blog_User_Detail_Model implements Serializable {
    String id="";
    String email="";
    String password="";
    String role="";
    String status="";
    String created="";
    String enabled="";
    String company_name="";
    String first_name="";
    String last_name="";
    String job="";
    String image="";
    String chat_approve="";
    String chat_status="";
    String chat_support_approve="";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getChat_approve() {
        return chat_approve;
    }

    public void setChat_approve(String chat_approve) {
        this.chat_approve = chat_approve;
    }

    public String getChat_status() {
        return chat_status;
    }

    public void setChat_status(String chat_status) {
        this.chat_status = chat_status;
    }

    public String getChat_support_approve() {
        return chat_support_approve;
    }

    public void setChat_support_approve(String chat_support_approve) {
        this.chat_support_approve = chat_support_approve;
    }
}
