package jaohar.com.jaohar.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;

import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.activities.ForgotPasswordActivity;
import jaohar.com.jaohar.activities.LoginActivity;
import jaohar.com.jaohar.activities.ManagerActivity;
import jaohar.com.jaohar.activities.OpenLinkActivity;
import jaohar.com.jaohar.activities.SignUpActivity;
import jaohar.com.jaohar.activities.VesselForSaleActivity;
import jaohar.com.jaohar.models.LoginModell;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

public class LoginUserFragment extends BaseFragment {

    String TAG = LoginUserFragment.this.getClass().getSimpleName();

    EditText editUserNameET, editPasswordET;
    Button btnLogin;
    TextView txtForgotPwd, termsTV;
    LinearLayout llDontHaveAccountSignUpLL;

    String strLoginType = "";
    String refreshedToken = "";

    public LoginUserFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        HomeActivity.bottomMainLL.setVisibility(View.GONE);
        HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
        HomeActivity.txtCenter.setGravity(Gravity.CENTER);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login_user, container, false);

        setStatusBar();

        getPushToken();

        setViewsIDs(view);

        setClickListner();

        return view;
    }

    private void getPushToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        //handle token error
                        Log.e(TAG, "**Get Instance Failed**", task.getException());
                        return;
                    }
                    // Get new Instance ID token
                    refreshedToken = task.getResult();
                    Log.e(TAG, "**Push Token**" + refreshedToken);

                });
    }

    private void setViewsIDs(View view) {
        termsTV = view.findViewById(R.id.termsTV);
        editUserNameET = view.findViewById(R.id.editUserNameET);
        editPasswordET = view.findViewById(R.id.editPasswordET);
        btnLogin = view.findViewById(R.id.btnLogin);
        txtForgotPwd = view.findViewById(R.id.txtForgotPwd);
        llDontHaveAccountSignUpLL = view.findViewById(R.id.llDontHaveAccountSignUpLL);

        if (getArguments() != null) {
            strLoginType = getArguments().getString(JaoharConstants.LOGIN_TYPE);
            if (strLoginType.equals(JaoharConstants.USER)) {
                HomeActivity.txtCenter.setText("User Login");
                txtForgotPwd.setVisibility(View.VISIBLE);
                llDontHaveAccountSignUpLL.setVisibility(View.VISIBLE);
            } else if (strLoginType.equals("VesselForSaleActivity")) {
                HomeActivity.txtCenter.setText("User Login");
                txtForgotPwd.setVisibility(View.VISIBLE);
                llDontHaveAccountSignUpLL.setVisibility(View.VISIBLE);
            } else if (strLoginType.equals(JaoharConstants.ADMIN)) {
                HomeActivity.txtCenter.setText("Manager Login");
                txtForgotPwd.setVisibility(View.VISIBLE);
                llDontHaveAccountSignUpLL.setVisibility(View.GONE);
            } else if (strLoginType.equals(JaoharConstants.BLOG)) {
                HomeActivity.txtCenter.setText("User Login");
                txtForgotPwd.setVisibility(View.VISIBLE);
                llDontHaveAccountSignUpLL.setVisibility(View.VISIBLE);
            }
        }

        addClicksToTermsAndPrivacyString();
    }

    private void setClickListner() {
        txtForgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strLoginType.equals(JaoharConstants.USER)) {
                    Intent mIntent = new Intent(getActivity(), ForgotPasswordActivity.class);
                    mIntent.putExtra(JaoharConstants.LOGIN_TYPE, strLoginType);
                    startActivity(mIntent);
                    overridePendingTransitionEnter(getActivity());
                } else if (strLoginType.equals("VesselForSaleActivity")) {
                    Intent mIntent = new Intent(getActivity(), ForgotPasswordActivity.class);
                    mIntent.putExtra(JaoharConstants.LOGIN_TYPE, strLoginType);
                    startActivity(mIntent);
                    overridePendingTransitionEnter(getActivity());
                } else if (strLoginType.equals(JaoharConstants.ADMIN)) {
                    Intent mIntent = new Intent(getActivity(), ForgotPasswordActivity.class);
                    mIntent.putExtra(JaoharConstants.LOGIN_TYPE, strLoginType);
                    startActivity(mIntent);
                    overridePendingTransitionEnter(getActivity());
                } else if (strLoginType.equals(JaoharConstants.BLOG)) {
                    Intent mIntent = new Intent(getActivity(), ForgotPasswordActivity.class);
                    mIntent.putExtra(JaoharConstants.LOGIN_TYPE, JaoharConstants.USER);
                    startActivity(mIntent);
                    overridePendingTransitionEnter(getActivity());
                }
            }
        });

        llDontHaveAccountSignUpLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strLoginType.equals(JaoharConstants.USER)) {
                    Intent mIntent = new Intent(getActivity(), SignUpActivity.class);
                    mIntent.putExtra(JaoharConstants.LOGIN_TYPE, strLoginType);
                    startActivity(mIntent);
                }
                if (strLoginType.equals(JaoharConstants.BLOG)) {
                    Intent mIntent = new Intent(getActivity(), SignUpActivity.class);
                    mIntent.putExtra(JaoharConstants.LOGIN_TYPE, JaoharConstants.USER);
                    startActivity(mIntent);
                } else if (strLoginType.equals("VesselForSaleActivity")) {
                    Intent mIntent = new Intent(getActivity(), SignUpActivity.class);
                    mIntent.putExtra(JaoharConstants.LOGIN_TYPE, "VesselForSaleActivity");
                    startActivity(mIntent);
                }
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editUserNameET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                } else if (Utilities.isValidEmaillId(editUserNameET.getText().toString()) != true) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.please_enter_valid_email));
                } else if (editPasswordET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.please_enter_password));
                } else {
                    /*Execute Login API*/
                    executeAPI();
                }
            }
        });
    }

    private void addClicksToTermsAndPrivacyString() {
        SpannableString SpanString = new SpannableString(getString(R.string.by_logging_in));

        ClickableSpan termsAndCondition = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent mIntent = new Intent(getActivity(), OpenLinkActivity.class);
                mIntent.putExtra("TITLE", "Terms & Conditions");
                mIntent.putExtra("LINK", JaoharConstants.TERMS_AND_CONDITIONS);
                startActivity(mIntent);

            }
        };

        // Character starting from 31 - 44 is privacy policy.
        // Character starting from 52 - 69 is Terms and condition.

        ClickableSpan privacy = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent mIntent = new Intent(getActivity(), OpenLinkActivity.class);
                mIntent.putExtra("TITLE", "Privacy Policy");
                mIntent.putExtra("LINK", JaoharConstants.PRIVACY_POLICY);
                startActivity(mIntent);
            }
        };

        SpanString.setSpan(termsAndCondition, 49, 67, 0);
        SpanString.setSpan(privacy, 30, 44, 0);
        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black)), 30, 44, 0);
        SpanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black)), 49, 67, 0);
        SpanString.setSpan(new UnderlineSpan(), 30, 44, 0);
        SpanString.setSpan(new UnderlineSpan(), 49, 67, 0);

        termsTV.setMovementMethod(LinkMovementMethod.getInstance());
        termsTV.setText(SpanString, TextView.BufferType.SPANNABLE);
        termsTV.setSelected(true);
    }

    public void executeAPI() {
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<LoginModell> call1 = mApiInterface.loginRequest(editUserNameET.getText().toString(), editPasswordET.getText().toString(), refreshedToken, "Android");
        call1.enqueue(new Callback<LoginModell>() {
            @Override
            public void onResponse(Call<LoginModell> call, retrofit2.Response<LoginModell> response) {
                AlertDialogManager.hideProgressDialog();
                LoginModell mModel = response.body();
                if (mModel.getStatus().equals("1")) {
                    JaoharPreference.writeString(getActivity(), JaoharPreference.Role, mModel.getData().getRole());
                    JaoharPreference.writeString(getActivity(), JaoharPreference.full_name, mModel.getData().getFirstName() + " " + mModel.getData().getLastName());
                    JaoharPreference.writeString(getActivity(), JaoharPreference.first_name, mModel.getData().getFirstName());

                    if (strLoginType.equals(JaoharConstants.USER) && mModel.getData().getRole().equals(JaoharConstants.USER)) {
                        JaoharPreference.writeString(getActivity(), JaoharPreference.USER_EMAIL, mModel.getData().getEmail());
                        JaoharPreference.writeString(getActivity(), JaoharPreference.USER_ID, mModel.getData().getId());
                        JaoharPreference.writeString(getActivity(), JaoharPreference.BLOGUSER_ID, mModel.getData().getId());
                        JaoharPreference.writeString(getActivity(), JaoharPreference.USER_ROLE, mModel.getData().getRole());
                        JaoharPreference.writeBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, true);
                        Intent mIntent = new Intent(getActivity(), HomeActivity.class);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mIntent.putExtra(JaoharConstants.LOGIN, "VesselForSale");
                        getActivity().startActivity(mIntent);
                        getActivity().finish();
                    } else if (strLoginType.equals(JaoharConstants.BLOG)) {
                        JaoharPreference.writeString(getActivity(), JaoharPreference.USER_EMAIL, mModel.getData().getEmail());
                        JaoharPreference.writeString(getActivity(), JaoharPreference.BLOGUSER_ID, mModel.getData().getId());
                        JaoharPreference.writeString(getActivity(), JaoharPreference.USER_ID, mModel.getData().getId());
                        JaoharPreference.writeString(getActivity(), JaoharPreference.USER_ROLE, mModel.getData().getRole());
                        JaoharPreference.writeBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, true);
                        Intent mIntent = new Intent(getActivity(), HomeActivity.class);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mIntent.putExtra(JaoharConstants.LOGIN, "BlogHOME");
                        getActivity().startActivity(mIntent);
                        getActivity().finish();
                    } else if (strLoginType.equals("VesselForSaleActivity") && mModel.getData().getRole().equals(JaoharConstants.USER)) {
                        JaoharPreference.writeString(getActivity(), JaoharPreference.USER_EMAIL, mModel.getData().getEmail());
                        JaoharPreference.writeString(getActivity(), JaoharPreference.USER_ID, mModel.getData().getId());
                        JaoharPreference.writeString(getActivity(), JaoharPreference.USER_ROLE, mModel.getData().getRole());
                        JaoharPreference.writeBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, true);
                        Intent mIntent = new Intent(getActivity(), VesselForSaleActivity.class);
                        getActivity().startActivity(mIntent);
                        getActivity().finish();
                    } else if (strLoginType.equals(JaoharConstants.USER) && mModel.getData().getRole().equals(JaoharConstants.STAFF)) {
                        JaoharPreference.writeString(getActivity(), JaoharPreference.STAFF_EMAIL, mModel.getData().getEmail());
                        JaoharPreference.writeString(getActivity(), JaoharPreference.STAFF_ID, mModel.getData().getId());
                        JaoharPreference.writeString(getActivity(), JaoharPreference.STAFF_ROLE, mModel.getData().getRole());
                        JaoharPreference.writeBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, true);
                        Intent mIntent = new Intent(getActivity(), HomeActivity.class);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mIntent.putExtra(JaoharConstants.LOGIN, "VesselForSale");
                        getActivity().startActivity(mIntent);
                        getActivity().finish();
                        getActivity().getFragmentManager().popBackStack();
                    } else if (strLoginType.equals("VesselForSaleActivity") && mModel.getData().getRole().equals(JaoharConstants.STAFF)) {
                        JaoharPreference.writeString(getActivity(), JaoharPreference.STAFF_EMAIL, mModel.getData().getEmail());
                        JaoharPreference.writeString(getActivity(), JaoharPreference.STAFF_ID, mModel.getData().getId());
                        JaoharPreference.writeString(getActivity(), JaoharPreference.STAFF_ROLE, mModel.getData().getRole());
                        JaoharPreference.writeBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, true);
                        Intent mIntent = new Intent(getActivity(), VesselForSaleActivity.class);
                        getActivity().startActivity(mIntent);
                        getActivity().finish();
                    } else if (strLoginType.equals(JaoharConstants.ADMIN) && mModel.getData().getRole().equals(JaoharConstants.ADMIN)) {
                        if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                            JaoharPreference.writeString(getActivity(), JaoharPreference.ADMIN_EMAIL, mModel.getData().getEmail());
                            JaoharPreference.writeString(getActivity(), JaoharPreference.ADMIN_ID, mModel.getData().getId());
                            JaoharPreference.writeString(getActivity(), JaoharPreference.ADMIN_ROLE, mModel.getData().getRole());
                            JaoharPreference.writeBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_ADMIN, true);
                            Intent mIntent = new Intent(getActivity(), ManagerActivity.class);
                            startActivity(mIntent);
                        } else {
                            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "Please enter staff credentials.");
                        }
                    } else if (mModel.getData().getRole().equals(JaoharConstants.OFFICE)) {
                        JaoharPreference.writeString(getActivity(), JaoharPreference.STAFF_EMAIL, mModel.getData().getEmail());
                        JaoharPreference.writeString(getActivity(), JaoharPreference.STAFF_ID, mModel.getData().getId());
                        JaoharPreference.writeString(getActivity(), JaoharPreference.STAFF_ROLE, mModel.getData().getRole());
                        JaoharPreference.writeBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, true);
                        Intent mIntent = new Intent(getActivity(), HomeActivity.class);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mIntent.putExtra(JaoharConstants.LOGIN, "VesselForSale");
                        getActivity().startActivity(mIntent);
                        getActivity().finish();
                        getActivity().getFragmentManager().popBackStack();
                    } else {
                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "Please enter user credentials.");
                    }
                } else {
                    AlertDialogManager.hideProgressDialog();
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "" + mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<LoginModell> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }
}