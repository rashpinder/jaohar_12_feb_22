package jaohar.com.jaohar.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;

/**
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class OurHistoryFragment extends Fragment {
    String TAG = OurHistoryFragment.this.getClass().getSimpleName();
    View rootView;

    public OurHistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        HomeActivity.bottomMainLL.setVisibility(View.GONE);
        HomeActivity.toolBarMainLL.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
        HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_our_history, container, false);

        return rootView;
    }
}
