package jaohar.com.jaohar.fragments;


import static android.app.Activity.RESULT_OK;
import static jaohar.com.jaohar.HomeActivity.mainRL;
import static jaohar.com.jaohar.HomeActivity.toolBarMainLL;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;
import com.makeramen.roundedimageview.RoundedImageView;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.adapters.DynamicLinksAdapter;
import jaohar.com.jaohar.adapters.staff_module.StaffTabAdapter;
import jaohar.com.jaohar.adapters.staff_module.Staff_news_Adapter;
import jaohar.com.jaohar.beans.DynamicLinksModel;
import jaohar.com.jaohar.beans.staff_module.Staff_Tab_Model;
import jaohar.com.jaohar.beans.staff_module.Staff_news_model;
import jaohar.com.jaohar.interfaces.ChangeFragmentInterface;
import jaohar.com.jaohar.interfaces.blog_module.ClickStaffModuleInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;


/**
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class StaffFragment extends Fragment {
    String TAG = StaffFragment.this.getClass().getSimpleName();
    View rootView;

    public static final int CAMERA_REQUEST = 111;
    public static final int GALLERY_REQUEST = 222;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private final String cameraStr = Manifest.permission.CAMERA;
    private final String readStorageStr = Manifest.permission.READ_EXTERNAL_STORAGE;

    private String mStoragePath;
    private String strBase64 = "", strBase64Profile = "";
    private String strrLoginID = "";
    Bitmap thumb = null;

    ArrayList<Staff_Tab_Model> mTabArrayList = new ArrayList<>();
    ArrayList<Staff_news_model> mArrayNewsList = new ArrayList<>();

    RecyclerView tabListRV, newsRV;
    TextView nameTV, aboutTV, jobTV;
    CircleImageView profileIMG;
    ImageView statusIMG, coverIMGSelect;
    RelativeLayout imageRL;
    RoundedImageView coverIMG;
    StaffTabAdapter mAdapter;
    Staff_news_Adapter mNewsAdapter;
    String strUnReadForum = "", strUnReadUK = "", unread_chat = "", unread_notifications = "";
    String ImageType = "", strFirstName = "", strLastName = "", strOnlineStatus = "";

    public StaffFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

        if (mTabArrayList != null)
            mTabArrayList.clear();

        if (mArrayNewsList != null)
            mArrayNewsList.clear();

        if (Utilities.isNetworkAvailable(Objects.requireNonNull(getActivity()))) {
            gettingAllAddressBookList();
        } else {
            AlertDialogManager.showAlertDialog(getActivity(), getResources().getString(R.string.app_name), getResources().getString(R.string.internetconnection));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //set status bar
        getActivity().getWindow().setStatusBarColor(Color.WHITE);
        getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_staff, container, false);

        setWidgetIDs(rootView);
        setClickListner();

        HomeActivity.txtCenter.setText(getString(R.string.staff));
        JaoharConstants.BOOLEAN_STAFF_LOGOUT = true;
        JaoharConstants.BOOLEAN_USER_LOGOUT = false;
        HomeActivity.bottomMainLL.setVisibility(View.GONE);
        HomeActivity.txtCenter.setTextColor(getActivity().getResources().getColor(R.color.black));
        HomeActivity.txtCenter.setGravity(Gravity.CENTER);

        HomeActivity.imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PerformOptionsClick();
            }
        });

        return rootView;
    }

    private void setWidgetIDs(View v) {
        tabListRV = v.findViewById(R.id.tabListRV);
        profileIMG = v.findViewById(R.id.profileIMG);
        coverIMG = v.findViewById(R.id.coverIMG);
        nameTV = v.findViewById(R.id.nameTV);
        aboutTV = v.findViewById(R.id.aboutTV);
        newsRV = v.findViewById(R.id.newsRV);
        jobTV = v.findViewById(R.id.jobTV);
        statusIMG = v.findViewById(R.id.statusIMG);
        imageRL = v.findViewById(R.id.imageRL);
        coverIMGSelect = v.findViewById(R.id.coverIMGSelect);

        final TextView first = v.findViewById(R.id.first);
        final TextView second = v.findViewById(R.id.second);

        final ValueAnimator animator = ValueAnimator.ofFloat(1.0f, 0.0f);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration(9000L);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                final float progress = (float) animation.getAnimatedValue();
                final float width = first.getWidth();
                final float translationX = width * progress;
                first.setTranslationX(translationX);
                second.setTranslationX(translationX - width);
            }
        });
        animator.start();
    }

    private void setClickListner() {
        imageRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strURL = JaoharPreference.readString(getActivity(), JaoharPreference.Role, "");
                Log.e(TAG, "Data== " + strURL);
                if (JaoharPreference.readString(getActivity(), JaoharPreference.Role, "").equals(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ROLE, ""))) {
                    JaoharConstants.BOOLEAN_USER_LOGOUT = true;
                    ImageType = "profile";
                    setUpCameraGalleryDialog();
//                    Intent mIntent = new Intent(getActivity(), UserProfileActivity.class);
//                    startActivity(mIntent);
                } else {
                    JaoharConstants.BOOLEAN_STAFF_LOGOUT = true;
                    ImageType = "profile";
                    setUpCameraGalleryDialog();
//                    Intent mIntent = new Intent(getActivity(), UserProfileActivity.class);
//                    startActivity(mIntent);
                }
            }
        });

        coverIMGSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageType = "cover";
                setUpCameraGalleryDialog();
            }
        });
    }

    public void setUpCameraGalleryDialog() {
        if (checkPermission()) {
//            openCameraGalleryDialog();
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }

    private void openCameraGalleryDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_display_dialog);

        TextView text_camra = dialog.findViewById(R.id.txt_camra);
        TextView text_gallery = dialog.findViewById(R.id.txt_gallery);
        TextView txt_files = dialog.findViewById(R.id.txt_files);
        TextView txt_cancel = dialog.findViewById(R.id.txt_cancel);

        text_camra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openCamera();
            }
        });
        text_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });
        txt_files.setVisibility(View.GONE);
        txt_files.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openFiles();
            }
        });
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        Log.e("error is occured", dialog.toString());
    }

    public void openFiles() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/png/jpg/jpeg");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 505);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(getActivity(), "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                String authorities = getActivity().getPackageName() + "com.dharmaniapps.fileprovider";
                Uri imageURI = FileProvider.getUriForFile(getActivity(), "jaohar.com.jaohar.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            }
        }
        startActivityForResult(takePictureIntent, CAMERA_REQUEST);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        String mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }

    public void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);
    }

    /*********
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/
    private boolean checkPermission() {
        int camera = ContextCompat.checkSelfPermission(getActivity(), cameraStr);
        int readStorage = ContextCompat.checkSelfPermission(getActivity(), readStorageStr);
        return camera == PackageManager.PERMISSION_GRANTED && readStorage == PackageManager.PERMISSION_GRANTED;
    }

    void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{cameraStr, readStorageStr}, PERMISSION_REQUEST_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {

//                            openCameraGalleryDialog();

                            onSelectImageClick();

                        } else {
                            Log.e(TAG, "onRequestPermissionsResult: failed");
                            requestPermission();

                        }
                    }
                }
                break;
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private void onSelectImageClick() {
        CropImage.startPickImageActivity(getActivity());
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setAspectRatio(70, 70)
                .setMultiTouchEnabled(false)
                .start(getContext(), this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getActivity(), data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(getActivity(), imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()

            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                try {

                    final InputStream imageStream = getActivity().getContentResolver().openInputStream(result.getUri());
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 10, baos);

                    if (ImageType.equals("cover")) {
                        coverIMG.setImageBitmap(selectedImage);
                        strBase64 = encodeTobase64(selectedImage);

                        if (!Utilities.isNetworkAvailable(getActivity())) {
                            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
                        } else {
                            if (JaoharConstants.BOOLEAN_USER_LOGOUT) {
                                executeUpdateCoverPicApi(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
                            } else if (JaoharConstants.BOOLEAN_STAFF_LOGOUT) {
                                executeUpdateCoverPicApi(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                            } else if (JaoharConstants.Is_click_form_Manager) {
                                executeUpdateCoverPicApi(JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, ""));
                            } else if (JaoharConstants.IS_CLICK_FROM_USER_VESSEL) {
                                executeUpdateCoverPicApi(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
                            }
                        }
                        Log.e(TAG, "**Image Base 64**" + strBase64);
                    } else {
                        profileIMG.setImageBitmap(selectedImage);
                        strBase64Profile = encodeTobase64(selectedImage);

                        if (!Utilities.isNetworkAvailable(getActivity())) {
                            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
                        } else {
                            if (JaoharConstants.BOOLEAN_USER_LOGOUT) {
                                executeAddAPI(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
                            } else if (JaoharConstants.BOOLEAN_STAFF_LOGOUT) {
                                executeAddAPI(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                            } else if (JaoharConstants.Is_click_form_Manager) {
                                executeAddAPI(JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, ""));
                            } else if (JaoharConstants.IS_CLICK_FROM_USER_VESSEL) {
                                executeAddAPI(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
                            }
                        }
                        Log.e(TAG, "**Image Base 64**" + strBase64Profile);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(getActivity(), "Cropping failed: " + result.getError(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    //       Method Correct Rotate Image when Capture From Camera....
    private Bitmap rotateImage(Bitmap bitmap) {
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(mStoragePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            default:
        }
        Bitmap rotate = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return rotate;
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private void gettingProfileDATA(String strLoginID) {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getuserProfileRequest(strLoginID);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "*****Response****" + response);
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    String strStatus = mJsonObject.getString("status");
                    String strMessage = mJsonObject.getString("message");
                    if (strStatus.equals("1")) {
                        parseResponce(response.body().toString());
                    } else {
                        AlertDialogManager.showAccountDiableDialog(getActivity(), getString(R.string.app_name), strMessage);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseResponce(String response) {
        JSONObject mJsonObject = null;
        try {
            mJsonObject = new JSONObject(response);
            if (!mJsonObject.isNull("data")) {
                JSONObject mJsonDATA = mJsonObject.getJSONObject("data");
                JaoharPreference.writeString(getActivity(), JaoharPreference.Role, mJsonDATA.getString("role"));

                if (!mJsonDATA.getString("first_name").equals("")) {
                    JaoharPreference.writeString(getActivity(), JaoharPreference.first_name, mJsonDATA.getString("first_name"));
                    HomeActivity.profileLL.setVisibility(View.VISIBLE);
                    JaoharPreference.writeString(getActivity(), JaoharPreference.full_name, mJsonDATA.getString("first_name") + " " + mJsonDATA.getString("last_name"));
                    HomeActivity.nameTV.setText(mJsonDATA.getString("first_name") + "  " + mJsonDATA.getString("last_name"));
                    nameTV.setText(mJsonDATA.getString("first_name") + "  " + mJsonDATA.getString("last_name"));

                    strFirstName = mJsonDATA.getString("first_name");

                    HomeActivity.signTV.setText("Hi, " + mJsonDATA.getString("first_name"));
                }

                if (!mJsonDATA.getString("last_name").equals("")) {
                    strLastName = mJsonDATA.getString("last_name");
                }

                if (!mJsonDATA.getString("unread_chat").equals("")) {
                    unread_chat = mJsonDATA.getString("unread_chat");
                }

                if (!mJsonDATA.getString("unread_notify").equals("")) {
                    unread_notifications = mJsonDATA.getString("unread_notify");
                    if (mJsonDATA.getString("unread_notify").equals("0")) {
                        HomeActivity.badgesTV.setVisibility(View.GONE);
                        HomeActivity.badgesTVUser.setVisibility(View.GONE);
                    } else {
                        HomeActivity.badgesTV.setVisibility(View.VISIBLE);
                        HomeActivity.badgesTVUser.setVisibility(View.VISIBLE);
                        if (Integer.parseInt(mJsonDATA.getString("unread_notify")) > 99) {
                            HomeActivity.badgesTV.setText("99+");
                            HomeActivity.badgesTVUser.setText("99+");
                        } else {
                            HomeActivity.badgesTV.setText(mJsonDATA.getString("unread_notify"));
                            HomeActivity.badgesTVUser.setText(mJsonDATA.getString("unread_notify"));
                        }
                    }
                } else {
                    HomeActivity.badgesTV.setVisibility(View.GONE);
                    HomeActivity.badgesTVUser.setVisibility(View.GONE);
                }

                if (!mJsonDATA.getString("image").equals("")) {
                    JaoharPreference.writeString(getActivity(), JaoharPreference.image, mJsonDATA.getString("image"));
                    String strImage = mJsonDATA.getString("image");
                    Glide.with(getActivity()).load(strImage).into(HomeActivity.profilePICIMG);

                    if (strBase64Profile.equals("")) {
                        Glide.with(getActivity()).load(strImage).into(profileIMG);
                        Glide.with(getActivity()).load(strImage).into(HomeActivity.profilePICIMG);
                    } else {
                        byte[] decodedString = Base64.decode(strBase64Profile, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        profileIMG.setImageBitmap(decodedByte);
                        HomeActivity.profilePICIMG.setImageBitmap(decodedByte);
                    }

                } else {
                    HomeActivity.profilePICIMG.setImageResource(R.drawable.profile);
                }
                if (!mJsonDATA.getString("cover_image").equals("")) {

                    String strImage = mJsonDATA.getString("cover_image");

                    if (strBase64.equals("")) {
                        Glide.with(getActivity()).load(strImage).into(coverIMG);
                    } else {
                        byte[] decodedString = Base64.decode(strBase64, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        coverIMG.setImageBitmap(decodedByte);
                    }
                }

                if (!mJsonDATA.getString("bio").equals("")) {

                    aboutTV.setText(mJsonDATA.getString("bio"));
                }
                if (!mJsonDATA.getString("job").equals("")) {

                    jobTV.setText(mJsonDATA.getString("job"));
                }
                if (!mJsonDATA.getString("unread_forum_message_count").equals("")) {

                    strUnReadForum = mJsonDATA.getString("unread_forum_message_count");
                }
                if (!mJsonDATA.getString("unread_uk_forum_message_count").equals("")) {

                    strUnReadUK = mJsonDATA.getString("unread_uk_forum_message_count");
                }

                if (!mJsonDATA.getString("chat_status").equals("")) {
                    String strImage = mJsonDATA.getString("chat_status");
                    strOnlineStatus = strImage;
                    if (strImage.equals("Available")) {
                        statusIMG.setImageDrawable(getResources().getDrawable(R.drawable.green_status_icon));
//                        HomeActivity.statusIMG.setImageDrawable(getResources().getDrawable(R.drawable.green_status_icon));
                    } else if (strImage.equals("Busy")) {
                        statusIMG.setImageDrawable(getResources().getDrawable(R.drawable.red_status_icon));
                    } else if (strImage.equals("Invisible")) {
                        statusIMG.setImageDrawable(getResources().getDrawable(R.drawable.grey_status_icon));
                    }
                }

                if (!mJsonDATA.getString("role").equals("")) {
                    if (mJsonDATA.getString("role").equals("staff")) {
                        HomeActivity.typetv.setText("Romania");
                    } else if (mJsonDATA.getString("role").equals("office")) {
                        HomeActivity.typetv.setText("UK Office");
                    } else {
                        HomeActivity.typetv.setText("Admin");
                    }
                }

                parseResponceTabData(mJsonDATA);
                parseResponceNewsData(mJsonDATA);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /*
     * Getting All email array list  for address showing
     * */
    private void gettingAllAddressBookList() {
        JaoharConstants.mEmailsArrayLIST.clear();
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getAllAddressBookMailsRequest(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "*****Response****" + response);
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    JSONArray mJsonArray = mJsonObject.getJSONArray("data");
                    for (int i = 0; i < mJsonArray.length(); i++) {
                        if (!mJsonArray.getString(i).equals("")) {
                            JaoharConstants.mEmailsArrayLIST.add(mJsonArray.getString(i));
                        }
                    }
                    Log.e(TAG, "*****Response****" + "Array List All EMAILS SIZE ======  " + JaoharConstants.mEmailsArrayLIST.size());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (getActivity() != null) {
                    if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                        if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                            gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                        } else {
                            gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
                        }
                    } else {
                        gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                    }
                }
                HomeActivity.txtCenter.setText("Staff");
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    /*
     * Parse Tab responce
     * */
    private void parseResponceTabData(JSONObject mJsonData) {
        try {
            JSONArray mJsonArray = mJsonData.getJSONArray("modules");
            if (mJsonArray != null) {
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJsonTabData = mJsonArray.getJSONObject(i);
                    Staff_Tab_Model mModel = new Staff_Tab_Model();
                    if (!mJsonTabData.getString("module_id").equals("")) {
                        mModel.setModule_id(mJsonTabData.getString("module_id"));
                    }
                    if (!mJsonTabData.getString("access_type").equals("")) {
                        mModel.setAccess_type(mJsonTabData.getString("access_type"));
                    }
                    if (!mJsonTabData.getString("module_name").equals("")) {
                        mModel.setModule_name(mJsonTabData.getString("module_name"));
                    }
                    if (!mJsonTabData.getString("module_link").equals("")) {
                        mModel.setModule_link(mJsonTabData.getString("module_link"));
                    }
                    if (!mJsonTabData.getString("module_image").equals("")) {
                        mModel.setModule_image(mJsonTabData.getString("module_image"));
                    }
                    if (!mJsonTabData.getString("app_image").equals("")) {
                        mModel.setApp_image(mJsonTabData.getString("app_image"));
                    }

                    if (!mJsonTabData.getString("type").equals("")) {
                        mModel.setType(mJsonTabData.getString("type"));
                    }
                    if (!mJsonTabData.getString("access_to").equals("")) {
                        mModel.setAccess_to(mJsonTabData.getString("access_to"));
                    }
                    if (!mJsonTabData.getString("disable").equals("")) {
                        mModel.setDisable(mJsonTabData.getString("disable"));
                    }
                    if (!mJsonTabData.getString("creation_date").equals("")) {
                        mModel.setCreation_date(mJsonTabData.getString("creation_date"));
                    }
                    mTabArrayList.add(mModel);
                }

                // SetAdapter
                setTabAdapter();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*
     * Parse News responce
     * */
    private void parseResponceNewsData(JSONObject mJsonData) {
        try {
            JSONArray mJsonArray = mJsonData.getJSONArray("all_news");
            if (mJsonArray != null) {
                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJsonTabData = mJsonArray.getJSONObject(i);
                    Staff_news_model mModel = new Staff_news_model();
                    if (!mJsonTabData.getString("created_at").equals("")) {
                        mModel.setCreated_at(mJsonTabData.getString("created_at"));
                    }
                    if (!mJsonTabData.getString("news_text").equals("")) {
                        mModel.setNews_text(mJsonTabData.getString("news_text"));
                    }
                    if (!mJsonTabData.getString("news_image").equals("")) {
                        mModel.setNews_image(mJsonTabData.getString("news_image"));
                    }
                    if (!mJsonTabData.getString("type").equals("")) {
                        mModel.setType(mJsonTabData.getString("type"));
                    }
                    mArrayNewsList.add(mModel);
                }

                // SetAdapter
                setNewsAdapter();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setTabAdapter() {
        tabListRV.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        mAdapter = new StaffTabAdapter(getActivity(), mTabArrayList, strUnReadForum, strUnReadUK,
                unread_chat, unread_notifications, new ClickStaffModuleInterface() {
            @Override
            public void mClickStaffModuleInterface(int position, String type) {
                JaoharConstants.is_Staff_FragmentClick = true;
                if (type.equals("blog")) {
                    HomeActivity.blogToolbar();
                    HomeActivity.txtCenter.setVisibility(View.VISIBLE);
                    HomeActivity.mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    HomeActivity.toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                    switchFragment(requireActivity(), new BlogFragment(), JaoharConstants.BLOG_FRAGMENT, false, null);
                } else if (type.equals("favourite")) {
                    HomeActivity.txtCenter.setText(getString(R.string.favorite_vessels));
                    HomeActivity.FavvesselsToolbar();
                    HomeActivity.txtCenter.setVisibility(View.VISIBLE);
                    mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                    switchFragment(requireActivity(), new FavoriteVesselFragment(), JaoharConstants.FavoriteVessels, false, null);
                } else if (type.equals("notifications")) {
                    HomeActivity.txtCenter.setText(getString(R.string.notification));
                    HomeActivity.notificationToolbar();
                    HomeActivity.txtCenter.setVisibility(View.VISIBLE);
                    mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                    switchFragment(requireActivity(), new NotificationFragment(), JaoharConstants.NotificationFragment, false, null);
                }
            }
        });
        tabListRV.setAdapter(mAdapter);
    }

    private void setNewsAdapter() {
        newsRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        mNewsAdapter = new Staff_news_Adapter(getActivity(), mArrayNewsList, new ChangeFragmentInterface() {
            @Override
            public void mChangeFragmentInterface(int position) {
                JaoharConstants.is_Staff_FragmentClick = true;
                HomeActivity.txtCenter.setText(getString(R.string.notification));
                HomeActivity.notificationToolbar();
                HomeActivity.txtCenter.setVisibility(View.VISIBLE);
                mainRL.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                toolBarMainLL.setBackgroundColor(getResources().getColor(R.color.white));
                switchFragment(Objects.requireNonNull(getActivity()), new NotificationFragment(), JaoharConstants.NotificationFragment, false, null);
            }
        });
        newsRV.setAdapter(mNewsAdapter);
    }

    /********
     *Replace Fragment In Activity
     **********/
    public static void switchFragment(FragmentActivity activity, Fragment fragment, String TAG, boolean addToStack, Bundle bundle) {
        FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (fragment != null) {
            // Replace current fragment by this new one
            ft.replace(R.id.layoutContainerLL, fragment, TAG);
            if (addToStack)
                ft.addToBackStack(null);
            if (bundle != null)
                fragment.setArguments(bundle);
            ft.commitAllowingStateLoss();
        }
    }

    /*
     * Update cover pic
     * */
    private Map<String, String> mParams() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", strrLoginID);
        params.put("image", strBase64);
        Log.e("**PARAMS**", params.toString());
        return params;
    }

    private void executeUpdateCoverPicApi(String strLoginID) {
        strrLoginID = strLoginID;
//        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.editCoverImage(mParams()).enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
//                hideProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response);
                StatusMsgModel mModel = response.body();
                Log.e(TAG, "******response*****" + response.body().toString());
                if (mModel.getStatus() == 1) {

                } else {
                    Toast.makeText(getActivity(), mModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), t.getMessage());
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private RecyclerView mRecyclerView;
    private DynamicLinksAdapter dynamicLinksAdapter;
    private ImageView closeIV;
    private final ArrayList<DynamicLinksModel> modelArrayList = new ArrayList<>();

    private void PerformOptionsClick() {
        if (getActivity() != null) {
            final Dialog searchDialog = new Dialog(getActivity());
            searchDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            searchDialog.setContentView(R.layout.bottomsheet_dymanic_links);
            searchDialog.setCanceledOnTouchOutside(false);
            searchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            Window window = searchDialog.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();

            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            window.setAttributes(wlp);
            searchDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

            mRecyclerView = searchDialog.findViewById(R.id.dymanicLinksRv);
            closeIV = searchDialog.findViewById(R.id.closeIV);

            closeIV.setVisibility(View.VISIBLE);

            closeIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    searchDialog.dismiss();
                }
            });

            searchDialog.show();
        }
        if (JaoharConstants.BOOLEAN_USER_LOGOUT) {
            if (getActivity()!=null)
            gettingDymanicLinks(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
        } else if (JaoharConstants.BOOLEAN_STAFF_LOGOUT) {
            if (getActivity()!=null)
            gettingDymanicLinks(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
        } else if (JaoharConstants.Is_click_form_Manager) {
            if (getActivity()!=null)
            gettingDymanicLinks(JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, ""));
        } else if (JaoharConstants.IS_CLICK_FROM_USER_VESSEL) {
            if (getActivity()!=null)
            gettingDymanicLinks(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
        }
    }

    private void gettingDymanicLinks(String LoginId) {
        String strUserId = "";

        if (!JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, "").equals("")) {
            strUserId = JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, "");
        } else {
            strUserId = JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, "");
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getAllDynamicLinksRequest(LoginId).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "*****Response****" + response);
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    String strStatus = mJsonObject.getString("status");
                    String strMessage = mJsonObject.getString("message");
                    if (strStatus.equals("1")) {
                        parseLinksResponce(response.body().toString());
                    } else {
                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), strMessage);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), t.getMessage());
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void parseLinksResponce(String response) {
        JSONObject mJsonObject = null;
        try {
            mJsonObject = new JSONObject(response);
            if (!mJsonObject.isNull("data")) {
                JSONArray mjsonArrayData = mJsonObject.getJSONArray("data");
                if (mjsonArrayData != null) {
                    ArrayList<DynamicLinksModel> mTempraryList = new ArrayList<>();
                    mTempraryList.clear();
                    for (int i = 0; i < mjsonArrayData.length(); i++) {
                        JSONObject mJsonDATA = mjsonArrayData.getJSONObject(i);
                        DynamicLinksModel mModel = new DynamicLinksModel();
                        if (!mJsonDATA.getString("link_id").equals("")) {
                            mModel.setLink_id(mJsonDATA.getString("link_id"));
                        }
                        if (!mJsonDATA.getString("link_name").equals("")) {
                            mModel.setLink_name(mJsonDATA.getString("link_name"));
                        }
                        if (!mJsonDATA.getString("link_value").equals("")) {
                            mModel.setLink_value(mJsonDATA.getString("link_value"));
                        }
                        if (!mJsonDATA.getString("link_image").equals("")) {
                            mModel.setLink_image(mJsonDATA.getString("link_image"));
                        }
                        mTempraryList.add(mModel);
                    }
                    modelArrayList.addAll(mTempraryList);
                    setAdapter();
                }
            }
        } catch (
                JSONException e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        dynamicLinksAdapter = new DynamicLinksAdapter(getActivity(), modelArrayList);
        mRecyclerView.setAdapter(dynamicLinksAdapter);
        dynamicLinksAdapter.notifyDataSetChanged();
    }

    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("first_name", strFirstName);
        mMap.put("last_name", strLastName);
        mMap.put("image", strBase64Profile);
        mMap.put("user_id", strrLoginID);
        mMap.put("online_status", strOnlineStatus);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeAddAPI(String strLoginID) {
        strrLoginID = strLoginID;
//        AlertDialogManager.showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.editUserProfileRequest(mParam());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
//                    mAlerDialog(mActivity, getString(R.string.app_name), mModel.getMessage());
                } else {
                    mAlerDialog(getActivity(), getString(R.string.app_name), mModel.getMessage()
                    );
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    public void mAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);
        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                getActivity().onBackPressed();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (AlertDialogManager.progressDialog.isShowing()) {
            AlertDialogManager.hideProgressDialog();
        }
    }
}








