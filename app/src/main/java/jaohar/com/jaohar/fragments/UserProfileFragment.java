package jaohar.com.jaohar.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
//import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonObject;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import jaohar.com.jaohar.HomeActivity;
import jaohar.com.jaohar.R;
import jaohar.com.jaohar.RetrofitApi.ApiClient;
import jaohar.com.jaohar.RetrofitApi.ApiInterface;
import jaohar.com.jaohar.models.StatusMsgModel;
import jaohar.com.jaohar.utils.AlertDialogManager;
import jaohar.com.jaohar.utils.JaoharConstants;
import jaohar.com.jaohar.utils.JaoharPreference;
import jaohar.com.jaohar.utils.Utilities;
import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_OK;
import static jaohar.com.jaohar.HomeActivity.imgRight;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileFragment extends Fragment {

    String TAG = UserProfileFragment.this.getClass().getSimpleName();

    public static final int CAMERA_REQUEST = 111;
    public static final int GALLERY_REQUEST = 222;
    private static final int PERMISSION_REQUEST_CODE = 200;

    private String cameraStr = Manifest.permission.CAMERA;
    private String readStorageStr = Manifest.permission.READ_EXTERNAL_STORAGE;

    private String refreshedToken = "";
    private LinearLayout llLeftLL;
    private String online_status = "";
    private ImageView itemDeleteIV;
    private CircleImageView mImage;
    private EditText editFirstNameET, editLastNAMEET;
    private Button btnAddNews;

    private String mStoragePath;
    private String strBase64 = "", strType = "";
    private String strrLoginID = "";

    public UserProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);

        //set status bar
        getActivity().getWindow().setStatusBarColor(Color.WHITE);
        getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        HomeActivity.bottomMainLL.setVisibility(View.GONE);
        HomeActivity.txtCenter.setVisibility(View.GONE);
        HomeActivity.imgRightLL.setVisibility(View.VISIBLE);
        imgRight.setVisibility(View.VISIBLE);
        imgRight.setImageResource(R.drawable.ic_logout);

        setViewsIDs(view);

        setClickListner();

        getPushToken();

        if (!Utilities.isNetworkAvailable(getActivity())) {
            AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
        } else {
            if (JaoharConstants.BOOLEAN_USER_LOGOUT) {
                if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                    if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                        gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                    } else {
                        gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
                    }
                } else {
                    gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                }
            } else if (JaoharConstants.BOOLEAN_STAFF_LOGOUT) {
                if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                    if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                        gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                    } else {
                        gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
                    }
                } else {
                    gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                }
            } else if (JaoharConstants.Is_click_form_Manager) {
                gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, ""));
            } else if (JaoharConstants.IS_CLICK_FROM_USER_VESSEL) {
                gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
            }
        }

        return view;
    }

    private void getPushToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {

                    if (!task.isSuccessful()) {
                        //handle token error
                        Log.e(TAG, "**Get Instance Failed**", task.getException());
                        return;
                    }
                    // Get new Instance ID token
                    refreshedToken = task.getResult();
                    Log.e(TAG, "**Push Token**" + refreshedToken);

                });
    }

    private void setViewsIDs(View v) {
        editLastNAMEET = v.findViewById(R.id.editLastNAMEET);
        editFirstNameET = v.findViewById(R.id.editFirstNameET);
        mImage = v.findViewById(R.id.mImage);
        llLeftLL = v.findViewById(R.id.llLeftLL);
        btnAddNews = v.findViewById(R.id.btnAddNews);
        itemDeleteIV = v.findViewById(R.id.itemDeleteIV);
        HomeActivity.imgRightLL.setVisibility(View.VISIBLE);
        ImageView imgRight = v.findViewById(R.id.imgRight);
        imgRight.setImageResource(R.drawable.ic_logout);

        setUpCityAdapter();
    }

    private void setClickListner() {
        HomeActivity.imgRightLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmDialog();

            }
        });

        try {
            HomeActivity.imgRightLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ConfirmDialog();
                }
            });
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        mImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpCameraGalleryDialog();
            }
        });

        itemDeleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpCameraGalleryDialog();
            }
        });

        btnAddNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utilities.isNetworkAvailable(getActivity())) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
                } else {
                    if (JaoharConstants.BOOLEAN_USER_LOGOUT) {
                        if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                            if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                                executeAddAPI(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                            } else {
                                executeAddAPI(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
                            }
                        } else {
                            executeAddAPI(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                        }
                    } else if (JaoharConstants.BOOLEAN_STAFF_LOGOUT) {
                        if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                            if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                                executeAddAPI(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                            } else {
                                executeAddAPI(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
                            }
                        } else {
                            executeAddAPI(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                        }
                    } else if (JaoharConstants.Is_click_form_Manager) {
                        executeAddAPI(JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, ""));
                    } else if (JaoharConstants.IS_CLICK_FROM_USER_VESSEL) {
                        if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                            if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                                executeAddAPI(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                            } else {
                                executeAddAPI(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
                            }
                        } else {
                            executeAddAPI(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                        }
                    }
                }
            }
        });

        llLeftLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    public void openFiles() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/png/jpg/jpeg");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 505);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(getActivity(), "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                String authorities = getActivity().getApplicationContext().getPackageName() + "com.dharmaniapps.fileprovider";
                Uri imageURI = FileProvider.getUriForFile(getActivity().getApplicationContext(), "jaohar.com.jaohar.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            }
        }
        startActivityForResult(takePictureIntent, CAMERA_REQUEST);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        String mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }

    public void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);
    }

    /*********
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/
    private boolean checkPermission() {
        int camera = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), cameraStr);
        int readStorage = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), readStorageStr);
        return camera == PackageManager.PERMISSION_GRANTED && readStorage == PackageManager.PERMISSION_GRANTED;
    }

    void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{cameraStr, readStorageStr}, PERMISSION_REQUEST_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                            onSelectImageClick();

                        } else {
                            Log.e(TAG, "onRequestPermissionsResult: failed");
                            requestPermission();

                        }
                    }
                }
                break;
        }
    }

    public void setUpCameraGalleryDialog() {
        if (checkPermission()) {
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private void onSelectImageClick() {
        CropImage.startPickImageActivity(getActivity());
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setAspectRatio(70, 70)
                .setMultiTouchEnabled(false)
                .start(getContext(), this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getActivity(), data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(getActivity(), imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()

            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                try {

                    final InputStream imageStream = getActivity().getContentResolver().openInputStream(result.getUri());
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 10, baos);
                    mImage.setImageBitmap(selectedImage);
                    strBase64 = encodeTobase64(selectedImage);

                    Log.e(TAG, "**Image Base 64**" + strBase64);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(getActivity(), "Cropping failed: " + result.getError(), Toast.LENGTH_SHORT).show();
            }
        }
//        if (resultCode == RESULT_OK) {
//            if (requestCode == GALLERY_REQUEST) {
//                try {
//                    if (data != null) {
//                        try {
//                            Bitmap bitmap;
//                            Uri uri = data.getData();
//                            File finalFile = new File(getRealPathFromURI(uri));
//
//                            BitmapFactory.Options options = new BitmapFactory.Options();
//                            options.inSampleSize = 0;
//
//                            bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
////                          For COnvert And rotate image
//                            ExifInterface exifInterface = null;
//                            try {
//                                exifInterface = new ExifInterface(finalFile.getAbsolutePath());
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//
//                            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
//                            Matrix matrix = new Matrix();
//                            switch (orientation) {
//                                case ExifInterface.ORIENTATION_ROTATE_90:
//                                    matrix.setRotate(90);
//                                    break;
//                                case ExifInterface.ORIENTATION_ROTATE_180:
//                                    matrix.setRotate(180);
//                                    break;
//                                case ExifInterface.ORIENTATION_ROTATE_270:
//                                    matrix.setRotate(270);
//                                    break;
//                                case ExifInterface.ORIENTATION_NORMAL:
//                                default:
//
//                            }
//                            JaoharConstants.IS_camera_Click = false;
//                            thumb = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
//                            mImage.setImageBitmap(thumb);
//                            strBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb);
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//                        Log.e(TAG, "*****No Picture Selected****");
//                        return;
//                    }
//                } finally {
//                }
//            }
//            if (requestCode == CAMERA_REQUEST) {
//                try {
//
//                    JaoharConstants.IS_camera_Click = true;
//
//                    thumb = ImageUtils.getInstant().rotateBitmapOrientation(mStoragePath);
//                    BitmapFactory.Options options = new BitmapFactory.Options();
//                    options.inSampleSize = 8;
//
//                    Bitmap bitmap = BitmapFactory.decodeFile(mStoragePath, options);
//                    Bitmap thumb1 = rotateImage(bitmap);
//
//                    mImage.setImageBitmap(thumb);
//
//                    strBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb);
//
////                    imgStampImageIV.setImageURI(uri);
//                    //strImageBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//            if (requestCode == 505) {
//                Bitmap bitmap;
//                String strDocumentPath, strDocumentName;
//                Uri uri = data.getData();
//                strDocumentPath = uri.getPath();
//                strDocumentPath = strDocumentPath.replace(" ", "_");
//                strDocumentName = uri.getLastPathSegment();
//                File finalFile = new File(getRealPathFromURI(uri));
//                BitmapFactory.Options options = new BitmapFactory.Options();
//                options.inSampleSize = 0;
//                bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
////                          For COnvert And rotate image
//                ExifInterface exifInterface = null;
//                try {
//                    exifInterface = new ExifInterface(finalFile.getAbsolutePath());
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
//                Matrix matrix = new Matrix();
//                switch (orientation) {
//                    case ExifInterface.ORIENTATION_ROTATE_90:
//                        matrix.setRotate(90);
//                        break;
//                    case ExifInterface.ORIENTATION_ROTATE_180:
//                        matrix.setRotate(180);
//                        break;
//                    case ExifInterface.ORIENTATION_ROTATE_270:
//                        matrix.setRotate(270);
//                        break;
//                    case ExifInterface.ORIENTATION_NORMAL:
//                    default:
//                }
//                JaoharConstants.IS_camera_Click = false;
//                thumb = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
//                mImage.setImageBitmap(thumb);
//                strBase64 = ImageUtils.getInstant().getBase64FromBitmap(thumb);
//            }
//        }
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    private void logOutAPI(String strLoginID) {
//      http://root.jaohar.net/Staging/JaoharWebServicesNew/logout.php
        String strUrl = "";
        if (JaoharConstants.Is_click_form_Manager == true) {
            executeAdminLogoutApi(strLoginID);
//            strUrl = JaoharConstants.ADMIN_LOGOUT_API + "?user_id=" + JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, "") + "&device_token=" + refreshedToken + "&device_type=" + "Android";
        } else {
            executeLogoutApi(strLoginID);
//            strUrl = JaoharConstants.LOGOUT_API + "?user_id=" + strLoginID + "&device_token=" + refreshedToken + "&device_type=" + "Android";
        }
    }

    //ADMIN_LOGOUT_API
    private void executeAdminLogoutApi(String strLoginID) {
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.adminLogoutRequest(strLoginID, refreshedToken, "Android");
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showLogoutDialog(getActivity(), getString(R.string.app_name), mModel.getMessage());
                } else {
                    mAlerDialog(getActivity(), getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private void executeLogoutApi(String strLoginID) {
        AlertDialogManager.showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.logoutRequest(strLoginID, refreshedToken, "Android");
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {
                    showLogoutDialog(getActivity(), getString(R.string.app_name), mModel.getMessage());
                } else {
                    mAlerDialog(getActivity(), getString(R.string.app_name), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("first_name", editFirstNameET.getText().toString());
        mMap.put("last_name", editLastNAMEET.getText().toString());
        mMap.put("image", strBase64);
        mMap.put("user_id", strrLoginID);
        mMap.put("online_status", online_status);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeAddAPI(String strLoginID) {
        strrLoginID = strLoginID;
        AlertDialogManager.showProgressDialog(getActivity());
//        String url = JaoharConstants.EditUserProfile_API;
//        JSONObject jsonObject = new JSONObject();

        if (strType.contains("(") && strType.contains(")")) {
            online_status = strType.replace("(", "").replace(")", "");
        } else {
            online_status = strType;
        }

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<StatusMsgModel> call1 = mApiInterface.editUserProfileRequest(mParam());
        call1.enqueue(new Callback<StatusMsgModel>() {
            @Override
            public void onResponse(Call<StatusMsgModel> call, retrofit2.Response<StatusMsgModel> response) {
                AlertDialogManager.hideProgressDialog();
                StatusMsgModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mAlerDialog(getActivity(), getString(R.string.app_name), mModel.getMessage());

                    if (!Utilities.isNetworkAvailable(getActivity())) {
                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.internetconnection));
                    } else {
                        if (JaoharConstants.BOOLEAN_USER_LOGOUT) {
                            if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                                if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                                    gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                                } else {
                                    gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
                                }
                            } else {
                                gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                            }
//                                        gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
                        } else if (JaoharConstants.BOOLEAN_STAFF_LOGOUT) {
                            if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                                if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                                    gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                                } else {
                                    gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
                                }
                            } else {
                                gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                            }
//                                        gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                        } else if (JaoharConstants.Is_click_form_Manager) {
                            gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, ""));
                        } else if (JaoharConstants.IS_CLICK_FROM_USER_VESSEL) {
                            if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                                if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                                    gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                                } else {
                                    gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
                                }
                            } else {
                                gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                            }
//                                        gettingProfileDATA(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
                        }
                    }

                } else {
                    mAlerDialog(getActivity(), getString(R.string.app_name), mModel.getMessage());
                }
            }


            @Override
            public void onFailure(Call<StatusMsgModel> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    public void mAlerDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);
        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
//                getActivity().onBackPressed();
            }
        });
        alertDialog.show();
    }

    public void showLogoutDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_customalert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = (TextView) alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = (TextView) alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = (TextView) alertDialog.findViewById(R.id.txtDismiss);
        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (JaoharConstants.BOOLEAN_USER_LOGOUT == true) {
                    JaoharConstants.BOOLEAN_STAFF_LOGOUT = false;
                    JaoharConstants.BOOLEAN_USER_LOGOUT = false;

                    JaoharConstants.IS_CLICK_FROM_USER_VESSEL = false;
                    JaoharConstants.Is_click_form_Manager = false;

                    SharedPreferences preferences = JaoharPreference.getPreferences(mActivity);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.remove(JaoharPreference.IS_LOGGED_IN_VESSELS);
                    editor.remove(JaoharPreference.USER_ID);
                    editor.remove(JaoharPreference.USER_EMAIL);
                    editor.remove(JaoharPreference.USER_ROLE);
                    editor.remove(JaoharPreference.first_name);
                    editor.remove(JaoharPreference.image);
                    editor.remove(JaoharPreference.full_name);
                    editor.remove(JaoharPreference.Role);

                    //clear staff data
                    editor.remove(JaoharPreference.IS_LOGGED_IN_STAFF);
                    editor.remove(JaoharPreference.STAFF_ID);
                    editor.remove(JaoharPreference.STAFF_EMAIL);
                    editor.remove(JaoharPreference.STAFF_ROLE);
                    editor.remove(JaoharPreference.first_name);
                    editor.remove(JaoharPreference.image);
                    editor.remove(JaoharPreference.full_name);
                    editor.remove(JaoharPreference.Role);

                    editor.apply();
                    editor.commit();
                    Intent mIntent = new Intent(mActivity, HomeActivity.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mIntent.putExtra(JaoharConstants.LOGIN, "Home");
                    mActivity.startActivity(mIntent);
                    mActivity.finish();
//                    overridePendingTransitionEnter();

                } else if (JaoharConstants.BOOLEAN_STAFF_LOGOUT == true) {
                    JaoharConstants.BOOLEAN_STAFF_LOGOUT = false;
                    JaoharConstants.BOOLEAN_USER_LOGOUT = false;

                    JaoharConstants.IS_CLICK_FROM_USER_VESSEL = false;
                    JaoharConstants.Is_click_form_Manager = false;
                    SharedPreferences preferences = JaoharPreference.getPreferences(mActivity);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.remove(JaoharPreference.IS_LOGGED_IN_STAFF);
                    editor.remove(JaoharPreference.STAFF_ID);
                    editor.remove(JaoharPreference.STAFF_EMAIL);
                    editor.remove(JaoharPreference.STAFF_ROLE);
                    editor.remove(JaoharPreference.first_name);
                    editor.remove(JaoharPreference.image);
                    editor.remove(JaoharPreference.full_name);
                    editor.remove(JaoharPreference.Role);

                    //clear user data
                    editor.remove(JaoharPreference.IS_LOGGED_IN_VESSELS);
                    editor.remove(JaoharPreference.USER_ID);
                    editor.remove(JaoharPreference.USER_EMAIL);
                    editor.remove(JaoharPreference.USER_ROLE);
                    editor.remove(JaoharPreference.first_name);
                    editor.remove(JaoharPreference.image);
                    editor.remove(JaoharPreference.full_name);
                    editor.remove(JaoharPreference.Role);

                    editor.apply();
                    editor.commit();
                    Intent mIntent = new Intent(mActivity, HomeActivity.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mIntent.putExtra(JaoharConstants.LOGIN, "Home");
                    mActivity.startActivity(mIntent);
                    mActivity.finish();
//                    overridePendingTransitionEnter();

                } else if (JaoharConstants.Is_click_form_Manager == true) {
                    JaoharConstants.BOOLEAN_STAFF_LOGOUT = false;
                    JaoharConstants.BOOLEAN_USER_LOGOUT = false;

                    JaoharConstants.IS_CLICK_FROM_USER_VESSEL = false;
                    JaoharConstants.Is_click_form_Manager = false;
                    SharedPreferences preferences = JaoharPreference.getPreferences(mActivity);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.remove(JaoharPreference.IS_LOGGED_IN_ADMIN);
                    editor.remove(JaoharPreference.ADMIN_ID);
                    editor.remove(JaoharPreference.ADMIN_EMAIL);
                    editor.remove(JaoharPreference.ADMIN_ROLE);
                    editor.remove(JaoharPreference.first_name);
                    editor.remove(JaoharPreference.image);
                    editor.remove(JaoharPreference.full_name);
                    editor.remove(JaoharPreference.Role);

                    editor.commit();
                    Intent mIntent = new Intent(mActivity, HomeActivity.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mIntent.putExtra(JaoharConstants.LOGIN, "StaffHome");
                    mActivity.startActivity(mIntent);
                    mActivity.finish();
//                    overridePendingTransitionEnter();

                } else if (JaoharConstants.IS_CLICK_FROM_USER_VESSEL == true) {
                    JaoharConstants.BOOLEAN_STAFF_LOGOUT = false;
                    JaoharConstants.BOOLEAN_USER_LOGOUT = false;
                    JaoharConstants.IS_CLICK_FROM_USER_VESSEL = false;
                    JaoharConstants.Is_click_form_Manager = false;
                    SharedPreferences preferences = JaoharPreference.getPreferences(mActivity);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.remove(JaoharPreference.IS_LOGGED_IN_VESSELS);
                    editor.remove(JaoharPreference.USER_ID);
                    editor.remove(JaoharPreference.USER_EMAIL);
                    editor.remove(JaoharPreference.USER_ROLE);
                    editor.remove(JaoharPreference.first_name);
                    editor.remove(JaoharPreference.image);
                    editor.remove(JaoharPreference.full_name);
                    editor.remove(JaoharPreference.Role);

                    editor.apply();
                    editor.commit();
                    Intent mIntent = new Intent(mActivity, HomeActivity.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mIntent.putExtra(JaoharConstants.LOGIN, "Home");
                    mActivity.startActivity(mIntent);
                    mActivity.finish();
//                    overridePendingTransitionEnter();
                }
            }
        });
        alertDialog.show();
    }

    private void gettingProfileDATA(String strLoginID) {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        Call<JsonObject> call1 = mApiInterface.getuserProfileRequest(strLoginID);
        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                Log.e(TAG, "*****Response****" + response);
                AlertDialogManager.hideProgressDialog();
                try {
                    JSONObject mJsonObject = new JSONObject(response.body().toString());
                    String strStatus = mJsonObject.getString("status");
                    if (strStatus.equals("1")) {
                        if (!mJsonObject.isNull("data")) {
                            JSONObject mJsonDATA = mJsonObject.getJSONObject("data");

                            if (!mJsonDATA.getString("image").equals("")) {
                                strBase64 = mJsonDATA.getString("image");
                                Glide.with(getActivity()).load(strBase64).into(mImage);
                                Glide.with(getActivity()).load(strBase64).into(HomeActivity.profilePICIMG);
                                JaoharPreference.writeString(getActivity(), JaoharPreference.image, mJsonDATA.getString("image"));
                            } else {
                                mImage.setImageResource(R.drawable.profile);
                            }
                            if (!mJsonDATA.getString("first_name").equals("")) {
                                editFirstNameET.setText(mJsonDATA.getString("first_name"));
                                editFirstNameET.setSelection(mJsonDATA.getString("first_name").length());
                                JaoharPreference.writeString(getActivity(), JaoharPreference.first_name, mJsonDATA.getString("first_name"));
                            }
                            if (!mJsonDATA.getString("last_name").equals("")) {
                                editLastNAMEET.setText(mJsonDATA.getString("last_name"));
                                editLastNAMEET.setSelection(mJsonDATA.getString("last_name").length());
                                HomeActivity.nameTV.setText(mJsonDATA.getString("first_name") + " " + mJsonDATA.getString("last_name"));
                                JaoharPreference.writeString(getActivity(), JaoharPreference.full_name, mJsonDATA.getString("first_name") + " " + mJsonDATA.getString("last_name"));
                            }
                            if (!mJsonDATA.getString("chat_status").equals("")) {
                                if (mJsonDATA.getString("chat_status").equals("Invisible")) {
                                    HomeActivity.selectSpinner.setSelection(2);
                                } else if (mJsonDATA.getString("chat_status").equals("Busy")) {
                                    HomeActivity.selectSpinner.setSelection(1);
                                } else if (mJsonDATA.getString("chat_status").equals("Available")) {
                                    HomeActivity.selectSpinner.setSelection(0);
                                }
                            }

                            if (mJsonDATA.getString("role").equals(JaoharConstants.USER)) {
                                JaoharPreference.writeString(getActivity(), JaoharPreference.USER_EMAIL, mJsonDATA.getString("email"));
                                JaoharPreference.writeString(getActivity(), JaoharPreference.USER_ID, mJsonDATA.getString("id"));
                                JaoharPreference.writeString(getActivity(), JaoharPreference.BLOGUSER_ID, mJsonDATA.getString("id"));
                                JaoharPreference.writeString(getActivity(), JaoharPreference.USER_ROLE, mJsonDATA.getString("role"));
                            } else if (mJsonDATA.getString("role").equals(JaoharConstants.STAFF)) {
                                JaoharPreference.writeString(getActivity(), JaoharPreference.STAFF_EMAIL, mJsonDATA.getString("email"));
                                JaoharPreference.writeString(getActivity(), JaoharPreference.STAFF_ID, mJsonDATA.getString("id"));
                                JaoharPreference.writeString(getActivity(), JaoharPreference.STAFF_ROLE, mJsonDATA.getString("role"));
                            } else if (mJsonDATA.getString("role").equals(JaoharConstants.ADMIN)) {
                                JaoharPreference.writeString(getActivity(), JaoharPreference.ADMIN_EMAIL, mJsonDATA.getString("email"));
                                JaoharPreference.writeString(getActivity(), JaoharPreference.ADMIN_ID, mJsonDATA.getString("id"));
                                JaoharPreference.writeString(getActivity(), JaoharPreference.ADMIN_ROLE, mJsonDATA.getString("role"));
                            } else if (mJsonDATA.getString("role").equals(JaoharConstants.OFFICE)) {
                                JaoharPreference.writeString(getActivity(), JaoharPreference.STAFF_EMAIL, mJsonDATA.getString("email"));
                                JaoharPreference.writeString(getActivity(), JaoharPreference.STAFF_ID, mJsonDATA.getString("id"));
                                JaoharPreference.writeString(getActivity(), JaoharPreference.STAFF_ROLE, mJsonDATA.getString("role"));
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "******error*****" + t.getMessage());
            }
        });
    }

    public void ConfirmDialog() {
        final Dialog deleteConfirmDialog = new Dialog(getActivity());
        deleteConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteConfirmDialog.setContentView(R.layout.dialog_delete_confirmation);
        deleteConfirmDialog.setCanceledOnTouchOutside(true);
        deleteConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView txtMessage = (TextView) deleteConfirmDialog.findViewById(R.id.txtMessage);
        txtMessage.setText(getString(R.string.are_you_sure_want_to_logout));
        TextView txtConfirm = (TextView) deleteConfirmDialog.findViewById(R.id.txtConfirm);
        TextView txtCacel = (TextView) deleteConfirmDialog.findViewById(R.id.txtCacel);

        txtConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
                /*Execute Delete API*/
                if (JaoharConstants.BOOLEAN_USER_LOGOUT == true) {
                    if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                        if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                            logOutAPI(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                        } else {
                            logOutAPI(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
                        }
                    } else {
                        logOutAPI(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                    }
                } else if (JaoharConstants.BOOLEAN_STAFF_LOGOUT == true) {
                    if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_VESSELS, false)) {
                        if (JaoharPreference.readBoolean(getActivity(), JaoharPreference.IS_LOGGED_IN_STAFF, false)) {
                            logOutAPI(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                        } else {
                            logOutAPI(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
                        }
                    } else {
                        logOutAPI(JaoharPreference.readString(getActivity(), JaoharPreference.STAFF_ID, ""));
                    }
                } else if (JaoharConstants.Is_click_form_Manager == true) {
                    logOutAPI(JaoharPreference.readString(getActivity(), JaoharPreference.ADMIN_ID, ""));
                } else if (JaoharConstants.IS_CLICK_FROM_USER_VESSEL == true) {
                    logOutAPI(JaoharPreference.readString(getActivity(), JaoharPreference.USER_ID, ""));
                }
            }
        });

        txtCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteConfirmDialog.dismiss();
            }
        });

        deleteConfirmDialog.show();
    }

    private void setUpCityAdapter() {
        final String[] mStatusArray = getResources().getStringArray(R.array.status_online_array);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, mStatusArray) {
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                    Context mContext = this.getContext();
                    LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    assert vi != null;
                    v = vi.inflate(R.layout.item_spinner, null);
                }

                TextView itemTextView = v.findViewById(R.id.itemTextView);
                itemTextView.setText(mStatusArray[position]);
                return v;
            }

        };
        HomeActivity.selectSpinner.setAdapter(adapter);

        /*Status Spinner Click Listener*/
        HomeActivity.selectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                strType = String.valueOf(parent.getItemAtPosition(position));

                ((TextView) view).setText(strType);

                ((TextView) view).setTextColor(getResources().getColor(R.color.grey)); //Change selected text color

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
