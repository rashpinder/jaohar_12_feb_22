package jaohar.com.jaohar.interfaces;
import jaohar.com.jaohar.models.CompanyData;

/**
 * Created by Dharmani Apps on 3/20/2018.
 */

public interface DeleteInternalNewsInterface {
    public void mDeleteNews(CompanyData mModel);
}
