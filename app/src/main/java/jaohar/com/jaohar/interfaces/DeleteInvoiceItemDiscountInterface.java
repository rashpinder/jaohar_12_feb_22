package jaohar.com.jaohar.interfaces;

import java.util.ArrayList;

import jaohar.com.jaohar.beans.DiscountModel;

/**
 * Created by dharmaniz on 8/6/18.
 */

public interface DeleteInvoiceItemDiscountInterface {
    public void deleteInvoiceItemDiscountInterface(ArrayList<DiscountModel>  mModel,int position);
}
