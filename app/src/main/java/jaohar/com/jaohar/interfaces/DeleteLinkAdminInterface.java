package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.LinkAdminModel;
import jaohar.com.jaohar.models.LinksToHomeData;

/**
 * Created by Dharmani Apps on 3/20/2018.
 */

public interface DeleteLinkAdminInterface {
    public void mDeleteLinkAdmin(LinksToHomeData mModel);
}
