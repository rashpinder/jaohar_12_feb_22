package jaohar.com.jaohar.interfaces;

import jaohar.com.jaohar.beans.InVoicesModel;
import jaohar.com.jaohar.models.AllInvoiceModel;

/**
 * Created by dharmaniz on 7/2/19.
 */

public interface SinglemailInvoiceInterface {
    void mSinglemailInvoice(InVoicesModel mInvoiceModel);
}
