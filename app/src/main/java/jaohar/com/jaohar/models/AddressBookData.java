package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.ArrayList;

public class AddressBookData {

    @SerializedName("all_contact_list")
    @Expose
    private ArrayList<AllContact> allContactList = null;
    @SerializedName("last_page")
    @Expose
    private String lastPage;

    public ArrayList<AllContact> getAllContactList() {
        return allContactList;
    }

    public void setAllContactList(ArrayList<AllContact> allContactList) {
        this.allContactList = allContactList;
    }

    public String getLastPage() {
        return lastPage;
    }

    public void setLastPage(String lastPage) {
        this.lastPage = lastPage;
    }

}
