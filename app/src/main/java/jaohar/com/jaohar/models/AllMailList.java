package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllMailList {

    @SerializedName("list_id")
    @Expose
    private String listId;
    @SerializedName("list_name")
    @Expose
    private String listName;
    @SerializedName("added_by")
    @Expose
    private String addedBy;
    @SerializedName("modification_date")
    @Expose
    private String modificationDate;

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public String getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(String modificationDate) {
        this.modificationDate = modificationDate;
    }

}
