package jaohar.com.jaohar.models;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class ContactByIdModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ContactByIdData data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ContactByIdData getData() {
        return data;
    }

    public void setData(ContactByIdData data) {
        this.data = data;
    }

}
