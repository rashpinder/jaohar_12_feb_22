package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetAllEducationalBlogsModel {
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private Data data;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }
    public class Data {

        @SerializedName("all_blogs")
        @Expose
        private ArrayList<AllBlog> allBlogs = null;
        @SerializedName("last_page")
        @Expose
        private String lastPage;

        public ArrayList<AllBlog> getAllBlogs() {
            return allBlogs;
        }

        public void setAllBlogs(ArrayList<AllBlog> allBlogs) {
            this.allBlogs = allBlogs;
        }

        public String getLastPage() {
            return lastPage;
        }

        public void setLastPage(String lastPage) {
            this.lastPage = lastPage;
        }
        public class AllBlog {

            @SerializedName("id")
            @Expose
            private String id;
            @SerializedName("file_name")
            @Expose
            private String fileName;
            @SerializedName("link")
            @Expose
            private String link;
            @SerializedName("link_title")
            @Expose
            private String linkTitle;
            @SerializedName("link_desc")
            @Expose
            private String linkDesc;
            @SerializedName("link_image")
            @Expose
            private String linkImage;
            @SerializedName("message")
            @Expose
            private String message;
            @SerializedName("disable")
            @Expose
            private String disable;
            @SerializedName("creation_date")
            @Expose
            private String creationDate;
            @SerializedName("is_featured")
            @Expose
            private String isFeatured;
            @SerializedName("sortOrder")
            @Expose
            private String sortOrder;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getFileName() {
                return fileName;
            }

            public void setFileName(String fileName) {
                this.fileName = fileName;
            }

            public String getLink() {
                return link;
            }

            public void setLink(String link) {
                this.link = link;
            }

            public String getLinkTitle() {
                return linkTitle;
            }

            public void setLinkTitle(String linkTitle) {
                this.linkTitle = linkTitle;
            }

            public String getLinkDesc() {
                return linkDesc;
            }

            public void setLinkDesc(String linkDesc) {
                this.linkDesc = linkDesc;
            }

            public String getLinkImage() {
                return linkImage;
            }

            public void setLinkImage(String linkImage) {
                this.linkImage = linkImage;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public String getDisable() {
                return disable;
            }

            public void setDisable(String disable) {
                this.disable = disable;
            }

            public String getCreationDate() {
                return creationDate;
            }

            public void setCreationDate(String creationDate) {
                this.creationDate = creationDate;
            }

            public String getIsFeatured() {
                return isFeatured;
            }

            public void setIsFeatured(String isFeatured) {
                this.isFeatured = isFeatured;
            }

            public String getSortOrder() {
                return sortOrder;
            }

            public void setSortOrder(String sortOrder) {
                this.sortOrder = sortOrder;
            }

        }

    }}





