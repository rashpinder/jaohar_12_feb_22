package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InvoiceDataModel {

    @SerializedName("template_id")
    @Expose
    private String templateId;
    @SerializedName("template_image")
    @Expose
    private String templateImage;
    @SerializedName("heading_color")
    @Expose
    private String headingColor;
    @SerializedName("content_color")
    @Expose
    private String contentColor;
    @SerializedName("table_head_color")
    @Expose
    private String tableHeadColor;
    @SerializedName("table_row_color")
    @Expose
    private String tableRowColor;
    @SerializedName("border_gradient_color")
    @Expose
    private String borderGradientColor;

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getTemplateImage() {
        return templateImage;
    }

    public void setTemplateImage(String templateImage) {
        this.templateImage = templateImage;
    }

    public String getHeadingColor() {
        return headingColor;
    }

    public void setHeadingColor(String headingColor) {
        this.headingColor = headingColor;
    }

    public String getContentColor() {
        return contentColor;
    }

    public void setContentColor(String contentColor) {
        this.contentColor = contentColor;
    }

    public String getTableHeadColor() {
        return tableHeadColor;
    }

    public void setTableHeadColor(String tableHeadColor) {
        this.tableHeadColor = tableHeadColor;
    }

    public String getTableRowColor() {
        return tableRowColor;
    }

    public void setTableRowColor(String tableRowColor) {
        this.tableRowColor = tableRowColor;
    }

    public String getBorderGradientColor() {
        return borderGradientColor;
    }

    public void setBorderGradientColor(String borderGradientColor) {
        this.borderGradientColor = borderGradientColor;
    }

}
