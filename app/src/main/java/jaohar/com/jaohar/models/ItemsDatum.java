package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemsDatum {

    @SerializedName("item_id")
    @Expose
    private String itemId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("item_serial_no")
    @Expose
    private String itemSerialNo;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("arraylistDiscount")
    @Expose
    private String arraylistDiscount;
    @SerializedName("TotalunitPrice")
    @Expose
    private String totalunitPrice;
    @SerializedName("TotalAmount")
    @Expose
    private Integer totalAmount;
    @SerializedName("TotalAmountUnit")
    @Expose
    private Integer totalAmountUnit;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getItemSerialNo() {
        return itemSerialNo;
    }

    public void setItemSerialNo(String itemSerialNo) {
        this.itemSerialNo = itemSerialNo;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getArraylistDiscount() {
        return arraylistDiscount;
    }

    public void setArraylistDiscount(String arraylistDiscount) {
        this.arraylistDiscount = arraylistDiscount;
    }

    public String getTotalunitPrice() {
        return totalunitPrice;
    }

    public void setTotalunitPrice(String totalunitPrice) {
        this.totalunitPrice = totalunitPrice;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getTotalAmountUnit() {
        return totalAmountUnit;
    }

    public void setTotalAmountUnit(Integer totalAmountUnit) {
        this.totalAmountUnit = totalAmountUnit;
    }

}