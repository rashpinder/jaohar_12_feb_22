package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MailData {

    @SerializedName("all_mail_lists")
    @Expose
    private ArrayList<AllMailList> allMailLists = null;
    @SerializedName("last_page")
    @Expose
    private String lastPage;

    public ArrayList<AllMailList> getAllMailLists() {
        return allMailLists;
    }

    public void setAllMailLists(ArrayList<AllMailList> allMailLists) {
        this.allMailLists = allMailLists;
    }

    public String getLastPage() {
        return lastPage;
    }

    public void setLastPage(String lastPage) {
        this.lastPage = lastPage;
    }

}
