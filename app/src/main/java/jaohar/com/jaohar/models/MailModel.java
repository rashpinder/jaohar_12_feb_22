package jaohar.com.jaohar.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MailModel {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("email")
    @Expose
    private String email;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
