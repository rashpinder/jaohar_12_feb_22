package jaohar.com.jaohar.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Signdata {

    @SerializedName("sign_id")
    @Expose
    private String signId;
    @SerializedName("sign_name")
    @Expose
    private String signName;
    @SerializedName("sign_image")
    @Expose
    private String signImage;
    @SerializedName("added_by")
    @Expose
    private String addedBy;
    @SerializedName("modification_date")
    @Expose
    private String modificationDate;
    @SerializedName("enable")
    @Expose
    private String enable;

    public String getSignId() {
        return signId;
    }

    public void setSignId(String signId) {
        this.signId = signId;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getSignImage() {
        return signImage;
    }

    public void setSignImage(String signImage) {
        this.signImage = signImage;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public String getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(String modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

    @Override
    public String toString() {
        return signName ;
    }
}
