package jaohar.com.jaohar.models.chatmodels;

import com.google.gson.annotations.SerializedName;

public class AddNewMessageModel{

	@SerializedName("append_data")
	private AllChatsItem appendData;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("update")
	private boolean update;

	@SerializedName("id")
	private int id;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public AllChatsItem getAppendData(){
		return appendData;
	}

	public String getUserId(){
		return userId;
	}

	public boolean isUpdate(){
		return update;
	}

	public int getId(){
		return id;
	}

	public String getMessage(){
		return message;
	}

	public int getStatus(){
		return status;
	}
}