package jaohar.com.jaohar.models.chatmodels;

import com.google.gson.annotations.SerializedName;

public class DeleteMessageModel{

	@SerializedName("append_data")
	private AppendData appendData;

	@SerializedName("update")
	private boolean update;

	@SerializedName("id")
	private String id;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public AppendData getAppendData(){
		return appendData;
	}

	public boolean isUpdate(){
		return update;
	}

	public String getId(){
		return id;
	}

	public String getMessage(){
		return message;
	}

	public int getStatus(){
		return status;
	}
}