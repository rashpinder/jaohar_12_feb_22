package jaohar.com.jaohar.models.chatmodels.chatusers;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data{

	@SerializedName("last_page")
	private String lastPage;

	@SerializedName("all_users")
	private List<AllUsersItem> allUsers;

	public void setLastPage(String lastPage){
		this.lastPage = lastPage;
	}

	public String getLastPage(){
		return lastPage;
	}

	public void setAllUsers(List<AllUsersItem> allUsers){
		this.allUsers = allUsers;
	}

	public List<AllUsersItem> getAllUsers(){
		return allUsers;
	}
}