package jaohar.com.jaohar.models.forummodels;

import com.google.gson.annotations.SerializedName;

public class AppendFullScreenData{

	@SerializedName("message_id")
	private String messageId;

	@SerializedName("message_type")
	private String messageType;

	public String getMessageId(){
		return messageId;
	}

	public String getMessageType(){
		return messageType;
	}
}