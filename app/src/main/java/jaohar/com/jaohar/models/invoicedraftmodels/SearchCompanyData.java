package jaohar.com.jaohar.models.invoicedraftmodels;

import com.google.gson.annotations.SerializedName;

public class SearchCompanyData{

	@SerializedName("added_by")
	private String addedBy;

	@SerializedName("Address4")
	private String address4;

	@SerializedName("enable")
	private String enable;

	@SerializedName("company_name")
	private String companyName;

	@SerializedName("Address5")
	private String address5;

	@SerializedName("modification_date")
	private String modificationDate;

	@SerializedName("Address2")
	private String address2;

	@SerializedName("Address3")
	private String address3;

	@SerializedName("id")
	private String id;

	@SerializedName("Address1")
	private String address1;

	public void setAddedBy(String addedBy){
		this.addedBy = addedBy;
	}

	public String getAddedBy(){
		return addedBy;
	}

	public void setAddress4(String address4){
		this.address4 = address4;
	}

	public String getAddress4(){
		return address4;
	}

	public void setEnable(String enable){
		this.enable = enable;
	}

	public String getEnable(){
		return enable;
	}

	public void setCompanyName(String companyName){
		this.companyName = companyName;
	}

	public String getCompanyName(){
		return companyName;
	}

	public void setAddress5(String address5){
		this.address5 = address5;
	}

	public String getAddress5(){
		return address5;
	}

	public void setModificationDate(String modificationDate){
		this.modificationDate = modificationDate;
	}

	public String getModificationDate(){
		return modificationDate;
	}

	public void setAddress2(String address2){
		this.address2 = address2;
	}

	public String getAddress2(){
		return address2;
	}

	public void setAddress3(String address3){
		this.address3 = address3;
	}

	public String getAddress3(){
		return address3;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setAddress1(String address1){
		this.address1 = address1;
	}

	public String getAddress1(){
		return address1;
	}
}