package jaohar.com.jaohar.models.invoicemodels;

import com.google.gson.annotations.SerializedName;

public class BankData{

	@SerializedName("iban_gbp")
	private String ibanGbp;

	@SerializedName("beneficiary_country")
	private String beneficiaryCountry;

	@SerializedName("address2")
	private String address2;

	@SerializedName("address1")
	private String address1;

	@SerializedName("iban_ron")
	private String ibanRon;

	@SerializedName("beneficiary_add")
	private String beneficiaryAdd;

	@SerializedName("iban_usd")
	private String ibanUsd;

	@SerializedName("beneficiary")
	private String beneficiary;

	@SerializedName("added_by")
	private String addedBy;

	@SerializedName("bank_id")
	private String bankId;

	@SerializedName("enable")
	private String enable;

	@SerializedName("iban_eur")
	private String ibanEur;

	@SerializedName("modification_date")
	private String modificationDate;

	@SerializedName("bank_name")
	private String bankName;

	@SerializedName("swift")
	private String swift;

	public void setIbanGbp(String ibanGbp){
		this.ibanGbp = ibanGbp;
	}

	public String getIbanGbp(){
		return ibanGbp;
	}

	public void setBeneficiaryCountry(String beneficiaryCountry){
		this.beneficiaryCountry = beneficiaryCountry;
	}

	public String getBeneficiaryCountry(){
		return beneficiaryCountry;
	}

	public void setAddress2(String address2){
		this.address2 = address2;
	}

	public String getAddress2(){
		return address2;
	}

	public void setAddress1(String address1){
		this.address1 = address1;
	}

	public String getAddress1(){
		return address1;
	}

	public void setIbanRon(String ibanRon){
		this.ibanRon = ibanRon;
	}

	public String getIbanRon(){
		return ibanRon;
	}

	public void setBeneficiaryAdd(String beneficiaryAdd){
		this.beneficiaryAdd = beneficiaryAdd;
	}

	public String getBeneficiaryAdd(){
		return beneficiaryAdd;
	}

	public void setIbanUsd(String ibanUsd){
		this.ibanUsd = ibanUsd;
	}

	public String getIbanUsd(){
		return ibanUsd;
	}

	public void setBeneficiary(String beneficiary){
		this.beneficiary = beneficiary;
	}

	public String getBeneficiary(){
		return beneficiary;
	}

	public void setAddedBy(String addedBy){
		this.addedBy = addedBy;
	}

	public String getAddedBy(){
		return addedBy;
	}

	public void setBankId(String bankId){
		this.bankId = bankId;
	}

	public String getBankId(){
		return bankId;
	}

	public void setEnable(String enable){
		this.enable = enable;
	}

	public String getEnable(){
		return enable;
	}

	public void setIbanEur(String ibanEur){
		this.ibanEur = ibanEur;
	}

	public String getIbanEur(){
		return ibanEur;
	}

	public void setModificationDate(String modificationDate){
		this.modificationDate = modificationDate;
	}

	public String getModificationDate(){
		return modificationDate;
	}

	public void setBankName(String bankName){
		this.bankName = bankName;
	}

	public String getBankName(){
		return bankName;
	}

	public void setSwift(String swift){
		this.swift = swift;
	}

	public String getSwift(){
		return swift;
	}
}