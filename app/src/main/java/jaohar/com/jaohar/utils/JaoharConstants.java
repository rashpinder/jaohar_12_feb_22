package jaohar.com.jaohar.utils;

import java.util.ArrayList;

import jaohar.com.jaohar.beans.MailingListSendingModel;
import jaohar.com.jaohar.models.MailModel;

/**
 * Created by Admin on 14/08/2017.
 **/

public class JaoharConstants {
    // Splash Time Out
    public static final int SPLASH_TIME_OUT = 1500;

    // WebServices
//  public static final String SERVER_URL = "http://dharmani.com/JaoharWebServices/";
//  public static final String SERVER_URL = "http://root.jaohar.net/Staging/JaoharWebServices/";

    // MAIN URL  live by rash
//    public static final String SERVER_URL = "https://root.jaohar.com/JaoharWebServicesNew/";
    // TestingURL
//    public static final String SERVER_URL = "https://root.jaohar.com/Staging/JaoharWebServicesNew/";

    // New Url change by Raman Dhiman(10-11-2021::::::::::::)
    /* dev.jaohar.com */
    public static final String SERVER_URL = "https://root.jaohar.com/Development/JaoharWebServicesNew/";

    // Working Socket URL
    public static final String SocketURL = "https://jaohar-uk.herokuapp.com";

    // API Section
    public static final String ADVANCED_SEARCH = SERVER_URL + "AdvancedSearch.php";
    public static final String ADVANCED_SEARCH_INVOICE = SERVER_URL + "AdvancedSearchInvoice.php";
    public static final String ADD_VESSELS = SERVER_URL + "AddVesselAndroid.php";
    public static final String EDIT_VESSELS = SERVER_URL + "EditVesselAndroid.php";
    public static final String EDIT_VESSELS_DOCUMENT = SERVER_URL + "EditFiveDocumentsAndroid.php";
    public static final String DELETE_VESSELS = SERVER_URL + "DeleteVessels.php?id=";//id=
    public static final String LOGIN_API = SERVER_URL + "login.php";
    public static final String LOGING_STAFF_API = SERVER_URL + "login_staff_android.php";

    public static final String LOGOUT_API = SERVER_URL + "logout.php";//id,token
    public static final String GetUserProfile_API = SERVER_URL + "GetUserProfile.php";//id
    public static final String EditUserProfile_API = SERVER_URL + "EditUserProfile.php";//id
    public static final String AddDeviceToken_API = SERVER_URL + "AddDeviceToken.php";//devicetype,token
    public static final String ADMIN_LOGOUT_API = SERVER_URL + "LogoutAdmin.php";
    public static final String SIGN_UP_API = SERVER_URL + "signup_android.php";
    public static final String WebLogin_API = SERVER_URL + "WebLogin.php";
    //  public static final String SIGN_UP_API = SERVER_URL + "signup.php";
    public static final String ADD_INVOICE = SERVER_URL + "AddInvoice.php";
    public static final String GET_ALL_INVOICE = SERVER_URL + "Get_all_invoice.php";
    public static final String GET_ALL_INVOICE_PAGINATION = SERVER_URL + "Get_all_invoice_pagination.php";
    public static final String Get_All_Universal_Invoice = SERVER_URL + "GetAllUniversalInvoice.php";
    public static final String Get_All_Universal_Invoice_Owner = SERVER_URL + "GetAllUniversalInvoiceOwner.php";

    // Forum APIS
    public static final String GetAllStaffForums = SERVER_URL + "GetAllStaffForums.php";
    public static final String GetAllStaffForumMessages = SERVER_URL + "GetAllStaffForumMessages.php";
    public static final String CheckStaffForumUpdation = SERVER_URL + "CheckStaffForumUpdation.php";
    public static final String AddStaffForumMessage = SERVER_URL + "AddStaffForumMessage.php";
    public static final String AddStaffForumMessageAndroid = SERVER_URL + "AddStaffForumMessageAndroid.php";
    public static final String AddStaffForumMessageNew = SERVER_URL + "AddStaffForumMessageNew.php";
    public static final String EditStaffForumMessage = SERVER_URL + "EditStaffForumMessage.php";
    public static final String EditStaffForumMessageNew = SERVER_URL + "EditStaffForumMessageNew.php";
    public static final String EditStaffForumMessageAndroid = SERVER_URL + "EditStaffForumMessageAndroid.php";
    public static final String AddStaffForum = SERVER_URL + "AddStaffForum.php";
    public static final String DeleteStaffForumMessage = SERVER_URL + "DeleteStaffForumMessage.php";
    public static final String UpdateStaffForumHeading = SERVER_URL + "UpdateStaffForumHeading.php";
    public static final String GetUserListStaffForum = SERVER_URL + "GetUserListStaffForum_Staff.php";
    public static final String GetUserListForum = SERVER_URL + "GetUserListForum_Staff.php";
    public static final String GetUserListForumAdmin = SERVER_URL + "GetUserListForum.php";
    // Admin Forum APIS
    public static final String CloseStaffForum = SERVER_URL + "CloseStaffForum.php";
    public static final String DeleteStaffForum = SERVER_URL + "DeleteStaffForum.php";
    public static final String GetAllClosedStaffForums = SERVER_URL + "GetAllClosedStaffForums.php";
    // Admin UK Office Forums
    public static final String GetAllForums = SERVER_URL + "GetAllForums.php";
    public static final String AddForum = SERVER_URL + "AddForum.php";
    public static final String DeleteForum = SERVER_URL + "DeleteForum.php";
    public static final String CloseForum = SERVER_URL + "CloseForum.php";
    public static final String GetAllClosedForums = SERVER_URL + "GetAllClosedForums.php";
    public static final String GetAllForumMessages = SERVER_URL + "GetAllForumMessages.php";
    public static final String AddForumMessageAndroid = SERVER_URL + "AddForumMessageAndroid.php";
    public static final String EditForumMessageAndroid = SERVER_URL + "EditForumMessageAndroid.php";
    public static final String DeleteForumMessage = SERVER_URL + "DeleteForumMessage.php";
    public static final String UpdateForumHeading = SERVER_URL + "UpdateForumHeading.php";
    public static final String CheckForumUpdation = SERVER_URL + "CheckForumUpdation.php";
    // New UK Forums
    public static final String AddForumMessageNew = SERVER_URL + "AddForumMessageNew.php";
    public static final String EditForumMessageNew = SERVER_URL + "EditForumMessageNew.php";

    // Universal Invoice
    public static final String SimpleSearchUniversalInvoice = SERVER_URL + "SimpleSearchUniversalInvoice.php";
    public static final String GetAllInvoiceTemplates = SERVER_URL + "GetAllInvoiceTemplates.php";
    public static final String GetAllOwners = SERVER_URL + "GetAllOwners.php";
    public static final String GetOwnerById = SERVER_URL + "GetOwnerById.php";
    public static final String DeleteOwner = SERVER_URL + "DeleteOwner.php";
    public static final String Get_All_Notifications = SERVER_URL + "GetAllNotifications.php";
    public static final String Delete_Mass_Push = SERVER_URL + "DeleteMassPush.php";
    public static final String Simple_Search_Invoice = SERVER_URL + "SimpleSearchInvoice.php";
    public static final String Get_All_Admin_And_Roles_By_Search = SERVER_URL + "GetAllAdminAndRolesBySearch.php";
    public static final String GET_VESSELS_DETAILS = SERVER_URL + "GetSingleVesselById.php";//input_vessel_id
    public static final String GET_ALL_ADMIN_ROLE = SERVER_URL + "GetAllAdminAndRoles.php";
    public static final String GET_ALL_NEW_REQUEST = SERVER_URL + "GetAllNewRequests.php";
    public static final String UPLOAD_DOCUMENTS = SERVER_URL + "UploadFiveDocuments.php";
    public static final String ENABLE_ACCOUNT = SERVER_URL + "EnableAccount.php";//user_id
    public static final String DISABLE_ACCOUNT = SERVER_URL + "DisableAccount.php";//user_id
    public static final String EDIT_ROLE = SERVER_URL + "EditRole.php";//user_id,new_role
    public static final String DELETE_USER = SERVER_URL + "DeleteUser.php";//user_id
    public static final String RESET_PASSWORD = SERVER_URL + "ResetPassword.php";//user_id
    public static final String ACCEPT_MEMBER_REQUEST = SERVER_URL + "AcceptMemberRequest.php";//user_id
    public static final String REJECT_MEMBER_REQUEST = SERVER_URL + "RejectMemberRequest.php";//user_id
    public static final String SUPPORT_API = SERVER_URL + "Support.php";
    public static final String FORGOT_PASSWORD_API = SERVER_URL + "ForgotPassword.php";//http://dharmani.com/JaoharWebServices/ForgotPassword.php?email=jaohar@jaohar.com
    public static final String GET_ALL_COMPANIES = SERVER_URL + "GetAllCompanies.php";//107
    public static final String ADD_COMPANY = SERVER_URL + "AddCompany.php";//107
    public static final String GET_ALL_SIGNS = SERVER_URL + "GetAllSigns.php";//107
    public static final String ADD_SIGNS = SERVER_URL + "AddSignAndroid.php";//107
    public static final String GET_ALL_STAMPS = SERVER_URL + "GetAllStamps.php";//107
    public static final String ADD_STAMPS = SERVER_URL + "AddStampAndroid.php";//107
    public static final String DELETE_SIGN = SERVER_URL + "DeleteSign.php";//107
    public static final String EDIT_SIGN = SERVER_URL + "EditSignAndroid.php";//107
    public static final String DELETE_STAMP = SERVER_URL + "DeleteStamp.php";//107
    public static final String EDIT_STAMP = SERVER_URL + "EditStampAndroid.php";//107
    public static final String DELETE_COMPANY = SERVER_URL + "DeleteCompany.php";//107
    public static final String EDIT_COMPANY = SERVER_URL + "EditCompany.php";//107
    public static final String ADD_BANK_DETAILS = SERVER_URL + "AddBankDetails.php";//107
    public static final String GET_ALL_BANK_DETAILS = SERVER_URL + "GetAllBanks.php";//107
    public static final String EDIT_BANK_DETAILS = SERVER_URL + "EditBankDetails.php";//107
    public static final String DELETE_BANK_DETAILS = SERVER_URL + "DeleteBankDetails.php";//107
    public static final String ADD_INVOICE_ITEM = SERVER_URL + "AddInvoiceItems.php";//107
    public static final String DELETE_INVOICE_ITEM = SERVER_URL + "DeleteInvoiceItem.php";//107
    public static final String EDIT_INVOICE_ITEM = SERVER_URL + "EditInvoiceItems.php";//107
    public static final String DELETE_INVOICE = SERVER_URL + "DeleteInvoice.php";//107
    public static final String DeleteAddressBookContact = SERVER_URL + "DeleteAddressBookContact.php";//107
    public static final String GETTING_ALL_VESSELS_INVOICE = SERVER_URL + "GetAllInvoiceVessels.php";//107
    public static final String GETTING_ALL_DATAFOR_INVOICE_EDIT = SERVER_URL + "GetAllDataForInvoiceEdit.php";//107
    public static final String EDIT_INVOICE = SERVER_URL + "EditInvoice.php";//107
    public static final String Get_Mass_Push_By_Id = SERVER_URL + "GetMassPushById.php";

    // INVOICE VESSEL
    public static final String ADD_INVOICE_VESSEL = SERVER_URL + "AddInvoiceVessel.php";//107
    public static final String DELETE_INVOICE_VESSEL = SERVER_URL + "DeleteInvoiceVessel.php";//107
    public static final String EDIT_INVOICE_VESSEL = SERVER_URL + "EditInvoiceVessel.php";//107

    // CURRECY
    public static final String GET_ALL_CURRENCIES = SERVER_URL + "GetAllCurrencies.php";//217
    public static final String ADD_CURRENCY = SERVER_URL + "AddCurrency.php";//217AD
    public static final String EDIT_CURRENCY = SERVER_URL + "EditCurrency.php";//217
    public static final String DELETE_CURRENCY = SERVER_URL + "DeleteCurrency.php";//217

    // Payment
    public static final String ADD_INVOICE_PAYMENT = SERVER_URL + "AddInvoicePayment.php";//217

    // Dynamic Addiction
    public static final String GET_ALL_STATUS = SERVER_URL + "GetStatus.php";//217
    public static final String ADD_STATUS = SERVER_URL + "AddStatus.php";//217
    public static final String EDIT_STATUS = SERVER_URL + "EditStatus.php";//217
    public static final String DELETE_STATUS = SERVER_URL + "DeleteStatus.php";//217
    public static final String GET_ALL_VESSEL_TYPES = SERVER_URL + "GetVessel_Types.php";
    public static final String ADD_VESSEL_TYPES = SERVER_URL + "AddVessel_Types.php";
    public static final String EDIT_VESSEL_TYPES = SERVER_URL + "EditVessel_Types.php";
    public static final String DELETE_VESSEL_TYPES = SERVER_URL + "DeleteVessel_Types.php";
    public static final String GET_ALL_NEWS = SERVER_URL + "GetAllNews.php";
    public static final String Get_Invoice_Mails = SERVER_URL + "GetInvoiceMails.php";

    public static final String ADD_NEWS = SERVER_URL + "AddNews.php";
    public static final String Add_Owner = SERVER_URL + "AddOwner.php";
    public static final String Edit_Owner = SERVER_URL + "EditOwner.php";
    public static final String DELETE_NEWS = SERVER_URL + "DeleteNews.php";
    public static final String EDIT_NEWS = SERVER_URL + "EditNews.php";
    public static final String Get_Latest_Static_News = SERVER_URL + "GetLatestStaticNews.php";

    public static final String Get_Latest_Static_Internal_News = SERVER_URL + "GetLatestStaticInternalNews.php";
    public static final String ALL_INTERNAL_NEWS = SERVER_URL + "GetInternalNews.php";
    public static final String Get_Company_News = SERVER_URL + "GetCompanyNews.php";
    public static final String ADD_INTERNAL_NEWS = SERVER_URL + "AddInternalNews.php";
    public static final String Add_Company_News = SERVER_URL + "AddCompanyNews.php";
    public static final String EDIT_INTERNAL_NEWS = SERVER_URL + "EditInternalNews.php";
    public static final String Edit_Company_News = SERVER_URL + "EditCompanyNews.php";
    public static final String DELETE_INTERNAL_NEWS = SERVER_URL + "DeleteInternalNews.php";

    public static final String ADD_LINK_ADMIN = SERVER_URL + "AddlinksToHome.php";
    public static final String EDIT_LINK_ADMIN = SERVER_URL + "EditlinksToHome.php";
    public static final String ALL_LINK_ADMIN = SERVER_URL + "GetlinksToHome.php";
    public static final String DELETE_LINK_ADMIN = SERVER_URL + "DeletelinkstoHome.php";

    public static final String ADD_LINK_STAFF = SERVER_URL + "Addlinks_To_Home_for_Staff.php";
    public static final String EDIT_LINK_STAFF = SERVER_URL + "Editlinks_To_Home_for_Staff.php";
    public static final String ALL_LINK_STAFF = SERVER_URL + "Getlinks_To_Home_for_Staff.php";
    public static final String DELETE_LINK_STAFF = SERVER_URL + "Deletelinks_To_Home_for_Staff.php";
    public static final String Get_Vessel_Class = SERVER_URL + "GetVessel_Class.php";
    public static final String Get_Invoice_Number = SERVER_URL + "GetInvoiceNo.php";
    public static final String Update_Thirty_Images = SERVER_URL + "UploadThirtyImages.php";
    public static final String Edit_Thirty_Images_Android = SERVER_URL + "EditThirtyImagesAndroid.php";
    public static final String Delete_Vessel_Photo_Doc = SERVER_URL + "DeleteVesselPhotoDoc.php";
    public static final String Update_Invoice_Pdf = SERVER_URL + "UpdateInvoicePdf.php";
    public static final String Edit_Mass_Push = SERVER_URL + "EditMassPush.php";
    public static final String Add_Mass_Push = SERVER_URL + "AddMassPush.php";
    public static final String GetAllMailLists = SERVER_URL + "GetAllMailLists.php";
    public static final String GetMailLists = SERVER_URL + "GetMailLists.php";
    public static final String AddMailList = SERVER_URL + "AddMailList.php";
    public static final String EditMailList = SERVER_URL + "EditMailList.php";
    public static final String DeleteMailList = SERVER_URL + "DeleteMailList.php";
    public static final String GetAllMailListContacts = SERVER_URL + "GetAllMailListContacts.php";
    public static final String AddMailListContact = SERVER_URL + "AddMailListContact.php";
    public static final String AddAddressBookContact = SERVER_URL + "AddAddressBookContact.php";
    public static final String DeleteMailListContact = SERVER_URL + "DeleteMailListContact.php";
    public static final String EditMailListContact = SERVER_URL + "EditMailListContact.php";
    public static final String EditAddressBookContact = SERVER_URL + "EditAddressBookContact.php";
    public static final String GetAddressBookById = SERVER_URL + "GetAddressBookById.php";
    public static final String Single_Vessals_URL = SERVER_URL + "MailVesselDetail.php";
    public static final String Multi_Vessals_URL = SERVER_URL + "MailMultipleVessels.php";
    public static final String Mail_To_List = SERVER_URL + "MailToList.php";
    public static final String Mail_To_List_Shell = SERVER_URL + "MailToList_Shell.php";
    public static final String MailVesselToList = SERVER_URL + "MailVesselToList.php";
    public static final String MailVesselToListShell = SERVER_URL + "MailVesselToListShell.php";

    public static final String Mail_To_Multiple_List = SERVER_URL + "MailToMultipleList.php";
    public static final String Mail_Send = SERVER_URL + "MailSend.php";
    public static final String Get_All_Address_Book_Contacts = SERVER_URL + "GetAllAddressBookContacts.php";
    public static final String Get_All_Address_Book_Mails = SERVER_URL + "GetAllAddressBookMails.php";
    public static final String Mail_Single_Invoice = SERVER_URL + "MailSingleInvoice.php";
    public static final String Mail_Multiple_Invoices = SERVER_URL + "MailMultipleInvoices.php";
    public static final String Mail_All_Invoices = SERVER_URL + "MailAllInvoices.php";
    public static final String Mail_Single_Universal_Invoice = SERVER_URL + "MailSingleUniversalInvoice.php";

    public static final String SearchNewRequest = SERVER_URL + "SearchNewRequest.php";

    // Modules API
    public static final String AddModule = SERVER_URL + "AddModule.php";
    public static final String EditModule = SERVER_URL + "EditModule.php";
    public static final String ControlModuleAccess = SERVER_URL + "ControlModuleAccess.php";
    public static final String ControlModuleDisability = SERVER_URL + "ControlModuleDisability.php";
    public static final String GetAllModules = SERVER_URL + "GetAllModules.php";
    public static final String GetAllModulesAdmin = SERVER_URL + "GetAllModulesAdmin.php";
    public static final String DeleteModule = SERVER_URL + "DeleteModule.php";
    public static final String GetModuleUsers = SERVER_URL + "GetModuleUsers.php";

    public static final String GetAllDynamicLinks = SERVER_URL + "GetAllDynamicLinks.php";

    // Fragment Tags
    public static final String HOME_FRAGMENT = "home_fragment";
    public static final String ABOUT_FRAGMENT = "about_fragment";
    public static final String CONTACTUS_FRAGMENT = "contact_us_fragment";
    public static final String OUR_HISTORY_FRAGMENT = "our_history_fragment";
    public static final String OUR_OFFICES_FRAGMENT = "our_offices_fragment";
    public static final String SERVICES_FRAGMENT = "services_fragment";
    public static final String STAFF_FRAGMENT = "staff_fragment";
    public static final String BLOG_FRAGMENT = "blog_fragment";
    public static final String VESSELES_FOR_SALE_FRAGMENT = "vesseles_for_sale_fragment";
    public static final String LOGIN_FRAGMENT = "login_fragment";
    public static final String SIGNUP_FRAGMENT = "sign_fragment";
    public static final String OPENLINK_FRAGMENT = "openlink_fragment";
    public static final String NotificationFragment = "NotificationFragment";
    public static final String FavoriteVessels = "FavoriteVessels";
    public static final String SUPPORT_FRAGMENT = "support_fragment";
    public static final String VESSEL_IN_TRASH = "Vessel_In_Trash";
    public static final String Manage_Utilities_Fragment = "Manage_Utilities_Fragment";
    public static final String Admin_Staff_Forum_Fragment = "Admin_Staff_Forum_Fragment";
    public static final String UK_forum_Admin_Fragment = "UK_forum_Admin_Fragment";

    // Links
    public static final String FACEBOOK_PAGE_LINK = "https://www.facebook.com/jaoharuklimited/";
    public static final String TWITTER_PAGE_LINK = "https://twitter.com/jaoharuk";
    public static final String INSTAGRAM_PAGE_LINK = "https://www.instagram.com/Jaohar_UK_Limited/";

    public static String DEVICE_TOKEN = "device_token";
    public static String GET_ALL_VESSELES_111 = SERVER_URL + "GetAllVessels.php";
    public static String GET_ALL_VESSELES = SERVER_URL + "GetAllVesselsPagination.php";

    public static String ROLE_SEARCH_API = SERVER_URL + "SearchVessel.php";

    public static String UpdateOnlineStatus = SERVER_URL + "UpdateOnlineStatus.php";

    /*
     * Blog API
     * */
    public static String GetAllBlogs = SERVER_URL + "GetAllBlogs.php";
    public static String GetAllBlogCommentFront = SERVER_URL + "GetAllBlogCommentFront.php";
    public static String AddBlogComment = SERVER_URL + "AddBlogComment.php";
    public static String DeleteBlogComment = SERVER_URL + "DeleteBlogComment.php";
    public static String GetAllBlogCategories = SERVER_URL + "GetAllBlogCategories.php";
    public static String DeleteBlogCategory = SERVER_URL + "DeleteBlogCategory.php";
    public static String EditBlogCategory = SERVER_URL + "EditBlogCategory.php";
    public static String AddBlogCategory = SERVER_URL + "AddBlogCategory.php";
    public static String DeleteBlog = SERVER_URL + "DeleteBlog.php";
    public static String EditBlog = SERVER_URL + "EditBlog.php";
    public static String AddBlog = SERVER_URL + "AddBlog.php";

    /*
     * Notifications API
     * */
    public static String GetNotificationList = SERVER_URL + "GetNotificationList.php";

    /*
     * Trash Module API
     * */
    public static String Get_All_Vessels_Trash = SERVER_URL + "GetAllVesselsTrash.php";
    public static String Get_All_Invoice_Trash = SERVER_URL + "GetAllInvoiceTrash.php";
    public static String GetAllUniversalInvoiceTrash = SERVER_URL + "GetAllUniversalInvoiceTrash.php";
    public static String GetAllCompaniesTrash = SERVER_URL + "GetAllCompaniesTrash.php";
    public static String GetAllInvoiceVesselsTrash = SERVER_URL + "GetAllInvoiceVesselsTrash.php";
    public static String GetAllCurrenciesTrash = SERVER_URL + "GetAllCurrenciesTrash.php";
    public static String Empty_Vessel_Trash = SERVER_URL + "EmptyVesselTrash.php";
    public static String EmptyInvoiceTrash = SERVER_URL + "EmptyInvoiceTrash.php";
    public static String EmptyCompanyTrash = SERVER_URL + "EmptyCompanyTrash.php";
    public static String EmptyBankTrash = SERVER_URL + "EmptyBankTrash.php";
    public static String EmptyInvoiceVesselTrash = SERVER_URL + "EmptyInvoiceVesselTrash.php";
    public static String EmptyCurrencyTrash = SERVER_URL + "EmptyCurrencyTrash.php";
    public static String EmptyUnivesalInvoiceTrash = SERVER_URL + "EmptyUniversalInvoiceTrash.php";
    public static String Recover_Single_Vessel_Trash = SERVER_URL + "RecoverSingleVesselTrash.php";
    public static String RecoverSingleInvoiceTrash = SERVER_URL + "RecoverSingleInvoiceTrash.php";
    public static String RecoverSingleUniversalInvoiceTrash = SERVER_URL + "RecoverSingleUniversalInvoiceTrash.php";
    public static String Delete_Single_Vessel_Trash = SERVER_URL + "DeleteSingleVesselTrash.php";
    public static String DeleteSingleInvoiceTrash = SERVER_URL + "DeleteSingleInvoiceTrash.php";
    public static String DeleteSingleCompanyTrash = SERVER_URL + "DeleteSingleCompanyTrash.php";
    public static String DeleteSingleBankTrash = SERVER_URL + "DeleteSingleBankTrash.php";
    public static String DeleteSingleInvoiceVesselTrash = SERVER_URL + "DeleteSingleInvoiceVesselTrash.php";
    public static String DeleteSingleCurrencyTrash = SERVER_URL + "DeleteSingleCurrencyTrash.php";
    public static String RecoverSingleCompanyTrash = SERVER_URL + "RecoverSingleCompanyTrash.php";
    public static String RecoverSingleBankTrash = SERVER_URL + "RecoverSingleBankTrash.php";
    public static String RecoverSingleInvoiceVesselTrash = SERVER_URL + "RecoverSingleInvoiceVesselTrash.php";
    public static String RecoverSingleCurrencyTrash = SERVER_URL + "RecoverSingleCurrencyTrash.php";
    public static String DeleteSingleUniversalInvoiceTrash = SERVER_URL + "DeleteSingleUniversalInvoiceTrash.php";
    public static String RecoverSelectedVesselTrash = SERVER_URL + "RecoverSelectedVesselTrash.php";
    public static String DeleteSelectedVesselTrash = SERVER_URL + "DeleteSelectedVesselTrash.php";
    public static String RecoverSelectedInvoiceTrash = SERVER_URL + "RecoverSelectedInvoiceTrash.php";
    public static String RecoverSelectedCurrencyTrash = SERVER_URL + "RecoverSelectedCurrencyTrash.php";
    public static String RecoverSelectedCompanyTrash = SERVER_URL + "RecoverSelectedCompanyTrash.php";
    public static String RecoverSelectedBankTrash = SERVER_URL + "RecoverSelectedBankTrash.php";
    public static String RecoverSelectedInvoiceVesselTrash = SERVER_URL + "RecoverSelectedInvoiceVesselTrash.php";
    public static String DeleteSelectedCompanyTrash = SERVER_URL + "DeleteSelectedCompanyTrash.php";
    public static String DeleteSelectedBankTrash = SERVER_URL + "DeleteSelectedBankTrash.php";
    public static String DeleteSelectedInvoiceVesselTrash = SERVER_URL + "DeleteSelectedInvoiceVesselTrash.php";
    public static String DeleteSelectedCurrencyTrash = SERVER_URL + "DeleteSelectedCurrencyTrash.php";
    public static String RecoverSelectedUniversalInvoiceTrash = SERVER_URL + "RecoverSelectedUniversalInvoiceTrash.php";
    public static String DeleteSelectedUniversalInvoiceTrash = SERVER_URL + "DeleteSelectedUniversalInvoiceTrash.php";
    public static String DeleteUniversalInvoice = SERVER_URL + "DeleteUniversalInvoice.php";
    public static String GetAllBankTrash = SERVER_URL + "GetAllBankTrash.php";

    // chat APIs
    public static String GetAllChatUsers = SERVER_URL + "GetAllChatUsers.php";
    public static String AddChatMessageNew = SERVER_URL + "AddChatMessageNew.php";
    public static String EditChatMessageNew = SERVER_URL + "EditChatMessageNew.php";
    public static String DeleteChatMessage = SERVER_URL + "DeleteChatMessage.php";
    public static String GetAllChatMessages = SERVER_URL + "GetAllChatMessages.php";
    public static String GetAllChatRooms = SERVER_URL + "GetAllChatRooms.php";
    public static String AddChatGroup = SERVER_URL + "AddChatGroup.php";
    public static String EditChatGroup = SERVER_URL + "EditChatGroup.php";
    public static String GetGroupDetail = SERVER_URL + "GetGroupDetail.php";
    public static String DeleteChat = SERVER_URL + "DeleteChat.php";
    public static String DeleteGroup = SERVER_URL + "DeleteGroup.php";
    public static String SearchChatRooms = SERVER_URL + "SearchChatRooms.php";
    public static String SearchChatUsers = SERVER_URL + "SearchChatUsers.php";

    // Educational Blogs APIs
    public static String GetAllEducationPosts = SERVER_URL + "GetAllEducationPosts.php";
    public static String GetSingleEducationPost = SERVER_URL + "GetSingleEducationPost.php";
    public static String GetEducationPostBySearch = SERVER_URL + "GetEducationPostBySearch.php";

    public static String EditCoverImage = SERVER_URL + "EditCoverImage.php";

    /*
     * Address book API
     * */
    public static String Get_All_Address_Book = SERVER_URL + "GetAllAddressBook.php";
    public static String SearchAddressBook = SERVER_URL + "SearchAddressBook.php";
    public static String STAFF = "staff";
    public static String OFFICE = "office";
    public static String USER = "user";
    public static String BLOG = "blog";
    public static String ADMIN = "admin";
    public static String LOGIN_TYPE = "login_type";
    public static String VESSELS_FOR_SALE_ACTIVITY = "vessel_for_sale_activity";
    public static String LOGIN = "Login";
    public static String STR_BACK = "str_back";
    public static String ALL_Items = "Items";
    public static String ALL_Parent_Discount = "discount";
    public static String Invoice_ID = "";
    public static String strSubject = "";
    public static String SearchType = "";
    public static String AllVesselType = "";
    public static String VesselForSaleType = "";
    public static String FavoriteVesselType = "";


    public static boolean BOOLEAN_STAFF_LOGOUT = false;
    public static boolean BOOLEAN_USER_LOGOUT = false;
    public static boolean Is_Value = false;
    public static boolean IS_STAMP_EDIT = false;
    public static boolean IS_SIGNATURE_EDIT = false;
    public static boolean IS_ITEM_EDIT = false;
    public static boolean IS_COMPANY_EDIT = false;
    public static boolean IS_CURRENCY_EDIT = false;
    public static boolean IS_INVOICE_VESSEL_EDIT = false;
    public static boolean IS_BANK_DETAILS_EDIT = false;
    public static boolean IS_SEE_ALL_INTERVALS_NEWS_CLICK = false;
    public static boolean IS_SEE_ALL_NEWS_CLICK = false;
    public static boolean IS_CLICK_FROM_VESSELS_FOR_SALE = false;
    public static boolean is_Staff_FragmentClick = false;
    public static boolean IS_CLICK_FROM_COMPANY = false;
    public static boolean IS_VESSEL_ADDED = false;
    public static boolean IS_VESSEL_EDITED = false;
    public static boolean IS_VESSEL_COPIED = false;
    public static boolean is_VessalsForSale = false;
    public static boolean is_VesselsFavorite = false;
    public static boolean IS_VESSAL_ACTIVITY = false;
    public static boolean IS_ACTIVITY_CLICK = false;
    public static boolean IS_EDIT_ITEM_CLICK = false;
    public static boolean ISupdate = false;
    public static boolean IS_Payment_Update = false;
    public static boolean IS_camera_Click = false;
    public static boolean IS_Click_From_Edit = false;
    public static boolean IS_Click_From_Copy = false;
    public static boolean IS_Click_From_DRAFT = false;
    public static boolean IS_Click_From_DRAFT_SAVE = false;
    public static boolean IS_Click_From_Edit_DRAFT = false;
    public static boolean IS_INVOICE_UPLOADED = false;
    public static boolean Is_click_form_Push = false;
    public static boolean Is_click_form_Manager = false;
    public static boolean Is_click_form_All_Vessals = false;
    public static boolean Is_click_form_All_Vessals_Manager = false;
    public static boolean Is_click_form_NEWS = false;
    public static boolean Is_click_form_Internal_NEWS = false;
    public static boolean IS_CLICK_FROM_USER_VESSEL = false;
    public static boolean IS_BACK = false;
    public static boolean IsAllVesselsClick = false;
    public static boolean IsAllVesselsClickManager = false;
    public static boolean IsAllVesselsDetailBack = false;
    public static boolean IS_INVOICE_BACK = false;
    public static boolean IS_INVOICE_BACK_FROM_DRAFT = false;
    /* admin */
    public static boolean is_Admin_FragmentClick = false;

    public static ArrayList<MailingListSendingModel> mArrayLISTDATA = new ArrayList<>();
    //    public static ArrayList<SendMailListModel> mArraySendMailListModel = new ArrayList<SendMailListModel>();
    public static ArrayList<MailModel> mArraySendMailListModel = new ArrayList<MailModel>();
    public static ArrayList<String> mEmailsArrayLIST = new ArrayList<>();

    // for chat module
    public static String ROLE = "role";
    public static String NOTIFICATION_TYPE = "notification_type";
    public static String PUSH = "push";
    public static String USER_FIRST_NAME = "user_first_name";
    public static String USER_LAST_NAME = "user_last_name";
    public static String USER_NAME = "user_name";
    public static String ROOM_ID = "room_id";
    public static String RECEIVER_ID = "receiver_id";
    public static String ONLINE_STATE = "online_state";
    public static String RECEIVER_PIC = "receiver_pic";
    public static String GROUP_ID = "group_id";
    public static String GROUP_NAME = "group_name";
    public static String GROUP_IMAGE = "group_image";
    public static String GROUP_PARTICIPANTS = "group_participants";

    // for educational blogs
    public static String BLOG_LINK = "blog_link";
    public static String BLOG_TITLE = "blog_title";

    // vessel APIs
    public static String GetRecentSearch = SERVER_URL + "GetRecentSearch.php";
    public static String MakeVesselFavorite = SERVER_URL + "MakeVesselFavorite.php";
    public static String UnFavoriteVessel = SERVER_URL + "UnFavoriteVessel.php";
    public static String GetAllFavoriteVessels = SERVER_URL + "GetAllFavoriteVessels.php";
    public static String SearchFavoriteVessel = SERVER_URL + "SearchFavoriteVessel.php";
    public static String AdvancedFavoriteVessel = SERVER_URL + "AdvancedFavoriteVessel.php";

    /* chat */
    public static final int Sender = 0;
    public static final int Receiver = 1;
    public static int intType;

    /* links */
    public static String PRIVACY_POLICY = "https://jaohar.com/privacy.html";
    public static String TERMS_AND_CONDITIONS = "https://jaohar.com/terms&services.html";

    /* side menu constants */
    public static String SIDE_MENU_CLICKED_ID = "side_menu_clicked_id";


    /* vessel details */
    public static String TITLE = "title";
}
